using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdScheduledIssuesService : Infrastructure.Services.Shared.IhdScheduledIssuesService
    {
        private Infrastructure.Repositories.Shared.IhdScheduledIssuesRepository repository;

        public hdScheduledIssuesService(IhdScheduledIssuesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdScheduledIssues entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdScheduledIssues> FindBy(Expression<Func<Domain.Incidents.hdScheduledIssues, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdScheduledIssues> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdScheduledIssues GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdScheduledIssues entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdScheduledIssues> GetByScheduledIssueID(Guid ScheduledIssueID)
        {
            return repository.GetByScheduledIssueID(ScheduledIssueID);
        }

	}
}