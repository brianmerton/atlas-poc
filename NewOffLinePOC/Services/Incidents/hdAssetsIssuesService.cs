using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAssetsIssuesService : Infrastructure.Services.Shared.IhdAssetsIssuesService
    {
        private Infrastructure.Repositories.Shared.IhdAssetsIssuesRepository repository;

        public hdAssetsIssuesService(IhdAssetsIssuesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAssetsIssues entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAssetsIssues> FindBy(Expression<Func<Domain.Incidents.hdAssetsIssues, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAssetsIssues> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAssetsIssues GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAssetsIssues entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdAssetsIssues> GetByAssetID(Guid AssetID)
        {
            return repository.GetByAssetID(AssetID);
        }

        public IEnumerable<Domain.Incidents.hdAssetsIssues> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

	}
}