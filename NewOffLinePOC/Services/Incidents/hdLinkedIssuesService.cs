using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdLinkedIssuesService : Infrastructure.Services.Shared.IhdLinkedIssuesService
    {
        private Infrastructure.Repositories.Shared.IhdLinkedIssuesRepository repository;

        public hdLinkedIssuesService(IhdLinkedIssuesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdLinkedIssues entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdLinkedIssues> FindBy(Expression<Func<Domain.Incidents.hdLinkedIssues, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdLinkedIssues> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdLinkedIssues GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdLinkedIssues entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdLinkedIssues> GetByIssue1ID(Guid Issue1ID)
        {
            return repository.GetByIssue1ID(Issue1ID);
        }

        public IEnumerable<Domain.Incidents.hdLinkedIssues> GetByIssue2ID(Guid Issue2ID)
        {
            return repository.GetByIssue2ID(Issue2ID);
        }

	}
}