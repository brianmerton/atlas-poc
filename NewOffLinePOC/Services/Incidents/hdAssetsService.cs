using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAssetsService : Infrastructure.Services.Shared.IhdAssetsService
    {
        private Infrastructure.Repositories.Shared.IhdAssetsRepository repository;

        public hdAssetsService(IhdAssetsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAssets entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAssets> FindBy(Expression<Func<Domain.Incidents.hdAssets, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAssets> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAssets GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAssets entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdAssets> GetByItemID(Guid ItemID)
        {
            return repository.GetByItemID(ItemID);
        }

	}
}