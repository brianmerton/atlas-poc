using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCustomFieldValuesService : Infrastructure.Services.Shared.IhdCustomFieldValuesService
    {
        private Infrastructure.Repositories.Shared.IhdCustomFieldValuesRepository repository;

        public hdCustomFieldValuesService(IhdCustomFieldValuesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdCustomFieldValues entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdCustomFieldValues> FindBy(Expression<Func<Domain.Incidents.hdCustomFieldValues, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdCustomFieldValues> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdCustomFieldValues GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdCustomFieldValues entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdCustomFieldValues> GetByFieldID(Guid FieldID)
        {
            return repository.GetByFieldID(FieldID);
        }

        public IEnumerable<Domain.Incidents.hdCustomFieldValues> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

	}
}