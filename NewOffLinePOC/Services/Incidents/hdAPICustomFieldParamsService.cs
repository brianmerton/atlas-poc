using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAPICustomFieldParamsService : Infrastructure.Services.Shared.IhdAPICustomFieldParamsService
    {
        private Infrastructure.Repositories.Shared.IhdAPICustomFieldParamsRepository repository;

        public hdAPICustomFieldParamsService(IhdAPICustomFieldParamsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAPICustomFieldParams entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldParams> FindBy(Expression<Func<Domain.Incidents.hdAPICustomFieldParams, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldParams> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAPICustomFieldParams GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAPICustomFieldParams entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdAPICustomFieldParams> GetByAPICustomFieldParamsID(Guid APICustomFieldParamsID)
        {
            return repository.GetByAPICustomFieldParamsID(APICustomFieldParamsID);
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldParams> GetByAPICustomFieldSettingsID(Guid APICustomFieldSettingsID)
        {
            return repository.GetByAPICustomFieldSettingsID(APICustomFieldSettingsID);
        }

	}
}