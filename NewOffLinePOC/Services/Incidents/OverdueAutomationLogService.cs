using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class OverdueAutomationLogService : Infrastructure.Services.Shared.IOverdueAutomationLogService
    {
        private Infrastructure.Repositories.Shared.IOverdueAutomationLogRepository repository;

        public OverdueAutomationLogService(IOverdueAutomationLogRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.OverdueAutomationLog entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.OverdueAutomationLog> FindBy(Expression<Func<Domain.Incidents.OverdueAutomationLog, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.OverdueAutomationLog> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.OverdueAutomationLog GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.OverdueAutomationLog entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.OverdueAutomationLog> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

        public IEnumerable<Domain.Incidents.OverdueAutomationLog> GetByRuleID(Guid RuleID)
        {
            return repository.GetByRuleID(RuleID);
        }

	}
}