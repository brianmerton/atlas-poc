using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAssetTypesService : Infrastructure.Services.Shared.IhdAssetTypesService
    {
        private Infrastructure.Repositories.Shared.IhdAssetTypesRepository repository;

        public hdAssetTypesService(IhdAssetTypesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAssetTypes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAssetTypes> FindBy(Expression<Func<Domain.Incidents.hdAssetTypes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAssetTypes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAssetTypes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAssetTypes entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdAssetTypes> GetByTypeID(Guid TypeID)
        {
            return repository.GetByTypeID(TypeID);
        }

	}
}