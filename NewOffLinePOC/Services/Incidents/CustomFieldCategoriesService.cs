using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class CustomFieldCategoriesService : Infrastructure.Services.Shared.ICustomFieldCategoriesService
    {
        private Infrastructure.Repositories.Shared.ICustomFieldCategoriesRepository repository;

        public CustomFieldCategoriesService(ICustomFieldCategoriesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.CustomFieldCategories entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.CustomFieldCategories> FindBy(Expression<Func<Domain.Incidents.CustomFieldCategories, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.CustomFieldCategories> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.CustomFieldCategories GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.CustomFieldCategories entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.CustomFieldCategories> GetByCategoryID(Guid CategoryID)
        {
            return repository.GetByCategoryID(CategoryID);
        }

        public IEnumerable<Domain.Incidents.CustomFieldCategories> GetByFieldID(Guid FieldID)
        {
            return repository.GetByFieldID(FieldID);
        }

	}
}