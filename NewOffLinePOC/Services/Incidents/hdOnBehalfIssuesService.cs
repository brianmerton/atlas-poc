using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdOnBehalfIssuesService : Infrastructure.Services.Shared.IhdOnBehalfIssuesService
    {
        private Infrastructure.Repositories.Shared.IhdOnBehalfIssuesRepository repository;

        public hdOnBehalfIssuesService(IhdOnBehalfIssuesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdOnBehalfIssues entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdOnBehalfIssues> FindBy(Expression<Func<Domain.Incidents.hdOnBehalfIssues, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdOnBehalfIssues> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdOnBehalfIssues GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdOnBehalfIssues entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdOnBehalfIssues> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

	}
}