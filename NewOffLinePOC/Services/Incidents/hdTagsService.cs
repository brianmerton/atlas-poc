using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdTagsService : Infrastructure.Services.Shared.IhdTagsService
    {
        private Infrastructure.Repositories.Shared.IhdTagsRepository repository;

        public hdTagsService(IhdTagsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdTags entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdTags> FindBy(Expression<Func<Domain.Incidents.hdTags, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdTags> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdTags GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdTags entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdTags> GetByTagID(Guid TagID)
        {
            return repository.GetByTagID(TagID);
        }

	}
}