using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCompaniesService : Infrastructure.Services.Shared.IhdCompaniesService
    {
        private Infrastructure.Repositories.Shared.IhdCompaniesRepository repository;

        public hdCompaniesService(IhdCompaniesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdCompanies entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdCompanies> FindBy(Expression<Func<Domain.Incidents.hdCompanies, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdCompanies> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdCompanies GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdCompanies entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdCompanies> GetByCompanyID(Guid CompanyID)
        {
            return repository.GetByCompanyID(CompanyID);
        }

	}
}