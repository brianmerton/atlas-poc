using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCommentTemplatesService : Infrastructure.Services.Shared.IhdCommentTemplatesService
    {
        private Infrastructure.Repositories.Shared.IhdCommentTemplatesRepository repository;

        public hdCommentTemplatesService(IhdCommentTemplatesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdCommentTemplates entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdCommentTemplates> FindBy(Expression<Func<Domain.Incidents.hdCommentTemplates, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdCommentTemplates> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdCommentTemplates GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdCommentTemplates entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdCommentTemplates> GetByTemplateID(Guid TemplateID)
        {
            return repository.GetByTemplateID(TemplateID);
        }

	}
}