using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdFileAttachmentsService : Infrastructure.Services.Shared.IhdFileAttachmentsService
    {
        private Infrastructure.Repositories.Shared.IhdFileAttachmentsRepository repository;

        public hdFileAttachmentsService(IhdFileAttachmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdFileAttachments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdFileAttachments> FindBy(Expression<Func<Domain.Incidents.hdFileAttachments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdFileAttachments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdFileAttachments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdFileAttachments entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdFileAttachments> GetByFileID(Guid FileID)
        {
            return repository.GetByFileID(FileID);
        }

	}
}