using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class InstancesService : Infrastructure.Services.Shared.IInstancesService
    {
        private Infrastructure.Repositories.Shared.IInstancesRepository repository;

        public InstancesService(IInstancesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.Instances entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.Instances> FindBy(Expression<Func<Domain.Incidents.Instances, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.Instances> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.Instances GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.Instances entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.Instances> GetByInstanceID(Guid InstanceID)
        {
            return repository.GetByInstanceID(InstanceID);
        }

	}
}