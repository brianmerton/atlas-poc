using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCustomFieldsService : Infrastructure.Services.Shared.IhdCustomFieldsService
    {
        private Infrastructure.Repositories.Shared.IhdCustomFieldsRepository repository;

        public hdCustomFieldsService(IhdCustomFieldsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdCustomFields entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdCustomFields> FindBy(Expression<Func<Domain.Incidents.hdCustomFields, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdCustomFields> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdCustomFields GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdCustomFields entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdCustomFields> GetByFieldID(Guid FieldID)
        {
            return repository.GetByFieldID(FieldID);
        }

	}
}