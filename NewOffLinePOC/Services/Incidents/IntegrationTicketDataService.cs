using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class IntegrationTicketDataService : Infrastructure.Services.Shared.IIntegrationTicketDataService
    {
        private Infrastructure.Repositories.Shared.IIntegrationTicketDataRepository repository;

        public IntegrationTicketDataService(IIntegrationTicketDataRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.IntegrationTicketData entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.IntegrationTicketData> FindBy(Expression<Func<Domain.Incidents.IntegrationTicketData, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.IntegrationTicketData> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.IntegrationTicketData GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.IntegrationTicketData entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.IntegrationTicketData> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

	}
}