using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCategorySubmitPermissionsService : Infrastructure.Services.Shared.IhdCategorySubmitPermissionsService
    {
        private Infrastructure.Repositories.Shared.IhdCategorySubmitPermissionsRepository repository;

        public hdCategorySubmitPermissionsService(IhdCategorySubmitPermissionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdCategorySubmitPermissions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdCategorySubmitPermissions> FindBy(Expression<Func<Domain.Incidents.hdCategorySubmitPermissions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdCategorySubmitPermissions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdCategorySubmitPermissions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdCategorySubmitPermissions entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdCategorySubmitPermissions> GetByCategoryID(Guid CategoryID)
        {
            return repository.GetByCategoryID(CategoryID);
        }

        public IEnumerable<Domain.Incidents.hdCategorySubmitPermissions> GetByUserID(Guid UserID)
        {
            return repository.GetByUserID(UserID);
        }

	}
}