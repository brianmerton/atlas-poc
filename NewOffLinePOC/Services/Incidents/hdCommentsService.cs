using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCommentsService : Infrastructure.Services.Shared.IhdCommentsService
    {
        private Infrastructure.Repositories.Shared.IhdCommentsRepository repository;

        public hdCommentsService(IhdCommentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdComments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdComments> FindBy(Expression<Func<Domain.Incidents.hdComments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdComments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdComments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdComments entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdComments> GetByCommentID(Guid CommentID)
        {
            return repository.GetByCommentID(CommentID);
        }

        public IEnumerable<Domain.Incidents.hdComments> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

        public IEnumerable<Domain.Incidents.hdComments> GetByUserID(Guid UserID)
        {
            return repository.GetByUserID(UserID);
        }

	}
}