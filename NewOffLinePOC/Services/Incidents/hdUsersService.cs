using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdUsersService : Infrastructure.Services.Shared.IhdUsersService
    {
        private Infrastructure.Repositories.Shared.IhdUsersRepository repository;

        public hdUsersService(IhdUsersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdUsers entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdUsers> FindBy(Expression<Func<Domain.Incidents.hdUsers, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdUsers> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdUsers GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdUsers entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdUsers> GetByInstanceID(Guid InstanceID)
        {
            return repository.GetByInstanceID(InstanceID);
        }

        public IEnumerable<Domain.Incidents.hdUsers> GetByUserID(Guid UserID)
        {
            return repository.GetByUserID(UserID);
        }

        public IEnumerable<Domain.Incidents.hdUsers> GetByUserName(Guid UserName)
        {
            return repository.GetByUserName(UserName);
        }

	}
}