using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdSettingsService : Infrastructure.Services.Shared.IhdSettingsService
    {
        private Infrastructure.Repositories.Shared.IhdSettingsRepository repository;

        public hdSettingsService(IhdSettingsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdSettings entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdSettings> FindBy(Expression<Func<Domain.Incidents.hdSettings, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdSettings> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdSettings GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdSettings entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdSettings> GetByInstanceID(Guid InstanceID)
        {
            return repository.GetByInstanceID(InstanceID);
        }

	}
}