using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdStatusService : Infrastructure.Services.Shared.IhdStatusService
    {
        private Infrastructure.Repositories.Shared.IhdStatusRepository repository;

        public hdStatusService(IhdStatusRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdStatus entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdStatus> FindBy(Expression<Func<Domain.Incidents.hdStatus, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdStatus> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdStatus GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdStatus entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdStatus> GetByStatusID(Guid StatusID)
        {
            return repository.GetByStatusID(StatusID);
        }

	}
}