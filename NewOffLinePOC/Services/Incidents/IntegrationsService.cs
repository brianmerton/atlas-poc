using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class IntegrationsService : Infrastructure.Services.Shared.IIntegrationsService
    {
        private Infrastructure.Repositories.Shared.IIntegrationsRepository repository;

        public IntegrationsService(IIntegrationsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.Integrations entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.Integrations> FindBy(Expression<Func<Domain.Incidents.Integrations, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.Integrations> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.Integrations GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.Integrations entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.Integrations> GetByInstanceID(Guid InstanceID)
        {
            return repository.GetByInstanceID(InstanceID);
        }

	}
}