using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdSectionsService : Infrastructure.Services.Shared.IhdSectionsService
    {
        private Infrastructure.Repositories.Shared.IhdSectionsRepository repository;

        public hdSectionsService(IhdSectionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdSections entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdSections> FindBy(Expression<Func<Domain.Incidents.hdSections, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdSections> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdSections GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdSections entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdSections> GetBySectionID(Guid SectionID)
        {
            return repository.GetBySectionID(SectionID);
        }

	}
}