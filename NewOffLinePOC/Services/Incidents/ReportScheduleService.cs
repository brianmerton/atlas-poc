using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class ReportScheduleService : Infrastructure.Services.Shared.IReportScheduleService
    {
        private Infrastructure.Repositories.Shared.IReportScheduleRepository repository;

        public ReportScheduleService(IReportScheduleRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.ReportSchedule entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.ReportSchedule> FindBy(Expression<Func<Domain.Incidents.ReportSchedule, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.ReportSchedule> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.ReportSchedule GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.ReportSchedule entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.ReportSchedule> GetByInstanceID(Guid InstanceID)
        {
            return repository.GetByInstanceID(InstanceID);
        }

	}
}