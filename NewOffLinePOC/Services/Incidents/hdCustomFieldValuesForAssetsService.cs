using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCustomFieldValuesForAssetsService : Infrastructure.Services.Shared.IhdCustomFieldValuesForAssetsService
    {
        private Infrastructure.Repositories.Shared.IhdCustomFieldValuesForAssetsRepository repository;

        public hdCustomFieldValuesForAssetsService(IhdCustomFieldValuesForAssetsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdCustomFieldValuesForAssets entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdCustomFieldValuesForAssets> FindBy(Expression<Func<Domain.Incidents.hdCustomFieldValuesForAssets, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdCustomFieldValuesForAssets> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdCustomFieldValuesForAssets GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdCustomFieldValuesForAssets entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdCustomFieldValuesForAssets> GetByFieldID(Guid FieldID)
        {
            return repository.GetByFieldID(FieldID);
        }

        public IEnumerable<Domain.Incidents.hdCustomFieldValuesForAssets> GetByItemID(Guid ItemID)
        {
            return repository.GetByItemID(ItemID);
        }

	}
}