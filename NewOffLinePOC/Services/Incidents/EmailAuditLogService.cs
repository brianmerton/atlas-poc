using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class EmailAuditLogService : Infrastructure.Services.Shared.IEmailAuditLogService
    {
        private Infrastructure.Repositories.Shared.IEmailAuditLogRepository repository;

        public EmailAuditLogService(IEmailAuditLogRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.EmailAuditLog entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.EmailAuditLog> FindBy(Expression<Func<Domain.Incidents.EmailAuditLog, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.EmailAuditLog> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.EmailAuditLog GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.EmailAuditLog entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.EmailAuditLog> GetByEmailAuditLogId(Guid EmailAuditLogId)
        {
            return repository.GetByEmailAuditLogId(EmailAuditLogId);
        }

	}
}