using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAssetManufacturersService : Infrastructure.Services.Shared.IhdAssetManufacturersService
    {
        private Infrastructure.Repositories.Shared.IhdAssetManufacturersRepository repository;

        public hdAssetManufacturersService(IhdAssetManufacturersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAssetManufacturers entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAssetManufacturers> FindBy(Expression<Func<Domain.Incidents.hdAssetManufacturers, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAssetManufacturers> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAssetManufacturers GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAssetManufacturers entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdAssetManufacturers> GetByManufacturerID(Guid ManufacturerID)
        {
            return repository.GetByManufacturerID(ManufacturerID);
        }

	}
}