using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdNewsService : Infrastructure.Services.Shared.IhdNewsService
    {
        private Infrastructure.Repositories.Shared.IhdNewsRepository repository;

        public hdNewsService(IhdNewsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdNews entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdNews> FindBy(Expression<Func<Domain.Incidents.hdNews, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdNews> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdNews GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdNews entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdNews> GetByNewsID(Guid NewsID)
        {
            return repository.GetByNewsID(NewsID);
        }

	}
}