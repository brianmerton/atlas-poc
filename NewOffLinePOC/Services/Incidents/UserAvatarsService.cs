using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class UserAvatarsService : Infrastructure.Services.Shared.IUserAvatarsService
    {
        private Infrastructure.Repositories.Shared.IUserAvatarsRepository repository;

        public UserAvatarsService(IUserAvatarsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.UserAvatars entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.UserAvatars> FindBy(Expression<Func<Domain.Incidents.UserAvatars, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.UserAvatars> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.UserAvatars GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.UserAvatars entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.UserAvatars> GetByUserID(Guid UserID)
        {
            return repository.GetByUserID(UserID);
        }

	}
}