using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdPOPServersService : Infrastructure.Services.Shared.IhdPOPServersService
    {
        private Infrastructure.Repositories.Shared.IhdPOPServersRepository repository;

        public hdPOPServersService(IhdPOPServersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdPOPServers entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdPOPServers> FindBy(Expression<Func<Domain.Incidents.hdPOPServers, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdPOPServers> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdPOPServers GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdPOPServers entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdPOPServers> GetByServerID(Guid ServerID)
        {
            return repository.GetByServerID(ServerID);
        }

	}
}