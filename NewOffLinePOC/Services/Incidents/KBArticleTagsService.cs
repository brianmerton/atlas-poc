using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class KBArticleTagsService : Infrastructure.Services.Shared.IKBArticleTagsService
    {
        private Infrastructure.Repositories.Shared.IKBArticleTagsRepository repository;

        public KBArticleTagsService(IKBArticleTagsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.KBArticleTags entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.KBArticleTags> FindBy(Expression<Func<Domain.Incidents.KBArticleTags, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.KBArticleTags> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.KBArticleTags GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.KBArticleTags entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.KBArticleTags> GetByArticleID(Guid ArticleID)
        {
            return repository.GetByArticleID(ArticleID);
        }

        public IEnumerable<Domain.Incidents.KBArticleTags> GetByTagID(Guid TagID)
        {
            return repository.GetByTagID(TagID);
        }

	}
}