using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCustomFieldOptionsService : Infrastructure.Services.Shared.IhdCustomFieldOptionsService
    {
        private Infrastructure.Repositories.Shared.IhdCustomFieldOptionsRepository repository;

        public hdCustomFieldOptionsService(IhdCustomFieldOptionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdCustomFieldOptions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdCustomFieldOptions> FindBy(Expression<Func<Domain.Incidents.hdCustomFieldOptions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdCustomFieldOptions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdCustomFieldOptions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdCustomFieldOptions entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdCustomFieldOptions> GetByOptionID(Guid OptionID)
        {
            return repository.GetByOptionID(OptionID);
        }

	}
}