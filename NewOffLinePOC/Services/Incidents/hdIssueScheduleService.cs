using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdIssueScheduleService : Infrastructure.Services.Shared.IhdIssueScheduleService
    {
        private Infrastructure.Repositories.Shared.IhdIssueScheduleRepository repository;

        public hdIssueScheduleService(IhdIssueScheduleRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdIssueSchedule entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdIssueSchedule> FindBy(Expression<Func<Domain.Incidents.hdIssueSchedule, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdIssueSchedule> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdIssueSchedule GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdIssueSchedule entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdIssueSchedule> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

	}
}