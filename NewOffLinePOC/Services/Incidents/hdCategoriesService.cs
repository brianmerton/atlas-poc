using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCategoriesService : Infrastructure.Services.Shared.IhdCategoriesService
    {
        private Infrastructure.Repositories.Shared.IhdCategoriesRepository repository;

        public hdCategoriesService(IhdCategoriesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdCategories entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdCategories> FindBy(Expression<Func<Domain.Incidents.hdCategories, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdCategories> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdCategories GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdCategories entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdCategories> GetByCategoryID(Guid CategoryID)
        {
            return repository.GetByCategoryID(CategoryID);
        }

	}
}