using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAssetSuppliersService : Infrastructure.Services.Shared.IhdAssetSuppliersService
    {
        private Infrastructure.Repositories.Shared.IhdAssetSuppliersRepository repository;

        public hdAssetSuppliersService(IhdAssetSuppliersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAssetSuppliers entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAssetSuppliers> FindBy(Expression<Func<Domain.Incidents.hdAssetSuppliers, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAssetSuppliers> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAssetSuppliers GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAssetSuppliers entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdAssetSuppliers> GetBySupplierID(Guid SupplierID)
        {
            return repository.GetBySupplierID(SupplierID);
        }

	}
}