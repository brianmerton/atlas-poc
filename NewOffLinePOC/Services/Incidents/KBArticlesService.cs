using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class KBArticlesService : Infrastructure.Services.Shared.IKBArticlesService
    {
        private Infrastructure.Repositories.Shared.IKBArticlesRepository repository;

        public KBArticlesService(IKBArticlesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.KBArticles entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.KBArticles> FindBy(Expression<Func<Domain.Incidents.KBArticles, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.KBArticles> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.KBArticles GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.KBArticles entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.KBArticles> GetByArticleID(Guid ArticleID)
        {
            return repository.GetByArticleID(ArticleID);
        }

	}
}