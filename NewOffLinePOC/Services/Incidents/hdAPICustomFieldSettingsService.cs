using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAPICustomFieldSettingsService : Infrastructure.Services.Shared.IhdAPICustomFieldSettingsService
    {
        private Infrastructure.Repositories.Shared.IhdAPICustomFieldSettingsRepository repository;

        public hdAPICustomFieldSettingsService(IhdAPICustomFieldSettingsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAPICustomFieldSettings entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> FindBy(Expression<Func<Domain.Incidents.hdAPICustomFieldSettings, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAPICustomFieldSettings GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAPICustomFieldSettings entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> GetByAPICustomFieldSettingsID(Guid APICustomFieldSettingsID)
        {
            return repository.GetByAPICustomFieldSettingsID(APICustomFieldSettingsID);
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> GetByFieldId(Guid FieldId)
        {
            return repository.GetByFieldId(FieldId);
        }

	}
}