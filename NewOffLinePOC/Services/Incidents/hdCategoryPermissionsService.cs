using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdCategoryPermissionsService : Infrastructure.Services.Shared.IhdCategoryPermissionsService
    {
        private Infrastructure.Repositories.Shared.IhdCategoryPermissionsRepository repository;

        public hdCategoryPermissionsService(IhdCategoryPermissionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdCategoryPermissions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdCategoryPermissions> FindBy(Expression<Func<Domain.Incidents.hdCategoryPermissions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdCategoryPermissions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdCategoryPermissions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdCategoryPermissions entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdCategoryPermissions> GetByCategoryID(Guid CategoryID)
        {
            return repository.GetByCategoryID(CategoryID);
        }

        public IEnumerable<Domain.Incidents.hdCategoryPermissions> GetByUserID(Guid UserID)
        {
            return repository.GetByUserID(UserID);
        }

	}
}