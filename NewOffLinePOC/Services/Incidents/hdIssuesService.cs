using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdIssuesService : Infrastructure.Services.Shared.IhdIssuesService
    {
        private Infrastructure.Repositories.Shared.IhdIssuesRepository repository;

        public hdIssuesService(IhdIssuesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdIssues entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdIssues> FindBy(Expression<Func<Domain.Incidents.hdIssues, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdIssues GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdIssues entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdIssues> GetByAssignedToUserID(Guid AssignedToUserID)
        {
            return repository.GetByAssignedToUserID(AssignedToUserID);
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetByCategoryID(Guid CategoryID)
        {
            return repository.GetByCategoryID(CategoryID);
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetByStatusID(Guid StatusID)
        {
            return repository.GetByStatusID(StatusID);
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetByUserID(Guid UserID)
        {
            return repository.GetByUserID(UserID);
        }

	}
}