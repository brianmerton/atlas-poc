using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAssetUsersService : Infrastructure.Services.Shared.IhdAssetUsersService
    {
        private Infrastructure.Repositories.Shared.IhdAssetUsersRepository repository;

        public hdAssetUsersService(IhdAssetUsersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAssetUsers entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAssetUsers> FindBy(Expression<Func<Domain.Incidents.hdAssetUsers, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAssetUsers> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAssetUsers GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAssetUsers entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdAssetUsers> GetByItemID(Guid ItemID)
        {
            return repository.GetByItemID(ItemID);
        }

        public IEnumerable<Domain.Incidents.hdAssetUsers> GetByUserID(Guid UserID)
        {
            return repository.GetByUserID(UserID);
        }

	}
}