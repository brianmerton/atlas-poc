using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdIssueSubscribersService : Infrastructure.Services.Shared.IhdIssueSubscribersService
    {
        private Infrastructure.Repositories.Shared.IhdIssueSubscribersRepository repository;

        public hdIssueSubscribersService(IhdIssueSubscribersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdIssueSubscribers entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdIssueSubscribers> FindBy(Expression<Func<Domain.Incidents.hdIssueSubscribers, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdIssueSubscribers> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdIssueSubscribers GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdIssueSubscribers entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdIssueSubscribers> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

        public IEnumerable<Domain.Incidents.hdIssueSubscribers> GetByUserID(Guid UserID)
        {
            return repository.GetByUserID(UserID);
        }

	}
}