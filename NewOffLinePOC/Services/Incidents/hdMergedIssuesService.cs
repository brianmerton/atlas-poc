using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdMergedIssuesService : Infrastructure.Services.Shared.IhdMergedIssuesService
    {
        private Infrastructure.Repositories.Shared.IhdMergedIssuesRepository repository;

        public hdMergedIssuesService(IhdMergedIssuesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdMergedIssues entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdMergedIssues> FindBy(Expression<Func<Domain.Incidents.hdMergedIssues, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdMergedIssues> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdMergedIssues GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdMergedIssues entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdMergedIssues> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

        public IEnumerable<Domain.Incidents.hdMergedIssues> GetByIssueMergedID(Guid IssueMergedID)
        {
            return repository.GetByIssueMergedID(IssueMergedID);
        }

	}
}