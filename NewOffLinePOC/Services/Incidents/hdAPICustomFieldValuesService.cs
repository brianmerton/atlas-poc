using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAPICustomFieldValuesService : Infrastructure.Services.Shared.IhdAPICustomFieldValuesService
    {
        private Infrastructure.Repositories.Shared.IhdAPICustomFieldValuesRepository repository;

        public hdAPICustomFieldValuesService(IhdAPICustomFieldValuesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAPICustomFieldValues entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldValues> FindBy(Expression<Func<Domain.Incidents.hdAPICustomFieldValues, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldValues> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAPICustomFieldValues GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAPICustomFieldValues entity)
        {
            repository.Update(entity);
        }
		
	}
}