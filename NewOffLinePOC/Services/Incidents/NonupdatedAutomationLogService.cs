using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class NonupdatedAutomationLogService : Infrastructure.Services.Shared.INonupdatedAutomationLogService
    {
        private Infrastructure.Repositories.Shared.INonupdatedAutomationLogRepository repository;

        public NonupdatedAutomationLogService(INonupdatedAutomationLogRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.NonupdatedAutomationLog entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.NonupdatedAutomationLog> FindBy(Expression<Func<Domain.Incidents.NonupdatedAutomationLog, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.NonupdatedAutomationLog> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.NonupdatedAutomationLog GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.NonupdatedAutomationLog entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.NonupdatedAutomationLog> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

        public IEnumerable<Domain.Incidents.NonupdatedAutomationLog> GetByRuleID(Guid RuleID)
        {
            return repository.GetByRuleID(RuleID);
        }

	}
}