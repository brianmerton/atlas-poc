using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class AutomationRulesService : Infrastructure.Services.Shared.IAutomationRulesService
    {
        private Infrastructure.Repositories.Shared.IAutomationRulesRepository repository;

        public AutomationRulesService(IAutomationRulesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.AutomationRules entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.AutomationRules> FindBy(Expression<Func<Domain.Incidents.AutomationRules, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.AutomationRules> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.AutomationRules GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.AutomationRules entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.AutomationRules> GetByRuleID(Guid RuleID)
        {
            return repository.GetByRuleID(RuleID);
        }

	}
}