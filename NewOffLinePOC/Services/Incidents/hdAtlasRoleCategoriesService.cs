using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdAtlasRoleCategoriesService : Infrastructure.Services.Shared.IhdAtlasRoleCategoriesService
    {
        private Infrastructure.Repositories.Shared.IhdAtlasRoleCategoriesRepository repository;

        public hdAtlasRoleCategoriesService(IhdAtlasRoleCategoriesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdAtlasRoleCategories entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdAtlasRoleCategories> FindBy(Expression<Func<Domain.Incidents.hdAtlasRoleCategories, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdAtlasRoleCategories> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdAtlasRoleCategories GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdAtlasRoleCategories entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdAtlasRoleCategories> GetByAtlasRoleId(Guid AtlasRoleId)
        {
            return repository.GetByAtlasRoleId(AtlasRoleId);
        }

        public IEnumerable<Domain.Incidents.hdAtlasRoleCategories> GetByCategoryId(Guid CategoryId)
        {
            return repository.GetByCategoryId(CategoryId);
        }

	}
}