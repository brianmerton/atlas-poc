using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class hdIssueTagsService : Infrastructure.Services.Shared.IhdIssueTagsService
    {
        private Infrastructure.Repositories.Shared.IhdIssueTagsRepository repository;

        public hdIssueTagsService(IhdIssueTagsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.hdIssueTags entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.hdIssueTags> FindBy(Expression<Func<Domain.Incidents.hdIssueTags, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.hdIssueTags> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.hdIssueTags GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.hdIssueTags entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.hdIssueTags> GetByIssueID(Guid IssueID)
        {
            return repository.GetByIssueID(IssueID);
        }

        public IEnumerable<Domain.Incidents.hdIssueTags> GetByTagID(Guid TagID)
        {
            return repository.GetByTagID(TagID);
        }

	}
}