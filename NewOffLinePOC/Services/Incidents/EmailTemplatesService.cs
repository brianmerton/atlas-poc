using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Incidents
{
    public class EmailTemplatesService : Infrastructure.Services.Shared.IEmailTemplatesService
    {
        private Infrastructure.Repositories.Shared.IEmailTemplatesRepository repository;

        public EmailTemplatesService(IEmailTemplatesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Incidents.EmailTemplates entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Incidents.EmailTemplates> FindBy(Expression<Func<Domain.Incidents.EmailTemplates, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Incidents.EmailTemplates> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Incidents.EmailTemplates GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Incidents.EmailTemplates entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Incidents.EmailTemplates> GetByInstanceID(Guid InstanceID)
        {
            return repository.GetByInstanceID(InstanceID);
        }

	}
}