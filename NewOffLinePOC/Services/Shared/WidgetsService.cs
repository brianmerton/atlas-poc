using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class WidgetsService : Infrastructure.Services.Shared.IWidgetsService
    {
        private Infrastructure.Repositories.Shared.IWidgetsRepository repository;

        public WidgetsService(IWidgetsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Widgets entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Widgets> FindBy(Expression<Func<Domain.Shared.Widgets, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Widgets> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Widgets GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Widgets entity)
        {
            repository.Update(entity);
        }
		
	}
}