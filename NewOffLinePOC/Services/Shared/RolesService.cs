using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class RolesService : Infrastructure.Services.Shared.IRolesService
    {
        private Infrastructure.Repositories.Shared.IRolesRepository repository;

        public RolesService(IRolesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Roles entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Roles> FindBy(Expression<Func<Domain.Shared.Roles, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Roles> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Roles GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Roles entity)
        {
            repository.Update(entity);
        }
		
	}
}