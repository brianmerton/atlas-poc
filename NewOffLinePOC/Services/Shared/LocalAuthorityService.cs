using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class LocalAuthorityService : Infrastructure.Services.Shared.ILocalAuthorityService
    {
        private Infrastructure.Repositories.Shared.ILocalAuthorityRepository repository;

        public LocalAuthorityService(ILocalAuthorityRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.LocalAuthority entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.LocalAuthority> FindBy(Expression<Func<Domain.Shared.LocalAuthority, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.LocalAuthority> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.LocalAuthority GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.LocalAuthority entity)
        {
            repository.Update(entity);
        }
		
	}
}