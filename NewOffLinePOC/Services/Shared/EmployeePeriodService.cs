using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class EmployeePeriodService : Infrastructure.Services.Shared.IEmployeePeriodService
    {
        private Infrastructure.Repositories.Shared.IEmployeePeriodRepository repository;

        public EmployeePeriodService(IEmployeePeriodRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.EmployeePeriod entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.EmployeePeriod> FindBy(Expression<Func<Domain.Shared.EmployeePeriod, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.EmployeePeriod> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.EmployeePeriod GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.EmployeePeriod entity)
        {
            repository.Update(entity);
        }
		
	}
}