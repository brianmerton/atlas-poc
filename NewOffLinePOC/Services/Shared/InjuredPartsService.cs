using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class InjuredPartsService : Infrastructure.Services.Shared.IInjuredPartsService
    {
        private Infrastructure.Repositories.Shared.IInjuredPartsRepository repository;

        public InjuredPartsService(IInjuredPartsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.InjuredParts entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.InjuredParts> FindBy(Expression<Func<Domain.Shared.InjuredParts, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.InjuredParts> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.InjuredParts GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.InjuredParts entity)
        {
            repository.Update(entity);
        }
		
	}
}