using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class OptionsService : Infrastructure.Services.Shared.IOptionsService
    {
        private Infrastructure.Repositories.Shared.IOptionsRepository repository;

        public OptionsService(IOptionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Options entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Options> FindBy(Expression<Func<Domain.Shared.Options, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Options> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Options GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Options entity)
        {
            repository.Update(entity);
        }
		
	}
}