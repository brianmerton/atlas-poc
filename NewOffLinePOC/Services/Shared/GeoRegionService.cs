using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class GeoRegionService : Infrastructure.Services.Shared.IGeoRegionService
    {
        private Infrastructure.Repositories.Shared.IGeoRegionRepository repository;

        public GeoRegionService(IGeoRegionRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.GeoRegion entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.GeoRegion> FindBy(Expression<Func<Domain.Shared.GeoRegion, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.GeoRegion> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.GeoRegion GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.GeoRegion entity)
        {
            repository.Update(entity);
        }
		
	}
}