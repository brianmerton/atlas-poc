using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class CheckListItemsService : Infrastructure.Services.Shared.ICheckListItemsService
    {
        private Infrastructure.Repositories.Shared.ICheckListItemsRepository repository;

        public CheckListItemsService(ICheckListItemsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.CheckListItems entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.CheckListItems> FindBy(Expression<Func<Domain.Shared.CheckListItems, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.CheckListItems> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.CheckListItems GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.CheckListItems entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.CheckListItems> GetByCheckItemId(Guid CheckItemId)
        {
            return repository.GetByCheckItemId(CheckItemId);
        }

        public IEnumerable<Domain.Shared.CheckListItems> GetByCheckListId(Guid CheckListId)
        {
            return repository.GetByCheckListId(CheckListId);
        }

	}
}