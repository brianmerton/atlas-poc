using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class InjuredPartyService : Infrastructure.Services.Shared.IInjuredPartyService
    {
        private Infrastructure.Repositories.Shared.IInjuredPartyRepository repository;

        public InjuredPartyService(IInjuredPartyRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.InjuredParty entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.InjuredParty> FindBy(Expression<Func<Domain.Shared.InjuredParty, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.InjuredParty> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.InjuredParty GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.InjuredParty entity)
        {
            repository.Update(entity);
        }
		
	}
}