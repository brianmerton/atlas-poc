using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class CheckListVersionService : Infrastructure.Services.Shared.ICheckListVersionService
    {
        private Infrastructure.Repositories.Shared.ICheckListVersionRepository repository;

        public CheckListVersionService(ICheckListVersionRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.CheckListVersion entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.CheckListVersion> FindBy(Expression<Func<Domain.Shared.CheckListVersion, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.CheckListVersion> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.CheckListVersion GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.CheckListVersion entity)
        {
            repository.Update(entity);
        }
		
	}
}