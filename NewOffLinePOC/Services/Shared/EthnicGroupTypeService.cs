using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class EthnicGroupTypeService : Infrastructure.Services.Shared.IEthnicGroupTypeService
    {
        private Infrastructure.Repositories.Shared.IEthnicGroupTypeRepository repository;

        public EthnicGroupTypeService(IEthnicGroupTypeRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.EthnicGroupType entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.EthnicGroupType> FindBy(Expression<Func<Domain.Shared.EthnicGroupType, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.EthnicGroupType> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.EthnicGroupType GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.EthnicGroupType entity)
        {
            repository.Update(entity);
        }
		
	}
}