using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class EmployeeStatusService : Infrastructure.Services.Shared.IEmployeeStatusService
    {
        private Infrastructure.Repositories.Shared.IEmployeeStatusRepository repository;

        public EmployeeStatusService(IEmployeeStatusRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.EmployeeStatus entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.EmployeeStatus> FindBy(Expression<Func<Domain.Shared.EmployeeStatus, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.EmployeeStatus> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.EmployeeStatus GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.EmployeeStatus entity)
        {
            repository.Update(entity);
        }
		
	}
}