using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class UsersIdentityService : Infrastructure.Services.Shared.IUsersIdentityService
    {
        private Infrastructure.Repositories.Shared.IUsersIdentityRepository repository;

        public UsersIdentityService(IUsersIdentityRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.UsersIdentity entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.UsersIdentity> FindBy(Expression<Func<Domain.Shared.UsersIdentity, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.UsersIdentity> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.UsersIdentity GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.UsersIdentity entity)
        {
            repository.Update(entity);
        }
		
	}
}