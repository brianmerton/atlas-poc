using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class RoleClientTypeMapService : Infrastructure.Services.Shared.IRoleClientTypeMapService
    {
        private Infrastructure.Repositories.Shared.IRoleClientTypeMapRepository repository;

        public RoleClientTypeMapService(IRoleClientTypeMapRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.RoleClientTypeMap entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.RoleClientTypeMap> FindBy(Expression<Func<Domain.Shared.RoleClientTypeMap, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.RoleClientTypeMap> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.RoleClientTypeMap GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.RoleClientTypeMap entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.RoleClientTypeMap> GetByClientTypeId(Guid ClientTypeId)
        {
            return repository.GetByClientTypeId(ClientTypeId);
        }

        public IEnumerable<Domain.Shared.RoleClientTypeMap> GetByRoleId(Guid RoleId)
        {
            return repository.GetByRoleId(RoleId);
        }

	}
}