using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class IncidentTypeService : Infrastructure.Services.Shared.IIncidentTypeService
    {
        private Infrastructure.Repositories.Shared.IIncidentTypeRepository repository;

        public IncidentTypeService(IIncidentTypeRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.IncidentType entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.IncidentType> FindBy(Expression<Func<Domain.Shared.IncidentType, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.IncidentType> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.IncidentType GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.IncidentType entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.IncidentType> GetByIncidentCategoryId(Guid IncidentCategoryId)
        {
            return repository.GetByIncidentCategoryId(IncidentCategoryId);
        }

	}
}