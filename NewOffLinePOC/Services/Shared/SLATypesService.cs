using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class SLATypesService : Infrastructure.Services.Shared.ISLATypesService
    {
        private Infrastructure.Repositories.Shared.ISLATypesRepository repository;

        public SLATypesService(ISLATypesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.SLATypes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.SLATypes> FindBy(Expression<Func<Domain.Shared.SLATypes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.SLATypes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.SLATypes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.SLATypes entity)
        {
            repository.Update(entity);
        }
		
	}
}