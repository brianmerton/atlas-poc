using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class MainFactorService : Infrastructure.Services.Shared.IMainFactorService
    {
        private Infrastructure.Repositories.Shared.IMainFactorRepository repository;

        public MainFactorService(IMainFactorRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.MainFactor entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.MainFactor> FindBy(Expression<Func<Domain.Shared.MainFactor, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.MainFactor> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.MainFactor GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.MainFactor entity)
        {
            repository.Update(entity);
        }
		
	}
}