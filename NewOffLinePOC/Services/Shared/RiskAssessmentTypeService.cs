using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class RiskAssessmentTypeService : Infrastructure.Services.Shared.IRiskAssessmentTypeService
    {
        private Infrastructure.Repositories.Shared.IRiskAssessmentTypeRepository repository;

        public RiskAssessmentTypeService(IRiskAssessmentTypeRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.RiskAssessmentType entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.RiskAssessmentType> FindBy(Expression<Func<Domain.Shared.RiskAssessmentType, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.RiskAssessmentType> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.RiskAssessmentType GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.RiskAssessmentType entity)
        {
            repository.Update(entity);
        }
		
	}
}