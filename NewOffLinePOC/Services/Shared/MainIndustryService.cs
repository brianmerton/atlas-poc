using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class MainIndustryService : Infrastructure.Services.Shared.IMainIndustryService
    {
        private Infrastructure.Repositories.Shared.IMainIndustryRepository repository;

        public MainIndustryService(IMainIndustryRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.MainIndustry entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.MainIndustry> FindBy(Expression<Func<Domain.Shared.MainIndustry, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.MainIndustry> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.MainIndustry GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.MainIndustry entity)
        {
            repository.Update(entity);
        }
		
	}
}