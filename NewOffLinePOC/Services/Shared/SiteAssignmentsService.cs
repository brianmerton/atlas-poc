using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class SiteAssignmentsService : Infrastructure.Services.Shared.ISiteAssignmentsService
    {
        private Infrastructure.Repositories.Shared.ISiteAssignmentsRepository repository;

        public SiteAssignmentsService(ISiteAssignmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.SiteAssignments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.SiteAssignments> FindBy(Expression<Func<Domain.Shared.SiteAssignments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.SiteAssignments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.SiteAssignments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.SiteAssignments entity)
        {
            repository.Update(entity);
        }
		
	}
}