using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class UsersService : Infrastructure.Services.Shared.IUsersService
    {
        private Infrastructure.Repositories.Shared.IUsersRepository repository;

        public UsersService(IUsersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Users entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Users> FindBy(Expression<Func<Domain.Shared.Users, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Users> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Users GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Users entity)
        {
            repository.Update(entity);
        }
		
	}
}