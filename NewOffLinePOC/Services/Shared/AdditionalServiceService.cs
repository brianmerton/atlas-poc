using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class AdditionalServiceService : Infrastructure.Services.Shared.IAdditionalServiceService
    {
        private Infrastructure.Repositories.Shared.IAdditionalServiceRepository repository;

        public AdditionalServiceService(IAdditionalServiceRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.AdditionalService entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.AdditionalService> FindBy(Expression<Func<Domain.Shared.AdditionalService, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.AdditionalService> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.AdditionalService GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.AdditionalService entity)
        {
            repository.Update(entity);
        }
		
	}
}