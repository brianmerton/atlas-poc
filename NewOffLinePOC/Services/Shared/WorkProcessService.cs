using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class WorkProcessService : Infrastructure.Services.Shared.IWorkProcessService
    {
        private Infrastructure.Repositories.Shared.IWorkProcessRepository repository;

        public WorkProcessService(IWorkProcessRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.WorkProcess entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.WorkProcess> FindBy(Expression<Func<Domain.Shared.WorkProcess, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.WorkProcess> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.WorkProcess GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.WorkProcess entity)
        {
            repository.Update(entity);
        }
		
	}
}