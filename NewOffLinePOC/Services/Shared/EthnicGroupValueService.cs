using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class EthnicGroupValueService : Infrastructure.Services.Shared.IEthnicGroupValueService
    {
        private Infrastructure.Repositories.Shared.IEthnicGroupValueRepository repository;

        public EthnicGroupValueService(IEthnicGroupValueRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.EthnicGroupValue entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.EthnicGroupValue> FindBy(Expression<Func<Domain.Shared.EthnicGroupValue, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.EthnicGroupValue> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.EthnicGroupValue GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.EthnicGroupValue entity)
        {
            repository.Update(entity);
        }
		
	}
}