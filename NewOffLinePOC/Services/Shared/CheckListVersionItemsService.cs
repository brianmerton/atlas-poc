using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class CheckListVersionItemsService : Infrastructure.Services.Shared.ICheckListVersionItemsService
    {
        private Infrastructure.Repositories.Shared.ICheckListVersionItemsRepository repository;

        public CheckListVersionItemsService(ICheckListVersionItemsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.CheckListVersionItems entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.CheckListVersionItems> FindBy(Expression<Func<Domain.Shared.CheckListVersionItems, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.CheckListVersionItems> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.CheckListVersionItems GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.CheckListVersionItems entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.CheckListVersionItems> GetByCheckItemId(Guid CheckItemId)
        {
            return repository.GetByCheckItemId(CheckItemId);
        }

        public IEnumerable<Domain.Shared.CheckListVersionItems> GetByCheckListVersionId(Guid CheckListVersionId)
        {
            return repository.GetByCheckListVersionId(CheckListVersionId);
        }

	}
}