using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class CoursesService : Infrastructure.Services.Shared.ICoursesService
    {
        private Infrastructure.Repositories.Shared.ICoursesRepository repository;

        public CoursesService(ICoursesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Courses entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Courses> FindBy(Expression<Func<Domain.Shared.Courses, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Courses> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Courses GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Courses entity)
        {
            repository.Update(entity);
        }
		
	}
}