﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class SectorsService : Infrastructure.Services.Shared.ISectorsService
    {
        private Infrastructure.Repositories.Shared.ISectorsRepository repository;

        public SectorsService(Infrastructure.Repositories.Shared.ISectorsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Sectors entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Sectors> FindBy(Expression<Func<Domain.Shared.Sectors, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Sectors> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Sectors GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Sectors entity)
        {
            repository.Update(entity);
        }


    }
}
