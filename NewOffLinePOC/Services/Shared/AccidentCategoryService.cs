using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class AccidentCategoryService : Infrastructure.Services.Shared.IAccidentCategoryService
    {
        private Infrastructure.Repositories.Shared.IAccidentCategoryRepository repository;

        public AccidentCategoryService(IAccidentCategoryRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.AccidentCategory entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.AccidentCategory> FindBy(Expression<Func<Domain.Shared.AccidentCategory, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.AccidentCategory> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.AccidentCategory GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.AccidentCategory entity)
        {
            repository.Update(entity);
        }
		
	}
}