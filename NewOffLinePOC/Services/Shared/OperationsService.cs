using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class OperationsService : Infrastructure.Services.Shared.IOperationsService
    {
        private Infrastructure.Repositories.Shared.IOperationsRepository repository;

        public OperationsService(IOperationsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Operations entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Operations> FindBy(Expression<Func<Domain.Shared.Operations, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Operations> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Operations GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Operations entity)
        {
            repository.Update(entity);
        }
		
	}
}