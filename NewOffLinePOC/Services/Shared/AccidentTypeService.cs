using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class AccidentTypeService : Infrastructure.Services.Shared.IAccidentTypeService
    {
        private Infrastructure.Repositories.Shared.IAccidentTypeRepository repository;

        public AccidentTypeService(IAccidentTypeRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.AccidentType entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.AccidentType> FindBy(Expression<Func<Domain.Shared.AccidentType, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.AccidentType> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.AccidentType GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.AccidentType entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.AccidentType> GetByAccidentCategoryId(Guid AccidentCategoryId)
        {
            return repository.GetByAccidentCategoryId(AccidentCategoryId);
        }

	}
}