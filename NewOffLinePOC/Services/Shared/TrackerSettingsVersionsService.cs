using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class TrackerSettingsVersionsService : Infrastructure.Services.Shared.ITrackerSettingsVersionsService
    {
        private Infrastructure.Repositories.Shared.ITrackerSettingsVersionsRepository repository;

        public TrackerSettingsVersionsService(ITrackerSettingsVersionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.TrackerSettingsVersions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.TrackerSettingsVersions> FindBy(Expression<Func<Domain.Shared.TrackerSettingsVersions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.TrackerSettingsVersions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.TrackerSettingsVersions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.TrackerSettingsVersions entity)
        {
            repository.Update(entity);
        }
		
	}
}