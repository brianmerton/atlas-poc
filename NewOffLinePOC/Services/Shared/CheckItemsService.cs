using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class CheckItemsService : Infrastructure.Services.Shared.ICheckItemsService
    {
        private Infrastructure.Repositories.Shared.ICheckItemsRepository repository;

        public CheckItemsService(ICheckItemsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.CheckItems entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.CheckItems> FindBy(Expression<Func<Domain.Shared.CheckItems, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.CheckItems> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.CheckItems GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.CheckItems entity)
        {
            repository.Update(entity);
        }
		
	}
}