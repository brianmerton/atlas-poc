using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class SubActivityService : Infrastructure.Services.Shared.ISubActivityService
    {
        private Infrastructure.Repositories.Shared.ISubActivityRepository repository;

        public SubActivityService(ISubActivityRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.SubActivity entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.SubActivity> FindBy(Expression<Func<Domain.Shared.SubActivity, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.SubActivity> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.SubActivity GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.SubActivity entity)
        {
            repository.Update(entity);
        }
		
	}
}