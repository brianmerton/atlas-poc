using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class InvQuestionsService : Infrastructure.Services.Shared.IInvQuestionsService
    {
        private Infrastructure.Repositories.Shared.IInvQuestionsRepository repository;

        public InvQuestionsService(IInvQuestionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.InvQuestions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.InvQuestions> FindBy(Expression<Func<Domain.Shared.InvQuestions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.InvQuestions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.InvQuestions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.InvQuestions entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.InvQuestions> GetByInvSectionId(Guid InvSectionId)
        {
            return repository.GetByInvSectionId(InvSectionId);
        }

	}
}