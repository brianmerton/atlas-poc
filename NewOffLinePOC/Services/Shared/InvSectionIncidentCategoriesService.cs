using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class InvSectionIncidentCategoriesService : Infrastructure.Services.Shared.IInvSectionIncidentCategoriesService
    {
        private Infrastructure.Repositories.Shared.IInvSectionIncidentCategoriesRepository repository;

        public InvSectionIncidentCategoriesService(IInvSectionIncidentCategoriesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.InvSectionIncidentCategories entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.InvSectionIncidentCategories> FindBy(Expression<Func<Domain.Shared.InvSectionIncidentCategories, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.InvSectionIncidentCategories> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.InvSectionIncidentCategories GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.InvSectionIncidentCategories entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.InvSectionIncidentCategories> GetByIncidentCategoryId(Guid IncidentCategoryId)
        {
            return repository.GetByIncidentCategoryId(IncidentCategoryId);
        }

        public IEnumerable<Domain.Shared.InvSectionIncidentCategories> GetByInvSectionId(Guid InvSectionId)
        {
            return repository.GetByInvSectionId(InvSectionId);
        }

	}
}