using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class SectorWorkspaceMapService : Infrastructure.Services.Shared.ISectorWorkspaceMapService
    {
        private Infrastructure.Repositories.Shared.ISectorWorkspaceMapRepository repository;

        public SectorWorkspaceMapService(ISectorWorkspaceMapRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.SectorWorkspaceMap entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.SectorWorkspaceMap> FindBy(Expression<Func<Domain.Shared.SectorWorkspaceMap, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.SectorWorkspaceMap> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.SectorWorkspaceMap GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.SectorWorkspaceMap entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.SectorWorkspaceMap> GetBySectorId(Guid SectorId)
        {
            return repository.GetBySectorId(SectorId);
        }

        public IEnumerable<Domain.Shared.SectorWorkspaceMap> GetByWorkspaceTypeId(Guid WorkspaceTypeId)
        {
            return repository.GetByWorkspaceTypeId(WorkspaceTypeId);
        }

	}
}