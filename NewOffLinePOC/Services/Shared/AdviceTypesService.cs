using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class AdviceTypesService : Infrastructure.Services.Shared.IAdviceTypesService
    {
        private Infrastructure.Repositories.Shared.IAdviceTypesRepository repository;

        public AdviceTypesService(IAdviceTypesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.AdviceTypes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.AdviceTypes> FindBy(Expression<Func<Domain.Shared.AdviceTypes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.AdviceTypes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.AdviceTypes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.AdviceTypes entity)
        {
            repository.Update(entity);
        }
		
	}
}