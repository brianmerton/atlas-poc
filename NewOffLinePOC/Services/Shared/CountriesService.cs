using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class CountriesService : Infrastructure.Services.Shared.ICountriesService
    {
        private Infrastructure.Repositories.Shared.ICountriesRepository repository;

        public CountriesService(ICountriesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Countries entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Countries> FindBy(Expression<Func<Domain.Shared.Countries, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Countries> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Countries GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Countries entity)
        {
            repository.Update(entity);
        }
		
	}
}