using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class VisitTypesService : Infrastructure.Services.Shared.IVisitTypesService
    {
        private Infrastructure.Repositories.Shared.IVisitTypesRepository repository;

        public VisitTypesService(IVisitTypesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.VisitTypes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.VisitTypes> FindBy(Expression<Func<Domain.Shared.VisitTypes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.VisitTypes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.VisitTypes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.VisitTypes entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.VisitTypes> GetByClientTypeId(Guid ClientTypeId)
        {
            return repository.GetByClientTypeId(ClientTypeId);
        }

	}
}