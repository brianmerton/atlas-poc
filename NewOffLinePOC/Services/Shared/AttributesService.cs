using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class AttributesService : Infrastructure.Services.Shared.IAttributesService
    {
        private Infrastructure.Repositories.Shared.IAttributesRepository repository;

        public AttributesService(IAttributesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Attributes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Attributes> FindBy(Expression<Func<Domain.Shared.Attributes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Attributes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Attributes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Attributes entity)
        {
            repository.Update(entity);
        }
		
	}
}