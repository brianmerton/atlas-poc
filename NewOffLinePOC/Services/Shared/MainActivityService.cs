using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class MainActivityService : Infrastructure.Services.Shared.IMainActivityService
    {
        private Infrastructure.Repositories.Shared.IMainActivityRepository repository;

        public MainActivityService(IMainActivityRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.MainActivity entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.MainActivity> FindBy(Expression<Func<Domain.Shared.MainActivity, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.MainActivity> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.MainActivity GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.MainActivity entity)
        {
            repository.Update(entity);
        }
		
	}
}