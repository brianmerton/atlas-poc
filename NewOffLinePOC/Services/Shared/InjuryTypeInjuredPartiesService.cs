using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class InjuryTypeInjuredPartiesService : Infrastructure.Services.Shared.IInjuryTypeInjuredPartiesService
    {
        private Infrastructure.Repositories.Shared.IInjuryTypeInjuredPartiesRepository repository;

        public InjuryTypeInjuredPartiesService(IInjuryTypeInjuredPartiesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.InjuryTypeInjuredParties entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.InjuryTypeInjuredParties> FindBy(Expression<Func<Domain.Shared.InjuryTypeInjuredParties, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.InjuryTypeInjuredParties> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.InjuryTypeInjuredParties GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.InjuryTypeInjuredParties entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.InjuryTypeInjuredParties> GetByInjuredPartyId(Guid InjuredPartyId)
        {
            return repository.GetByInjuredPartyId(InjuredPartyId);
        }

        public IEnumerable<Domain.Shared.InjuryTypeInjuredParties> GetByInjuryTypeId(Guid InjuryTypeId)
        {
            return repository.GetByInjuryTypeId(InjuryTypeId);
        }

	}
}