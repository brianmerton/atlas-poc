using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class InjuryTypeService : Infrastructure.Services.Shared.IInjuryTypeService
    {
        private Infrastructure.Repositories.Shared.IInjuryTypeRepository repository;

        public InjuryTypeService(IInjuryTypeRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.InjuryType entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.InjuryType> FindBy(Expression<Func<Domain.Shared.InjuryType, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.InjuryType> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.InjuryType GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.InjuryType entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.InjuryType> GetByInjuredPartyId(Guid InjuredPartyId)
        {
            return repository.GetByInjuredPartyId(InjuredPartyId);
        }

	}
}