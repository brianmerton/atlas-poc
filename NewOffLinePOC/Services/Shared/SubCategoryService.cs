using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class SubCategoryService : Infrastructure.Services.Shared.ISubCategoryService
    {
        private Infrastructure.Repositories.Shared.ISubCategoryRepository repository;

        public SubCategoryService(ISubCategoryRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.SubCategory entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.SubCategory> FindBy(Expression<Func<Domain.Shared.SubCategory, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.SubCategory> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.SubCategory GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.SubCategory entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.SubCategory> GetByCreatedBy(Guid CreatedBy)
        {
            return repository.GetByCreatedBy(CreatedBy);
        }

        public IEnumerable<Domain.Shared.SubCategory> GetByModifiedBy(Guid ModifiedBy)
        {
            return repository.GetByModifiedBy(ModifiedBy);
        }

	}
}