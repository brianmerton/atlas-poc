using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class InvSectionsService : Infrastructure.Services.Shared.IInvSectionsService
    {
        private Infrastructure.Repositories.Shared.IInvSectionsRepository repository;

        public InvSectionsService(IInvSectionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.InvSections entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.InvSections> FindBy(Expression<Func<Domain.Shared.InvSections, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.InvSections> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.InvSections GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.InvSections entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.InvSections> GetByParentSectionId(Guid ParentSectionId)
        {
            return repository.GetByParentSectionId(ParentSectionId);
        }

	}
}