using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class EmploymentTypeService : Infrastructure.Services.Shared.IEmploymentTypeService
    {
        private Infrastructure.Repositories.Shared.IEmploymentTypeRepository repository;

        public EmploymentTypeService(IEmploymentTypeRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.EmploymentType entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.EmploymentType> FindBy(Expression<Func<Domain.Shared.EmploymentType, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.EmploymentType> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.EmploymentType GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.EmploymentType entity)
        {
            repository.Update(entity);
        }
		
	}
}