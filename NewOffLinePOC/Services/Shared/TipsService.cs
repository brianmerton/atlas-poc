using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class TipsService : Infrastructure.Services.Shared.ITipsService
    {
        private Infrastructure.Repositories.Shared.ITipsRepository repository;

        public TipsService(ITipsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Tips entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Tips> FindBy(Expression<Func<Domain.Shared.Tips, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Tips> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Tips GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Tips entity)
        {
            repository.Update(entity);
        }
		
	}
}