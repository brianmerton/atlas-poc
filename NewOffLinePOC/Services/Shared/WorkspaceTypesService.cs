using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class WorkspaceTypesService : Infrastructure.Services.Shared.IWorkspaceTypesService
    {
        private Infrastructure.Repositories.Shared.IWorkspaceTypesRepository repository;

        public WorkspaceTypesService(IWorkspaceTypesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.WorkspaceTypes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.WorkspaceTypes> FindBy(Expression<Func<Domain.Shared.WorkspaceTypes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.WorkspaceTypes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.WorkspaceTypes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.WorkspaceTypes entity)
        {
            repository.Update(entity);
        }
		
	}
}