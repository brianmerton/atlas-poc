using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class IncidentCategoryService : Infrastructure.Services.Shared.IIncidentCategoryService
    {
        private Infrastructure.Repositories.Shared.IIncidentCategoryRepository repository;

        public IncidentCategoryService(IIncidentCategoryRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.IncidentCategory entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.IncidentCategory> FindBy(Expression<Func<Domain.Shared.IncidentCategory, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.IncidentCategory> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.IncidentCategory GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.IncidentCategory entity)
        {
            repository.Update(entity);
        }
		
	}
}