using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class CountiesService : Infrastructure.Services.Shared.ICountiesService
    {
        private Infrastructure.Repositories.Shared.ICountiesRepository repository;

        public CountiesService(ICountiesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.Counties entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.Counties> FindBy(Expression<Func<Domain.Shared.Counties, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.Counties> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.Counties GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.Counties entity)
        {
            repository.Update(entity);
        }
		
	}
}