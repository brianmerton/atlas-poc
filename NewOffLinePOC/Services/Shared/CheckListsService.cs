using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class CheckListsService : Infrastructure.Services.Shared.ICheckListsService
    {
        private Infrastructure.Repositories.Shared.ICheckListsRepository repository;

        public CheckListsService(ICheckListsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.CheckLists entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.CheckLists> FindBy(Expression<Func<Domain.Shared.CheckLists, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.CheckLists> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.CheckLists GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.CheckLists entity)
        {
            repository.Update(entity);
        }
		
	}
}