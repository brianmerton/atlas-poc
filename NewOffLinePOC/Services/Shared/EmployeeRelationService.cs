using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class EmployeeRelationService : Infrastructure.Services.Shared.IEmployeeRelationService
    {
        private Infrastructure.Repositories.Shared.IEmployeeRelationRepository repository;

        public EmployeeRelationService(IEmployeeRelationRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.EmployeeRelation entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.EmployeeRelation> FindBy(Expression<Func<Domain.Shared.EmployeeRelation, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.EmployeeRelation> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.EmployeeRelation GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.EmployeeRelation entity)
        {
            repository.Update(entity);
        }
		
	}
}