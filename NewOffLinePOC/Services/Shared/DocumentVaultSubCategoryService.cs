using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class DocumentVaultSubCategoryService : Infrastructure.Services.Shared.IDocumentVaultSubCategoryService
    {
        private Infrastructure.Repositories.Shared.IDocumentVaultSubCategoryRepository repository;

        public DocumentVaultSubCategoryService(IDocumentVaultSubCategoryRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.DocumentVaultSubCategory entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.DocumentVaultSubCategory> FindBy(Expression<Func<Domain.Shared.DocumentVaultSubCategory, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.DocumentVaultSubCategory> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.DocumentVaultSubCategory GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.DocumentVaultSubCategory entity)
        {
            repository.Update(entity);
        }
		
	}
}