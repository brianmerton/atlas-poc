using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class TrackerSettingsService : Infrastructure.Services.Shared.ITrackerSettingsService
    {
        private Infrastructure.Repositories.Shared.ITrackerSettingsRepository repository;

        public TrackerSettingsService(ITrackerSettingsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.TrackerSettings entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.TrackerSettings> FindBy(Expression<Func<Domain.Shared.TrackerSettings, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.TrackerSettings> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.TrackerSettings GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.TrackerSettings entity)
        {
            repository.Update(entity);
        }
		
	}
}