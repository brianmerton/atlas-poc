using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class EmployeeTypeService : Infrastructure.Services.Shared.IEmployeeTypeService
    {
        private Infrastructure.Repositories.Shared.IEmployeeTypeRepository repository;

        public EmployeeTypeService(IEmployeeTypeRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.EmployeeType entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.EmployeeType> FindBy(Expression<Func<Domain.Shared.EmployeeType, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.EmployeeType> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.EmployeeType GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.EmployeeType entity)
        {
            repository.Update(entity);
        }
		
	}
}