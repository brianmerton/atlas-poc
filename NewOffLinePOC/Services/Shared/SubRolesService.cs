using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Shared
{
    public class SubRolesService : Infrastructure.Services.Shared.ISubRolesService
    {
        private Infrastructure.Repositories.Shared.ISubRolesRepository repository;

        public SubRolesService(ISubRolesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Shared.SubRoles entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Shared.SubRoles> FindBy(Expression<Func<Domain.Shared.SubRoles, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Shared.SubRoles> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Shared.SubRoles GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Shared.SubRoles entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Shared.SubRoles> GetByCreatedBy(Guid CreatedBy)
        {
            return repository.GetByCreatedBy(CreatedBy);
        }

        public IEnumerable<Domain.Shared.SubRoles> GetByModifiedBy(Guid ModifiedBy)
        {
            return repository.GetByModifiedBy(ModifiedBy);
        }

	}
}