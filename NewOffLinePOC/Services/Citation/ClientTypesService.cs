using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class ClientTypesService : Infrastructure.Services.Shared.IClientTypesService
    {
        private Infrastructure.Repositories.Shared.IClientTypesRepository repository;

        public ClientTypesService(IClientTypesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.ClientTypes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.ClientTypes> FindBy(Expression<Func<Domain.Citation.ClientTypes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.ClientTypes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.ClientTypes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.ClientTypes entity)
        {
            repository.Update(entity);
        }
		
	}
}