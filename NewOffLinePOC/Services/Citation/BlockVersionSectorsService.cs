using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class BlockVersionSectorsService : Infrastructure.Services.Shared.IBlockVersionSectorsService
    {
        private Infrastructure.Repositories.Shared.IBlockVersionSectorsRepository repository;

        public BlockVersionSectorsService(IBlockVersionSectorsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.BlockVersionSectors entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.BlockVersionSectors> FindBy(Expression<Func<Domain.Citation.BlockVersionSectors, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.BlockVersionSectors> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.BlockVersionSectors GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.BlockVersionSectors entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.BlockVersionSectors> GetByBlockVersionId(Guid BlockVersionId)
        {
            return repository.GetByBlockVersionId(BlockVersionId);
        }

        public IEnumerable<Domain.Citation.BlockVersionSectors> GetBySectorId(Guid SectorId)
        {
            return repository.GetBySectorId(SectorId);
        }

	}
}