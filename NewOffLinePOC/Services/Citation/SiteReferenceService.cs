using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class SiteReferenceService : Infrastructure.Services.Shared.ISiteReferenceService
    {
        private Infrastructure.Repositories.Shared.ISiteReferenceRepository repository;

        public SiteReferenceService(ISiteReferenceRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.SiteReference entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.SiteReference> FindBy(Expression<Func<Domain.Citation.SiteReference, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.SiteReference> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.SiteReference GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.SiteReference entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.SiteReference> GetByCompanyId(Guid CompanyId)
        {
            return repository.GetByCompanyId(CompanyId);
        }

	}
}