using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class BlockVersionService : Infrastructure.Services.Shared.IBlockVersionService
    {
        private Infrastructure.Repositories.Shared.IBlockVersionRepository repository;

        public BlockVersionService(IBlockVersionRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.BlockVersion entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.BlockVersion> FindBy(Expression<Func<Domain.Citation.BlockVersion, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.BlockVersion> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.BlockVersion GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.BlockVersion entity)
        {
            repository.Update(entity);
        }
		
	}
}