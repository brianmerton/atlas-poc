using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class WorkItemsService : Infrastructure.Services.Shared.IWorkItemsService
    {
        private Infrastructure.Repositories.Shared.IWorkItemsRepository repository;

        public WorkItemsService(IWorkItemsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.WorkItems entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.WorkItems> FindBy(Expression<Func<Domain.Citation.WorkItems, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.WorkItems> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.WorkItems GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.WorkItems entity)
        {
            repository.Update(entity);
        }
		
	}
}