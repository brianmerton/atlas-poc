using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class DocumentReferencesService : Infrastructure.Services.Shared.IDocumentReferencesService
    {
        private Infrastructure.Repositories.Shared.IDocumentReferencesRepository repository;

        public DocumentReferencesService(IDocumentReferencesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.DocumentReferences entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.DocumentReferences> FindBy(Expression<Func<Domain.Citation.DocumentReferences, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.DocumentReferences> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.DocumentReferences GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.DocumentReferences entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.DocumentReferences> GetByCreatedBy(Guid CreatedBy)
        {
            return repository.GetByCreatedBy(CreatedBy);
        }

        public IEnumerable<Domain.Citation.DocumentReferences> GetByModifiedBy(Guid ModifiedBy)
        {
            return repository.GetByModifiedBy(ModifiedBy);
        }

        public IEnumerable<Domain.Citation.DocumentReferences> GetByTenantId(Guid TenantId)
        {
            return repository.GetByTenantId(TenantId);
        }

	}
}