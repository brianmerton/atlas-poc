using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class BlockVersionCountriesService : Infrastructure.Services.Shared.IBlockVersionCountriesService
    {
        private Infrastructure.Repositories.Shared.IBlockVersionCountriesRepository repository;

        public BlockVersionCountriesService(IBlockVersionCountriesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.BlockVersionCountries entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.BlockVersionCountries> FindBy(Expression<Func<Domain.Citation.BlockVersionCountries, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.BlockVersionCountries> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.BlockVersionCountries GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.BlockVersionCountries entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.BlockVersionCountries> GetByBlockVersionId(Guid BlockVersionId)
        {
            return repository.GetByBlockVersionId(BlockVersionId);
        }

        public IEnumerable<Domain.Citation.BlockVersionCountries> GetByCountryId(Guid CountryId)
        {
            return repository.GetByCountryId(CountryId);
        }

	}
}