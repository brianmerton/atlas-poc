using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class ContactDetailsService : Infrastructure.Services.Shared.IContactDetailsService
    {
        private Infrastructure.Repositories.Shared.IContactDetailsRepository repository;

        public ContactDetailsService(IContactDetailsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.ContactDetails entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.ContactDetails> FindBy(Expression<Func<Domain.Citation.ContactDetails, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.ContactDetails> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.ContactDetails GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.ContactDetails entity)
        {
            repository.Update(entity);
        }
		
	}
}