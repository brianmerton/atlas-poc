using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class AdviceCardAssignmentsService : Infrastructure.Services.Shared.IAdviceCardAssignmentsService
    {
        private Infrastructure.Repositories.Shared.IAdviceCardAssignmentsRepository repository;

        public AdviceCardAssignmentsService(IAdviceCardAssignmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.AdviceCardAssignments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.AdviceCardAssignments> FindBy(Expression<Func<Domain.Citation.AdviceCardAssignments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.AdviceCardAssignments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.AdviceCardAssignments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.AdviceCardAssignments entity)
        {
            repository.Update(entity);
        }
		
	}
}