using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class BlockService : Infrastructure.Services.Shared.IBlockService
    {
        private Infrastructure.Repositories.Shared.IBlockRepository repository;

        public BlockService(IBlockRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.Block entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.Block> FindBy(Expression<Func<Domain.Citation.Block, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.Block> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.Block GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.Block entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.Block> GetByCountryId(Guid CountryId)
        {
            return repository.GetByCountryId(CountryId);
        }

        public IEnumerable<Domain.Citation.Block> GetBySectorId(Guid SectorId)
        {
            return repository.GetBySectorId(SectorId);
        }

        public IEnumerable<Domain.Citation.Block> GetBySubCategoryId(Guid SubCategoryId)
        {
            return repository.GetBySubCategoryId(SubCategoryId);
        }

	}
}