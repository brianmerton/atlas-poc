using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class LogsService : Infrastructure.Services.Shared.ILogsService
    {
        private Infrastructure.Repositories.Shared.ILogsRepository repository;

        public LogsService(ILogsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.Logs entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.Logs> FindBy(Expression<Func<Domain.Citation.Logs, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.Logs> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.Logs GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.Logs entity)
        {
            repository.Update(entity);
        }
		
	}
}