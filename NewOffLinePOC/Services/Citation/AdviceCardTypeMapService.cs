using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class AdviceCardTypeMapService : Infrastructure.Services.Shared.IAdviceCardTypeMapService
    {
        private Infrastructure.Repositories.Shared.IAdviceCardTypeMapRepository repository;

        public AdviceCardTypeMapService(IAdviceCardTypeMapRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.AdviceCardTypeMap entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.AdviceCardTypeMap> FindBy(Expression<Func<Domain.Citation.AdviceCardTypeMap, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.AdviceCardTypeMap> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.AdviceCardTypeMap GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.AdviceCardTypeMap entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.AdviceCardTypeMap> GetByAdviceCardId(Guid AdviceCardId)
        {
            return repository.GetByAdviceCardId(AdviceCardId);
        }

        public IEnumerable<Domain.Citation.AdviceCardTypeMap> GetByAdviceTypeId(Guid AdviceTypeId)
        {
            return repository.GetByAdviceTypeId(AdviceTypeId);
        }

	}
}