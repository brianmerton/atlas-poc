using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class AdviceCardsService : Infrastructure.Services.Shared.IAdviceCardsService
    {
        private Infrastructure.Repositories.Shared.IAdviceCardsRepository repository;

        public AdviceCardsService(IAdviceCardsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.AdviceCards entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.AdviceCards> FindBy(Expression<Func<Domain.Citation.AdviceCards, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.AdviceCards> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.AdviceCards GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.AdviceCards entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.AdviceCards> GetByCompanyId(Guid CompanyId)
        {
            return repository.GetByCompanyId(CompanyId);
        }

	}
}