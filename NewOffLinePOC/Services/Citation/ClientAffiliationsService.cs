using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class ClientAffiliationsService : Infrastructure.Services.Shared.IClientAffiliationsService
    {
        private Infrastructure.Repositories.Shared.IClientAffiliationsRepository repository;

        public ClientAffiliationsService(IClientAffiliationsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.ClientAffiliations entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.ClientAffiliations> FindBy(Expression<Func<Domain.Citation.ClientAffiliations, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.ClientAffiliations> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.ClientAffiliations GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.ClientAffiliations entity)
        {
            repository.Update(entity);
        }
		
	}
}