using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class BlockCountriesService : Infrastructure.Services.Shared.IBlockCountriesService
    {
        private Infrastructure.Repositories.Shared.IBlockCountriesRepository repository;

        public BlockCountriesService(IBlockCountriesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.BlockCountries entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.BlockCountries> FindBy(Expression<Func<Domain.Citation.BlockCountries, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.BlockCountries> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.BlockCountries GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.BlockCountries entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.BlockCountries> GetByBlockId(Guid BlockId)
        {
            return repository.GetByBlockId(BlockId);
        }

        public IEnumerable<Domain.Citation.BlockCountries> GetByCountryId(Guid CountryId)
        {
            return repository.GetByCountryId(CountryId);
        }

	}
}