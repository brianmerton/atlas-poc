using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class AssignmentsService : Infrastructure.Services.Shared.IAssignmentsService
    {
        private Infrastructure.Repositories.Shared.IAssignmentsRepository repository;

        public AssignmentsService(IAssignmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.Assignments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.Assignments> FindBy(Expression<Func<Domain.Citation.Assignments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.Assignments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.Assignments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.Assignments entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.Assignments> GetByCompanyId(Guid CompanyId)
        {
            return repository.GetByCompanyId(CompanyId);
        }

	}
}