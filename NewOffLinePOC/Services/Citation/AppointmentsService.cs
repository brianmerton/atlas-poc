using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class AppointmentsService : Infrastructure.Services.Shared.IAppointmentsService
    {
        private Infrastructure.Repositories.Shared.IAppointmentsRepository repository;

        public AppointmentsService(IAppointmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.Appointments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.Appointments> FindBy(Expression<Func<Domain.Citation.Appointments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.Appointments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.Appointments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.Appointments entity)
        {
            repository.Update(entity);
        }
		
	}
}