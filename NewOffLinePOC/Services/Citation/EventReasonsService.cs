using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class EventReasonsService : Infrastructure.Services.Shared.IEventReasonsService
    {
        private Infrastructure.Repositories.Shared.IEventReasonsRepository repository;

        public EventReasonsService(IEventReasonsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.EventReasons entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.EventReasons> FindBy(Expression<Func<Domain.Citation.EventReasons, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.EventReasons> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.EventReasons GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.EventReasons entity)
        {
            repository.Update(entity);
        }
		
	}
}