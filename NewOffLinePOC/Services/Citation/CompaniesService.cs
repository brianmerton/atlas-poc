using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class CompaniesService : Infrastructure.Services.Shared.ICompaniesService
    {
        private Infrastructure.Repositories.Shared.ICompaniesRepository repository;

        public CompaniesService(ICompaniesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.Companies entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.Companies> FindBy(Expression<Func<Domain.Citation.Companies, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.Companies> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.Companies GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.Companies entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.Companies> GetBySectorId(Guid SectorId)
        {
            return repository.GetBySectorId(SectorId);
        }

	}
}