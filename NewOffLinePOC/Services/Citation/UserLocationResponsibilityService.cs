using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class UserLocationResponsibilityService : Infrastructure.Services.Shared.IUserLocationResponsibilityService
    {
        private Infrastructure.Repositories.Shared.IUserLocationResponsibilityRepository repository;

        public UserLocationResponsibilityService(IUserLocationResponsibilityRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.UserLocationResponsibility entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.UserLocationResponsibility> FindBy(Expression<Func<Domain.Citation.UserLocationResponsibility, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.UserLocationResponsibility> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.UserLocationResponsibility GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.UserLocationResponsibility entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.UserLocationResponsibility> GetByUserId(Guid UserId)
        {
            return repository.GetByUserId(UserId);
        }

	}
}