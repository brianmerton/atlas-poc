using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class TemplateVersionService : Infrastructure.Services.Shared.ITemplateVersionService
    {
        private Infrastructure.Repositories.Shared.ITemplateVersionRepository repository;

        public TemplateVersionService(ITemplateVersionRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.TemplateVersion entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.TemplateVersion> FindBy(Expression<Func<Domain.Citation.TemplateVersion, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.TemplateVersion> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.TemplateVersion GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.TemplateVersion entity)
        {
            repository.Update(entity);
        }
		
	}
}