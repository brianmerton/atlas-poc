using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class TemplatesService : Infrastructure.Services.Shared.ITemplatesService
    {
        private Infrastructure.Repositories.Shared.ITemplatesRepository repository;

        public TemplatesService(ITemplatesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.Templates entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.Templates> FindBy(Expression<Func<Domain.Citation.Templates, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.Templates> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.Templates GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.Templates entity)
        {
            repository.Update(entity);
        }
		
	}
}