using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class APIClientService : Infrastructure.Services.Shared.IAPIClientService
    {
        private Infrastructure.Repositories.Shared.IAPIClientRepository repository;

        public APIClientService(IAPIClientRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.APIClient entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.APIClient> FindBy(Expression<Func<Domain.Citation.APIClient, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.APIClient> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.APIClient GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.APIClient entity)
        {
            repository.Update(entity);
        }
		
	}
}