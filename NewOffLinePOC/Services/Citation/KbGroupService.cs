using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class KbGroupService : Infrastructure.Services.Shared.IKbGroupService
    {
        private Infrastructure.Repositories.Shared.IKbGroupRepository repository;

        public KbGroupService(IKbGroupRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.KbGroup entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.KbGroup> FindBy(Expression<Func<Domain.Citation.KbGroup, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.KbGroup> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.KbGroup GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.KbGroup entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.KbGroup> GetByCreatedBy(Guid CreatedBy)
        {
            return repository.GetByCreatedBy(CreatedBy);
        }

        public IEnumerable<Domain.Citation.KbGroup> GetByModifiedBy(Guid ModifiedBy)
        {
            return repository.GetByModifiedBy(ModifiedBy);
        }

	}
}