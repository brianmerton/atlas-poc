using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class BlockInContainerService : Infrastructure.Services.Shared.IBlockInContainerService
    {
        private Infrastructure.Repositories.Shared.IBlockInContainerRepository repository;

        public BlockInContainerService(IBlockInContainerRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.BlockInContainer entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.BlockInContainer> FindBy(Expression<Func<Domain.Citation.BlockInContainer, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.BlockInContainer> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.BlockInContainer GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.BlockInContainer entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.BlockInContainer> GetByBlockId(Guid BlockId)
        {
            return repository.GetByBlockId(BlockId);
        }

        public IEnumerable<Domain.Citation.BlockInContainer> GetByContainerId(Guid ContainerId)
        {
            return repository.GetByContainerId(ContainerId);
        }

	}
}