using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class BlockInDocumentService : Infrastructure.Services.Shared.IBlockInDocumentService
    {
        private Infrastructure.Repositories.Shared.IBlockInDocumentRepository repository;

        public BlockInDocumentService(IBlockInDocumentRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.BlockInDocument entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.BlockInDocument> FindBy(Expression<Func<Domain.Citation.BlockInDocument, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.BlockInDocument> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.BlockInDocument GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.BlockInDocument entity)
        {
            repository.Update(entity);
        }
		
	}
}