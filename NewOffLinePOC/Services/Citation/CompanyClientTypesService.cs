using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class CompanyClientTypesService : Infrastructure.Services.Shared.ICompanyClientTypesService
    {
        private Infrastructure.Repositories.Shared.ICompanyClientTypesRepository repository;

        public CompanyClientTypesService(ICompanyClientTypesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.CompanyClientTypes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.CompanyClientTypes> FindBy(Expression<Func<Domain.Citation.CompanyClientTypes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.CompanyClientTypes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.CompanyClientTypes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.CompanyClientTypes entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.CompanyClientTypes> GetByClientTypeId(Guid ClientTypeId)
        {
            return repository.GetByClientTypeId(ClientTypeId);
        }

        public IEnumerable<Domain.Citation.CompanyClientTypes> GetByCompanyId(Guid CompanyId)
        {
            return repository.GetByCompanyId(CompanyId);
        }

	}
}