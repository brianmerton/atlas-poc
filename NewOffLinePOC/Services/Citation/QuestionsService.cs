using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class QuestionsService : Infrastructure.Services.Shared.IQuestionsService
    {
        private Infrastructure.Repositories.Shared.IQuestionsRepository repository;

        public QuestionsService(IQuestionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.Questions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.Questions> FindBy(Expression<Func<Domain.Citation.Questions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.Questions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.Questions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.Questions entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.Questions> GetByAssociatedContentId(Guid AssociatedContentId)
        {
            return repository.GetByAssociatedContentId(AssociatedContentId);
        }

	}
}