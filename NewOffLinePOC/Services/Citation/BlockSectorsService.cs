using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class BlockSectorsService : Infrastructure.Services.Shared.IBlockSectorsService
    {
        private Infrastructure.Repositories.Shared.IBlockSectorsRepository repository;

        public BlockSectorsService(IBlockSectorsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.BlockSectors entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.BlockSectors> FindBy(Expression<Func<Domain.Citation.BlockSectors, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.BlockSectors> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.BlockSectors GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.BlockSectors entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.BlockSectors> GetByBlockId(Guid BlockId)
        {
            return repository.GetByBlockId(BlockId);
        }

        public IEnumerable<Domain.Citation.BlockSectors> GetBySectorId(Guid SectorId)
        {
            return repository.GetBySectorId(SectorId);
        }

	}
}