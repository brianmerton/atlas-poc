using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Citation
{
    public class MonitoringService : Infrastructure.Services.Shared.IMonitoringService
    {
        private Infrastructure.Repositories.Shared.IMonitoringRepository repository;

        public MonitoringService(IMonitoringRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Citation.Monitoring entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Citation.Monitoring> FindBy(Expression<Func<Domain.Citation.Monitoring, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Citation.Monitoring> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Citation.Monitoring GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Citation.Monitoring entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Citation.Monitoring> GetByOperationId(Guid OperationId)
        {
            return repository.GetByOperationId(OperationId);
        }

        public IEnumerable<Domain.Citation.Monitoring> GetByUserId(Guid UserId)
        {
            return repository.GetByUserId(UserId);
        }

	}
}