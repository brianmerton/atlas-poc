using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class AppointmentActivitiesService : Infrastructure.Services.Shared.IAppointmentActivitiesService
    {
        private Infrastructure.Repositories.Shared.IAppointmentActivitiesRepository repository;

        public AppointmentActivitiesService(IAppointmentActivitiesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.AppointmentActivities entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.AppointmentActivities> FindBy(Expression<Func<Domain.Customer.AppointmentActivities, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.AppointmentActivities> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.AppointmentActivities GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.AppointmentActivities entity)
        {
            repository.Update(entity);
        }
		
	}
}