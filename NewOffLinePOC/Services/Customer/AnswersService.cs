using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class AnswersService : Infrastructure.Services.Shared.IAnswersService
    {
        private Infrastructure.Repositories.Shared.IAnswersRepository repository;

        public AnswersService(IAnswersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Answers entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Answers> FindBy(Expression<Func<Domain.Customer.Answers, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Answers> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Answers GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Answers entity)
        {
            repository.Update(entity);
        }
		
	}
}