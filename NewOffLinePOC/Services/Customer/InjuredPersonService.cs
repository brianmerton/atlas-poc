using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class InjuredPersonService : Infrastructure.Services.Shared.IInjuredPersonService
    {
        private Infrastructure.Repositories.Shared.IInjuredPersonRepository repository;

        public InjuredPersonService(IInjuredPersonRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.InjuredPerson entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.InjuredPerson> FindBy(Expression<Func<Domain.Customer.InjuredPerson, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.InjuredPerson> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.InjuredPerson GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.InjuredPerson entity)
        {
            repository.Update(entity);
        }
		
	}
}