using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class ControlCheckListsService : Infrastructure.Services.Shared.IControlCheckListsService
    {
        private Infrastructure.Repositories.Shared.IControlCheckListsRepository repository;

        public ControlCheckListsService(IControlCheckListsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.ControlCheckLists entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.ControlCheckLists> FindBy(Expression<Func<Domain.Customer.ControlCheckLists, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.ControlCheckLists> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.ControlCheckLists GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.ControlCheckLists entity)
        {
            repository.Update(entity);
        }
		
	}
}