using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class ExistReportService : Infrastructure.Services.Shared.IExistReportService
    {
        private Infrastructure.Repositories.Shared.IExistReportRepository repository;

        public ExistReportService(IExistReportRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.ExistReport entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.ExistReport> FindBy(Expression<Func<Domain.Customer.ExistReport, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.ExistReport> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.ExistReport GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.ExistReport entity)
        {
            repository.Update(entity);
        }
		
	}
}