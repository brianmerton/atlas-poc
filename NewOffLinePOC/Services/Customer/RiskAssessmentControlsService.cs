using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RiskAssessmentControlsService : Infrastructure.Services.Shared.IRiskAssessmentControlsService
    {
        private Infrastructure.Repositories.Shared.IRiskAssessmentControlsRepository repository;

        public RiskAssessmentControlsService(IRiskAssessmentControlsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.RiskAssessmentControls entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.RiskAssessmentControls> FindBy(Expression<Func<Domain.Customer.RiskAssessmentControls, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.RiskAssessmentControls> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.RiskAssessmentControls GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.RiskAssessmentControls entity)
        {
            repository.Update(entity);
        }
		
	}
}