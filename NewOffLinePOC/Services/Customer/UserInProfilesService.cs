using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class UserInProfilesService : Infrastructure.Services.Shared.IUserInProfilesService
    {
        private Infrastructure.Repositories.Shared.IUserInProfilesRepository repository;

        public UserInProfilesService(IUserInProfilesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.UserInProfiles entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.UserInProfiles> FindBy(Expression<Func<Domain.Customer.UserInProfiles, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.UserInProfiles> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.UserInProfiles GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.UserInProfiles entity)
        {
            repository.Update(entity);
        }
		
	}
}