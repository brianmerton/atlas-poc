using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class AddressesService : Infrastructure.Services.Shared.IAddressesService
    {
        private Infrastructure.Repositories.Shared.IAddressesRepository repository;

        public AddressesService(IAddressesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Addresses entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Addresses> FindBy(Expression<Func<Domain.Customer.Addresses, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Addresses> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Addresses GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Addresses entity)
        {
            repository.Update(entity);
        }
		
	}
}