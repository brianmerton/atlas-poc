using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RiskAssessmentsService : Infrastructure.Services.Shared.IRiskAssessmentsService
    {
        private Infrastructure.Repositories.Shared.IRiskAssessmentsRepository repository;

        public RiskAssessmentsService(IRiskAssessmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.RiskAssessments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.RiskAssessments> FindBy(Expression<Func<Domain.Customer.RiskAssessments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.RiskAssessments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.RiskAssessments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.RiskAssessments entity)
        {
            repository.Update(entity);
        }
		
	}
}