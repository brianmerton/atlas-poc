using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class EthnicGroupService : Infrastructure.Services.Shared.IEthnicGroupService
    {
        private Infrastructure.Repositories.Shared.IEthnicGroupRepository repository;

        public EthnicGroupService(IEthnicGroupRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.EthnicGroup entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.EthnicGroup> FindBy(Expression<Func<Domain.Customer.EthnicGroup, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.EthnicGroup> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.EthnicGroup GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.EthnicGroup entity)
        {
            repository.Update(entity);
        }
		
	}
}