using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class MSSitesService : Infrastructure.Services.Shared.IMSSitesService
    {
        private Infrastructure.Repositories.Shared.IMSSitesRepository repository;

        public MSSitesService(IMSSitesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.MSSites entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.MSSites> FindBy(Expression<Func<Domain.Customer.MSSites, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.MSSites> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.MSSites GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.MSSites entity)
        {
            repository.Update(entity);
        }
		
	}
}