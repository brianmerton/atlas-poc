using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class MSAdditionalInfoService : Infrastructure.Services.Shared.IMSAdditionalInfoService
    {
        private Infrastructure.Repositories.Shared.IMSAdditionalInfoRepository repository;

        public MSAdditionalInfoService(IMSAdditionalInfoRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.MSAdditionalInfo entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.MSAdditionalInfo> FindBy(Expression<Func<Domain.Customer.MSAdditionalInfo, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.MSAdditionalInfo> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.MSAdditionalInfo GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.MSAdditionalInfo entity)
        {
            repository.Update(entity);
        }
		
	}
}