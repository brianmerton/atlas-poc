using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class EmployeePayrollDetailsService : Infrastructure.Services.Shared.IEmployeePayrollDetailsService
    {
        private Infrastructure.Repositories.Shared.IEmployeePayrollDetailsRepository repository;

        public EmployeePayrollDetailsService(IEmployeePayrollDetailsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.EmployeePayrollDetails entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.EmployeePayrollDetails> FindBy(Expression<Func<Domain.Customer.EmployeePayrollDetails, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.EmployeePayrollDetails> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.EmployeePayrollDetails GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.EmployeePayrollDetails entity)
        {
            repository.Update(entity);
        }
		
	}
}