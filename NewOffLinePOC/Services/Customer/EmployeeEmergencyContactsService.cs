using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class EmployeeEmergencyContactsService : Infrastructure.Services.Shared.IEmployeeEmergencyContactsService
    {
        private Infrastructure.Repositories.Shared.IEmployeeEmergencyContactsRepository repository;

        public EmployeeEmergencyContactsService(IEmployeeEmergencyContactsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.EmployeeEmergencyContacts entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.EmployeeEmergencyContacts> FindBy(Expression<Func<Domain.Customer.EmployeeEmergencyContacts, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.EmployeeEmergencyContacts> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.EmployeeEmergencyContacts GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.EmployeeEmergencyContacts entity)
        {
            repository.Update(entity);
        }
		
	}
}