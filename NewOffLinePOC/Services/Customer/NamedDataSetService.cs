using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class NamedDataSetService : Infrastructure.Services.Shared.INamedDataSetService
    {
        private Infrastructure.Repositories.Shared.INamedDataSetRepository repository;

        public NamedDataSetService(INamedDataSetRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.NamedDataSet entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.NamedDataSet> FindBy(Expression<Func<Domain.Customer.NamedDataSet, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.NamedDataSet> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.NamedDataSet GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.NamedDataSet entity)
        {
            repository.Update(entity);
        }
		
	}
}