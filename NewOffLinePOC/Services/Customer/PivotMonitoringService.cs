using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class PivotMonitoringService : Infrastructure.Services.Shared.IPivotMonitoringService
    {
        private Infrastructure.Repositories.Shared.IPivotMonitoringRepository repository;

        public PivotMonitoringService(IPivotMonitoringRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.PivotMonitoring entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.PivotMonitoring> FindBy(Expression<Func<Domain.Customer.PivotMonitoring, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.PivotMonitoring> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.PivotMonitoring GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.PivotMonitoring entity)
        {
            repository.Update(entity);
        }
		
	}
}