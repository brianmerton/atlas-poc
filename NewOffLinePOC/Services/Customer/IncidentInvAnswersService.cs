using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class IncidentInvAnswersService : Infrastructure.Services.Shared.IIncidentInvAnswersService
    {
        private Infrastructure.Repositories.Shared.IIncidentInvAnswersRepository repository;

        public IncidentInvAnswersService(IIncidentInvAnswersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.IncidentInvAnswers entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.IncidentInvAnswers> FindBy(Expression<Func<Domain.Customer.IncidentInvAnswers, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.IncidentInvAnswers> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.IncidentInvAnswers GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.IncidentInvAnswers entity)
        {
            repository.Update(entity);
        }
		
	}
}