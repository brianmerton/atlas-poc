using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class InjuredBodyPartsService : Infrastructure.Services.Shared.IInjuredBodyPartsService
    {
        private Infrastructure.Repositories.Shared.IInjuredBodyPartsRepository repository;

        public InjuredBodyPartsService(IInjuredBodyPartsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.InjuredBodyParts entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.InjuredBodyParts> FindBy(Expression<Func<Domain.Customer.InjuredBodyParts, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.InjuredBodyParts> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.InjuredBodyParts GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.InjuredBodyParts entity)
        {
            repository.Update(entity);
        }
		
	}
}