using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RolesService : Infrastructure.Services.Shared.IRolesService
    {
        private Infrastructure.Repositories.Shared.IRolesRepository repository;

        public RolesService(IRolesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Roles entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Roles> FindBy(Expression<Func<Domain.Customer.Roles, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Roles> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Roles GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Roles entity)
        {
            repository.Update(entity);
        }
		
	}
}