using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class InvAnswersService : Infrastructure.Services.Shared.IInvAnswersService
    {
        private Infrastructure.Repositories.Shared.IInvAnswersRepository repository;

        public InvAnswersService(IInvAnswersRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.InvAnswers entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.InvAnswers> FindBy(Expression<Func<Domain.Customer.InvAnswers, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.InvAnswers> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.InvAnswers GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.InvAnswers entity)
        {
            repository.Update(entity);
        }
		
	}
}