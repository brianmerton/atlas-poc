using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class AttributeValuesService : Infrastructure.Services.Shared.IAttributeValuesService
    {
        private Infrastructure.Repositories.Shared.IAttributeValuesRepository repository;

        public AttributeValuesService(IAttributeValuesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.AttributeValues entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.AttributeValues> FindBy(Expression<Func<Domain.Customer.AttributeValues, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.AttributeValues> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.AttributeValues GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.AttributeValues entity)
        {
            repository.Update(entity);
        }
		
	}
}