using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class MSEventsService : Infrastructure.Services.Shared.IMSEventsService
    {
        private Infrastructure.Repositories.Shared.IMSEventsRepository repository;

        public MSEventsService(IMSEventsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.MSEvents entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.MSEvents> FindBy(Expression<Func<Domain.Customer.MSEvents, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.MSEvents> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.MSEvents GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.MSEvents entity)
        {
            repository.Update(entity);
        }
		
	}
}