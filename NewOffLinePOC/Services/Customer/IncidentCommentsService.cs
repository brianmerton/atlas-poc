using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class IncidentCommentsService : Infrastructure.Services.Shared.IIncidentCommentsService
    {
        private Infrastructure.Repositories.Shared.IIncidentCommentsRepository repository;

        public IncidentCommentsService(IIncidentCommentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.IncidentComments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.IncidentComments> FindBy(Expression<Func<Domain.Customer.IncidentComments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.IncidentComments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.IncidentComments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.IncidentComments entity)
        {
            repository.Update(entity);
        }
		
	}
}