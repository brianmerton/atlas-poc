using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class ControlHazardsService : Infrastructure.Services.Shared.IControlHazardsService
    {
        private Infrastructure.Repositories.Shared.IControlHazardsRepository repository;

        public ControlHazardsService(IControlHazardsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.ControlHazards entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.ControlHazards> FindBy(Expression<Func<Domain.Customer.ControlHazards, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.ControlHazards> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.ControlHazards GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.ControlHazards entity)
        {
            repository.Update(entity);
        }
		
	}
}