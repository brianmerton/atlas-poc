using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class UserInRolesService : Infrastructure.Services.Shared.IUserInRolesService
    {
        private Infrastructure.Repositories.Shared.IUserInRolesRepository repository;

        public UserInRolesService(IUserInRolesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.UserInRoles entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.UserInRoles> FindBy(Expression<Func<Domain.Customer.UserInRoles, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.UserInRoles> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.UserInRoles GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.UserInRoles entity)
        {
            repository.Update(entity);
        }
		
	}
}