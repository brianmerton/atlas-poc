using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class HazardSectorsService : Infrastructure.Services.Shared.IHazardSectorsService
    {
        private Infrastructure.Repositories.Shared.IHazardSectorsRepository repository;

        public HazardSectorsService(IHazardSectorsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.HazardSectors entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.HazardSectors> FindBy(Expression<Func<Domain.Customer.HazardSectors, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.HazardSectors> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.HazardSectors GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.HazardSectors entity)
        {
            repository.Update(entity);
        }
		
	}
}