using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class UserCheckListItemsService : Infrastructure.Services.Shared.IUserCheckListItemsService
    {
        private Infrastructure.Repositories.Shared.IUserCheckListItemsRepository repository;

        public UserCheckListItemsService(IUserCheckListItemsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.UserCheckListItems entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.UserCheckListItems> FindBy(Expression<Func<Domain.Customer.UserCheckListItems, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.UserCheckListItems> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.UserCheckListItems GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.UserCheckListItems entity)
        {
            repository.Update(entity);
        }
		
	}
}