using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class HazardsService : Infrastructure.Services.Shared.IHazardsService
    {
        private Infrastructure.Repositories.Shared.IHazardsRepository repository;

        public HazardsService(IHazardsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Hazards entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Hazards> FindBy(Expression<Func<Domain.Customer.Hazards, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Hazards> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Hazards GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Hazards entity)
        {
            repository.Update(entity);
        }
		
	}
}