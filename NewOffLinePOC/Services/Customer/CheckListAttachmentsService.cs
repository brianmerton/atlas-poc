using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class CheckListAttachmentsService : Infrastructure.Services.Shared.ICheckListAttachmentsService
    {
        private Infrastructure.Repositories.Shared.ICheckListAttachmentsRepository repository;

        public CheckListAttachmentsService(ICheckListAttachmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.CheckListAttachments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.CheckListAttachments> FindBy(Expression<Func<Domain.Customer.CheckListAttachments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.CheckListAttachments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.CheckListAttachments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.CheckListAttachments entity)
        {
            repository.Update(entity);
        }
		
	}
}