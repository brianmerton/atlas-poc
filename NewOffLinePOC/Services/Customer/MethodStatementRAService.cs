using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class MethodStatementRAService : Infrastructure.Services.Shared.IMethodStatementRAService
    {
        private Infrastructure.Repositories.Shared.IMethodStatementRARepository repository;

        public MethodStatementRAService(IMethodStatementRARepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.MethodStatementRA entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.MethodStatementRA> FindBy(Expression<Func<Domain.Customer.MethodStatementRA, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.MethodStatementRA> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.MethodStatementRA GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.MethodStatementRA entity)
        {
            repository.Update(entity);
        }
		
	}
}