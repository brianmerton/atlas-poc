using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class DocumentsService : Infrastructure.Services.Shared.IDocumentsService
    {
        private Infrastructure.Repositories.Shared.IDocumentsRepository repository;

        public DocumentsService(IDocumentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Documents entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Documents> FindBy(Expression<Func<Domain.Customer.Documents, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Documents> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Documents GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Documents entity)
        {
            repository.Update(entity);
        }
		
	}
}