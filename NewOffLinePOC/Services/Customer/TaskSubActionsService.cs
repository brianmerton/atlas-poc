using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class TaskSubActionsService : Infrastructure.Services.Shared.ITaskSubActionsService
    {
        private Infrastructure.Repositories.Shared.ITaskSubActionsRepository repository;

        public TaskSubActionsService(ITaskSubActionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.TaskSubActions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.TaskSubActions> FindBy(Expression<Func<Domain.Customer.TaskSubActions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.TaskSubActions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.TaskSubActions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.TaskSubActions entity)
        {
            repository.Update(entity);
        }
		
	}
}