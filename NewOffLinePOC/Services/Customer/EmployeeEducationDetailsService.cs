using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class EmployeeEducationDetailsService : Infrastructure.Services.Shared.IEmployeeEducationDetailsService
    {
        private Infrastructure.Repositories.Shared.IEmployeeEducationDetailsRepository repository;

        public EmployeeEducationDetailsService(IEmployeeEducationDetailsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.EmployeeEducationDetails entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.EmployeeEducationDetails> FindBy(Expression<Func<Domain.Customer.EmployeeEducationDetails, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.EmployeeEducationDetails> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.EmployeeEducationDetails GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.EmployeeEducationDetails entity)
        {
            repository.Update(entity);
        }
		
	}
}