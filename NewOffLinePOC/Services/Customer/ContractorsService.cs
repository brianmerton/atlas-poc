using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class ContractorsService : Infrastructure.Services.Shared.IContractorsService
    {
        private Infrastructure.Repositories.Shared.IContractorsRepository repository;

        public ContractorsService(IContractorsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Contractors entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Contractors> FindBy(Expression<Func<Domain.Customer.Contractors, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Contractors> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Contractors GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Contractors entity)
        {
            repository.Update(entity);
        }
		
	}
}