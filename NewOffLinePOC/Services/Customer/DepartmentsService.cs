using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class DepartmentsService : Infrastructure.Services.Shared.IDepartmentsService
    {
        private Infrastructure.Repositories.Shared.IDepartmentsRepository repository;

        public DepartmentsService(IDepartmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Departments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Departments> FindBy(Expression<Func<Domain.Customer.Departments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Departments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Departments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Departments entity)
        {
            repository.Update(entity);
        }
		
	}
}