using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class JobTitlesService : Infrastructure.Services.Shared.IJobTitlesService
    {
        private Infrastructure.Repositories.Shared.IJobTitlesRepository repository;

        public JobTitlesService(IJobTitlesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.JobTitles entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.JobTitles> FindBy(Expression<Func<Domain.Customer.JobTitles, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.JobTitles> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.JobTitles GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.JobTitles entity)
        {
            repository.Update(entity);
        }
		
	}
}