using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RiskAssessmentWorkspaceTypesService : Infrastructure.Services.Shared.IRiskAssessmentWorkspaceTypesService
    {
        private Infrastructure.Repositories.Shared.IRiskAssessmentWorkspaceTypesRepository repository;

        public RiskAssessmentWorkspaceTypesService(IRiskAssessmentWorkspaceTypesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.RiskAssessmentWorkspaceTypes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.RiskAssessmentWorkspaceTypes> FindBy(Expression<Func<Domain.Customer.RiskAssessmentWorkspaceTypes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.RiskAssessmentWorkspaceTypes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.RiskAssessmentWorkspaceTypes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.RiskAssessmentWorkspaceTypes entity)
        {
            repository.Update(entity);
        }
		
	}
}