using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class ControlsService : Infrastructure.Services.Shared.IControlsService
    {
        private Infrastructure.Repositories.Shared.IControlsRepository repository;

        public ControlsService(IControlsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Controls entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Controls> FindBy(Expression<Func<Domain.Customer.Controls, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Controls> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Controls GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Controls entity)
        {
            repository.Update(entity);
        }
		
	}
}