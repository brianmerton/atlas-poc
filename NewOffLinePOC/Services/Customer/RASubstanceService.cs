using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RASubstanceService : Infrastructure.Services.Shared.IRASubstanceService
    {
        private Infrastructure.Repositories.Shared.IRASubstanceRepository repository;

        public RASubstanceService(IRASubstanceRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.RASubstance entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.RASubstance> FindBy(Expression<Func<Domain.Customer.RASubstance, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.RASubstance> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.RASubstance GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.RASubstance entity)
        {
            repository.Update(entity);
        }
		
	}
}