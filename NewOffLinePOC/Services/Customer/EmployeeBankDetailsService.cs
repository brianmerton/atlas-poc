using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class EmployeeBankDetailsService : Infrastructure.Services.Shared.IEmployeeBankDetailsService
    {
        private Infrastructure.Repositories.Shared.IEmployeeBankDetailsRepository repository;

        public EmployeeBankDetailsService(IEmployeeBankDetailsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.EmployeeBankDetails entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.EmployeeBankDetails> FindBy(Expression<Func<Domain.Customer.EmployeeBankDetails, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.EmployeeBankDetails> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.EmployeeBankDetails GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.EmployeeBankDetails entity)
        {
            repository.Update(entity);
        }
		
	}
}