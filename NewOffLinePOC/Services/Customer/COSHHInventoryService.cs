using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class COSHHInventoryService : Infrastructure.Services.Shared.ICOSHHInventoryService
    {
        private Infrastructure.Repositories.Shared.ICOSHHInventoryRepository repository;

        public COSHHInventoryService(ICOSHHInventoryRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.COSHHInventory entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.COSHHInventory> FindBy(Expression<Func<Domain.Customer.COSHHInventory, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.COSHHInventory> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.COSHHInventory GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.COSHHInventory entity)
        {
            repository.Update(entity);
        }
		
	}
}