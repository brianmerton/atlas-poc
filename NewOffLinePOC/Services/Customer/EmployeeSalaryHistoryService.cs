using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class EmployeeSalaryHistoryService : Infrastructure.Services.Shared.IEmployeeSalaryHistoryService
    {
        private Infrastructure.Repositories.Shared.IEmployeeSalaryHistoryRepository repository;

        public EmployeeSalaryHistoryService(IEmployeeSalaryHistoryRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.EmployeeSalaryHistory entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.EmployeeSalaryHistory> FindBy(Expression<Func<Domain.Customer.EmployeeSalaryHistory, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.EmployeeSalaryHistory> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.EmployeeSalaryHistory GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.EmployeeSalaryHistory entity)
        {
            repository.Update(entity);
        }
		
	}
}