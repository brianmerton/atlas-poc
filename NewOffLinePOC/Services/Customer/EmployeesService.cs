using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class EmployeesService : Infrastructure.Services.Shared.IEmployeesService
    {
        private Infrastructure.Repositories.Shared.IEmployeesRepository repository;

        public EmployeesService(IEmployeesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Employees entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Employees> FindBy(Expression<Func<Domain.Customer.Employees, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Employees> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Employees GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Employees entity)
        {
            repository.Update(entity);
        }
		
	}
}