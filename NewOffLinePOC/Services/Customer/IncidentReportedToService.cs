using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class IncidentReportedToService : Infrastructure.Services.Shared.IIncidentReportedToService
    {
        private Infrastructure.Repositories.Shared.IIncidentReportedToRepository repository;

        public IncidentReportedToService(IIncidentReportedToRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.IncidentReportedTo entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.IncidentReportedTo> FindBy(Expression<Func<Domain.Customer.IncidentReportedTo, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.IncidentReportedTo> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.IncidentReportedTo GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.IncidentReportedTo entity)
        {
            repository.Update(entity);
        }
		
	}
}