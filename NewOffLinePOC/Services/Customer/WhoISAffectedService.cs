using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class WhoISAffectedService : Infrastructure.Services.Shared.IWhoISAffectedService
    {
        private Infrastructure.Repositories.Shared.IWhoISAffectedRepository repository;

        public WhoISAffectedService(IWhoISAffectedRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.WhoISAffected entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.WhoISAffected> FindBy(Expression<Func<Domain.Customer.WhoISAffected, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.WhoISAffected> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.WhoISAffected GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.WhoISAffected entity)
        {
            repository.Update(entity);
        }
		
	}
}