using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class MSAttachmentsService : Infrastructure.Services.Shared.IMSAttachmentsService
    {
        private Infrastructure.Repositories.Shared.IMSAttachmentsRepository repository;

        public MSAttachmentsService(IMSAttachmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.MSAttachments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.MSAttachments> FindBy(Expression<Func<Domain.Customer.MSAttachments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.MSAttachments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.MSAttachments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.MSAttachments entity)
        {
            repository.Update(entity);
        }
		
	}
}