using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class UserProfileRoleMapService : Infrastructure.Services.Shared.IUserProfileRoleMapService
    {
        private Infrastructure.Repositories.Shared.IUserProfileRoleMapRepository repository;

        public UserProfileRoleMapService(IUserProfileRoleMapRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.UserProfileRoleMap entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.UserProfileRoleMap> FindBy(Expression<Func<Domain.Customer.UserProfileRoleMap, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.UserProfileRoleMap> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.UserProfileRoleMap GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.UserProfileRoleMap entity)
        {
            repository.Update(entity);
        }
		
	}
}