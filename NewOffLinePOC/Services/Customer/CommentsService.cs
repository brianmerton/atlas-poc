using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class CommentsService : Infrastructure.Services.Shared.ICommentsService
    {
        private Infrastructure.Repositories.Shared.ICommentsRepository repository;

        public CommentsService(ICommentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Comments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Comments> FindBy(Expression<Func<Domain.Customer.Comments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Comments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Comments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Comments entity)
        {
            repository.Update(entity);
        }
		
	}
}