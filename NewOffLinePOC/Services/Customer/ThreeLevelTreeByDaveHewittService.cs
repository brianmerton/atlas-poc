using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class ThreeLevelTreeByDaveHewittService : Infrastructure.Services.Shared.IThreeLevelTreeByDaveHewittService
    {
        private Infrastructure.Repositories.Shared.IThreeLevelTreeByDaveHewittRepository repository;

        public ThreeLevelTreeByDaveHewittService(IThreeLevelTreeByDaveHewittRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.ThreeLevelTreeByDaveHewitt entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.ThreeLevelTreeByDaveHewitt> FindBy(Expression<Func<Domain.Customer.ThreeLevelTreeByDaveHewitt, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.ThreeLevelTreeByDaveHewitt> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.ThreeLevelTreeByDaveHewitt GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.ThreeLevelTreeByDaveHewitt entity)
        {
            repository.Update(entity);
        }
		
	}
}