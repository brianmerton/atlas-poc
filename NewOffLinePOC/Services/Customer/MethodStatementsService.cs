using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class MethodStatementsService : Infrastructure.Services.Shared.IMethodStatementsService
    {
        private Infrastructure.Repositories.Shared.IMethodStatementsRepository repository;

        public MethodStatementsService(IMethodStatementsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.MethodStatements entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.MethodStatements> FindBy(Expression<Func<Domain.Customer.MethodStatements, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.MethodStatements> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.MethodStatements GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.MethodStatements entity)
        {
            repository.Update(entity);
        }
		
	}
}