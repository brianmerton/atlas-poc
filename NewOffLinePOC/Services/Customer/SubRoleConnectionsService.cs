using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class SubRoleConnectionsService : Infrastructure.Services.Shared.ISubRoleConnectionsService
    {
        private Infrastructure.Repositories.Shared.ISubRoleConnectionsRepository repository;

        public SubRoleConnectionsService(ISubRoleConnectionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.SubRoleConnections entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.SubRoleConnections> FindBy(Expression<Func<Domain.Customer.SubRoleConnections, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.SubRoleConnections> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.SubRoleConnections GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.SubRoleConnections entity)
        {
            repository.Update(entity);
        }
		
	}
}