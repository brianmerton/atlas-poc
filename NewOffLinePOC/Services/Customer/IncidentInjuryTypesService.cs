using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class IncidentInjuryTypesService : Infrastructure.Services.Shared.IIncidentInjuryTypesService
    {
        private Infrastructure.Repositories.Shared.IIncidentInjuryTypesRepository repository;

        public IncidentInjuryTypesService(IIncidentInjuryTypesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.IncidentInjuryTypes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.IncidentInjuryTypes> FindBy(Expression<Func<Domain.Customer.IncidentInjuryTypes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.IncidentInjuryTypes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.IncidentInjuryTypes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.IncidentInjuryTypes entity)
        {
            repository.Update(entity);
        }
		
	}
}