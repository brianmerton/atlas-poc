using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class AttachmentsService : Infrastructure.Services.Shared.IAttachmentsService
    {
        private Infrastructure.Repositories.Shared.IAttachmentsRepository repository;

        public AttachmentsService(IAttachmentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Attachments entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Attachments> FindBy(Expression<Func<Domain.Customer.Attachments, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Attachments> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Attachments GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Attachments entity)
        {
            repository.Update(entity);
        }
		
	}
}