using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class EmployeeJobHistoryService : Infrastructure.Services.Shared.IEmployeeJobHistoryService
    {
        private Infrastructure.Repositories.Shared.IEmployeeJobHistoryRepository repository;

        public EmployeeJobHistoryService(IEmployeeJobHistoryRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.EmployeeJobHistory entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.EmployeeJobHistory> FindBy(Expression<Func<Domain.Customer.EmployeeJobHistory, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.EmployeeJobHistory> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.EmployeeJobHistory GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.EmployeeJobHistory entity)
        {
            repository.Update(entity);
        }
		
	}
}