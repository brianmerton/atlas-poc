using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class UserCoursesService : Infrastructure.Services.Shared.IUserCoursesService
    {
        private Infrastructure.Repositories.Shared.IUserCoursesRepository repository;

        public UserCoursesService(IUserCoursesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.UserCourses entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.UserCourses> FindBy(Expression<Func<Domain.Customer.UserCourses, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.UserCourses> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.UserCourses GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.UserCourses entity)
        {
            repository.Update(entity);
        }
		
	}
}