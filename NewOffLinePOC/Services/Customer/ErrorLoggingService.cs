using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class ErrorLoggingService : Infrastructure.Services.Shared.IErrorLoggingService
    {
        private Infrastructure.Repositories.Shared.IErrorLoggingRepository repository;

        public ErrorLoggingService(IErrorLoggingRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.ErrorLogging entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.ErrorLogging> FindBy(Expression<Func<Domain.Customer.ErrorLogging, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.ErrorLogging> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.ErrorLogging GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.ErrorLogging entity)
        {
            repository.Update(entity);
        }
		
	}
}