using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class TaskActivitiesService : Infrastructure.Services.Shared.ITaskActivitiesService
    {
        private Infrastructure.Repositories.Shared.ITaskActivitiesRepository repository;

        public TaskActivitiesService(ITaskActivitiesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.TaskActivities entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.TaskActivities> FindBy(Expression<Func<Domain.Customer.TaskActivities, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.TaskActivities> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.TaskActivities GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.TaskActivities entity)
        {
            repository.Update(entity);
        }
		
	}
}