using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class JobsService : Infrastructure.Services.Shared.IJobsService
    {
        private Infrastructure.Repositories.Shared.IJobsRepository repository;

        public JobsService(IJobsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Jobs entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Jobs> FindBy(Expression<Func<Domain.Customer.Jobs, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Jobs> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Jobs GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Jobs entity)
        {
            repository.Update(entity);
        }
		
	}
}