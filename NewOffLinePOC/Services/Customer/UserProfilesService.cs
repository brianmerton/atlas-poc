using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class UserProfilesService : Infrastructure.Services.Shared.IUserProfilesService
    {
        private Infrastructure.Repositories.Shared.IUserProfilesRepository repository;

        public UserProfilesService(IUserProfilesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.UserProfiles entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.UserProfiles> FindBy(Expression<Func<Domain.Customer.UserProfiles, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.UserProfiles> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.UserProfiles GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.UserProfiles entity)
        {
            repository.Update(entity);
        }
		
	}
}