using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RegionsService : Infrastructure.Services.Shared.IRegionsService
    {
        private Infrastructure.Repositories.Shared.IRegionsRepository repository;

        public RegionsService(IRegionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Regions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Regions> FindBy(Expression<Func<Domain.Customer.Regions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Regions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Regions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Regions entity)
        {
            repository.Update(entity);
        }
		
	}
}