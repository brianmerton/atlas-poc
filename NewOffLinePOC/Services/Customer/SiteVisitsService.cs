using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class SiteVisitsService : Infrastructure.Services.Shared.ISiteVisitsService
    {
        private Infrastructure.Repositories.Shared.ISiteVisitsRepository repository;

        public SiteVisitsService(ISiteVisitsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.SiteVisits entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.SiteVisits> FindBy(Expression<Func<Domain.Customer.SiteVisits, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.SiteVisits> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.SiteVisits GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.SiteVisits entity)
        {
            repository.Update(entity);
        }
		
	}
}