using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RAProceduresService : Infrastructure.Services.Shared.IRAProceduresService
    {
        private Infrastructure.Repositories.Shared.IRAProceduresRepository repository;

        public RAProceduresService(IRAProceduresRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.RAProcedures entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.RAProcedures> FindBy(Expression<Func<Domain.Customer.RAProcedures, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.RAProcedures> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.RAProcedures GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.RAProcedures entity)
        {
            repository.Update(entity);
        }
		
	}
}