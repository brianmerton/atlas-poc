using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class DocumentVersionsService : Infrastructure.Services.Shared.IDocumentVersionsService
    {
        private Infrastructure.Repositories.Shared.IDocumentVersionsRepository repository;

        public DocumentVersionsService(IDocumentVersionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.DocumentVersions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.DocumentVersions> FindBy(Expression<Func<Domain.Customer.DocumentVersions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.DocumentVersions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.DocumentVersions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.DocumentVersions entity)
        {
            repository.Update(entity);
        }
		
	}
}