using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class SitesService : Infrastructure.Services.Shared.ISitesService
    {
        private Infrastructure.Repositories.Shared.ISitesRepository repository;

        public SitesService(ISitesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Sites entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Sites> FindBy(Expression<Func<Domain.Customer.Sites, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Sites> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Sites GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Sites entity)
        {
            repository.Update(entity);
        }
		
	}
}