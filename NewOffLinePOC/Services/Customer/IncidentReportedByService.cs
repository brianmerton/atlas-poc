using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class IncidentReportedByService : Infrastructure.Services.Shared.IIncidentReportedByService
    {
        private Infrastructure.Repositories.Shared.IIncidentReportedByRepository repository;

        public IncidentReportedByService(IIncidentReportedByRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.IncidentReportedBy entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.IncidentReportedBy> FindBy(Expression<Func<Domain.Customer.IncidentReportedBy, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.IncidentReportedBy> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.IncidentReportedBy GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.IncidentReportedBy entity)
        {
            repository.Update(entity);
        }
		
	}
}