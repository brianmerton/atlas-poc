using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class UserCheckListsService : Infrastructure.Services.Shared.IUserCheckListsService
    {
        private Infrastructure.Repositories.Shared.IUserCheckListsRepository repository;

        public UserCheckListsService(IUserCheckListsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.UserCheckLists entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.UserCheckLists> FindBy(Expression<Func<Domain.Customer.UserCheckLists, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.UserCheckLists> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.UserCheckLists GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.UserCheckLists entity)
        {
            repository.Update(entity);
        }
		
	}
}