using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class EmployeeTrainingDetailsService : Infrastructure.Services.Shared.IEmployeeTrainingDetailsService
    {
        private Infrastructure.Repositories.Shared.IEmployeeTrainingDetailsRepository repository;

        public EmployeeTrainingDetailsService(IEmployeeTrainingDetailsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.EmployeeTrainingDetails entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.EmployeeTrainingDetails> FindBy(Expression<Func<Domain.Customer.EmployeeTrainingDetails, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.EmployeeTrainingDetails> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.EmployeeTrainingDetails GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.EmployeeTrainingDetails entity)
        {
            repository.Update(entity);
        }
		
	}
}