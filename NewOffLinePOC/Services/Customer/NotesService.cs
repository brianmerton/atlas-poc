using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class NotesService : Infrastructure.Services.Shared.INotesService
    {
        private Infrastructure.Repositories.Shared.INotesRepository repository;

        public NotesService(INotesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Notes entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Notes> FindBy(Expression<Func<Domain.Customer.Notes, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Notes> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Notes GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Notes entity)
        {
            repository.Update(entity);
        }
		
	}
}