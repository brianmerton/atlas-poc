using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RiskAssessmentHazardsService : Infrastructure.Services.Shared.IRiskAssessmentHazardsService
    {
        private Infrastructure.Repositories.Shared.IRiskAssessmentHazardsRepository repository;

        public RiskAssessmentHazardsService(IRiskAssessmentHazardsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.RiskAssessmentHazards entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.RiskAssessmentHazards> FindBy(Expression<Func<Domain.Customer.RiskAssessmentHazards, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.RiskAssessmentHazards> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.RiskAssessmentHazards GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.RiskAssessmentHazards entity)
        {
            repository.Update(entity);
        }
		
	}
}