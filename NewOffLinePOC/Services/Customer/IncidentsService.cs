using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class IncidentsService : Infrastructure.Services.Shared.IIncidentsService
    {
        private Infrastructure.Repositories.Shared.IIncidentsRepository repository;

        public IncidentsService(IIncidentsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.Incidents entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.Incidents> FindBy(Expression<Func<Domain.Customer.Incidents, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.Incidents> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.Incidents GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.Incidents entity)
        {
            repository.Update(entity);
        }
		
	}
}