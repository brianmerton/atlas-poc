using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RiskAssessmentSectorsService : Infrastructure.Services.Shared.IRiskAssessmentSectorsService
    {
        private Infrastructure.Repositories.Shared.IRiskAssessmentSectorsRepository repository;

        public RiskAssessmentSectorsService(IRiskAssessmentSectorsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.RiskAssessmentSectors entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.RiskAssessmentSectors> FindBy(Expression<Func<Domain.Customer.RiskAssessmentSectors, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.RiskAssessmentSectors> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.RiskAssessmentSectors GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.RiskAssessmentSectors entity)
        {
            repository.Update(entity);
        }
		
	}
}