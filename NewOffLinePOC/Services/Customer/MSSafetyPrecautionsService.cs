using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class MSSafetyPrecautionsService : Infrastructure.Services.Shared.IMSSafetyPrecautionsService
    {
        private Infrastructure.Repositories.Shared.IMSSafetyPrecautionsRepository repository;

        public MSSafetyPrecautionsService(IMSSafetyPrecautionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.MSSafetyPrecautions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.MSSafetyPrecautions> FindBy(Expression<Func<Domain.Customer.MSSafetyPrecautions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.MSSafetyPrecautions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.MSSafetyPrecautions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.MSSafetyPrecautions entity)
        {
            repository.Update(entity);
        }
		
	}
}