using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class AboutInjuryService : Infrastructure.Services.Shared.IAboutInjuryService
    {
        private Infrastructure.Repositories.Shared.IAboutInjuryRepository repository;

        public AboutInjuryService(IAboutInjuryRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.AboutInjury entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.AboutInjury> FindBy(Expression<Func<Domain.Customer.AboutInjury, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.AboutInjury> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.AboutInjury GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.AboutInjury entity)
        {
            repository.Update(entity);
        }
		
	}
}