using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Customer
{
    public class RAEvidenceService : Infrastructure.Services.Shared.IRAEvidenceService
    {
        private Infrastructure.Repositories.Shared.IRAEvidenceRepository repository;

        public RAEvidenceService(IRAEvidenceRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Customer.RAEvidence entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Customer.RAEvidence> FindBy(Expression<Func<Domain.Customer.RAEvidence, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Customer.RAEvidence> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Customer.RAEvidence GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Customer.RAEvidence entity)
        {
            repository.Update(entity);
        }
		
	}
}