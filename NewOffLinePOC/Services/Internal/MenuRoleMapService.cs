using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class MenuRoleMapService : Infrastructure.Services.Shared.IMenuRoleMapService
    {
        private Infrastructure.Repositories.Shared.IMenuRoleMapRepository repository;

        public MenuRoleMapService(IMenuRoleMapRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.MenuRoleMap entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.MenuRoleMap> FindBy(Expression<Func<Domain.Internal.MenuRoleMap, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.MenuRoleMap> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.MenuRoleMap GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.MenuRoleMap entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Internal.MenuRoleMap> GetByMenuId(Guid MenuId)
        {
            return repository.GetByMenuId(MenuId);
        }

        public IEnumerable<Domain.Internal.MenuRoleMap> GetByRoleId(Guid RoleId)
        {
            return repository.GetByRoleId(RoleId);
        }

	}
}