using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class RoleOperationMapService : Infrastructure.Services.Shared.IRoleOperationMapService
    {
        private Infrastructure.Repositories.Shared.IRoleOperationMapRepository repository;

        public RoleOperationMapService(IRoleOperationMapRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.RoleOperationMap entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.RoleOperationMap> FindBy(Expression<Func<Domain.Internal.RoleOperationMap, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.RoleOperationMap> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.RoleOperationMap GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.RoleOperationMap entity)
        {
            repository.Update(entity);
        }
		
        public IEnumerable<Domain.Internal.RoleOperationMap> GetByOperationId(Guid OperationId)
        {
            return repository.GetByOperationId(OperationId);
        }

        public IEnumerable<Domain.Internal.RoleOperationMap> GetByRoleId(Guid RoleId)
        {
            return repository.GetByRoleId(RoleId);
        }

	}
}