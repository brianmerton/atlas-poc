using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class MigrationsService : Infrastructure.Services.Shared.IMigrationsService
    {
        private Infrastructure.Repositories.Shared.IMigrationsRepository repository;

        public MigrationsService(IMigrationsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.Migrations entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.Migrations> FindBy(Expression<Func<Domain.Internal.Migrations, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.Migrations> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.Migrations GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.Migrations entity)
        {
            repository.Update(entity);
        }
		
	}
}