using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class TockensService : Infrastructure.Services.Shared.ITockensService
    {
        private Infrastructure.Repositories.Shared.ITockensRepository repository;

        public TockensService(ITockensRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.Tockens entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.Tockens> FindBy(Expression<Func<Domain.Internal.Tockens, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.Tockens> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.Tockens GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.Tockens entity)
        {
            repository.Update(entity);
        }
		
	}
}