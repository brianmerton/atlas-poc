using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class HelpEntriesService : Infrastructure.Services.Shared.IHelpEntriesService
    {
        private Infrastructure.Repositories.Shared.IHelpEntriesRepository repository;

        public HelpEntriesService(IHelpEntriesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.HelpEntries entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.HelpEntries> FindBy(Expression<Func<Domain.Internal.HelpEntries, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.HelpEntries> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.HelpEntries GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.HelpEntries entity)
        {
            repository.Update(entity);
        }
		
	}
}