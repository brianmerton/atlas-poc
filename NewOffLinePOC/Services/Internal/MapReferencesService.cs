using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class MapReferencesService : Infrastructure.Services.Shared.IMapReferencesService
    {
        private Infrastructure.Repositories.Shared.IMapReferencesRepository repository;

        public MapReferencesService(IMapReferencesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.MapReferences entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.MapReferences> FindBy(Expression<Func<Domain.Internal.MapReferences, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.MapReferences> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.MapReferences GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.MapReferences entity)
        {
            repository.Update(entity);
        }
		
	}
}