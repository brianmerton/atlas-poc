using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class OperationsService : Infrastructure.Services.Shared.IOperationsService
    {
        private Infrastructure.Repositories.Shared.IOperationsRepository repository;

        public OperationsService(IOperationsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.Operations entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.Operations> FindBy(Expression<Func<Domain.Internal.Operations, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.Operations> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.Operations GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.Operations entity)
        {
            repository.Update(entity);
        }
		
	}
}