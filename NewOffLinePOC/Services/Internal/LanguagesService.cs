using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class LanguagesService : Infrastructure.Services.Shared.ILanguagesService
    {
        private Infrastructure.Repositories.Shared.ILanguagesRepository repository;

        public LanguagesService(ILanguagesRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.Languages entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.Languages> FindBy(Expression<Func<Domain.Internal.Languages, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.Languages> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.Languages GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.Languages entity)
        {
            repository.Update(entity);
        }
		
	}
}