using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class SessionsService : Infrastructure.Services.Shared.ISessionsService
    {
        private Infrastructure.Repositories.Shared.ISessionsRepository repository;

        public SessionsService(ISessionsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.Sessions entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.Sessions> FindBy(Expression<Func<Domain.Internal.Sessions, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.Sessions> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.Sessions GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.Sessions entity)
        {
            repository.Update(entity);
        }
		
	}
}