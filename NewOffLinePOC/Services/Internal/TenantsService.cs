using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class TenantsService : Infrastructure.Services.Shared.ITenantsService
    {
        private Infrastructure.Repositories.Shared.ITenantsRepository repository;

        public TenantsService(ITenantsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.Tenants entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.Tenants> FindBy(Expression<Func<Domain.Internal.Tenants, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.Tenants> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.Tenants GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.Tenants entity)
        {
            repository.Update(entity);
        }
		
	}
}