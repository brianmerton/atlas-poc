using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class MenusService : Infrastructure.Services.Shared.IMenusService
    {
        private Infrastructure.Repositories.Shared.IMenusRepository repository;

        public MenusService(IMenusRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.Menus entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.Menus> FindBy(Expression<Func<Domain.Internal.Menus, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.Menus> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.Menus GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.Menus entity)
        {
            repository.Update(entity);
        }
		
	}
}