using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Internal
{
    public class ErrorsService : Infrastructure.Services.Shared.IErrorsService
    {
        private Infrastructure.Repositories.Shared.IErrorsRepository repository;

        public ErrorsService(IErrorsRepository repository)
        {
            this.repository = repository;
        }

        public Guid Add(Domain.Internal.Errors entity)
        {
            return repository.Add(entity);
        }

        public void DeleteById(Guid id)
        {
            repository.DeleteById(id);
        }

        public IEnumerable<Domain.Internal.Errors> FindBy(Expression<Func<Domain.Internal.Errors, bool>> predicate)
        {
            return repository.FindBy(predicate);
        }

        public IEnumerable<Domain.Internal.Errors> GetAll()
        {
            return repository.GetAll();
        }

        public Domain.Internal.Errors GetById(Guid id)
        {
            return repository.GetById(id);
        }

        public void Update(Domain.Internal.Errors entity)
        {
            repository.Update(entity);
        }
		
	}
}