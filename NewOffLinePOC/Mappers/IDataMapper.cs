﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;

namespace Mappers
{
    public interface IDataMapper<IDataReader, T>
    {
        T Map (IDataReader input);
    }
}
