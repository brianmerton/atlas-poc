
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class RoleOperationMapMapper : IDataMapper<IDataReader, Domain.Internal.RoleOperationMap>
    {
        public Domain.Internal.RoleOperationMap Map(IDataReader reader)
        {
			var RoleOperationMapToMap = new Domain.Internal.RoleOperationMap();
			RoleOperationMapToMap.OperationId = (Guid)reader["OperationId"];
			RoleOperationMapToMap.RoleId = (Guid)reader["RoleId"];
			return RoleOperationMapToMap;
		}
	}
}
