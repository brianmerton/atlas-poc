
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class SessionsMapper : IDataMapper<IDataReader, Domain.Internal.Sessions>
    {
        public Domain.Internal.Sessions Map(IDataReader reader)
        {
			var SessionsToMap = new Domain.Internal.Sessions();
			SessionsToMap.EndDate = Convert.ToDateTime(reader["EndDate"]);
			SessionsToMap.Id = (Guid)reader["Id"];
			SessionsToMap.SessionId = reader["SessionId"].ToString();
			SessionsToMap.StartDate = Convert.ToDateTime(reader["StartDate"]);
			SessionsToMap.UserId = (Guid)reader["UserId"];
			SessionsToMap.UserName = reader["UserName"].ToString();
			return SessionsToMap;
		}
	}
}
