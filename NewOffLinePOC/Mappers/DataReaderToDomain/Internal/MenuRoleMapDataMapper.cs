
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class MenuRoleMapMapper : IDataMapper<IDataReader, Domain.Internal.MenuRoleMap>
    {
        public Domain.Internal.MenuRoleMap Map(IDataReader reader)
        {
			var MenuRoleMapToMap = new Domain.Internal.MenuRoleMap();
			MenuRoleMapToMap.MenuId = (Guid)reader["MenuId"];
			MenuRoleMapToMap.RoleId = (Guid)reader["RoleId"];
			return MenuRoleMapToMap;
		}
	}
}
