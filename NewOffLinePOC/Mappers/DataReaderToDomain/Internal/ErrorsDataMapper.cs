
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class ErrorsMapper : IDataMapper<IDataReader, Domain.Internal.Errors>
    {
        public Domain.Internal.Errors Map(IDataReader reader)
        {
			var ErrorsToMap = new Domain.Internal.Errors();
			ErrorsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			ErrorsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			ErrorsToMap.Id = (Guid)reader["Id"];
			ErrorsToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			ErrorsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			ErrorsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			ErrorsToMap.Message = reader["Message"].ToString();
			ErrorsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			ErrorsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			ErrorsToMap.StackTrace = reader["StackTrace"].ToString();
			ErrorsToMap.Version = reader["Version"].ToString();
			return ErrorsToMap;
		}
	}
}
