
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class OperationsMapper : IDataMapper<IDataReader, Domain.Internal.Operations>
    {
        public Domain.Internal.Operations Map(IDataReader reader)
        {
			var OperationsToMap = new Domain.Internal.Operations();
			OperationsToMap.BusinessManagerName = reader["BusinessManagerName"].ToString();
			OperationsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			OperationsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			OperationsToMap.Id = (Guid)reader["Id"];
			OperationsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			OperationsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			OperationsToMap.MethodName = reader["MethodName"].ToString();
			OperationsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			OperationsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			OperationsToMap.Version = reader["Version"].ToString();
			return OperationsToMap;
		}
	}
}
