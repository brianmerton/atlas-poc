
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class TenantsMapper : IDataMapper<IDataReader, Domain.Internal.Tenants>
    {
        public Domain.Internal.Tenants Map(IDataReader reader)
        {
			var TenantsToMap = new Domain.Internal.Tenants();
			TenantsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			TenantsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			TenantsToMap.DataBaseName = reader["DataBaseName"].ToString();
			TenantsToMap.HostName = reader["HostName"].ToString();
			TenantsToMap.Id = (Guid)reader["Id"];
			TenantsToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			TenantsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			TenantsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			TenantsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			TenantsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			TenantsToMap.Name = reader["Name"].ToString();
			TenantsToMap.ServerName = reader["ServerName"].ToString();
			TenantsToMap.Version = reader["Version"].ToString();
			TenantsToMap.WebRole = reader["WebRole"].ToString();
			TenantsToMap.WorkerRole = reader["WorkerRole"].ToString();
			return TenantsToMap;
		}
	}
}
