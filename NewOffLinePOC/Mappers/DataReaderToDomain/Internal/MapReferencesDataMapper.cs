
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class MapReferencesMapper : IDataMapper<IDataReader, Domain.Internal.MapReferences>
    {
        public Domain.Internal.MapReferences Map(IDataReader reader)
        {
			var MapReferencesToMap = new Domain.Internal.MapReferences();
			MapReferencesToMap.ExternalReferenceKey = reader["ExternalReferenceKey"].ToString();
			MapReferencesToMap.Id = (Guid)reader["Id"];
			MapReferencesToMap.ObjectId = (Guid)reader["ObjectId"];
			MapReferencesToMap.Type = Convert.ToInt32(reader["Type"]);
			return MapReferencesToMap;
		}
	}
}
