
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class LanguagesMapper : IDataMapper<IDataReader, Domain.Internal.Languages>
    {
        public Domain.Internal.Languages Map(IDataReader reader)
        {
			var LanguagesToMap = new Domain.Internal.Languages();
			LanguagesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			LanguagesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			LanguagesToMap.Id = (Guid)reader["Id"];
			LanguagesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			LanguagesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			LanguagesToMap.ISOName = reader["ISOName"].ToString();
			LanguagesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			LanguagesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			LanguagesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			LanguagesToMap.Name = reader["Name"].ToString();
			LanguagesToMap.ResourceFileId = (Guid)reader["ResourceFileId"];
			LanguagesToMap.Version = reader["Version"].ToString();
			return LanguagesToMap;
		}
	}
}
