
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class MenusMapper : IDataMapper<IDataReader, Domain.Internal.Menus>
    {
        public Domain.Internal.Menus Map(IDataReader reader)
        {
			var MenusToMap = new Domain.Internal.Menus();
			MenusToMap.Action = reader["Action"].ToString();
			MenusToMap.CreatedBy = (Guid)reader["CreatedBy"];
			MenusToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			MenusToMap.CssClass = reader["CssClass"].ToString();
			MenusToMap.Description = reader["Description"].ToString();
			MenusToMap.Id = (Guid)reader["Id"];
			MenusToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			MenusToMap.Key = reader["Key"].ToString();
			MenusToMap.LCid = Convert.ToInt32(reader["LCid"]);
			MenusToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			MenusToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			MenusToMap.NavigateTo = reader["NavigateTo"].ToString();
			MenusToMap.OrderNumber = Convert.ToInt32(reader["OrderNumber"]);
			MenusToMap.ParentId = (Guid)reader["ParentId"];
			MenusToMap.PassParams = Convert.ToBoolean(reader["PassParams"]);
			MenusToMap.RequireCid = Convert.ToBoolean(reader["RequireCid"]);
			MenusToMap.Sector = reader["Sector"].ToString();
			MenusToMap.Title = reader["Title"].ToString();
			MenusToMap.Version = reader["Version"].ToString();
			return MenusToMap;
		}
	}
}
