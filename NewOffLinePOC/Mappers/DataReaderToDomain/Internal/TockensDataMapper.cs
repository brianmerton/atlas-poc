
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class TockensMapper : IDataMapper<IDataReader, Domain.Internal.Tockens>
    {
        public Domain.Internal.Tockens Map(IDataReader reader)
        {
			var TockensToMap = new Domain.Internal.Tockens();
			TockensToMap.Data = (byte[])reader["Data"];
			TockensToMap.Hash = reader["Hash"].ToString();
			TockensToMap.Id = (Guid)reader["Id"];
			return TockensToMap;
		}
	}
}
