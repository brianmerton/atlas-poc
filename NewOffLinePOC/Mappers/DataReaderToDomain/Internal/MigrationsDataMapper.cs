
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class MigrationsMapper : IDataMapper<IDataReader, Domain.Internal.Migrations>
    {
        public Domain.Internal.Migrations Map(IDataReader reader)
        {
			var MigrationsToMap = new Domain.Internal.Migrations();
			MigrationsToMap.Id = (Guid)reader["Id"];
			MigrationsToMap.TypeName = reader["TypeName"].ToString();
			return MigrationsToMap;
		}
	}
}
