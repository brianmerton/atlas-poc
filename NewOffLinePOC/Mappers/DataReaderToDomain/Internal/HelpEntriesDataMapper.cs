
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Internal
{
    public class HelpEntriesMapper : IDataMapper<IDataReader, Domain.Internal.HelpEntries>
    {
        public Domain.Internal.HelpEntries Map(IDataReader reader)
        {
			var HelpEntriesToMap = new Domain.Internal.HelpEntries();
			HelpEntriesToMap.Controller = reader["Controller"].ToString();
			HelpEntriesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			HelpEntriesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			HelpEntriesToMap.Description = reader["Description"].ToString();
			HelpEntriesToMap.Id = (Guid)reader["Id"];
			HelpEntriesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			HelpEntriesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			HelpEntriesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			HelpEntriesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			HelpEntriesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			HelpEntriesToMap.Title = reader["Title"].ToString();
			HelpEntriesToMap.Version = reader["Version"].ToString();
			return HelpEntriesToMap;
		}
	}
}
