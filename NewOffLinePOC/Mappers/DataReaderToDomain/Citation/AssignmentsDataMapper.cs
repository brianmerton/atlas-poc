
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class AssignmentsMapper : IDataMapper<IDataReader, Domain.Citation.Assignments>
    {
        public Domain.Citation.Assignments Map(IDataReader reader)
        {
			var AssignmentsToMap = new Domain.Citation.Assignments();
			AssignmentsToMap.CompanyId = (Guid)reader["CompanyId"];
			AssignmentsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			AssignmentsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			AssignmentsToMap.Id = (Guid)reader["Id"];
			AssignmentsToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			AssignmentsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			AssignmentsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			AssignmentsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			AssignmentsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			AssignmentsToMap.UserId = (Guid)reader["UserId"];
			AssignmentsToMap.Version = reader["Version"].ToString();
			return AssignmentsToMap;
		}
	}
}
