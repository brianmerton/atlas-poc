
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class QuestionsMapper : IDataMapper<IDataReader, Domain.Citation.Questions>
    {
        public Domain.Citation.Questions Map(IDataReader reader)
        {
			var QuestionsToMap = new Domain.Citation.Questions();
			QuestionsToMap.ActionType = Convert.ToInt32(reader["ActionType"]);
			QuestionsToMap.AssociatedContentId = (Guid)reader["AssociatedContentId"];
			QuestionsToMap.AttributeId = (Guid)reader["AttributeId"];
			QuestionsToMap.BlockId = (Guid)reader["BlockId"];
			QuestionsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			QuestionsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			QuestionsToMap.DefaultObservation = reader["DefaultObservation"].ToString();
			QuestionsToMap.Description = reader["Description"].ToString();
			QuestionsToMap.ExpectedAnswer = Convert.ToInt32(reader["ExpectedAnswer"]);
			QuestionsToMap.Id = (Guid)reader["Id"];
			QuestionsToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			QuestionsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			QuestionsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			QuestionsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			QuestionsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			QuestionsToMap.OrderIndex = Convert.ToInt32(reader["OrderIndex"]);
			QuestionsToMap.Priority = Convert.ToInt32(reader["Priority"]);
			QuestionsToMap.Target = reader["Target"].ToString();
			QuestionsToMap.Title = reader["Title"].ToString();
			QuestionsToMap.Type = Convert.ToInt32(reader["Type"]);
			QuestionsToMap.Version = reader["Version"].ToString();
			return QuestionsToMap;
		}
	}
}
