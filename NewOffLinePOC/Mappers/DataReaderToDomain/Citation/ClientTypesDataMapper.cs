
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class ClientTypesMapper : IDataMapper<IDataReader, Domain.Citation.ClientTypes>
    {
        public Domain.Citation.ClientTypes Map(IDataReader reader)
        {
			var ClientTypesToMap = new Domain.Citation.ClientTypes();
			ClientTypesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			ClientTypesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			ClientTypesToMap.Id = (Guid)reader["Id"];
			ClientTypesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			ClientTypesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			ClientTypesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			ClientTypesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			ClientTypesToMap.Name = reader["Name"].ToString();
			ClientTypesToMap.ShortCode = reader["ShortCode"].ToString();
			ClientTypesToMap.Version = reader["Version"].ToString();
			return ClientTypesToMap;
		}
	}
}
