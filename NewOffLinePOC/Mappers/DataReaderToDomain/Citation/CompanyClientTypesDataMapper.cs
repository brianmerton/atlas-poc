
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class CompanyClientTypesMapper : IDataMapper<IDataReader, Domain.Citation.CompanyClientTypes>
    {
        public Domain.Citation.CompanyClientTypes Map(IDataReader reader)
        {
			var CompanyClientTypesToMap = new Domain.Citation.CompanyClientTypes();
			CompanyClientTypesToMap.ClientTypeId = (Guid)reader["ClientTypeId"];
			CompanyClientTypesToMap.CompanyId = (Guid)reader["CompanyId"];
			return CompanyClientTypesToMap;
		}
	}
}
