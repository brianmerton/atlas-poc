
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class BlockCountriesMapper : IDataMapper<IDataReader, Domain.Citation.BlockCountries>
    {
        public Domain.Citation.BlockCountries Map(IDataReader reader)
        {
			var BlockCountriesToMap = new Domain.Citation.BlockCountries();
			BlockCountriesToMap.BlockId = (Guid)reader["BlockId"];
			BlockCountriesToMap.CountryId = (Guid)reader["CountryId"];
			return BlockCountriesToMap;
		}
	}
}
