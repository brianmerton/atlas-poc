
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class AdviceCardTypeMapMapper : IDataMapper<IDataReader, Domain.Citation.AdviceCardTypeMap>
    {
        public Domain.Citation.AdviceCardTypeMap Map(IDataReader reader)
        {
			var AdviceCardTypeMapToMap = new Domain.Citation.AdviceCardTypeMap();
			AdviceCardTypeMapToMap.AdviceCardId = (Guid)reader["AdviceCardId"];
			AdviceCardTypeMapToMap.AdviceTypeId = (Guid)reader["AdviceTypeId"];
			return AdviceCardTypeMapToMap;
		}
	}
}
