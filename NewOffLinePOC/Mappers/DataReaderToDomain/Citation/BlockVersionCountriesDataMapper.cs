
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class BlockVersionCountriesMapper : IDataMapper<IDataReader, Domain.Citation.BlockVersionCountries>
    {
        public Domain.Citation.BlockVersionCountries Map(IDataReader reader)
        {
			var BlockVersionCountriesToMap = new Domain.Citation.BlockVersionCountries();
			BlockVersionCountriesToMap.BlockVersionId = (Guid)reader["BlockVersionId"];
			BlockVersionCountriesToMap.CountryId = (Guid)reader["CountryId"];
			return BlockVersionCountriesToMap;
		}
	}
}
