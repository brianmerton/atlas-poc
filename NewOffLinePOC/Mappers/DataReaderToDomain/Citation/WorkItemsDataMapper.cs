
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class WorkItemsMapper : IDataMapper<IDataReader, Domain.Citation.WorkItems>
    {
        public Domain.Citation.WorkItems Map(IDataReader reader)
        {
			var WorkItemsToMap = new Domain.Citation.WorkItems();
			WorkItemsToMap.AttemptsCount = Convert.ToInt32(reader["AttemptsCount"]);
			WorkItemsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			WorkItemsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			WorkItemsToMap.Data = (byte[])reader["Data"];
			WorkItemsToMap.ErrorMessage = reader["ErrorMessage"].ToString();
			WorkItemsToMap.ErrorStack = reader["ErrorStack"].ToString();
			WorkItemsToMap.Id = (Guid)reader["Id"];
			WorkItemsToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			WorkItemsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			WorkItemsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			WorkItemsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			WorkItemsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			WorkItemsToMap.ProcessJobId = (Guid)reader["ProcessJobId"];
			WorkItemsToMap.ScheduledStart = Convert.ToDateTime(reader["ScheduledStart"]);
			WorkItemsToMap.State = Convert.ToInt32(reader["State"]);
			WorkItemsToMap.TenantId = (Guid)reader["TenantId"];
			WorkItemsToMap.Version = reader["Version"].ToString();
			return WorkItemsToMap;
		}
	}
}
