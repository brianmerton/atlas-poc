
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class BlockMapper : IDataMapper<IDataReader, Domain.Citation.Block>
    {
        public Domain.Citation.Block Map(IDataReader reader)
        {
			var BlockToMap = new Domain.Citation.Block();
			BlockToMap.Area = Convert.ToInt32(reader["Area"]);
			BlockToMap.Category = Convert.ToInt32(reader["Category"]);
			BlockToMap.Checked = Convert.ToBoolean(reader["Checked"]);
			BlockToMap.Comment = reader["Comment"].ToString();
			BlockToMap.Content = reader["Content"].ToString();
			BlockToMap.CountryId = (Guid)reader["CountryId"];
			BlockToMap.CreatedBy = (Guid)reader["CreatedBy"];
			BlockToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			BlockToMap.DeletionState = Convert.ToInt32(reader["DeletionState"]);
			BlockToMap.Description = reader["Description"].ToString();
			BlockToMap.Discriminator = reader["Discriminator"].ToString();
			BlockToMap.FixedPositionIndex = Convert.ToInt32(reader["FixedPositionIndex"]);
			BlockToMap.Id = (Guid)reader["Id"];
			BlockToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			BlockToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			BlockToMap.IsOriginal = Convert.ToBoolean(reader["IsOriginal"]);
			BlockToMap.IsSystem = Convert.ToBoolean(reader["IsSystem"]);
			BlockToMap.KbGroupId = (Guid)reader["KbGroupId"];
			BlockToMap.LastChange = Convert.ToInt32(reader["LastChange"]);
			BlockToMap.LCid = Convert.ToInt32(reader["LCid"]);
			BlockToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			BlockToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			BlockToMap.Multiplicity = Convert.ToInt32(reader["Multiplicity"]);
			BlockToMap.Observation = reader["Observation"].ToString();
			BlockToMap.Options_ActAsGroup = Convert.ToBoolean(reader["Options_ActAsGroup"]);
			BlockToMap.Options_IncludeIndexNumbers = Convert.ToBoolean(reader["Options_IncludeIndexNumbers"]);
			BlockToMap.Options_IsReadonly = Convert.ToBoolean(reader["Options_IsReadonly"]);
			BlockToMap.Options_LandscapeLayout = Convert.ToBoolean(reader["Options_LandscapeLayout"]);
			BlockToMap.Options_PageBreakAfter = Convert.ToBoolean(reader["Options_PageBreakAfter"]);
			BlockToMap.Options_PageBreakBefore = Convert.ToBoolean(reader["Options_PageBreakBefore"]);
			BlockToMap.OrderIndex = Convert.ToInt32(reader["OrderIndex"]);
			BlockToMap.OriginalBlockId = (Guid)reader["OriginalBlockId"];
			BlockToMap.ParentBlockId = (Guid)reader["ParentBlockId"];
			BlockToMap.Priority = Convert.ToInt32(reader["Priority"]);
			BlockToMap.Recommendation = reader["Recommendation"].ToString();
			BlockToMap.RootId = (Guid)reader["RootId"];
			BlockToMap.SectorId = (Guid)reader["SectorId"];
			BlockToMap.SourceBlockId = (Guid)reader["SourceBlockId"];
			BlockToMap.SourceBlockVersion = reader["SourceBlockVersion"].ToString();
			BlockToMap.SubCategoryId = (Guid)reader["SubCategoryId"];
			BlockToMap.Title = reader["Title"].ToString();
			BlockToMap.Type = Convert.ToInt32(reader["Type"]);
			BlockToMap.Version = reader["Version"].ToString();
			return BlockToMap;
		}
	}
}
