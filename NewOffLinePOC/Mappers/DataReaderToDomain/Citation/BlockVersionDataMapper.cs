
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class BlockVersionMapper : IDataMapper<IDataReader, Domain.Citation.BlockVersion>
    {
        public Domain.Citation.BlockVersion Map(IDataReader reader)
        {
			var BlockVersionToMap = new Domain.Citation.BlockVersion();
			BlockVersionToMap.Area = Convert.ToInt32(reader["Area"]);
			BlockVersionToMap.Category = Convert.ToInt32(reader["Category"]);
			BlockVersionToMap.Checked = Convert.ToBoolean(reader["Checked"]);
			BlockVersionToMap.Comment = reader["Comment"].ToString();
			BlockVersionToMap.Content = reader["Content"].ToString();
			BlockVersionToMap.CountryId = (Guid)reader["CountryId"];
			BlockVersionToMap.CreatedBy = (Guid)reader["CreatedBy"];
			BlockVersionToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			BlockVersionToMap.DeletionState = Convert.ToInt32(reader["DeletionState"]);
			BlockVersionToMap.Description = reader["Description"].ToString();
			BlockVersionToMap.Discriminator = reader["Discriminator"].ToString();
			BlockVersionToMap.FixedPositionIndex = Convert.ToInt32(reader["FixedPositionIndex"]);
			BlockVersionToMap.Id = (Guid)reader["Id"];
			BlockVersionToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			BlockVersionToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			BlockVersionToMap.IsOriginal = Convert.ToBoolean(reader["IsOriginal"]);
			BlockVersionToMap.IsSystem = Convert.ToBoolean(reader["IsSystem"]);
			BlockVersionToMap.KbGroupId = (Guid)reader["KbGroupId"];
			BlockVersionToMap.LastChange = Convert.ToInt32(reader["LastChange"]);
			BlockVersionToMap.LCid = Convert.ToInt32(reader["LCid"]);
			BlockVersionToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			BlockVersionToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			BlockVersionToMap.Multiplicity = Convert.ToInt32(reader["Multiplicity"]);
			BlockVersionToMap.Observation = reader["Observation"].ToString();
			BlockVersionToMap.Options_ActAsGroup = Convert.ToBoolean(reader["Options_ActAsGroup"]);
			BlockVersionToMap.Options_IncludeIndexNumbers = Convert.ToBoolean(reader["Options_IncludeIndexNumbers"]);
			BlockVersionToMap.Options_IsReadonly = Convert.ToBoolean(reader["Options_IsReadonly"]);
			BlockVersionToMap.Options_LandscapeLayout = Convert.ToBoolean(reader["Options_LandscapeLayout"]);
			BlockVersionToMap.Options_PageBreakAfter = Convert.ToBoolean(reader["Options_PageBreakAfter"]);
			BlockVersionToMap.Options_PageBreakBefore = Convert.ToBoolean(reader["Options_PageBreakBefore"]);
			BlockVersionToMap.OrderIndex = Convert.ToInt32(reader["OrderIndex"]);
			BlockVersionToMap.OriginalBlockId = (Guid)reader["OriginalBlockId"];
			BlockVersionToMap.ParentBlockId = (Guid)reader["ParentBlockId"];
			BlockVersionToMap.Priority = Convert.ToInt32(reader["Priority"]);
			BlockVersionToMap.PrototypeId = (Guid)reader["PrototypeId"];
			BlockVersionToMap.Recommendation = reader["Recommendation"].ToString();
			BlockVersionToMap.SectorId = (Guid)reader["SectorId"];
			BlockVersionToMap.SourceBlockId = (Guid)reader["SourceBlockId"];
			BlockVersionToMap.SourceBlockVersion = reader["SourceBlockVersion"].ToString();
			BlockVersionToMap.SubCategoryId = (Guid)reader["SubCategoryId"];
			BlockVersionToMap.Title = reader["Title"].ToString();
			BlockVersionToMap.Type = Convert.ToInt32(reader["Type"]);
			BlockVersionToMap.Version = reader["Version"].ToString();
			return BlockVersionToMap;
		}
	}
}
