
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class LogsMapper : IDataMapper<IDataReader, Domain.Citation.Logs>
    {
        public Domain.Citation.Logs Map(IDataReader reader)
        {
			var LogsToMap = new Domain.Citation.Logs();
			LogsToMap.Category = Convert.ToInt32(reader["Category"]);
			LogsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			LogsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			LogsToMap.Id = (Guid)reader["Id"];
			LogsToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			LogsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			LogsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			LogsToMap.Message = reader["Message"].ToString();
			LogsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			LogsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			LogsToMap.Type = Convert.ToInt32(reader["Type"]);
			LogsToMap.Version = reader["Version"].ToString();
			return LogsToMap;
		}
	}
}
