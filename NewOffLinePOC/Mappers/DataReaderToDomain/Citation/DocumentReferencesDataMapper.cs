
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class DocumentReferencesMapper : IDataMapper<IDataReader, Domain.Citation.DocumentReferences>
    {
        public Domain.Citation.DocumentReferences Map(IDataReader reader)
        {
			var DocumentReferencesToMap = new Domain.Citation.DocumentReferences();
			DocumentReferencesToMap.Category = Convert.ToInt32(reader["Category"]);
			DocumentReferencesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			DocumentReferencesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			DocumentReferencesToMap.Id = (Guid)reader["Id"];
			DocumentReferencesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			DocumentReferencesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			DocumentReferencesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			DocumentReferencesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			DocumentReferencesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			DocumentReferencesToMap.RegardingObjectId = (Guid)reader["RegardingObjectId"];
			DocumentReferencesToMap.RegardingObjectTypeCode = Convert.ToInt32(reader["RegardingObjectTypeCode"]);
			DocumentReferencesToMap.TemplateId = (Guid)reader["TemplateId"];
			DocumentReferencesToMap.TenantId = (Guid)reader["TenantId"];
			DocumentReferencesToMap.Title = reader["Title"].ToString();
			DocumentReferencesToMap.Usage = Convert.ToInt32(reader["Usage"]);
			DocumentReferencesToMap.Version = reader["Version"].ToString();
			return DocumentReferencesToMap;
		}
	}
}
