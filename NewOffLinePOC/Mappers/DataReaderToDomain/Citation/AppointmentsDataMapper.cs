
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class AppointmentsMapper : IDataMapper<IDataReader, Domain.Citation.Appointments>
    {
        public Domain.Citation.Appointments Map(IDataReader reader)
        {
			var AppointmentsToMap = new Domain.Citation.Appointments();
			AppointmentsToMap.CompanyId = (Guid)reader["CompanyId"];
			AppointmentsToMap.ConsultantId = (Guid)reader["ConsultantId"];
			AppointmentsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			AppointmentsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			AppointmentsToMap.EndDateTime = Convert.ToDateTime(reader["EndDateTime"]);
			AppointmentsToMap.exchCalItemID = reader["exchCalItemID"].ToString();
			AppointmentsToMap.Id = (Guid)reader["Id"];
			AppointmentsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			AppointmentsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			AppointmentsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			AppointmentsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			AppointmentsToMap.Notes = reader["Notes"].ToString();
			AppointmentsToMap.PrototypeId = (Guid)reader["PrototypeId"];
			AppointmentsToMap.Reason = reader["Reason"].ToString();
			AppointmentsToMap.SiteId = (Guid)reader["SiteId"];
			AppointmentsToMap.SLATypeId = (Guid)reader["SLATypeId"];
			AppointmentsToMap.StartDateTime = Convert.ToDateTime(reader["StartDateTime"]);
			AppointmentsToMap.Status = Convert.ToInt32(reader["Status"]);
			AppointmentsToMap.TaskId = (Guid)reader["TaskId"];
			AppointmentsToMap.TimeSpent = decimal
			AppointmentsToMap.Version = reader["Version"].ToString();
			return AppointmentsToMap;
		}
	}
}
