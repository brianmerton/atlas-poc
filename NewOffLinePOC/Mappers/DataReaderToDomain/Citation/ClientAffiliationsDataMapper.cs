
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class ClientAffiliationsMapper : IDataMapper<IDataReader, Domain.Citation.ClientAffiliations>
    {
        public Domain.Citation.ClientAffiliations Map(IDataReader reader)
        {
			var ClientAffiliationsToMap = new Domain.Citation.ClientAffiliations();
			ClientAffiliationsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			ClientAffiliationsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			ClientAffiliationsToMap.Id = (Guid)reader["Id"];
			ClientAffiliationsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			ClientAffiliationsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			ClientAffiliationsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			ClientAffiliationsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			ClientAffiliationsToMap.Name = reader["Name"].ToString();
			ClientAffiliationsToMap.PictureId = (Guid)reader["PictureId"];
			ClientAffiliationsToMap.SequenceId = Convert.ToInt32(reader["SequenceId"]);
			ClientAffiliationsToMap.Version = reader["Version"].ToString();
			return ClientAffiliationsToMap;
		}
	}
}
