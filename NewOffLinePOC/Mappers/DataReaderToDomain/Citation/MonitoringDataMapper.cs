
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class MonitoringMapper : IDataMapper<IDataReader, Domain.Citation.Monitoring>
    {
        public Domain.Citation.Monitoring Map(IDataReader reader)
        {
			var MonitoringToMap = new Domain.Citation.Monitoring();
			MonitoringToMap.CreatedBy = (Guid)reader["CreatedBy"];
			MonitoringToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			MonitoringToMap.Data = (byte[])reader["Data"];
			MonitoringToMap.Id = (Guid)reader["Id"];
			MonitoringToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			MonitoringToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			MonitoringToMap.IsFailed = Convert.ToBoolean(reader["IsFailed"]);
			MonitoringToMap.LCid = Convert.ToInt32(reader["LCid"]);
			MonitoringToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			MonitoringToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			MonitoringToMap.OperationDate = Convert.ToDateTime(reader["OperationDate"]);
			MonitoringToMap.OperationId = (Guid)reader["OperationId"];
			MonitoringToMap.UserId = (Guid)reader["UserId"];
			MonitoringToMap.Version = reader["Version"].ToString();
			return MonitoringToMap;
		}
	}
}
