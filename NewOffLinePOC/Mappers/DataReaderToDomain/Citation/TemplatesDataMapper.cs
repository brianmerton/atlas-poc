
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class TemplatesMapper : IDataMapper<IDataReader, Domain.Citation.Templates>
    {
        public Domain.Citation.Templates Map(IDataReader reader)
        {
			var TemplatesToMap = new Domain.Citation.Templates();
			TemplatesToMap.Area = Convert.ToInt32(reader["Area"]);
			TemplatesToMap.Category = Convert.ToInt32(reader["Category"]);
			TemplatesToMap.Comment = reader["Comment"].ToString();
			TemplatesToMap.CountryId = (Guid)reader["CountryId"];
			TemplatesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			TemplatesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			TemplatesToMap.DeletionState = Convert.ToInt32(reader["DeletionState"]);
			TemplatesToMap.Description = reader["Description"].ToString();
			TemplatesToMap.Id = (Guid)reader["Id"];
			TemplatesToMap.IsActive = Convert.ToBoolean(reader["IsActive"]);
			TemplatesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			TemplatesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			TemplatesToMap.LastChange = Convert.ToInt32(reader["LastChange"]);
			TemplatesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			TemplatesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			TemplatesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			TemplatesToMap.OriginalTemplateId = (Guid)reader["OriginalTemplateId"];
			TemplatesToMap.SectorId = (Guid)reader["SectorId"];
			TemplatesToMap.TargetEntityOTC = reader["TargetEntityOTC"].ToString();
			TemplatesToMap.Title = reader["Title"].ToString();
			TemplatesToMap.Version = reader["Version"].ToString();
			return TemplatesToMap;
		}
	}
}
