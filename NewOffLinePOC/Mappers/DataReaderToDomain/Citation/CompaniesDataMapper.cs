
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class CompaniesMapper : IDataMapper<IDataReader, Domain.Citation.Companies>
    {
        public Domain.Citation.Companies Map(IDataReader reader)
        {
			var CompaniesToMap = new Domain.Citation.Companies();
			CompaniesToMap.ClientAffiliationId = (Guid)reader["ClientAffiliationId"];
			CompaniesToMap.ClientArea = Convert.ToInt32(reader["ClientArea"]);
			CompaniesToMap.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]);
			CompaniesToMap.ContractStartDate = Convert.ToDateTime(reader["ContractStartDate"]);
			CompaniesToMap.CountryId = (Guid)reader["CountryId"];
			CompaniesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			CompaniesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			CompaniesToMap.Description = reader["Description"].ToString();
			CompaniesToMap.ExtraEvents = Convert.ToBoolean(reader["ExtraEvents"]);
			CompaniesToMap.FullName = reader["FullName"].ToString();
			CompaniesToMap.Id = (Guid)reader["Id"];
			CompaniesToMap.IsActive = Convert.ToBoolean(reader["IsActive"]);
			CompaniesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			CompaniesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			CompaniesToMap.jitbitCompanyId = Convert.ToInt32(reader["jitbitCompanyId"]);
			CompaniesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			CompaniesToMap.Migrate = Convert.ToBoolean(reader["Migrate"]);
			CompaniesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			CompaniesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			CompaniesToMap.ParentCompanyId = (Guid)reader["ParentCompanyId"];
			CompaniesToMap.PostCode = reader["PostCode"].ToString();
			CompaniesToMap.Prefix = reader["Prefix"].ToString();
			CompaniesToMap.SalesforceAccountID = reader["SalesforceAccountID"].ToString();
			CompaniesToMap.SectorId = (Guid)reader["SectorId"];
			CompaniesToMap.ShortName = reader["ShortName"].ToString();
			CompaniesToMap.SiteUrl = reader["SiteUrl"].ToString();
			CompaniesToMap.SystemEvents = Convert.ToBoolean(reader["SystemEvents"]);
			CompaniesToMap.TenantName = reader["TenantName"].ToString();
			CompaniesToMap.Version = reader["Version"].ToString();
			return CompaniesToMap;
		}
	}
}
