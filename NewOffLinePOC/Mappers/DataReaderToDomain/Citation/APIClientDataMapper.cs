
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class APIClientMapper : IDataMapper<IDataReader, Domain.Citation.APIClient>
    {
        public Domain.Citation.APIClient Map(IDataReader reader)
        {
			var APIClientToMap = new Domain.Citation.APIClient();
			APIClientToMap.ClientId = reader["ClientId"].ToString();
			APIClientToMap.CreatedBy = (Guid)reader["CreatedBy"];
			APIClientToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			APIClientToMap.Id = (Guid)reader["Id"];
			APIClientToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			APIClientToMap.LCid = Convert.ToInt32(reader["LCid"]);
			APIClientToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			APIClientToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			APIClientToMap.Name = reader["Name"].ToString();
			APIClientToMap.OriginInfo = reader["OriginInfo"].ToString();
			APIClientToMap.SecretKey = reader["SecretKey"].ToString();
			APIClientToMap.TenantName = reader["TenantName"].ToString();
			APIClientToMap.UsersIdentityId = (Guid)reader["UsersIdentityId"];
			APIClientToMap.Version = reader["Version"].ToString();
			return APIClientToMap;
		}
	}
}
