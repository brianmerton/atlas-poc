
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class UserLocationResponsibilityMapper : IDataMapper<IDataReader, Domain.Citation.UserLocationResponsibility>
    {
        public Domain.Citation.UserLocationResponsibility Map(IDataReader reader)
        {
			var UserLocationResponsibilityToMap = new Domain.Citation.UserLocationResponsibility();
			UserLocationResponsibilityToMap.CreatedBy = (Guid)reader["CreatedBy"];
			UserLocationResponsibilityToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			UserLocationResponsibilityToMap.Id = (Guid)reader["Id"];
			UserLocationResponsibilityToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			UserLocationResponsibilityToMap.LCid = Convert.ToInt32(reader["LCid"]);
			UserLocationResponsibilityToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			UserLocationResponsibilityToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			UserLocationResponsibilityToMap.PostCode = reader["PostCode"].ToString();
			UserLocationResponsibilityToMap.UserId = (Guid)reader["UserId"];
			UserLocationResponsibilityToMap.Version = reader["Version"].ToString();
			return UserLocationResponsibilityToMap;
		}
	}
}
