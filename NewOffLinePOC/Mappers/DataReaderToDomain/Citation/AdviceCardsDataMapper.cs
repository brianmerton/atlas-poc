
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class AdviceCardsMapper : IDataMapper<IDataReader, Domain.Citation.AdviceCards>
    {
        public Domain.Citation.AdviceCards Map(IDataReader reader)
        {
			var AdviceCardsToMap = new Domain.Citation.AdviceCards();
			AdviceCardsToMap.CardNumber = Convert.ToInt32(reader["CardNumber"]);
			AdviceCardsToMap.CompanyId = (Guid)reader["CompanyId"];
			AdviceCardsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			AdviceCardsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			AdviceCardsToMap.Id = (Guid)reader["Id"];
			AdviceCardsToMap.IsActive = Convert.ToBoolean(reader["IsActive"]);
			AdviceCardsToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			AdviceCardsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			AdviceCardsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			AdviceCardsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			AdviceCardsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			AdviceCardsToMap.Version = reader["Version"].ToString();
			return AdviceCardsToMap;
		}
	}
}
