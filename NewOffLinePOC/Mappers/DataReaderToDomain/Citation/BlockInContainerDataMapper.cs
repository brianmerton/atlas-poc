
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class BlockInContainerMapper : IDataMapper<IDataReader, Domain.Citation.BlockInContainer>
    {
        public Domain.Citation.BlockInContainer Map(IDataReader reader)
        {
			var BlockInContainerToMap = new Domain.Citation.BlockInContainer();
			BlockInContainerToMap.BlockId = (Guid)reader["BlockId"];
			BlockInContainerToMap.ContainerId = (Guid)reader["ContainerId"];
			BlockInContainerToMap.OrderIndex = Convert.ToInt32(reader["OrderIndex"]);
			return BlockInContainerToMap;
		}
	}
}
