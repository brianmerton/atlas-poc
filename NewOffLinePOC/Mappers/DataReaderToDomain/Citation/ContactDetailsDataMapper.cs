
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class ContactDetailsMapper : IDataMapper<IDataReader, Domain.Citation.ContactDetails>
    {
        public Domain.Citation.ContactDetails Map(IDataReader reader)
        {
			var ContactDetailsToMap = new Domain.Citation.ContactDetails();
			ContactDetailsToMap.ClientTypeId = (Guid)reader["ClientTypeId"];
			ContactDetailsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			ContactDetailsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			ContactDetailsToMap.Email = reader["Email"].ToString();
			ContactDetailsToMap.FirstName = reader["FirstName"].ToString();
			ContactDetailsToMap.Id = (Guid)reader["Id"];
			ContactDetailsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			ContactDetailsToMap.LastName = reader["LastName"].ToString();
			ContactDetailsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			ContactDetailsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			ContactDetailsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			ContactDetailsToMap.Notes = reader["Notes"].ToString();
			ContactDetailsToMap.Phone = reader["Phone"].ToString();
			ContactDetailsToMap.UserId = (Guid)reader["UserId"];
			ContactDetailsToMap.Version = reader["Version"].ToString();
			return ContactDetailsToMap;
		}
	}
}
