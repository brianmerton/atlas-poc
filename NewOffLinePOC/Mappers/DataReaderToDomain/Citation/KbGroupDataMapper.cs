
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class KbGroupMapper : IDataMapper<IDataReader, Domain.Citation.KbGroup>
    {
        public Domain.Citation.KbGroup Map(IDataReader reader)
        {
			var KbGroupToMap = new Domain.Citation.KbGroup();
			KbGroupToMap.CreatedBy = (Guid)reader["CreatedBy"];
			KbGroupToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			KbGroupToMap.Id = (Guid)reader["Id"];
			KbGroupToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			KbGroupToMap.LCid = Convert.ToInt32(reader["LCid"]);
			KbGroupToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			KbGroupToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			KbGroupToMap.Name = reader["Name"].ToString();
			KbGroupToMap.Version = reader["Version"].ToString();
			return KbGroupToMap;
		}
	}
}
