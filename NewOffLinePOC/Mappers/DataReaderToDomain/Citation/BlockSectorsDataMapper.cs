
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class BlockSectorsMapper : IDataMapper<IDataReader, Domain.Citation.BlockSectors>
    {
        public Domain.Citation.BlockSectors Map(IDataReader reader)
        {
			var BlockSectorsToMap = new Domain.Citation.BlockSectors();
			BlockSectorsToMap.BlockId = (Guid)reader["BlockId"];
			BlockSectorsToMap.SectorId = (Guid)reader["SectorId"];
			return BlockSectorsToMap;
		}
	}
}
