
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class BlockInDocumentMapper : IDataMapper<IDataReader, Domain.Citation.BlockInDocument>
    {
        public Domain.Citation.BlockInDocument Map(IDataReader reader)
        {
			var BlockInDocumentToMap = new Domain.Citation.BlockInDocument();
			BlockInDocumentToMap.BlockId = (Guid)reader["BlockId"];
			BlockInDocumentToMap.DocumentId = (Guid)reader["DocumentId"];
			BlockInDocumentToMap.Id = (Guid)reader["Id"];
			return BlockInDocumentToMap;
		}
	}
}
