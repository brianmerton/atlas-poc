
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class SiteReferenceMapper : IDataMapper<IDataReader, Domain.Citation.SiteReference>
    {
        public Domain.Citation.SiteReference Map(IDataReader reader)
        {
			var SiteReferenceToMap = new Domain.Citation.SiteReference();
			SiteReferenceToMap.CompanyId = (Guid)reader["CompanyId"];
			SiteReferenceToMap.CreatedBy = (Guid)reader["CreatedBy"];
			SiteReferenceToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			SiteReferenceToMap.Id = (Guid)reader["Id"];
			SiteReferenceToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			SiteReferenceToMap.IsHeadOffice = Convert.ToBoolean(reader["IsHeadOffice"]);
			SiteReferenceToMap.LCid = Convert.ToInt32(reader["LCid"]);
			SiteReferenceToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			SiteReferenceToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			SiteReferenceToMap.Name = reader["Name"].ToString();
			SiteReferenceToMap.PostCode = reader["PostCode"].ToString();
			SiteReferenceToMap.SystemEvents = Convert.ToBoolean(reader["SystemEvents"]);
			SiteReferenceToMap.Version = reader["Version"].ToString();
			return SiteReferenceToMap;
		}
	}
}
