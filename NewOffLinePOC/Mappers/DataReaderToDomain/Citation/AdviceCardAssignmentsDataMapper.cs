
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class AdviceCardAssignmentsMapper : IDataMapper<IDataReader, Domain.Citation.AdviceCardAssignments>
    {
        public Domain.Citation.AdviceCardAssignments Map(IDataReader reader)
        {
			var AdviceCardAssignmentsToMap = new Domain.Citation.AdviceCardAssignments();
			AdviceCardAssignmentsToMap.AdviceCardId = (Guid)reader["AdviceCardId"];
			AdviceCardAssignmentsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			AdviceCardAssignmentsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			AdviceCardAssignmentsToMap.Id = (Guid)reader["Id"];
			AdviceCardAssignmentsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			AdviceCardAssignmentsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			AdviceCardAssignmentsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			AdviceCardAssignmentsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			AdviceCardAssignmentsToMap.UserId = (Guid)reader["UserId"];
			AdviceCardAssignmentsToMap.Version = reader["Version"].ToString();
			return AdviceCardAssignmentsToMap;
		}
	}
}
