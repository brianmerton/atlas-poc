
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class TemplateVersionMapper : IDataMapper<IDataReader, Domain.Citation.TemplateVersion>
    {
        public Domain.Citation.TemplateVersion Map(IDataReader reader)
        {
			var TemplateVersionToMap = new Domain.Citation.TemplateVersion();
			TemplateVersionToMap.Area = Convert.ToInt32(reader["Area"]);
			TemplateVersionToMap.Category = Convert.ToInt32(reader["Category"]);
			TemplateVersionToMap.Comment = reader["Comment"].ToString();
			TemplateVersionToMap.CountryId = (Guid)reader["CountryId"];
			TemplateVersionToMap.CreatedBy = (Guid)reader["CreatedBy"];
			TemplateVersionToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			TemplateVersionToMap.DeletionState = Convert.ToInt32(reader["DeletionState"]);
			TemplateVersionToMap.Description = reader["Description"].ToString();
			TemplateVersionToMap.Id = (Guid)reader["Id"];
			TemplateVersionToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			TemplateVersionToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			TemplateVersionToMap.LastChange = Convert.ToInt32(reader["LastChange"]);
			TemplateVersionToMap.LCid = Convert.ToInt32(reader["LCid"]);
			TemplateVersionToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			TemplateVersionToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			TemplateVersionToMap.OriginalTemplateId = (Guid)reader["OriginalTemplateId"];
			TemplateVersionToMap.PrototypeId = (Guid)reader["PrototypeId"];
			TemplateVersionToMap.SectorId = (Guid)reader["SectorId"];
			TemplateVersionToMap.TargetEntityOTC = reader["TargetEntityOTC"].ToString();
			TemplateVersionToMap.Title = reader["Title"].ToString();
			TemplateVersionToMap.Version = reader["Version"].ToString();
			return TemplateVersionToMap;
		}
	}
}
