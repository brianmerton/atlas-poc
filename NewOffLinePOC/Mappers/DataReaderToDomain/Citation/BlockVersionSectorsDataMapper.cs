
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class BlockVersionSectorsMapper : IDataMapper<IDataReader, Domain.Citation.BlockVersionSectors>
    {
        public Domain.Citation.BlockVersionSectors Map(IDataReader reader)
        {
			var BlockVersionSectorsToMap = new Domain.Citation.BlockVersionSectors();
			BlockVersionSectorsToMap.BlockVersionId = (Guid)reader["BlockVersionId"];
			BlockVersionSectorsToMap.SectorId = (Guid)reader["SectorId"];
			return BlockVersionSectorsToMap;
		}
	}
}
