
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Citation
{
    public class EventReasonsMapper : IDataMapper<IDataReader, Domain.Citation.EventReasons>
    {
        public Domain.Citation.EventReasons Map(IDataReader reader)
        {
			var EventReasonsToMap = new Domain.Citation.EventReasons();
			EventReasonsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			EventReasonsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			EventReasonsToMap.Id = (Guid)reader["Id"];
			EventReasonsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			EventReasonsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			EventReasonsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			EventReasonsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			EventReasonsToMap.Reason = reader["Reason"].ToString();
			EventReasonsToMap.ShortCode = reader["ShortCode"].ToString();
			EventReasonsToMap.Version = reader["Version"].ToString();
			return EventReasonsToMap;
		}
	}
}
