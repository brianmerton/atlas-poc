
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAtlasRoleCategoriesMapper : IDataMapper<IDataReader, Domain.Incidents.hdAtlasRoleCategories>
    {
        public Domain.Incidents.hdAtlasRoleCategories Map(IDataReader reader)
        {
			var hdAtlasRoleCategoriesToMap = new Domain.Incidents.hdAtlasRoleCategories();
			hdAtlasRoleCategoriesToMap.AreaType = Convert.ToInt32(reader["AreaType"]);
			hdAtlasRoleCategoriesToMap.AtlasRoleId = (Guid)reader["AtlasRoleId"];
			hdAtlasRoleCategoriesToMap.CategoryId = Convert.ToInt32(reader["CategoryId"]);
			return hdAtlasRoleCategoriesToMap;
		}
	}
}
