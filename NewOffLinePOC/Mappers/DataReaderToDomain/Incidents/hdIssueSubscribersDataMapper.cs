
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdIssueSubscribersMapper : IDataMapper<IDataReader, Domain.Incidents.hdIssueSubscribers>
    {
        public Domain.Incidents.hdIssueSubscribers Map(IDataReader reader)
        {
			var hdIssueSubscribersToMap = new Domain.Incidents.hdIssueSubscribers();
			hdIssueSubscribersToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			hdIssueSubscribersToMap.UserID = Convert.ToInt32(reader["UserID"]);
			return hdIssueSubscribersToMap;
		}
	}
}
