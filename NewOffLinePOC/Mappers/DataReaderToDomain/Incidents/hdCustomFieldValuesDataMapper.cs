
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCustomFieldValuesMapper : IDataMapper<IDataReader, Domain.Incidents.hdCustomFieldValues>
    {
        public Domain.Incidents.hdCustomFieldValues Map(IDataReader reader)
        {
			var hdCustomFieldValuesToMap = new Domain.Incidents.hdCustomFieldValues();
			hdCustomFieldValuesToMap.AtlasRefId = (Guid)reader["AtlasRefId"];
			hdCustomFieldValuesToMap.FieldID = Convert.ToInt32(reader["FieldID"]);
			hdCustomFieldValuesToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			hdCustomFieldValuesToMap.Value = sql_variant
			return hdCustomFieldValuesToMap;
		}
	}
}
