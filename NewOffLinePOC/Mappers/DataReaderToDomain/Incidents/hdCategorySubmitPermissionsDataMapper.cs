
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCategorySubmitPermissionsMapper : IDataMapper<IDataReader, Domain.Incidents.hdCategorySubmitPermissions>
    {
        public Domain.Incidents.hdCategorySubmitPermissions Map(IDataReader reader)
        {
			var hdCategorySubmitPermissionsToMap = new Domain.Incidents.hdCategorySubmitPermissions();
			hdCategorySubmitPermissionsToMap.CategoryID = Convert.ToInt32(reader["CategoryID"]);
			hdCategorySubmitPermissionsToMap.UserID = Convert.ToInt32(reader["UserID"]);
			return hdCategorySubmitPermissionsToMap;
		}
	}
}
