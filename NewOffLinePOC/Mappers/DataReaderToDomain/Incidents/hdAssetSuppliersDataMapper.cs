
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAssetSuppliersMapper : IDataMapper<IDataReader, Domain.Incidents.hdAssetSuppliers>
    {
        public Domain.Incidents.hdAssetSuppliers Map(IDataReader reader)
        {
			var hdAssetSuppliersToMap = new Domain.Incidents.hdAssetSuppliers();
			hdAssetSuppliersToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdAssetSuppliersToMap.Name = reader["Name"].ToString();
			hdAssetSuppliersToMap.SupplierID = Convert.ToInt32(reader["SupplierID"]);
			return hdAssetSuppliersToMap;
		}
	}
}
