
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdIssuesMapper : IDataMapper<IDataReader, Domain.Incidents.hdIssues>
    {
        public Domain.Incidents.hdIssues Map(IDataReader reader)
        {
			var hdIssuesToMap = new Domain.Incidents.hdIssues();
			hdIssuesToMap.AssignedToUserID = Convert.ToInt32(reader["AssignedToUserID"]);
			hdIssuesToMap.Body = reader["Body"].ToString();
			hdIssuesToMap.CategoryID = Convert.ToInt32(reader["CategoryID"]);
			hdIssuesToMap.ChannelId = Convert.ToInt32(reader["ChannelId"]);
			hdIssuesToMap.DueDate = Convert.ToDateTime(reader["DueDate"]);
			hdIssuesToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdIssuesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			hdIssuesToMap.IssueDate = Convert.ToDateTime(reader["IssueDate"]);
			hdIssuesToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			hdIssuesToMap.KBForTechsOnly = Convert.ToBoolean(reader["KBForTechsOnly"]);
			hdIssuesToMap.LastUpdated = Convert.ToDateTime(reader["LastUpdated"]);
			hdIssuesToMap.Priority = Convert.ToInt32(reader["Priority"]);
			hdIssuesToMap.PublishToKB = Convert.ToBoolean(reader["PublishToKB"]);
			hdIssuesToMap.ResolvedDate = Convert.ToDateTime(reader["ResolvedDate"]);
			hdIssuesToMap.StartDate = Convert.ToDateTime(reader["StartDate"]);
			hdIssuesToMap.StatusID = Convert.ToInt32(reader["StatusID"]);
			hdIssuesToMap.Subject = reader["Subject"].ToString();
			hdIssuesToMap.TimeSpentInSeconds = Convert.ToInt32(reader["TimeSpentInSeconds"]);
			hdIssuesToMap.UpdatedByPerformer = Convert.ToBoolean(reader["UpdatedByPerformer"]);
			hdIssuesToMap.UpdatedByUser = Convert.ToBoolean(reader["UpdatedByUser"]);
			hdIssuesToMap.UpdatedForTechView = Convert.ToBoolean(reader["UpdatedForTechView"]);
			hdIssuesToMap.UserID = Convert.ToInt32(reader["UserID"]);
			return hdIssuesToMap;
		}
	}
}
