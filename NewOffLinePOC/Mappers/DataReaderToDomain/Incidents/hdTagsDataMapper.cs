
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdTagsMapper : IDataMapper<IDataReader, Domain.Incidents.hdTags>
    {
        public Domain.Incidents.hdTags Map(IDataReader reader)
        {
			var hdTagsToMap = new Domain.Incidents.hdTags();
			hdTagsToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdTagsToMap.Name = reader["Name"].ToString();
			hdTagsToMap.TagID = Convert.ToInt32(reader["TagID"]);
			return hdTagsToMap;
		}
	}
}
