
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdSectionsMapper : IDataMapper<IDataReader, Domain.Incidents.hdSections>
    {
        public Domain.Incidents.hdSections Map(IDataReader reader)
        {
			var hdSectionsToMap = new Domain.Incidents.hdSections();
			hdSectionsToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdSectionsToMap.Name = reader["Name"].ToString();
			hdSectionsToMap.OrderByNumber = Convert.ToInt32(reader["OrderByNumber"]);
			hdSectionsToMap.SectionID = Convert.ToInt32(reader["SectionID"]);
			return hdSectionsToMap;
		}
	}
}
