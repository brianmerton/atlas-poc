
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAPICustomFieldSettingsMapper : IDataMapper<IDataReader, Domain.Incidents.hdAPICustomFieldSettings>
    {
        public Domain.Incidents.hdAPICustomFieldSettings Map(IDataReader reader)
        {
			var hdAPICustomFieldSettingsToMap = new Domain.Incidents.hdAPICustomFieldSettings();
			hdAPICustomFieldSettingsToMap.APICustomFieldSettingsID = Convert.ToInt32(reader["APICustomFieldSettingsID"]);
			hdAPICustomFieldSettingsToMap.APILink = reader["APILink"].ToString();
			hdAPICustomFieldSettingsToMap.ClientId = reader["ClientId"].ToString();
			hdAPICustomFieldSettingsToMap.FieldId = Convert.ToInt32(reader["FieldId"]);
			hdAPICustomFieldSettingsToMap.SecurityKey = reader["SecurityKey"].ToString();
			hdAPICustomFieldSettingsToMap.TextField = reader["TextField"].ToString();
			hdAPICustomFieldSettingsToMap.UrlLink = reader["UrlLink"].ToString();
			hdAPICustomFieldSettingsToMap.ValueField = reader["ValueField"].ToString();
			return hdAPICustomFieldSettingsToMap;
		}
	}
}
