
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAssetUsersMapper : IDataMapper<IDataReader, Domain.Incidents.hdAssetUsers>
    {
        public Domain.Incidents.hdAssetUsers Map(IDataReader reader)
        {
			var hdAssetUsersToMap = new Domain.Incidents.hdAssetUsers();
			hdAssetUsersToMap.ItemID = Convert.ToInt32(reader["ItemID"]);
			hdAssetUsersToMap.UserID = Convert.ToInt32(reader["UserID"]);
			return hdAssetUsersToMap;
		}
	}
}
