
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAssetTypesMapper : IDataMapper<IDataReader, Domain.Incidents.hdAssetTypes>
    {
        public Domain.Incidents.hdAssetTypes Map(IDataReader reader)
        {
			var hdAssetTypesToMap = new Domain.Incidents.hdAssetTypes();
			hdAssetTypesToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdAssetTypesToMap.Name = reader["Name"].ToString();
			hdAssetTypesToMap.TypeID = Convert.ToInt32(reader["TypeID"]);
			return hdAssetTypesToMap;
		}
	}
}
