
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdPOPServersMapper : IDataMapper<IDataReader, Domain.Incidents.hdPOPServers>
    {
        public Domain.Incidents.hdPOPServers Map(IDataReader reader)
        {
			var hdPOPServersToMap = new Domain.Incidents.hdPOPServers();
			hdPOPServersToMap.HostName = reader["HostName"].ToString();
			hdPOPServersToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdPOPServersToMap.IsImap = Convert.ToBoolean(reader["IsImap"]);
			hdPOPServersToMap.NewTicketsCategoryID = Convert.ToInt32(reader["NewTicketsCategoryID"]);
			hdPOPServersToMap.POPLogin = reader["POPLogin"].ToString();
			hdPOPServersToMap.POPPassword = reader["POPPassword"].ToString();
			hdPOPServersToMap.Port = Convert.ToInt32(reader["Port"]);
			hdPOPServersToMap.ServerID = Convert.ToInt32(reader["ServerID"]);
			hdPOPServersToMap.SharedMailBoxAlias = reader["SharedMailBoxAlias"].ToString();
			hdPOPServersToMap.UseSSL = Convert.ToBoolean(reader["UseSSL"]);
			return hdPOPServersToMap;
		}
	}
}
