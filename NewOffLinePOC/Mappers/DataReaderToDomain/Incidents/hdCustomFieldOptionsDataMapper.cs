
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCustomFieldOptionsMapper : IDataMapper<IDataReader, Domain.Incidents.hdCustomFieldOptions>
    {
        public Domain.Incidents.hdCustomFieldOptions Map(IDataReader reader)
        {
			var hdCustomFieldOptionsToMap = new Domain.Incidents.hdCustomFieldOptions();
			hdCustomFieldOptionsToMap.FieldID = Convert.ToInt32(reader["FieldID"]);
			hdCustomFieldOptionsToMap.IsDefault = Convert.ToBoolean(reader["IsDefault"]);
			hdCustomFieldOptionsToMap.OptionID = Convert.ToInt32(reader["OptionID"]);
			hdCustomFieldOptionsToMap.OptionValue = reader["OptionValue"].ToString();
			hdCustomFieldOptionsToMap.ParentCustomFieldOptionId = Convert.ToInt32(reader["ParentCustomFieldOptionId"]);
			return hdCustomFieldOptionsToMap;
		}
	}
}
