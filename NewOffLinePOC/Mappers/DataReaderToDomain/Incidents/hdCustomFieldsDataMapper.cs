
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCustomFieldsMapper : IDataMapper<IDataReader, Domain.Incidents.hdCustomFields>
    {
        public Domain.Incidents.hdCustomFields Map(IDataReader reader)
        {
			var hdCustomFieldsToMap = new Domain.Incidents.hdCustomFields();
			hdCustomFieldsToMap.FieldID = Convert.ToInt32(reader["FieldID"]);
			hdCustomFieldsToMap.FieldName = reader["FieldName"].ToString();
			hdCustomFieldsToMap.ForTechsOnly = Convert.ToBoolean(reader["ForTechsOnly"]);
			hdCustomFieldsToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdCustomFieldsToMap.Mandatory = Convert.ToBoolean(reader["Mandatory"]);
			hdCustomFieldsToMap.OrderByNumber = Convert.ToInt32(reader["OrderByNumber"]);
			hdCustomFieldsToMap.ParentCustomFieldId = Convert.ToInt32(reader["ParentCustomFieldId"]);
			hdCustomFieldsToMap.ShowInGrid = Convert.ToBoolean(reader["ShowInGrid"]);
			hdCustomFieldsToMap.Type = Convert.ToInt32(reader["Type"]);
			hdCustomFieldsToMap.UsageType = Convert.ToInt32(reader["UsageType"]);
			return hdCustomFieldsToMap;
		}
	}
}
