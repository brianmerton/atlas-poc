
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAssetsMapper : IDataMapper<IDataReader, Domain.Incidents.hdAssets>
    {
        public Domain.Incidents.hdAssets Map(IDataReader reader)
        {
			var hdAssetsToMap = new Domain.Incidents.hdAssets();
			hdAssetsToMap.Comments = reader["Comments"].ToString();
			hdAssetsToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdAssetsToMap.ItemID = Convert.ToInt32(reader["ItemID"]);
			hdAssetsToMap.Location = reader["Location"].ToString();
			hdAssetsToMap.ManufacturerID = Convert.ToInt32(reader["ManufacturerID"]);
			hdAssetsToMap.ModelName = reader["ModelName"].ToString();
			hdAssetsToMap.Quantity = Convert.ToInt32(reader["Quantity"]);
			hdAssetsToMap.SerialNumber = reader["SerialNumber"].ToString();
			hdAssetsToMap.SupplierID = Convert.ToInt32(reader["SupplierID"]);
			hdAssetsToMap.TypeID = Convert.ToInt32(reader["TypeID"]);
			return hdAssetsToMap;
		}
	}
}
