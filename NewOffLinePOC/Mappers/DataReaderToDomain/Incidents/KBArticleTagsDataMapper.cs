
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class KBArticleTagsMapper : IDataMapper<IDataReader, Domain.Incidents.KBArticleTags>
    {
        public Domain.Incidents.KBArticleTags Map(IDataReader reader)
        {
			var KBArticleTagsToMap = new Domain.Incidents.KBArticleTags();
			KBArticleTagsToMap.ArticleID = Convert.ToInt32(reader["ArticleID"]);
			KBArticleTagsToMap.TagID = Convert.ToInt32(reader["TagID"]);
			return KBArticleTagsToMap;
		}
	}
}
