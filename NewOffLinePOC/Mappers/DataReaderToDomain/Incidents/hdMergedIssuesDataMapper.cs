
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdMergedIssuesMapper : IDataMapper<IDataReader, Domain.Incidents.hdMergedIssues>
    {
        public Domain.Incidents.hdMergedIssues Map(IDataReader reader)
        {
			var hdMergedIssuesToMap = new Domain.Incidents.hdMergedIssues();
			hdMergedIssuesToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			hdMergedIssuesToMap.IssueMergedID = Convert.ToInt32(reader["IssueMergedID"]);
			return hdMergedIssuesToMap;
		}
	}
}
