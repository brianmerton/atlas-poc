
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class EmailTemplatesMapper : IDataMapper<IDataReader, Domain.Incidents.EmailTemplates>
    {
        public Domain.Incidents.EmailTemplates Map(IDataReader reader)
        {
			var EmailTemplatesToMap = new Domain.Incidents.EmailTemplates();
			EmailTemplatesToMap.ClosedTicketEmailSubject = reader["ClosedTicketEmailSubject"].ToString();
			EmailTemplatesToMap.ClosedTicketEmailTemplate = reader["ClosedTicketEmailTemplate"].ToString();
			EmailTemplatesToMap.EmailSubjectTemplate = reader["EmailSubjectTemplate"].ToString();
			EmailTemplatesToMap.EmailTemplate = reader["EmailTemplate"].ToString();
			EmailTemplatesToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			EmailTemplatesToMap.NewIssueConfirmationBody = reader["NewIssueConfirmationBody"].ToString();
			EmailTemplatesToMap.NewIssueConfirmationSubj = reader["NewIssueConfirmationSubj"].ToString();
			EmailTemplatesToMap.NewIssueEmailSubject = reader["NewIssueEmailSubject"].ToString();
			EmailTemplatesToMap.NewIssueEmailTemplate = reader["NewIssueEmailTemplate"].ToString();
			EmailTemplatesToMap.WelcomeEmailBody = reader["WelcomeEmailBody"].ToString();
			EmailTemplatesToMap.WelcomeEmailSubj = reader["WelcomeEmailSubj"].ToString();
			return EmailTemplatesToMap;
		}
	}
}
