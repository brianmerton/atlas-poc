
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class UserAvatarsMapper : IDataMapper<IDataReader, Domain.Incidents.UserAvatars>
    {
        public Domain.Incidents.UserAvatars Map(IDataReader reader)
        {
			var UserAvatarsToMap = new Domain.Incidents.UserAvatars();
			UserAvatarsToMap.ImageData = (byte[])reader["ImageData"];
			UserAvatarsToMap.UserID = Convert.ToInt32(reader["UserID"]);
			return UserAvatarsToMap;
		}
	}
}
