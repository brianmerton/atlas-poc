
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdIssueScheduleMapper : IDataMapper<IDataReader, Domain.Incidents.hdIssueSchedule>
    {
        public Domain.Incidents.hdIssueSchedule Map(IDataReader reader)
        {
			var hdIssueScheduleToMap = new Domain.Incidents.hdIssueSchedule();
			hdIssueScheduleToMap.FirstRecurrenceDate = Convert.ToDateTime(reader["FirstRecurrenceDate"]);
			hdIssueScheduleToMap.FreqType = Convert.ToInt32(reader["FreqType"]);
			hdIssueScheduleToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			hdIssueScheduleToMap.PrevRepeatsCount = Convert.ToInt32(reader["PrevRepeatsCount"]);
			hdIssueScheduleToMap.ReopenOriginal = Convert.ToBoolean(reader["ReopenOriginal"]);
			return hdIssueScheduleToMap;
		}
	}
}
