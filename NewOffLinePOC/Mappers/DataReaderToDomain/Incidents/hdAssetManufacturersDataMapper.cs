
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAssetManufacturersMapper : IDataMapper<IDataReader, Domain.Incidents.hdAssetManufacturers>
    {
        public Domain.Incidents.hdAssetManufacturers Map(IDataReader reader)
        {
			var hdAssetManufacturersToMap = new Domain.Incidents.hdAssetManufacturers();
			hdAssetManufacturersToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdAssetManufacturersToMap.ManufacturerID = Convert.ToInt32(reader["ManufacturerID"]);
			hdAssetManufacturersToMap.Name = reader["Name"].ToString();
			return hdAssetManufacturersToMap;
		}
	}
}
