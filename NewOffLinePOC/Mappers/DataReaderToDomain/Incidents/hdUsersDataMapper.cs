
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdUsersMapper : IDataMapper<IDataReader, Domain.Incidents.hdUsers>
    {
        public Domain.Incidents.hdUsers Map(IDataReader reader)
        {
			var hdUsersToMap = new Domain.Incidents.hdUsers();
			hdUsersToMap.AdminTutorialShown = Convert.ToBoolean(reader["AdminTutorialShown"]);
			hdUsersToMap.ATLASUSERID = (Guid)reader["ATLASUSERID"];
			hdUsersToMap.CompanyID = Convert.ToInt32(reader["CompanyID"]);
			hdUsersToMap.Department = reader["Department"].ToString();
			hdUsersToMap.Disabled = Convert.ToBoolean(reader["Disabled"]);
			hdUsersToMap.Email = reader["Email"].ToString();
			hdUsersToMap.FirstName = reader["FirstName"].ToString();
			hdUsersToMap.Greeting = reader["Greeting"].ToString();
			hdUsersToMap.HostName = reader["HostName"].ToString();
			hdUsersToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdUsersToMap.IPAddress = reader["IPAddress"].ToString();
			hdUsersToMap.IsAdministrator = Convert.ToBoolean(reader["IsAdministrator"]);
			hdUsersToMap.IsManager = Convert.ToBoolean(reader["IsManager"]);
			hdUsersToMap.Lang = reader["Lang"].ToString();
			hdUsersToMap.LastName = reader["LastName"].ToString();
			hdUsersToMap.LastSeen = Convert.ToDateTime(reader["LastSeen"]);
			hdUsersToMap.Location = reader["Location"].ToString();
			hdUsersToMap.Notes = reader["Notes"].ToString();
			hdUsersToMap.Password = reader["Password"].ToString();
			hdUsersToMap.Phone = reader["Phone"].ToString();
			hdUsersToMap.PushToken = reader["PushToken"].ToString();
			hdUsersToMap.SendEmail = Convert.ToBoolean(reader["SendEmail"]);
			hdUsersToMap.Signature = reader["Signature"].ToString();
			hdUsersToMap.TechTutorialShown = Convert.ToBoolean(reader["TechTutorialShown"]);
			hdUsersToMap.UserAgent = reader["UserAgent"].ToString();
			hdUsersToMap.UserID = Convert.ToInt32(reader["UserID"]);
			hdUsersToMap.UserName = reader["UserName"].ToString();
			return hdUsersToMap;
		}
	}
}
