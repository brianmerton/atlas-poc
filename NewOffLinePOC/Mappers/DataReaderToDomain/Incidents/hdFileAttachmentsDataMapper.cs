
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdFileAttachmentsMapper : IDataMapper<IDataReader, Domain.Incidents.hdFileAttachments>
    {
        public Domain.Incidents.hdFileAttachments Map(IDataReader reader)
        {
			var hdFileAttachmentsToMap = new Domain.Incidents.hdFileAttachments();
			hdFileAttachmentsToMap.AtlasDocumentId = (Guid)reader["AtlasDocumentId"];
			hdFileAttachmentsToMap.CommentID = Convert.ToInt32(reader["CommentID"]);
			hdFileAttachmentsToMap.DropboxUrl = reader["DropboxUrl"].ToString();
			hdFileAttachmentsToMap.FileData = (byte[])reader["FileData"];
			hdFileAttachmentsToMap.FileHash = (byte[])reader["FileHash"];
			hdFileAttachmentsToMap.FileID = Convert.ToInt32(reader["FileID"]);
			hdFileAttachmentsToMap.FileName = reader["FileName"].ToString();
			hdFileAttachmentsToMap.FileSize = Convert.ToInt32(reader["FileSize"]);
			hdFileAttachmentsToMap.GoogleDriveUrl = reader["GoogleDriveUrl"].ToString();
			hdFileAttachmentsToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			hdFileAttachmentsToMap.KBArticleID = Convert.ToInt32(reader["KBArticleID"]);
			hdFileAttachmentsToMap.UserID = Convert.ToInt32(reader["UserID"]);
			return hdFileAttachmentsToMap;
		}
	}
}
