
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCompaniesMapper : IDataMapper<IDataReader, Domain.Incidents.hdCompanies>
    {
        public Domain.Incidents.hdCompanies Map(IDataReader reader)
        {
			var hdCompaniesToMap = new Domain.Incidents.hdCompanies();
			hdCompaniesToMap.ATLASCOMPANYID = (Guid)reader["ATLASCOMPANYID"];
			hdCompaniesToMap.CompanyID = Convert.ToInt32(reader["CompanyID"]);
			hdCompaniesToMap.CRMID = Convert.ToInt32(reader["CRMID"]);
			hdCompaniesToMap.Disabled = Convert.ToBoolean(reader["Disabled"]);
			hdCompaniesToMap.EmailDomain = reader["EmailDomain"].ToString();
			hdCompaniesToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdCompaniesToMap.Name = reader["Name"].ToString();
			hdCompaniesToMap.Notes = reader["Notes"].ToString();
			return hdCompaniesToMap;
		}
	}
}
