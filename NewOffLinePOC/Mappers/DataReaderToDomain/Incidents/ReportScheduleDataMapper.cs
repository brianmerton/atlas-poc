
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class ReportScheduleMapper : IDataMapper<IDataReader, Domain.Incidents.ReportSchedule>
    {
        public Domain.Incidents.ReportSchedule Map(IDataReader reader)
        {
			var ReportScheduleToMap = new Domain.Incidents.ReportSchedule();
			ReportScheduleToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			ReportScheduleToMap.LastSent = Convert.ToDateTime(reader["LastSent"]);
			ReportScheduleToMap.ReportModelXml = reader["ReportModelXml"].ToString();
			return ReportScheduleToMap;
		}
	}
}
