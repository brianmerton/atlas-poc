
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCategoriesMapper : IDataMapper<IDataReader, Domain.Incidents.hdCategories>
    {
        public Domain.Incidents.hdCategories Map(IDataReader reader)
        {
			var hdCategoriesToMap = new Domain.Incidents.hdCategories();
			hdCategoriesToMap.CategoryID = Convert.ToInt32(reader["CategoryID"]);
			hdCategoriesToMap.ForSpecificUsers = Convert.ToBoolean(reader["ForSpecificUsers"]);
			hdCategoriesToMap.ForTechsOnly = Convert.ToBoolean(reader["ForTechsOnly"]);
			hdCategoriesToMap.FromAddress = reader["FromAddress"].ToString();
			hdCategoriesToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdCategoriesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			hdCategoriesToMap.Name = reader["Name"].ToString();
			hdCategoriesToMap.OrderByNumber = Convert.ToInt32(reader["OrderByNumber"]);
			hdCategoriesToMap.SectionID = Convert.ToInt32(reader["SectionID"]);
			return hdCategoriesToMap;
		}
	}
}
