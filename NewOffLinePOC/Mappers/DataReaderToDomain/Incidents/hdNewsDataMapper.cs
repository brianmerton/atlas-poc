
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdNewsMapper : IDataMapper<IDataReader, Domain.Incidents.hdNews>
    {
        public Domain.Incidents.hdNews Map(IDataReader reader)
        {
			var hdNewsToMap = new Domain.Incidents.hdNews();
			hdNewsToMap.Body = reader["Body"].ToString();
			hdNewsToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdNewsToMap.NewsDate = Convert.ToDateTime(reader["NewsDate"]);
			hdNewsToMap.NewsID = Convert.ToInt32(reader["NewsID"]);
			hdNewsToMap.Subject = reader["Subject"].ToString();
			hdNewsToMap.UserID = Convert.ToInt32(reader["UserID"]);
			return hdNewsToMap;
		}
	}
}
