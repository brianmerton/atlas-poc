
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class AutomationRulesMapper : IDataMapper<IDataReader, Domain.Incidents.AutomationRules>
    {
        public Domain.Incidents.AutomationRules Map(IDataReader reader)
        {
			var AutomationRulesToMap = new Domain.Incidents.AutomationRules();
			AutomationRulesToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			AutomationRulesToMap.OrderByNumber = Convert.ToInt32(reader["OrderByNumber"]);
			AutomationRulesToMap.RuleID = Convert.ToInt32(reader["RuleID"]);
			AutomationRulesToMap.RuleXml = reader["RuleXml"].ToString();
			return AutomationRulesToMap;
		}
	}
}
