
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAPICustomFieldParamsMapper : IDataMapper<IDataReader, Domain.Incidents.hdAPICustomFieldParams>
    {
        public Domain.Incidents.hdAPICustomFieldParams Map(IDataReader reader)
        {
			var hdAPICustomFieldParamsToMap = new Domain.Incidents.hdAPICustomFieldParams();
			hdAPICustomFieldParamsToMap.APICustomFieldParamsID = Convert.ToInt32(reader["APICustomFieldParamsID"]);
			hdAPICustomFieldParamsToMap.APICustomFieldSettingsID = Convert.ToInt32(reader["APICustomFieldSettingsID"]);
			hdAPICustomFieldParamsToMap.JitbitReference = reader["JitbitReference"].ToString();
			hdAPICustomFieldParamsToMap.ParamName = reader["ParamName"].ToString();
			return hdAPICustomFieldParamsToMap;
		}
	}
}
