
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class IntegrationTicketDataMapper : IDataMapper<IDataReader, Domain.Incidents.IntegrationTicketData>
    {
        public Domain.Incidents.IntegrationTicketData Map(IDataReader reader)
        {
			var IntegrationTicketDataToMap = new Domain.Incidents.IntegrationTicketData();
			IntegrationTicketDataToMap.BitbucketIssueUrl = reader["BitbucketIssueUrl"].ToString();
			IntegrationTicketDataToMap.GitHubIssueUrl = reader["GitHubIssueUrl"].ToString();
			IntegrationTicketDataToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			IntegrationTicketDataToMap.JiraIssueUrl = reader["JiraIssueUrl"].ToString();
			return IntegrationTicketDataToMap;
		}
	}
}
