
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdIssueTagsMapper : IDataMapper<IDataReader, Domain.Incidents.hdIssueTags>
    {
        public Domain.Incidents.hdIssueTags Map(IDataReader reader)
        {
			var hdIssueTagsToMap = new Domain.Incidents.hdIssueTags();
			hdIssueTagsToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			hdIssueTagsToMap.TagID = Convert.ToInt32(reader["TagID"]);
			return hdIssueTagsToMap;
		}
	}
}
