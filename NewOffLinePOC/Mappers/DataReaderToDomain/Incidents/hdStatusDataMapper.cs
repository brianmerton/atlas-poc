
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdStatusMapper : IDataMapper<IDataReader, Domain.Incidents.hdStatus>
    {
        public Domain.Incidents.hdStatus Map(IDataReader reader)
        {
			var hdStatusToMap = new Domain.Incidents.hdStatus();
			hdStatusToMap.ButtonCaption = reader["ButtonCaption"].ToString();
			hdStatusToMap.ForTechsOnly = Convert.ToBoolean(reader["ForTechsOnly"]);
			hdStatusToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdStatusToMap.Name = reader["Name"].ToString();
			hdStatusToMap.StatusID = Convert.ToInt32(reader["StatusID"]);
			return hdStatusToMap;
		}
	}
}
