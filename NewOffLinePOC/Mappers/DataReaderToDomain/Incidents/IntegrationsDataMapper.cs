
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class IntegrationsMapper : IDataMapper<IDataReader, Domain.Incidents.Integrations>
    {
        public Domain.Incidents.Integrations Map(IDataReader reader)
        {
			var IntegrationsToMap = new Domain.Incidents.Integrations();
			IntegrationsToMap.BitbucketEnabled = Convert.ToBoolean(reader["BitbucketEnabled"]);
			IntegrationsToMap.BitbucketPassword = reader["BitbucketPassword"].ToString();
			IntegrationsToMap.BitbucketUsername = reader["BitbucketUsername"].ToString();
			IntegrationsToMap.DropboxAppID = reader["DropboxAppID"].ToString();
			IntegrationsToMap.DropboxEnabled = Convert.ToBoolean(reader["DropboxEnabled"]);
			IntegrationsToMap.GitHubEnabled = Convert.ToBoolean(reader["GitHubEnabled"]);
			IntegrationsToMap.GitHubPassword = reader["GitHubPassword"].ToString();
			IntegrationsToMap.GitHubUsername = reader["GitHubUsername"].ToString();
			IntegrationsToMap.HipChatAuthToken = reader["HipChatAuthToken"].ToString();
			IntegrationsToMap.HipChatEnabled = Convert.ToBoolean(reader["HipChatEnabled"]);
			IntegrationsToMap.HipChatRoomId = Convert.ToInt32(reader["HipChatRoomId"]);
			IntegrationsToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			IntegrationsToMap.JiraEnabled = Convert.ToBoolean(reader["JiraEnabled"]);
			IntegrationsToMap.JiraPassword = reader["JiraPassword"].ToString();
			IntegrationsToMap.JiraUrl = reader["JiraUrl"].ToString();
			IntegrationsToMap.JiraUsername = reader["JiraUsername"].ToString();
			IntegrationsToMap.SlackEnabled = Convert.ToBoolean(reader["SlackEnabled"]);
			IntegrationsToMap.SlackWebhookURL = reader["SlackWebhookURL"].ToString();
			return IntegrationsToMap;
		}
	}
}
