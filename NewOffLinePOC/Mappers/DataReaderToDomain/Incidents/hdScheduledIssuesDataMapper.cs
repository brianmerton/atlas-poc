
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdScheduledIssuesMapper : IDataMapper<IDataReader, Domain.Incidents.hdScheduledIssues>
    {
        public Domain.Incidents.hdScheduledIssues Map(IDataReader reader)
        {
			var hdScheduledIssuesToMap = new Domain.Incidents.hdScheduledIssues();
			hdScheduledIssuesToMap.OriginalIssueID = Convert.ToInt32(reader["OriginalIssueID"]);
			hdScheduledIssuesToMap.ScheduledIssueID = Convert.ToInt32(reader["ScheduledIssueID"]);
			return hdScheduledIssuesToMap;
		}
	}
}
