
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAssetsIssuesMapper : IDataMapper<IDataReader, Domain.Incidents.hdAssetsIssues>
    {
        public Domain.Incidents.hdAssetsIssues Map(IDataReader reader)
        {
			var hdAssetsIssuesToMap = new Domain.Incidents.hdAssetsIssues();
			hdAssetsIssuesToMap.AssetID = Convert.ToInt32(reader["AssetID"]);
			hdAssetsIssuesToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			return hdAssetsIssuesToMap;
		}
	}
}
