
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCategoryPermissionsMapper : IDataMapper<IDataReader, Domain.Incidents.hdCategoryPermissions>
    {
        public Domain.Incidents.hdCategoryPermissions Map(IDataReader reader)
        {
			var hdCategoryPermissionsToMap = new Domain.Incidents.hdCategoryPermissions();
			hdCategoryPermissionsToMap.CategoryID = Convert.ToInt32(reader["CategoryID"]);
			hdCategoryPermissionsToMap.UserID = Convert.ToInt32(reader["UserID"]);
			return hdCategoryPermissionsToMap;
		}
	}
}
