
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCommentTemplatesMapper : IDataMapper<IDataReader, Domain.Incidents.hdCommentTemplates>
    {
        public Domain.Incidents.hdCommentTemplates Map(IDataReader reader)
        {
			var hdCommentTemplatesToMap = new Domain.Incidents.hdCommentTemplates();
			hdCommentTemplatesToMap.Body = reader["Body"].ToString();
			hdCommentTemplatesToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdCommentTemplatesToMap.Name = reader["Name"].ToString();
			hdCommentTemplatesToMap.TemplateID = Convert.ToInt32(reader["TemplateID"]);
			return hdCommentTemplatesToMap;
		}
	}
}
