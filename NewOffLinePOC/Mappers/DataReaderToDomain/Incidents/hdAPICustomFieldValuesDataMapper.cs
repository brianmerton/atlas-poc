
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdAPICustomFieldValuesMapper : IDataMapper<IDataReader, Domain.Incidents.hdAPICustomFieldValues>
    {
        public Domain.Incidents.hdAPICustomFieldValues Map(IDataReader reader)
        {
			var hdAPICustomFieldValuesToMap = new Domain.Incidents.hdAPICustomFieldValues();
			hdAPICustomFieldValuesToMap.Fieldid = Convert.ToInt32(reader["Fieldid"]);
			hdAPICustomFieldValuesToMap.IssueId = Convert.ToInt32(reader["IssueId"]);
			hdAPICustomFieldValuesToMap.Value = (Guid)reader["Value"];
			return hdAPICustomFieldValuesToMap;
		}
	}
}
