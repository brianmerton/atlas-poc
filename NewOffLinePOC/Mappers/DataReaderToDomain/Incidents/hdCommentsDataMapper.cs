
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCommentsMapper : IDataMapper<IDataReader, Domain.Incidents.hdComments>
    {
        public Domain.Incidents.hdComments Map(IDataReader reader)
        {
			var hdCommentsToMap = new Domain.Incidents.hdComments();
			hdCommentsToMap.Body = reader["Body"].ToString();
			hdCommentsToMap.CommentDate = Convert.ToDateTime(reader["CommentDate"]);
			hdCommentsToMap.CommentID = Convert.ToInt32(reader["CommentID"]);
			hdCommentsToMap.EmailHeaders = reader["EmailHeaders"].ToString();
			hdCommentsToMap.ForTechsOnly = Convert.ToBoolean(reader["ForTechsOnly"]);
			hdCommentsToMap.HiddenFromKB = Convert.ToBoolean(reader["HiddenFromKB"]);
			hdCommentsToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			hdCommentsToMap.IsSystem = Convert.ToBoolean(reader["IsSystem"]);
			hdCommentsToMap.Recipients = reader["Recipients"].ToString();
			hdCommentsToMap.UserID = Convert.ToInt32(reader["UserID"]);
			return hdCommentsToMap;
		}
	}
}
