
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdOnBehalfIssuesMapper : IDataMapper<IDataReader, Domain.Incidents.hdOnBehalfIssues>
    {
        public Domain.Incidents.hdOnBehalfIssues Map(IDataReader reader)
        {
			var hdOnBehalfIssuesToMap = new Domain.Incidents.hdOnBehalfIssues();
			hdOnBehalfIssuesToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			hdOnBehalfIssuesToMap.SubmittedByUserID = Convert.ToInt32(reader["SubmittedByUserID"]);
			return hdOnBehalfIssuesToMap;
		}
	}
}
