
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdSettingsMapper : IDataMapper<IDataReader, Domain.Incidents.hdSettings>
    {
        public Domain.Incidents.hdSettings Map(IDataReader reader)
        {
			var hdSettingsToMap = new Domain.Incidents.hdSettings();
			hdSettingsToMap.ActiveTabTextColor = reader["ActiveTabTextColor"].ToString();
			hdSettingsToMap.AllowAnonymousKBAccess = Convert.ToBoolean(reader["AllowAnonymousKBAccess"]);
			hdSettingsToMap.AllowGuestSubmission = Convert.ToBoolean(reader["AllowGuestSubmission"]);
			hdSettingsToMap.AllowUserRegistration = Convert.ToBoolean(reader["AllowUserRegistration"]);
			hdSettingsToMap.AutoAssignFirstReplier = Convert.ToBoolean(reader["AutoAssignFirstReplier"]);
			hdSettingsToMap.AutoCloseImportedCases = Convert.ToBoolean(reader["AutoCloseImportedCases"]);
			hdSettingsToMap.AutoLoginSharedSecret = reader["AutoLoginSharedSecret"].ToString();
			hdSettingsToMap.CRMRootUrl = reader["CRMRootUrl"].ToString();
			hdSettingsToMap.CRMSharedKey = reader["CRMSharedKey"].ToString();
			hdSettingsToMap.CustomCSS = reader["CustomCSS"].ToString();
			hdSettingsToMap.DaysToCloseAfter = Convert.ToInt32(reader["DaysToCloseAfter"]);
			hdSettingsToMap.DisableAssets = Convert.ToBoolean(reader["DisableAssets"]);
			hdSettingsToMap.DisableAutoTimer = Convert.ToBoolean(reader["DisableAutoTimer"]);
			hdSettingsToMap.DisableAvatars = Convert.ToBoolean(reader["DisableAvatars"]);
			hdSettingsToMap.DisableCloseNotification = Convert.ToBoolean(reader["DisableCloseNotification"]);
			hdSettingsToMap.DisableKB = Convert.ToBoolean(reader["DisableKB"]);
			hdSettingsToMap.DisableTicketConfirmationNotification = Convert.ToBoolean(reader["DisableTicketConfirmationNotification"]);
			hdSettingsToMap.DontAddCCToSubscribers = Convert.ToBoolean(reader["DontAddCCToSubscribers"]);
			hdSettingsToMap.DontReopenOnReply = Convert.ToBoolean(reader["DontReopenOnReply"]);
			hdSettingsToMap.EmailNotificationsEnabled = Convert.ToBoolean(reader["EmailNotificationsEnabled"]);
			hdSettingsToMap.EmailOtherTechsOnTakeover = Convert.ToBoolean(reader["EmailOtherTechsOnTakeover"]);
			hdSettingsToMap.EnableSaml = Convert.ToBoolean(reader["EnableSaml"]);
			hdSettingsToMap.FromAddress = reader["FromAddress"].ToString();
			hdSettingsToMap.FromName = reader["FromName"].ToString();
			hdSettingsToMap.HeaderBGColor = char
			hdSettingsToMap.HeaderTextColor = char
			hdSettingsToMap.HidePoweredBy = Convert.ToBoolean(reader["HidePoweredBy"]);
			hdSettingsToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			hdSettingsToMap.Lang = reader["Lang"].ToString();
			hdSettingsToMap.LogoImage = (byte[])reader["LogoImage"];
			hdSettingsToMap.MailCheckerAllowUnregisteredUsers = Convert.ToBoolean(reader["MailCheckerAllowUnregisteredUsers"]);
			hdSettingsToMap.MailCheckerNewIssuesCategoryID = Convert.ToInt32(reader["MailCheckerNewIssuesCategoryID"]);
			hdSettingsToMap.MaxAttachSize = Convert.ToInt32(reader["MaxAttachSize"]);
			hdSettingsToMap.MenuBarBGColor = char
			hdSettingsToMap.MenuBarTextColor = char
			hdSettingsToMap.NewIssuesDefaultCategoryID = Convert.ToInt32(reader["NewIssuesDefaultCategoryID"]);
			hdSettingsToMap.NewTicketNote = reader["NewTicketNote"].ToString();
			hdSettingsToMap.NotifyAllTechsOnReply = Convert.ToBoolean(reader["NotifyAllTechsOnReply"]);
			hdSettingsToMap.RemoteLoginURL = reader["RemoteLoginURL"].ToString();
			hdSettingsToMap.ReplyToAddress = reader["ReplyToAddress"].ToString();
			hdSettingsToMap.RestrictIssueClosing = Convert.ToBoolean(reader["RestrictIssueClosing"]);
			hdSettingsToMap.RestrictIssueDeletion = Convert.ToBoolean(reader["RestrictIssueDeletion"]);
			hdSettingsToMap.SameCompanyUserPermission = Convert.ToBoolean(reader["SameCompanyUserPermission"]);
			hdSettingsToMap.SamlCertificate = reader["SamlCertificate"].ToString();
			hdSettingsToMap.SamlEndpoint = reader["SamlEndpoint"].ToString();
			hdSettingsToMap.SeeEveryonesIssues = Convert.ToBoolean(reader["SeeEveryonesIssues"]);
			hdSettingsToMap.SendAdminEmailOnNewIssue = Convert.ToBoolean(reader["SendAdminEmailOnNewIssue"]);
			hdSettingsToMap.SendTechniciansEmailOnNewIssue = Convert.ToBoolean(reader["SendTechniciansEmailOnNewIssue"]);
			hdSettingsToMap.SendTechSMSOnNewIssue = Convert.ToBoolean(reader["SendTechSMSOnNewIssue"]);
			hdSettingsToMap.ServerTimeOffset = Convert.ToInt32(reader["ServerTimeOffset"]);
			hdSettingsToMap.SMTPAuthRequired = Convert.ToBoolean(reader["SMTPAuthRequired"]);
			hdSettingsToMap.SMTPPassword = reader["SMTPPassword"].ToString();
			hdSettingsToMap.SMTPPort = Convert.ToInt32(reader["SMTPPort"]);
			hdSettingsToMap.SMTPServer = reader["SMTPServer"].ToString();
			hdSettingsToMap.SMTPUserName = reader["SMTPUserName"].ToString();
			hdSettingsToMap.SMTPUseSSL = Convert.ToBoolean(reader["SMTPUseSSL"]);
			hdSettingsToMap.TimezoneID = reader["TimezoneID"].ToString();
			hdSettingsToMap.Title = reader["Title"].ToString();
			hdSettingsToMap.UseDefaultSmtp = Convert.ToBoolean(reader["UseDefaultSmtp"]);
			hdSettingsToMap.UseFromNameForUserOriginatedMsgs = Convert.ToBoolean(reader["UseFromNameForUserOriginatedMsgs"]);
			hdSettingsToMap.WorkHourFrom = Convert.ToInt32(reader["WorkHourFrom"]);
			hdSettingsToMap.WorkHourTo = Convert.ToInt32(reader["WorkHourTo"]);
			return hdSettingsToMap;
		}
	}
}
