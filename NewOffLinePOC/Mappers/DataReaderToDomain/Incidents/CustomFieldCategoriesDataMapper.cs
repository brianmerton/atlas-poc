
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class CustomFieldCategoriesMapper : IDataMapper<IDataReader, Domain.Incidents.CustomFieldCategories>
    {
        public Domain.Incidents.CustomFieldCategories Map(IDataReader reader)
        {
			var CustomFieldCategoriesToMap = new Domain.Incidents.CustomFieldCategories();
			CustomFieldCategoriesToMap.CategoryID = Convert.ToInt32(reader["CategoryID"]);
			CustomFieldCategoriesToMap.FieldID = Convert.ToInt32(reader["FieldID"]);
			return CustomFieldCategoriesToMap;
		}
	}
}
