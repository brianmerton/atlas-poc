
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class NonupdatedAutomationLogMapper : IDataMapper<IDataReader, Domain.Incidents.NonupdatedAutomationLog>
    {
        public Domain.Incidents.NonupdatedAutomationLog Map(IDataReader reader)
        {
			var NonupdatedAutomationLogToMap = new Domain.Incidents.NonupdatedAutomationLog();
			NonupdatedAutomationLogToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			NonupdatedAutomationLogToMap.RuleID = Convert.ToInt32(reader["RuleID"]);
			return NonupdatedAutomationLogToMap;
		}
	}
}
