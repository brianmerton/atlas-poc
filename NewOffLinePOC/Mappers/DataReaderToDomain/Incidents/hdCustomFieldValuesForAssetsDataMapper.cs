
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdCustomFieldValuesForAssetsMapper : IDataMapper<IDataReader, Domain.Incidents.hdCustomFieldValuesForAssets>
    {
        public Domain.Incidents.hdCustomFieldValuesForAssets Map(IDataReader reader)
        {
			var hdCustomFieldValuesForAssetsToMap = new Domain.Incidents.hdCustomFieldValuesForAssets();
			hdCustomFieldValuesForAssetsToMap.FieldID = Convert.ToInt32(reader["FieldID"]);
			hdCustomFieldValuesForAssetsToMap.ItemID = Convert.ToInt32(reader["ItemID"]);
			hdCustomFieldValuesForAssetsToMap.Value = sql_variant
			return hdCustomFieldValuesForAssetsToMap;
		}
	}
}
