
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class hdLinkedIssuesMapper : IDataMapper<IDataReader, Domain.Incidents.hdLinkedIssues>
    {
        public Domain.Incidents.hdLinkedIssues Map(IDataReader reader)
        {
			var hdLinkedIssuesToMap = new Domain.Incidents.hdLinkedIssues();
			hdLinkedIssuesToMap.Issue1ID = Convert.ToInt32(reader["Issue1ID"]);
			hdLinkedIssuesToMap.Issue2ID = Convert.ToInt32(reader["Issue2ID"]);
			return hdLinkedIssuesToMap;
		}
	}
}
