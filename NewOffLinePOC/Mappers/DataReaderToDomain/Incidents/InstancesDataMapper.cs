
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class InstancesMapper : IDataMapper<IDataReader, Domain.Incidents.Instances>
    {
        public Domain.Incidents.Instances Map(IDataReader reader)
        {
			var InstancesToMap = new Domain.Incidents.Instances();
			InstancesToMap.AvangateSubscriptionID = reader["AvangateSubscriptionID"].ToString();
			InstancesToMap.CustomDomain = reader["CustomDomain"].ToString();
			InstancesToMap.CustomerID = Convert.ToInt32(reader["CustomerID"]);
			InstancesToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			InstancesToMap.Name = reader["Name"].ToString();
			InstancesToMap.ValidTill = Convert.ToDateTime(reader["ValidTill"]);
			InstancesToMap.WidgetInstalled = Convert.ToBoolean(reader["WidgetInstalled"]);
			return InstancesToMap;
		}
	}
}
