
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class OverdueAutomationLogMapper : IDataMapper<IDataReader, Domain.Incidents.OverdueAutomationLog>
    {
        public Domain.Incidents.OverdueAutomationLog Map(IDataReader reader)
        {
			var OverdueAutomationLogToMap = new Domain.Incidents.OverdueAutomationLog();
			OverdueAutomationLogToMap.IssueID = Convert.ToInt32(reader["IssueID"]);
			OverdueAutomationLogToMap.RuleID = Convert.ToInt32(reader["RuleID"]);
			return OverdueAutomationLogToMap;
		}
	}
}
