
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class EmailAuditLogMapper : IDataMapper<IDataReader, Domain.Incidents.EmailAuditLog>
    {
        public Domain.Incidents.EmailAuditLog Map(IDataReader reader)
        {
			var EmailAuditLogToMap = new Domain.Incidents.EmailAuditLog();
			EmailAuditLogToMap.Attachment = reader["Attachment"].ToString();
			EmailAuditLogToMap.Body = reader["Body"].ToString();
			EmailAuditLogToMap.CCEmailAddress = reader["CCEmailAddress"].ToString();
			EmailAuditLogToMap.CreatedDateTime = Convert.ToDateTime(reader["CreatedDateTime"]);
			EmailAuditLogToMap.EmailAuditLogId = Convert.ToInt32(reader["EmailAuditLogId"]);
			EmailAuditLogToMap.FromEmailAddress = reader["FromEmailAddress"].ToString();
			EmailAuditLogToMap.IsInBound = Convert.ToBoolean(reader["IsInBound"]);
			EmailAuditLogToMap.IssueId = Convert.ToInt32(reader["IssueId"]);
			EmailAuditLogToMap.ReceivedDateTime = Convert.ToDateTime(reader["ReceivedDateTime"]);
			EmailAuditLogToMap.SentDateTime = Convert.ToDateTime(reader["SentDateTime"]);
			EmailAuditLogToMap.ServerID = Convert.ToInt32(reader["ServerID"]);
			EmailAuditLogToMap.Subject = reader["Subject"].ToString();
			EmailAuditLogToMap.ToEmailAddress = reader["ToEmailAddress"].ToString();
			return EmailAuditLogToMap;
		}
	}
}
