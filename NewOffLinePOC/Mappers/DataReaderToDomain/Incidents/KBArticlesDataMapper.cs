
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Incidents
{
    public class KBArticlesMapper : IDataMapper<IDataReader, Domain.Incidents.KBArticles>
    {
        public Domain.Incidents.KBArticles Map(IDataReader reader)
        {
			var KBArticlesToMap = new Domain.Incidents.KBArticles();
			KBArticlesToMap.ArticleID = Convert.ToInt32(reader["ArticleID"]);
			KBArticlesToMap.Body = reader["Body"].ToString();
			KBArticlesToMap.CategoryID = Convert.ToInt32(reader["CategoryID"]);
			KBArticlesToMap.ForTechsOnly = Convert.ToBoolean(reader["ForTechsOnly"]);
			KBArticlesToMap.InstanceID = Convert.ToInt32(reader["InstanceID"]);
			KBArticlesToMap.OrderByNumber = Convert.ToInt32(reader["OrderByNumber"]);
			KBArticlesToMap.Subject = reader["Subject"].ToString();
			return KBArticlesToMap;
		}
	}
}
