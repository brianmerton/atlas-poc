
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Customer
{
    public class RiskAssessmentWorkspaceTypesMapper : IDataMapper<IDataReader, Domain.Customer.RiskAssessmentWorkspaceTypes>
    {
        public Domain.Customer.RiskAssessmentWorkspaceTypes Map(IDataReader reader)
        {
			var RiskAssessmentWorkspaceTypesToMap = new Domain.Customer.RiskAssessmentWorkspaceTypes();
			
			return RiskAssessmentWorkspaceTypesToMap;
		}
	}
}
