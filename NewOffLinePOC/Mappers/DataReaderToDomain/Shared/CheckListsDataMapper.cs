
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class CheckListsMapper : IDataMapper<IDataReader, Domain.Shared.CheckLists>
    {
        public Domain.Shared.CheckLists Map(IDataReader reader)
        {
			var CheckListsToMap = new Domain.Shared.CheckLists();
			CheckListsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			CheckListsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			CheckListsToMap.Id = (Guid)reader["Id"];
			CheckListsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			CheckListsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			CheckListsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			CheckListsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			CheckListsToMap.Name = reader["Name"].ToString();
			CheckListsToMap.Periodicity = Convert.ToInt32(reader["Periodicity"]);
			CheckListsToMap.Version = reader["Version"].ToString();
			return CheckListsToMap;
		}
	}
}
