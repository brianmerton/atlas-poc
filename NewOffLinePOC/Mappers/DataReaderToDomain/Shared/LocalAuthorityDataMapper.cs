
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class LocalAuthorityMapper : IDataMapper<IDataReader, Domain.Shared.LocalAuthority>
    {
        public Domain.Shared.LocalAuthority Map(IDataReader reader)
        {
			var LocalAuthorityToMap = new Domain.Shared.LocalAuthority();
			LocalAuthorityToMap.CreatedBy = (Guid)reader["CreatedBy"];
			LocalAuthorityToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			LocalAuthorityToMap.GeoRegionId = (Guid)reader["GeoRegionId"];
			LocalAuthorityToMap.Id = (Guid)reader["Id"];
			LocalAuthorityToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			LocalAuthorityToMap.LCid = Convert.ToInt32(reader["LCid"]);
			LocalAuthorityToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			LocalAuthorityToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			LocalAuthorityToMap.Name = reader["Name"].ToString();
			LocalAuthorityToMap.Version = reader["Version"].ToString();
			return LocalAuthorityToMap;
		}
	}
}
