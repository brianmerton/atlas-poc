
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class SectorWorkspaceMapMapper : IDataMapper<IDataReader, Domain.Shared.SectorWorkspaceMap>
    {
        public Domain.Shared.SectorWorkspaceMap Map(IDataReader reader)
        {
			var SectorWorkspaceMapToMap = new Domain.Shared.SectorWorkspaceMap();
			SectorWorkspaceMapToMap.CreatedBy = (Guid)reader["CreatedBy"];
			SectorWorkspaceMapToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			SectorWorkspaceMapToMap.Icon = (Guid)reader["Icon"];
			SectorWorkspaceMapToMap.Id = (Guid)reader["Id"];
			SectorWorkspaceMapToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			SectorWorkspaceMapToMap.Label = reader["Label"].ToString();
			SectorWorkspaceMapToMap.LCid = Convert.ToInt32(reader["LCid"]);
			SectorWorkspaceMapToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			SectorWorkspaceMapToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			SectorWorkspaceMapToMap.SectorId = (Guid)reader["SectorId"];
			SectorWorkspaceMapToMap.Version = reader["Version"].ToString();
			SectorWorkspaceMapToMap.WorkspaceTypeId = (Guid)reader["WorkspaceTypeId"];
			return SectorWorkspaceMapToMap;
		}
	}
}
