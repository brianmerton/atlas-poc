
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class OptionsMapper : IDataMapper<IDataReader, Domain.Shared.Options>
    {
        public Domain.Shared.Options Map(IDataReader reader)
        {
			var OptionsToMap = new Domain.Shared.Options();
			OptionsToMap.AttributeId = (Guid)reader["AttributeId"];
			OptionsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			OptionsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			OptionsToMap.Id = (Guid)reader["Id"];
			OptionsToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			OptionsToMap.IsDefault = Convert.ToBoolean(reader["IsDefault"]);
			OptionsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			OptionsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			OptionsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			OptionsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			OptionsToMap.OrderIndex = Convert.ToInt32(reader["OrderIndex"]);
			OptionsToMap.Text = reader["Text"].ToString();
			OptionsToMap.Version = reader["Version"].ToString();
			return OptionsToMap;
		}
	}
}
