
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class CoursesMapper : IDataMapper<IDataReader, Domain.Shared.Courses>
    {
        public Domain.Shared.Courses Map(IDataReader reader)
        {
			var CoursesToMap = new Domain.Shared.Courses();
			CoursesToMap.CourseType = Convert.ToInt32(reader["CourseType"]);
			CoursesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			CoursesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			CoursesToMap.Description = reader["Description"].ToString();
			CoursesToMap.Id = (Guid)reader["Id"];
			CoursesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			CoursesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			CoursesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			CoursesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			CoursesToMap.Title = reader["Title"].ToString();
			CoursesToMap.Version = reader["Version"].ToString();
			return CoursesToMap;
		}
	}
}
