
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class SectorsMapper : IDataMapper<IDataReader, Domain.Shared.Sectors>
    {
        public Domain.Shared.Sectors Map(IDataReader reader)
        {
			var SectorsToMap = new Domain.Shared.Sectors();
			SectorsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			SectorsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			SectorsToMap.Id = (Guid)reader["Id"];
			SectorsToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			SectorsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			SectorsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			SectorsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			SectorsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			SectorsToMap.Name = reader["Name"].ToString();
			SectorsToMap.PictureId = (Guid)reader["PictureId"];
			SectorsToMap.Version = reader["Version"].ToString();
			return SectorsToMap;
		}
	}
}
