
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class InjuryTypeInjuredPartiesMapper : IDataMapper<IDataReader, Domain.Shared.InjuryTypeInjuredParties>
    {
        public Domain.Shared.InjuryTypeInjuredParties Map(IDataReader reader)
        {
			var InjuryTypeInjuredPartiesToMap = new Domain.Shared.InjuryTypeInjuredParties();
			InjuryTypeInjuredPartiesToMap.InjuredPartyId = (Guid)reader["InjuredPartyId"];
			InjuryTypeInjuredPartiesToMap.InjuryTypeId = (Guid)reader["InjuryTypeId"];
			return InjuryTypeInjuredPartiesToMap;
		}
	}
}
