
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class TipsMapper : IDataMapper<IDataReader, Domain.Shared.Tips>
    {
        public Domain.Shared.Tips Map(IDataReader reader)
        {
			var TipsToMap = new Domain.Shared.Tips();
			TipsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			TipsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			TipsToMap.Id = (Guid)reader["Id"];
			TipsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			TipsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			TipsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			TipsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			TipsToMap.Service = Convert.ToInt32(reader["Service"]);
			TipsToMap.Tip = reader["Tip"].ToString();
			TipsToMap.Url = reader["Url"].ToString();
			TipsToMap.Version = reader["Version"].ToString();
			return TipsToMap;
		}
	}
}
