
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class VisitTypesMapper : IDataMapper<IDataReader, Domain.Shared.VisitTypes>
    {
        public Domain.Shared.VisitTypes Map(IDataReader reader)
        {
			var VisitTypesToMap = new Domain.Shared.VisitTypes();
			VisitTypesToMap.ClientTypeId = (Guid)reader["ClientTypeId"];
			VisitTypesToMap.Color = reader["Color"].ToString();
			VisitTypesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			VisitTypesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			VisitTypesToMap.Description = reader["Description"].ToString();
			VisitTypesToMap.Duration = Convert.ToInt32(reader["Duration"]);
			VisitTypesToMap.Id = (Guid)reader["Id"];
			VisitTypesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			VisitTypesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			VisitTypesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			VisitTypesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			VisitTypesToMap.Name = reader["Name"].ToString();
			VisitTypesToMap.Version = reader["Version"].ToString();
			return VisitTypesToMap;
		}
	}
}
