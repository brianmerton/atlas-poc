
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class WorkProcessMapper : IDataMapper<IDataReader, Domain.Shared.WorkProcess>
    {
        public Domain.Shared.WorkProcess Map(IDataReader reader)
        {
			var WorkProcessToMap = new Domain.Shared.WorkProcess();
			WorkProcessToMap.CreatedBy = (Guid)reader["CreatedBy"];
			WorkProcessToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			WorkProcessToMap.Id = (Guid)reader["Id"];
			WorkProcessToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			WorkProcessToMap.LCid = Convert.ToInt32(reader["LCid"]);
			WorkProcessToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			WorkProcessToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			WorkProcessToMap.Name = reader["Name"].ToString();
			WorkProcessToMap.Version = reader["Version"].ToString();
			return WorkProcessToMap;
		}
	}
}
