
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class CheckItemsMapper : IDataMapper<IDataReader, Domain.Shared.CheckItems>
    {
        public Domain.Shared.CheckItems Map(IDataReader reader)
        {
			var CheckItemsToMap = new Domain.Shared.CheckItems();
			CheckItemsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			CheckItemsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			CheckItemsToMap.Id = (Guid)reader["Id"];
			CheckItemsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			CheckItemsToMap.ItemText = reader["ItemText"].ToString();
			CheckItemsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			CheckItemsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			CheckItemsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			CheckItemsToMap.Number = Convert.ToInt32(reader["Number"]);
			CheckItemsToMap.Version = reader["Version"].ToString();
			return CheckItemsToMap;
		}
	}
}
