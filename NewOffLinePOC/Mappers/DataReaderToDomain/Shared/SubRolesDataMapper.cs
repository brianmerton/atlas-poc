
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class SubRolesMapper : IDataMapper<IDataReader, Domain.Shared.SubRoles>
    {
        public Domain.Shared.SubRoles Map(IDataReader reader)
        {
			var SubRolesToMap = new Domain.Shared.SubRoles();
			SubRolesToMap.Area = Convert.ToInt32(reader["Area"]);
			SubRolesToMap.AttributeId = (Guid)reader["AttributeId"];
			SubRolesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			SubRolesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			SubRolesToMap.Id = (Guid)reader["Id"];
			SubRolesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			SubRolesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			SubRolesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			SubRolesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			SubRolesToMap.Name = reader["Name"].ToString();
			SubRolesToMap.Version = reader["Version"].ToString();
			return SubRolesToMap;
		}
	}
}
