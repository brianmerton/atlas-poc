
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class AccidentCategoryMapper : IDataMapper<IDataReader, Domain.Shared.AccidentCategory>
    {
        public Domain.Shared.AccidentCategory Map(IDataReader reader)
        {
			var AccidentCategoryToMap = new Domain.Shared.AccidentCategory();
			AccidentCategoryToMap.CreatedBy = (Guid)reader["CreatedBy"];
			AccidentCategoryToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			AccidentCategoryToMap.Description = reader["Description"].ToString();
			AccidentCategoryToMap.Id = (Guid)reader["Id"];
			AccidentCategoryToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			AccidentCategoryToMap.LCid = Convert.ToInt32(reader["LCid"]);
			AccidentCategoryToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			AccidentCategoryToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			AccidentCategoryToMap.Name = reader["Name"].ToString();
			AccidentCategoryToMap.Version = reader["Version"].ToString();
			return AccidentCategoryToMap;
		}
	}
}
