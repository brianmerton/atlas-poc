
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class CountiesMapper : IDataMapper<IDataReader, Domain.Shared.Counties>
    {
        public Domain.Shared.Counties Map(IDataReader reader)
        {
			var CountiesToMap = new Domain.Shared.Counties();
			CountiesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			CountiesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			CountiesToMap.Id = (Guid)reader["Id"];
			CountiesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			CountiesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			CountiesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			CountiesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			CountiesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			CountiesToMap.Name = reader["Name"].ToString();
			CountiesToMap.Version = reader["Version"].ToString();
			return CountiesToMap;
		}
	}
}
