
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class SLATypesMapper : IDataMapper<IDataReader, Domain.Shared.SLATypes>
    {
        public Domain.Shared.SLATypes Map(IDataReader reader)
        {
			var SLATypesToMap = new Domain.Shared.SLATypes();
			SLATypesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			SLATypesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			SLATypesToMap.CreateReminderBefore = Convert.ToInt32(reader["CreateReminderBefore"]);
			SLATypesToMap.CreateTaskBefore = Convert.ToInt32(reader["CreateTaskBefore"]);
			SLATypesToMap.DaysAfter = Convert.ToInt32(reader["DaysAfter"]);
			SLATypesToMap.EmailTemplateId = (Guid)reader["EmailTemplateId"];
			SLATypesToMap.IconId = (Guid)reader["IconId"];
			SLATypesToMap.Id = (Guid)reader["Id"];
			SLATypesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			SLATypesToMap.IsRecursive = Convert.ToBoolean(reader["IsRecursive"]);
			SLATypesToMap.IsSystemTriggered = Convert.ToBoolean(reader["IsSystemTriggered"]);
			SLATypesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			SLATypesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			SLATypesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			SLATypesToMap.OnlyforHeadOffice = Convert.ToBoolean(reader["OnlyforHeadOffice"]);
			SLATypesToMap.SLADateType = Convert.ToInt32(reader["SLADateType"]);
			SLATypesToMap.Version = reader["Version"].ToString();
			SLATypesToMap.VisitTypeId = (Guid)reader["VisitTypeId"];
			return SLATypesToMap;
		}
	}
}
