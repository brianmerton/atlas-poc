
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class CheckListVersionMapper : IDataMapper<IDataReader, Domain.Shared.CheckListVersion>
    {
        public Domain.Shared.CheckListVersion Map(IDataReader reader)
        {
			var CheckListVersionToMap = new Domain.Shared.CheckListVersion();
			CheckListVersionToMap.Comment = reader["Comment"].ToString();
			CheckListVersionToMap.CreatedBy = (Guid)reader["CreatedBy"];
			CheckListVersionToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			CheckListVersionToMap.Id = (Guid)reader["Id"];
			CheckListVersionToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			CheckListVersionToMap.LCid = Convert.ToInt32(reader["LCid"]);
			CheckListVersionToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			CheckListVersionToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			CheckListVersionToMap.Name = reader["Name"].ToString();
			CheckListVersionToMap.Periodicity = Convert.ToInt32(reader["Periodicity"]);
			CheckListVersionToMap.PrototypeId = (Guid)reader["PrototypeId"];
			CheckListVersionToMap.Version = reader["Version"].ToString();
			return CheckListVersionToMap;
		}
	}
}
