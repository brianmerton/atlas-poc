
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class UsersIdentityMapper : IDataMapper<IDataReader, Domain.Shared.UsersIdentity>
    {
        public Domain.Shared.UsersIdentity Map(IDataReader reader)
        {
			var UsersIdentityToMap = new Domain.Shared.UsersIdentity();
			UsersIdentityToMap.CreatedBy = (Guid)reader["CreatedBy"];
			UsersIdentityToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			UsersIdentityToMap.Email = reader["Email"].ToString();
			UsersIdentityToMap.Id = (Guid)reader["Id"];
			UsersIdentityToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			UsersIdentityToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			UsersIdentityToMap.LCid = Convert.ToInt32(reader["LCid"]);
			UsersIdentityToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			UsersIdentityToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			UsersIdentityToMap.Password = reader["Password"].ToString();
			UsersIdentityToMap.PasswordAnswer = reader["PasswordAnswer"].ToString();
			UsersIdentityToMap.PasswordQuestion = reader["PasswordQuestion"].ToString();
			UsersIdentityToMap.ResetToken = (Guid)reader["ResetToken"];
			UsersIdentityToMap.TcAccepted = Convert.ToBoolean(reader["TcAccepted"]);
			UsersIdentityToMap.TcBrowserMetaData = reader["TcBrowserMetaData"].ToString();
			UsersIdentityToMap.TcIpAddress = reader["TcIpAddress"].ToString();
			UsersIdentityToMap.TcShownOn = Convert.ToDateTime(reader["TcShownOn"]);
			UsersIdentityToMap.TcVersion = reader["TcVersion"].ToString();
			UsersIdentityToMap.TenantName = reader["TenantName"].ToString();
			UsersIdentityToMap.TokenExpiryDate = Convert.ToDateTime(reader["TokenExpiryDate"]);
			UsersIdentityToMap.Version = reader["Version"].ToString();
			return UsersIdentityToMap;
		}
	}
}
