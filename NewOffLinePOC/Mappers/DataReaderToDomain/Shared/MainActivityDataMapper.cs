
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class MainActivityMapper : IDataMapper<IDataReader, Domain.Shared.MainActivity>
    {
        public Domain.Shared.MainActivity Map(IDataReader reader)
        {
			var MainActivityToMap = new Domain.Shared.MainActivity();
			MainActivityToMap.CreatedBy = (Guid)reader["CreatedBy"];
			MainActivityToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			MainActivityToMap.Id = (Guid)reader["Id"];
			MainActivityToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			MainActivityToMap.LCid = Convert.ToInt32(reader["LCid"]);
			MainActivityToMap.MainIndustryId = (Guid)reader["MainIndustryId"];
			MainActivityToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			MainActivityToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			MainActivityToMap.Name = reader["Name"].ToString();
			MainActivityToMap.Version = reader["Version"].ToString();
			return MainActivityToMap;
		}
	}
}
