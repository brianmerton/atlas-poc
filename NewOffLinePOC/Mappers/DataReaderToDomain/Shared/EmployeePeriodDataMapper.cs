
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class EmployeePeriodMapper : IDataMapper<IDataReader, Domain.Shared.EmployeePeriod>
    {
        public Domain.Shared.EmployeePeriod Map(IDataReader reader)
        {
			var EmployeePeriodToMap = new Domain.Shared.EmployeePeriod();
			EmployeePeriodToMap.CreatedBy = (Guid)reader["CreatedBy"];
			EmployeePeriodToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			EmployeePeriodToMap.Id = (Guid)reader["Id"];
			EmployeePeriodToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			EmployeePeriodToMap.LCid = Convert.ToInt32(reader["LCid"]);
			EmployeePeriodToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			EmployeePeriodToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			EmployeePeriodToMap.Name = reader["Name"].ToString();
			EmployeePeriodToMap.Version = reader["Version"].ToString();
			return EmployeePeriodToMap;
		}
	}
}
