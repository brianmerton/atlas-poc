
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class EthnicGroupValueMapper : IDataMapper<IDataReader, Domain.Shared.EthnicGroupValue>
    {
        public Domain.Shared.EthnicGroupValue Map(IDataReader reader)
        {
			var EthnicGroupValueToMap = new Domain.Shared.EthnicGroupValue();
			EthnicGroupValueToMap.CreatedBy = (Guid)reader["CreatedBy"];
			EthnicGroupValueToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			EthnicGroupValueToMap.EthnicGroupTypeId = (Guid)reader["EthnicGroupTypeId"];
			EthnicGroupValueToMap.EthnicGroupValueType = Convert.ToInt32(reader["EthnicGroupValueType"]);
			EthnicGroupValueToMap.Id = (Guid)reader["Id"];
			EthnicGroupValueToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			EthnicGroupValueToMap.LCid = Convert.ToInt32(reader["LCid"]);
			EthnicGroupValueToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			EthnicGroupValueToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			EthnicGroupValueToMap.Name = reader["Name"].ToString();
			EthnicGroupValueToMap.Version = reader["Version"].ToString();
			return EthnicGroupValueToMap;
		}
	}
}
