
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class EmployeeTypeMapper : IDataMapper<IDataReader, Domain.Shared.EmployeeType>
    {
        public Domain.Shared.EmployeeType Map(IDataReader reader)
        {
			var EmployeeTypeToMap = new Domain.Shared.EmployeeType();
			EmployeeTypeToMap.CreatedBy = (Guid)reader["CreatedBy"];
			EmployeeTypeToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			EmployeeTypeToMap.Id = (Guid)reader["Id"];
			EmployeeTypeToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			EmployeeTypeToMap.LCid = Convert.ToInt32(reader["LCid"]);
			EmployeeTypeToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			EmployeeTypeToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			EmployeeTypeToMap.Name = reader["Name"].ToString();
			EmployeeTypeToMap.Version = reader["Version"].ToString();
			return EmployeeTypeToMap;
		}
	}
}
