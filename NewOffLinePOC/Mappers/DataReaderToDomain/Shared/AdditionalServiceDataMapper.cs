
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class AdditionalServiceMapper : IDataMapper<IDataReader, Domain.Shared.AdditionalService>
    {
        public Domain.Shared.AdditionalService Map(IDataReader reader)
        {
			var AdditionalServiceToMap = new Domain.Shared.AdditionalService();
            AdditionalServiceToMap.Cost = Convert.ToDecimal(reader["Cost"]);
			AdditionalServiceToMap.CreatedBy = (Guid)reader["CreatedBy"];
			AdditionalServiceToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			AdditionalServiceToMap.Description = reader["Description"].ToString();
			AdditionalServiceToMap.Id = (Guid)reader["Id"];
			AdditionalServiceToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			AdditionalServiceToMap.LCid = Convert.ToInt32(reader["LCid"]);
			AdditionalServiceToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			AdditionalServiceToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			AdditionalServiceToMap.Title = reader["Title"].ToString();
			AdditionalServiceToMap.Version = reader["Version"].ToString();
			return AdditionalServiceToMap;
		}
	}
}
