
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class AdviceTypesMapper : IDataMapper<IDataReader, Domain.Shared.AdviceTypes>
    {
        public Domain.Shared.AdviceTypes Map(IDataReader reader)
        {
			var AdviceTypesToMap = new Domain.Shared.AdviceTypes();
			AdviceTypesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			AdviceTypesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			AdviceTypesToMap.Id = (Guid)reader["Id"];
			AdviceTypesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			AdviceTypesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			AdviceTypesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			AdviceTypesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			AdviceTypesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			AdviceTypesToMap.Name = reader["Name"].ToString();
			AdviceTypesToMap.Version = reader["Version"].ToString();
			return AdviceTypesToMap;
		}
	}
}
