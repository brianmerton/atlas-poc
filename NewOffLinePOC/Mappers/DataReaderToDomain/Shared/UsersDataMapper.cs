
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class UsersMapper : IDataMapper<IDataReader, Domain.Shared.Users>
    {
        public Domain.Shared.Users Map(IDataReader reader)
        {
			var UsersToMap = new Domain.Shared.Users();
			UsersToMap.Area = Convert.ToInt32(reader["Area"]);
			UsersToMap.CompanyId = (Guid)reader["CompanyId"];
			UsersToMap.CreatedBy = (Guid)reader["CreatedBy"];
			UsersToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			UsersToMap.Email = reader["Email"].ToString();
			UsersToMap.FirstName = reader["FirstName"].ToString();
			UsersToMap.HasEmployee = Convert.ToBoolean(reader["HasEmployee"]);
			UsersToMap.Id = (Guid)reader["Id"];
			UsersToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			UsersToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			UsersToMap.jitbitUserId = Convert.ToInt32(reader["jitbitUserId"]);
			UsersToMap.LCid = Convert.ToInt32(reader["LCid"]);
			UsersToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			UsersToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			UsersToMap.PictureId = (Guid)reader["PictureId"];
			UsersToMap.Qualifications = reader["Qualifications"].ToString();
			UsersToMap.SalesforceUserID = reader["SalesforceUserID"].ToString();
			UsersToMap.SecondName = reader["SecondName"].ToString();
			UsersToMap.SignatureFileId = (Guid)reader["SignatureFileId"];
			UsersToMap.Telephone = reader["Telephone"].ToString();
			UsersToMap.Version = reader["Version"].ToString();
			return UsersToMap;
		}
	}
}
