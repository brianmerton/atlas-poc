
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class CheckListItemsMapper : IDataMapper<IDataReader, Domain.Shared.CheckListItems>
    {
        public Domain.Shared.CheckListItems Map(IDataReader reader)
        {
			var CheckListItemsToMap = new Domain.Shared.CheckListItems();
			CheckListItemsToMap.CheckItemId = (Guid)reader["CheckItemId"];
			CheckListItemsToMap.CheckListId = (Guid)reader["CheckListId"];
			return CheckListItemsToMap;
		}
	}
}
