
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class EthnicGroupTypeMapper : IDataMapper<IDataReader, Domain.Shared.EthnicGroupType>
    {
        public Domain.Shared.EthnicGroupType Map(IDataReader reader)
        {
			var EthnicGroupTypeToMap = new Domain.Shared.EthnicGroupType();
			EthnicGroupTypeToMap.CreatedBy = (Guid)reader["CreatedBy"];
			EthnicGroupTypeToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			EthnicGroupTypeToMap.Id = (Guid)reader["Id"];
			EthnicGroupTypeToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			EthnicGroupTypeToMap.LCid = Convert.ToInt32(reader["LCid"]);
			EthnicGroupTypeToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			EthnicGroupTypeToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			EthnicGroupTypeToMap.Name = reader["Name"].ToString();
			EthnicGroupTypeToMap.Version = reader["Version"].ToString();
			return EthnicGroupTypeToMap;
		}
	}
}
