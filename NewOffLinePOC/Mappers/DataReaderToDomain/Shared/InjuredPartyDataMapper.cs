
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class InjuredPartyMapper : IDataMapper<IDataReader, Domain.Shared.InjuredParty>
    {
        public Domain.Shared.InjuredParty Map(IDataReader reader)
        {
			var InjuredPartyToMap = new Domain.Shared.InjuredParty();
			InjuredPartyToMap.CreatedBy = (Guid)reader["CreatedBy"];
			InjuredPartyToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			InjuredPartyToMap.Id = (Guid)reader["Id"];
			InjuredPartyToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			InjuredPartyToMap.LCid = Convert.ToInt32(reader["LCid"]);
			InjuredPartyToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			InjuredPartyToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			InjuredPartyToMap.Name = reader["Name"].ToString();
			InjuredPartyToMap.Version = reader["Version"].ToString();
			return InjuredPartyToMap;
		}
	}
}
