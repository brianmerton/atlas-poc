
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class RolesMapper : IDataMapper<IDataReader, Domain.Shared.Roles>
    {
        public Domain.Shared.Roles Map(IDataReader reader)
        {
			var RolesToMap = new Domain.Shared.Roles();
			RolesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			RolesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			RolesToMap.Description = reader["Description"].ToString();
			RolesToMap.Id = (Guid)reader["Id"];
			RolesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			RolesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			RolesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			RolesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			RolesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			RolesToMap.Name = reader["Name"].ToString();
			RolesToMap.Version = reader["Version"].ToString();
			return RolesToMap;
		}
	}
}
