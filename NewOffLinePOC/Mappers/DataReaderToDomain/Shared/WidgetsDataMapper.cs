
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class WidgetsMapper : IDataMapper<IDataReader, Domain.Shared.Widgets>
    {
        public Domain.Shared.Widgets Map(IDataReader reader)
        {
			var WidgetsToMap = new Domain.Shared.Widgets();
			WidgetsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			WidgetsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			WidgetsToMap.Id = (Guid)reader["Id"];
			WidgetsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			WidgetsToMap.IsLeft = Convert.ToBoolean(reader["IsLeft"]);
			WidgetsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			WidgetsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			WidgetsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			WidgetsToMap.NColumns = Convert.ToInt32(reader["NColumns"]);
			WidgetsToMap.RoleId = (Guid)reader["RoleId"];
			WidgetsToMap.Sequence = Convert.ToInt32(reader["Sequence"]);
			WidgetsToMap.Version = reader["Version"].ToString();
			WidgetsToMap.Widget = Convert.ToInt32(reader["Widget"]);
			return WidgetsToMap;
		}
	}
}
