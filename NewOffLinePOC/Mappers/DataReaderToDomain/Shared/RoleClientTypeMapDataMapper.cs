
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class RoleClientTypeMapMapper : IDataMapper<IDataReader, Domain.Shared.RoleClientTypeMap>
    {
        public Domain.Shared.RoleClientTypeMap Map(IDataReader reader)
        {
			var RoleClientTypeMapToMap = new Domain.Shared.RoleClientTypeMap();
			RoleClientTypeMapToMap.ClientTypeId = (Guid)reader["ClientTypeId"];
			RoleClientTypeMapToMap.RoleId = (Guid)reader["RoleId"];
			return RoleClientTypeMapToMap;
		}
	}
}
