
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class WorkspaceTypesMapper : IDataMapper<IDataReader, Domain.Shared.WorkspaceTypes>
    {
        public Domain.Shared.WorkspaceTypes Map(IDataReader reader)
        {
			var WorkspaceTypesToMap = new Domain.Shared.WorkspaceTypes();
			WorkspaceTypesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			WorkspaceTypesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			WorkspaceTypesToMap.Id = (Guid)reader["Id"];
			WorkspaceTypesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			WorkspaceTypesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			WorkspaceTypesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			WorkspaceTypesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			WorkspaceTypesToMap.Name = reader["Name"].ToString();
			WorkspaceTypesToMap.PictureId = (Guid)reader["PictureId"];
			WorkspaceTypesToMap.Version = reader["Version"].ToString();
			return WorkspaceTypesToMap;
		}
	}
}
