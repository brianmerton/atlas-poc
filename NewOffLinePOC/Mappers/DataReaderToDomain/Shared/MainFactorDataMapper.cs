
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class MainFactorMapper : IDataMapper<IDataReader, Domain.Shared.MainFactor>
    {
        public Domain.Shared.MainFactor Map(IDataReader reader)
        {
			var MainFactorToMap = new Domain.Shared.MainFactor();
			MainFactorToMap.CreatedBy = (Guid)reader["CreatedBy"];
			MainFactorToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			MainFactorToMap.Id = (Guid)reader["Id"];
			MainFactorToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			MainFactorToMap.LCid = Convert.ToInt32(reader["LCid"]);
			MainFactorToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			MainFactorToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			MainFactorToMap.Name = reader["Name"].ToString();
			MainFactorToMap.Version = reader["Version"].ToString();
			return MainFactorToMap;
		}
	}
}
