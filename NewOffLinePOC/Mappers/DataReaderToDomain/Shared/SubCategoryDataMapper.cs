
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class SubCategoryMapper : IDataMapper<IDataReader, Domain.Shared.SubCategory>
    {
        public Domain.Shared.SubCategory Map(IDataReader reader)
        {
			var SubCategoryToMap = new Domain.Shared.SubCategory();
			SubCategoryToMap.CreatedBy = (Guid)reader["CreatedBy"];
			SubCategoryToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			SubCategoryToMap.Id = (Guid)reader["Id"];
			SubCategoryToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			SubCategoryToMap.LCid = Convert.ToInt32(reader["LCid"]);
			SubCategoryToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			SubCategoryToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			SubCategoryToMap.Name = reader["Name"].ToString();
			SubCategoryToMap.Version = reader["Version"].ToString();
			return SubCategoryToMap;
		}
	}
}
