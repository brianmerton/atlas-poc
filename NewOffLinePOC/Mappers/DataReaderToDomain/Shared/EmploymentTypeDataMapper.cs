
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class EmploymentTypeMapper : IDataMapper<IDataReader, Domain.Shared.EmploymentType>
    {
        public Domain.Shared.EmploymentType Map(IDataReader reader)
        {
			var EmploymentTypeToMap = new Domain.Shared.EmploymentType();
			EmploymentTypeToMap.CreatedBy = (Guid)reader["CreatedBy"];
			EmploymentTypeToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			EmploymentTypeToMap.Id = (Guid)reader["Id"];
			EmploymentTypeToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			EmploymentTypeToMap.LCid = Convert.ToInt32(reader["LCid"]);
			EmploymentTypeToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			EmploymentTypeToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			EmploymentTypeToMap.Name = reader["Name"].ToString();
			EmploymentTypeToMap.Version = reader["Version"].ToString();
			return EmploymentTypeToMap;
		}
	}
}
