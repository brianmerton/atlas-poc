
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class EmployeeStatusMapper : IDataMapper<IDataReader, Domain.Shared.EmployeeStatus>
    {
        public Domain.Shared.EmployeeStatus Map(IDataReader reader)
        {
			var EmployeeStatusToMap = new Domain.Shared.EmployeeStatus();
			EmployeeStatusToMap.CreatedBy = (Guid)reader["CreatedBy"];
			EmployeeStatusToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			EmployeeStatusToMap.Id = (Guid)reader["Id"];
			EmployeeStatusToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			EmployeeStatusToMap.LCid = Convert.ToInt32(reader["LCid"]);
			EmployeeStatusToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			EmployeeStatusToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			EmployeeStatusToMap.Name = reader["Name"].ToString();
			EmployeeStatusToMap.Version = reader["Version"].ToString();
			return EmployeeStatusToMap;
		}
	}
}
