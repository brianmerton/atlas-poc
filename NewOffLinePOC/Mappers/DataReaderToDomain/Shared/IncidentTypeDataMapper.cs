
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class IncidentTypeMapper : IDataMapper<IDataReader, Domain.Shared.IncidentType>
    {
        public Domain.Shared.IncidentType Map(IDataReader reader)
        {
			var IncidentTypeToMap = new Domain.Shared.IncidentType();
			IncidentTypeToMap.CreatedBy = (Guid)reader["CreatedBy"];
			IncidentTypeToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			IncidentTypeToMap.Id = (Guid)reader["Id"];
			IncidentTypeToMap.IncidentCategoryId = (Guid)reader["IncidentCategoryId"];
			IncidentTypeToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			IncidentTypeToMap.LCid = Convert.ToInt32(reader["LCid"]);
			IncidentTypeToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			IncidentTypeToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			IncidentTypeToMap.Name = reader["Name"].ToString();
			IncidentTypeToMap.ShouldRIDDOR = Convert.ToBoolean(reader["ShouldRIDDOR"]);
			IncidentTypeToMap.Version = reader["Version"].ToString();
			return IncidentTypeToMap;
		}
	}
}
