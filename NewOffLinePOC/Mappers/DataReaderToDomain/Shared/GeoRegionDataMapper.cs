
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class GeoRegionMapper : IDataMapper<IDataReader, Domain.Shared.GeoRegion>
    {
        public Domain.Shared.GeoRegion Map(IDataReader reader)
        {
			var GeoRegionToMap = new Domain.Shared.GeoRegion();
			GeoRegionToMap.CountryId = (Guid)reader["CountryId"];
			GeoRegionToMap.CreatedBy = (Guid)reader["CreatedBy"];
			GeoRegionToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			GeoRegionToMap.Id = (Guid)reader["Id"];
			GeoRegionToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			GeoRegionToMap.LCid = Convert.ToInt32(reader["LCid"]);
			GeoRegionToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			GeoRegionToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			GeoRegionToMap.Name = reader["Name"].ToString();
			GeoRegionToMap.Version = reader["Version"].ToString();
			return GeoRegionToMap;
		}
	}
}
