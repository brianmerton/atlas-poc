
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class InvSectionsMapper : IDataMapper<IDataReader, Domain.Shared.InvSections>
    {
        public Domain.Shared.InvSections Map(IDataReader reader)
        {
			var InvSectionsToMap = new Domain.Shared.InvSections();
			InvSectionsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			InvSectionsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			InvSectionsToMap.Id = (Guid)reader["Id"];
			InvSectionsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			InvSectionsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			InvSectionsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			InvSectionsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			InvSectionsToMap.ParentSectionId = (Guid)reader["ParentSectionId"];
			InvSectionsToMap.SectionName = reader["SectionName"].ToString();
			InvSectionsToMap.SectorId = (Guid)reader["SectorId"];
			InvSectionsToMap.Sequence = Convert.ToInt32(reader["Sequence"]);
			InvSectionsToMap.Version = reader["Version"].ToString();
			return InvSectionsToMap;
		}
	}
}
