
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class InjuryTypeMapper : IDataMapper<IDataReader, Domain.Shared.InjuryType>
    {
        public Domain.Shared.InjuryType Map(IDataReader reader)
        {
			var InjuryTypeToMap = new Domain.Shared.InjuryType();
			InjuryTypeToMap.CreatedBy = (Guid)reader["CreatedBy"];
			InjuryTypeToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			InjuryTypeToMap.Id = (Guid)reader["Id"];
			InjuryTypeToMap.InjuredPartyId = (Guid)reader["InjuredPartyId"];
			InjuryTypeToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			InjuryTypeToMap.LCid = Convert.ToInt32(reader["LCid"]);
			InjuryTypeToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			InjuryTypeToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			InjuryTypeToMap.Name = reader["Name"].ToString();
			InjuryTypeToMap.NeedRIDDOR = Convert.ToBoolean(reader["NeedRIDDOR"]);
			InjuryTypeToMap.Version = reader["Version"].ToString();
			return InjuryTypeToMap;
		}
	}
}
