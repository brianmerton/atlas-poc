
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class IncidentCategoryMapper : IDataMapper<IDataReader, Domain.Shared.IncidentCategory>
    {
        public Domain.Shared.IncidentCategory Map(IDataReader reader)
        {
			var IncidentCategoryToMap = new Domain.Shared.IncidentCategory();
			IncidentCategoryToMap.CreatedBy = (Guid)reader["CreatedBy"];
			IncidentCategoryToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			IncidentCategoryToMap.Description = reader["Description"].ToString();
			IncidentCategoryToMap.Id = (Guid)reader["Id"];
			IncidentCategoryToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			IncidentCategoryToMap.LCid = Convert.ToInt32(reader["LCid"]);
			IncidentCategoryToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			IncidentCategoryToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			IncidentCategoryToMap.Name = reader["Name"].ToString();
			IncidentCategoryToMap.Version = reader["Version"].ToString();
			return IncidentCategoryToMap;
		}
	}
}
