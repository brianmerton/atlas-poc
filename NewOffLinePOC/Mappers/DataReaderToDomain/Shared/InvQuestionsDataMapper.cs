
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class InvQuestionsMapper : IDataMapper<IDataReader, Domain.Shared.InvQuestions>
    {
        public Domain.Shared.InvQuestions Map(IDataReader reader)
        {
			var InvQuestionsToMap = new Domain.Shared.InvQuestions();
			InvQuestionsToMap.AnswerType = Convert.ToInt32(reader["AnswerType"]);
			InvQuestionsToMap.AttachObjectType = Convert.ToInt32(reader["AttachObjectType"]);
			InvQuestionsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			InvQuestionsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			InvQuestionsToMap.Id = (Guid)reader["Id"];
			InvQuestionsToMap.InvSectionId = (Guid)reader["InvSectionId"];
			InvQuestionsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			InvQuestionsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			InvQuestionsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			InvQuestionsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			InvQuestionsToMap.Question = reader["Question"].ToString();
			InvQuestionsToMap.RelatedAttribute = reader["RelatedAttribute"].ToString();
			InvQuestionsToMap.RelatedObject = Convert.ToInt32(reader["RelatedObject"]);
			InvQuestionsToMap.Sequence = Convert.ToInt32(reader["Sequence"]);
			InvQuestionsToMap.Validations = reader["Validations"].ToString();
			InvQuestionsToMap.Version = reader["Version"].ToString();
			return InvQuestionsToMap;
		}
	}
}
