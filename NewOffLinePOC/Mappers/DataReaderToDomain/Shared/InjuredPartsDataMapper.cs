
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class InjuredPartsMapper : IDataMapper<IDataReader, Domain.Shared.InjuredParts>
    {
        public Domain.Shared.InjuredParts Map(IDataReader reader)
        {
			var InjuredPartsToMap = new Domain.Shared.InjuredParts();
			InjuredPartsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			InjuredPartsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			InjuredPartsToMap.Id = (Guid)reader["Id"];
			InjuredPartsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			InjuredPartsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			InjuredPartsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			InjuredPartsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			InjuredPartsToMap.Name = reader["Name"].ToString();
			InjuredPartsToMap.NeedRIDDOR = Convert.ToBoolean(reader["NeedRIDDOR"]);
			InjuredPartsToMap.Version = reader["Version"].ToString();
			return InjuredPartsToMap;
		}
	}
}
