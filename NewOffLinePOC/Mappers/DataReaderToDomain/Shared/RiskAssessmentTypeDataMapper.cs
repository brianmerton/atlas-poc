
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class RiskAssessmentTypeMapper : IDataMapper<IDataReader, Domain.Shared.RiskAssessmentType>
    {
        public Domain.Shared.RiskAssessmentType Map(IDataReader reader)
        {
			var RiskAssessmentTypeToMap = new Domain.Shared.RiskAssessmentType();
			RiskAssessmentTypeToMap.CreatedBy = (Guid)reader["CreatedBy"];
			RiskAssessmentTypeToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			RiskAssessmentTypeToMap.Id = (Guid)reader["Id"];
			RiskAssessmentTypeToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			RiskAssessmentTypeToMap.LCid = Convert.ToInt32(reader["LCid"]);
			RiskAssessmentTypeToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			RiskAssessmentTypeToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			RiskAssessmentTypeToMap.Name = reader["Name"].ToString();
			RiskAssessmentTypeToMap.PictureId = (Guid)reader["PictureId"];
			RiskAssessmentTypeToMap.Sequence = Convert.ToInt32(reader["Sequence"]);
			RiskAssessmentTypeToMap.Version = reader["Version"].ToString();
			return RiskAssessmentTypeToMap;
		}
	}
}
