
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class InvSectionIncidentCategoriesMapper : IDataMapper<IDataReader, Domain.Shared.InvSectionIncidentCategories>
    {
        public Domain.Shared.InvSectionIncidentCategories Map(IDataReader reader)
        {
			var InvSectionIncidentCategoriesToMap = new Domain.Shared.InvSectionIncidentCategories();
			InvSectionIncidentCategoriesToMap.IncidentCategoryId = (Guid)reader["IncidentCategoryId"];
			InvSectionIncidentCategoriesToMap.InvSectionId = (Guid)reader["InvSectionId"];
			return InvSectionIncidentCategoriesToMap;
		}
	}
}
