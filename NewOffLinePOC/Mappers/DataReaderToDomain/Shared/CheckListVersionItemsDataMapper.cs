
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class CheckListVersionItemsMapper : IDataMapper<IDataReader, Domain.Shared.CheckListVersionItems>
    {
        public Domain.Shared.CheckListVersionItems Map(IDataReader reader)
        {
			var CheckListVersionItemsToMap = new Domain.Shared.CheckListVersionItems();
			CheckListVersionItemsToMap.CheckItemId = (Guid)reader["CheckItemId"];
			CheckListVersionItemsToMap.CheckListVersionId = (Guid)reader["CheckListVersionId"];
			return CheckListVersionItemsToMap;
		}
	}
}
