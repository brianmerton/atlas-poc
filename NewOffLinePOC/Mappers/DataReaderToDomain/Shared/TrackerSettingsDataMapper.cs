
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class TrackerSettingsMapper : IDataMapper<IDataReader, Domain.Shared.TrackerSettings>
    {
        public Domain.Shared.TrackerSettings Map(IDataReader reader)
        {
			var TrackerSettingsToMap = new Domain.Shared.TrackerSettings();
			TrackerSettingsToMap.AppointmentBookedColor = reader["AppointmentBookedColor"].ToString();
			TrackerSettingsToMap.AppointmentCanceleldColor = reader["AppointmentCanceleldColor"].ToString();
			TrackerSettingsToMap.AppointmentCompletedColor = reader["AppointmentCompletedColor"].ToString();
			TrackerSettingsToMap.AppointmentOverdueColor = reader["AppointmentOverdueColor"].ToString();
			TrackerSettingsToMap.AppointmentPlannedColor = reader["AppointmentPlannedColor"].ToString();
			TrackerSettingsToMap.AppointmentRescheduledColor = reader["AppointmentRescheduledColor"].ToString();
			TrackerSettingsToMap.Comment = reader["Comment"].ToString();
			TrackerSettingsToMap.ConsultantMinimalTimeSlot = decimal
			TrackerSettingsToMap.ConsultantWorkStartAt = decimal
			TrackerSettingsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			TrackerSettingsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			TrackerSettingsToMap.Id = (Guid)reader["Id"];
			TrackerSettingsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			TrackerSettingsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			TrackerSettingsToMap.MatchMax = Convert.ToInt32(reader["MatchMax"]);
			TrackerSettingsToMap.MatchStep = Convert.ToInt32(reader["MatchStep"]);
			TrackerSettingsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			TrackerSettingsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			TrackerSettingsToMap.SLAOverdueDays = Convert.ToInt32(reader["SLAOverdueDays"]);
			TrackerSettingsToMap.TempAssignmentDaysAfter = Convert.ToInt32(reader["TempAssignmentDaysAfter"]);
			TrackerSettingsToMap.TempAssignmentDaysBefore = Convert.ToInt32(reader["TempAssignmentDaysBefore"]);
			TrackerSettingsToMap.Version = reader["Version"].ToString();
			return TrackerSettingsToMap;
		}
	}
}
