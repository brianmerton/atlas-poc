
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class AttributesMapper : IDataMapper<IDataReader, Domain.Shared.Attributes>
    {
        public Domain.Shared.Attributes Map(IDataReader reader)
        {
			var AttributesToMap = new Domain.Shared.Attributes();
			AttributesToMap.Area = Convert.ToInt32(reader["Area"]);
			AttributesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			AttributesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			AttributesToMap.DisplayName = reader["DisplayName"].ToString();
			AttributesToMap.Group = reader["Group"].ToString();
			AttributesToMap.Id = (Guid)reader["Id"];
			AttributesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			AttributesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			AttributesToMap.IsMandatory = Convert.ToBoolean(reader["IsMandatory"]);
			AttributesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			AttributesToMap.LogicalName = reader["LogicalName"].ToString();
			AttributesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			AttributesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			AttributesToMap.ObjectTypeCode = reader["ObjectTypeCode"].ToString();
			AttributesToMap.ParentAttributeId = (Guid)reader["ParentAttributeId"];
			AttributesToMap.Type = Convert.ToInt32(reader["Type"]);
			AttributesToMap.Version = reader["Version"].ToString();
			return AttributesToMap;
		}
	}
}
