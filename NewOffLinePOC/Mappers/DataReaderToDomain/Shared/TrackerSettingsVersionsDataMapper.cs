
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class TrackerSettingsVersionsMapper : IDataMapper<IDataReader, Domain.Shared.TrackerSettingsVersions>
    {
        public Domain.Shared.TrackerSettingsVersions Map(IDataReader reader)
        {
			var TrackerSettingsVersionsToMap = new Domain.Shared.TrackerSettingsVersions();
			TrackerSettingsVersionsToMap.AppointmentBookedColor = reader["AppointmentBookedColor"].ToString();
			TrackerSettingsVersionsToMap.AppointmentCanceleldColor = reader["AppointmentCanceleldColor"].ToString();
			TrackerSettingsVersionsToMap.AppointmentCompletedColor = reader["AppointmentCompletedColor"].ToString();
			TrackerSettingsVersionsToMap.AppointmentOverdueColor = reader["AppointmentOverdueColor"].ToString();
			TrackerSettingsVersionsToMap.AppointmentPlannedColor = reader["AppointmentPlannedColor"].ToString();
			TrackerSettingsVersionsToMap.AppointmentRescheduledColor = reader["AppointmentRescheduledColor"].ToString();
			TrackerSettingsVersionsToMap.Comment = reader["Comment"].ToString();
			TrackerSettingsVersionsToMap.ConsultantMinimalTimeSlot = decimal
			TrackerSettingsVersionsToMap.ConsultantWorkStartAt = decimal
			TrackerSettingsVersionsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			TrackerSettingsVersionsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			TrackerSettingsVersionsToMap.Id = (Guid)reader["Id"];
			TrackerSettingsVersionsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			TrackerSettingsVersionsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			TrackerSettingsVersionsToMap.MatchMax = Convert.ToInt32(reader["MatchMax"]);
			TrackerSettingsVersionsToMap.MatchStep = Convert.ToInt32(reader["MatchStep"]);
			TrackerSettingsVersionsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			TrackerSettingsVersionsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			TrackerSettingsVersionsToMap.PrototypeId = (Guid)reader["PrototypeId"];
			TrackerSettingsVersionsToMap.SLAOverdueDays = Convert.ToInt32(reader["SLAOverdueDays"]);
			TrackerSettingsVersionsToMap.TempAssignmentDaysAfter = Convert.ToInt32(reader["TempAssignmentDaysAfter"]);
			TrackerSettingsVersionsToMap.TempAssignmentDaysBefore = Convert.ToInt32(reader["TempAssignmentDaysBefore"]);
			TrackerSettingsVersionsToMap.Version = reader["Version"].ToString();
			return TrackerSettingsVersionsToMap;
		}
	}
}
