
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class MainIndustryMapper : IDataMapper<IDataReader, Domain.Shared.MainIndustry>
    {
        public Domain.Shared.MainIndustry Map(IDataReader reader)
        {
			var MainIndustryToMap = new Domain.Shared.MainIndustry();
			MainIndustryToMap.CreatedBy = (Guid)reader["CreatedBy"];
			MainIndustryToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			MainIndustryToMap.Id = (Guid)reader["Id"];
			MainIndustryToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			MainIndustryToMap.LCid = Convert.ToInt32(reader["LCid"]);
			MainIndustryToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			MainIndustryToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			MainIndustryToMap.Name = reader["Name"].ToString();
			MainIndustryToMap.Version = reader["Version"].ToString();
			return MainIndustryToMap;
		}
	}
}
