
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class CountriesMapper : IDataMapper<IDataReader, Domain.Shared.Countries>
    {
        public Domain.Shared.Countries Map(IDataReader reader)
        {
			var CountriesToMap = new Domain.Shared.Countries();
			CountriesToMap.CreatedBy = (Guid)reader["CreatedBy"];
			CountriesToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			CountriesToMap.Id = (Guid)reader["Id"];
			CountriesToMap.IsArchived = Convert.ToBoolean(reader["IsArchived"]);
			CountriesToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			CountriesToMap.ISOCode = Convert.ToInt32(reader["ISOCode"]);
			CountriesToMap.LCid = Convert.ToInt32(reader["LCid"]);
			CountriesToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			CountriesToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			CountriesToMap.Name = reader["Name"].ToString();
			CountriesToMap.Version = reader["Version"].ToString();
			return CountriesToMap;
		}
	}
}
