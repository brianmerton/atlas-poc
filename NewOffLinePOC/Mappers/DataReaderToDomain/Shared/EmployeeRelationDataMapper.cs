
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class EmployeeRelationMapper : IDataMapper<IDataReader, Domain.Shared.EmployeeRelation>
    {
        public Domain.Shared.EmployeeRelation Map(IDataReader reader)
        {
			var EmployeeRelationToMap = new Domain.Shared.EmployeeRelation();
			EmployeeRelationToMap.CreatedBy = (Guid)reader["CreatedBy"];
			EmployeeRelationToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			EmployeeRelationToMap.Id = (Guid)reader["Id"];
			EmployeeRelationToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			EmployeeRelationToMap.LCid = Convert.ToInt32(reader["LCid"]);
			EmployeeRelationToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			EmployeeRelationToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			EmployeeRelationToMap.Name = reader["Name"].ToString();
			EmployeeRelationToMap.Version = reader["Version"].ToString();
			return EmployeeRelationToMap;
		}
	}
}
