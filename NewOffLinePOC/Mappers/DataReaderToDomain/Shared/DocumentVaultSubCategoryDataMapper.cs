
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class DocumentVaultSubCategoryMapper : IDataMapper<IDataReader, Domain.Shared.DocumentVaultSubCategory>
    {
        public Domain.Shared.DocumentVaultSubCategory Map(IDataReader reader)
        {
			var DocumentVaultSubCategoryToMap = new Domain.Shared.DocumentVaultSubCategory();
			DocumentVaultSubCategoryToMap.CreatedBy = (Guid)reader["CreatedBy"];
			DocumentVaultSubCategoryToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			DocumentVaultSubCategoryToMap.Id = (Guid)reader["Id"];
			DocumentVaultSubCategoryToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			DocumentVaultSubCategoryToMap.LCid = Convert.ToInt32(reader["LCid"]);
			DocumentVaultSubCategoryToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			DocumentVaultSubCategoryToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			DocumentVaultSubCategoryToMap.Name = reader["Name"].ToString();
			DocumentVaultSubCategoryToMap.Version = reader["Version"].ToString();
			return DocumentVaultSubCategoryToMap;
		}
	}
}
