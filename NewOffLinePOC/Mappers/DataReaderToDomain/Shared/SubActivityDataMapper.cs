
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class SubActivityMapper : IDataMapper<IDataReader, Domain.Shared.SubActivity>
    {
        public Domain.Shared.SubActivity Map(IDataReader reader)
        {
			var SubActivityToMap = new Domain.Shared.SubActivity();
			SubActivityToMap.CreatedBy = (Guid)reader["CreatedBy"];
			SubActivityToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			SubActivityToMap.Id = (Guid)reader["Id"];
			SubActivityToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			SubActivityToMap.LCid = Convert.ToInt32(reader["LCid"]);
			SubActivityToMap.MainActivityId = (Guid)reader["MainActivityId"];
			SubActivityToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			SubActivityToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			SubActivityToMap.Name = reader["Name"].ToString();
			SubActivityToMap.SICCode = reader["SICCode"].ToString();
			SubActivityToMap.Version = reader["Version"].ToString();
			return SubActivityToMap;
		}
	}
}
