
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class AccidentTypeMapper : IDataMapper<IDataReader, Domain.Shared.AccidentType>
    {
        public Domain.Shared.AccidentType Map(IDataReader reader)
        {
			var AccidentTypeToMap = new Domain.Shared.AccidentType();
			AccidentTypeToMap.AccidentCategoryId = (Guid)reader["AccidentCategoryId"];
			AccidentTypeToMap.CreatedBy = (Guid)reader["CreatedBy"];
			AccidentTypeToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			AccidentTypeToMap.Id = (Guid)reader["Id"];
			AccidentTypeToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			AccidentTypeToMap.LCid = Convert.ToInt32(reader["LCid"]);
			AccidentTypeToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			AccidentTypeToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			AccidentTypeToMap.Name = reader["Name"].ToString();
			AccidentTypeToMap.ShouldRIDDOR = Convert.ToBoolean(reader["ShouldRIDDOR"]);
			AccidentTypeToMap.Version = reader["Version"].ToString();
			return AccidentTypeToMap;
		}
	}
}
