
using System;
using System.Data;

namespace Mappers.DataReaderToDomain.Shared
{
    public class SiteAssignmentsMapper : IDataMapper<IDataReader, Domain.Shared.SiteAssignments>
    {
        public Domain.Shared.SiteAssignments Map(IDataReader reader)
        {
			var SiteAssignmentsToMap = new Domain.Shared.SiteAssignments();
			SiteAssignmentsToMap.CompanyId = (Guid)reader["CompanyId"];
			SiteAssignmentsToMap.CreatedBy = (Guid)reader["CreatedBy"];
			SiteAssignmentsToMap.CreatedOn = Convert.ToDateTime(reader["CreatedOn"]);
			SiteAssignmentsToMap.Id = (Guid)reader["Id"];
			SiteAssignmentsToMap.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
			SiteAssignmentsToMap.LCid = Convert.ToInt32(reader["LCid"]);
			SiteAssignmentsToMap.ModifiedBy = (Guid)reader["ModifiedBy"];
			SiteAssignmentsToMap.ModifiedOn = Convert.ToDateTime(reader["ModifiedOn"]);
			SiteAssignmentsToMap.SiteId = (Guid)reader["SiteId"];
			SiteAssignmentsToMap.UserId = (Guid)reader["UserId"];
			SiteAssignmentsToMap.Version = reader["Version"].ToString();
			return SiteAssignmentsToMap;
		}
	}
}
