using System;
namespace Infrastructure.Services.Internal
{
    public interface IMenusService
    {
        Guid Add(Domain.Internal.Menus entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Internal.Menus> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.Menus, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Internal.Menus> GetAll();
		Domain.Internal.Menus GetById(Guid id);
		void Update(Domain.Internal.Menus entity);
		
	}
}