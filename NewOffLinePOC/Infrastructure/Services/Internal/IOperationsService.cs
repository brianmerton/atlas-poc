using System;
namespace Infrastructure.Services.Internal
{
    public interface IOperationsService
    {
        Guid Add(Domain.Internal.Operations entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Internal.Operations> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.Operations, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Internal.Operations> GetAll();
		Domain.Internal.Operations GetById(Guid id);
		void Update(Domain.Internal.Operations entity);
		
	}
}