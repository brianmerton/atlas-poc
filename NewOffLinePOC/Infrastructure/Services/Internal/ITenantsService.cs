using System;
namespace Infrastructure.Services.Internal
{
    public interface ITenantsService
    {
        Guid Add(Domain.Internal.Tenants entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Internal.Tenants> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.Tenants, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Internal.Tenants> GetAll();
		Domain.Internal.Tenants GetById(Guid id);
		void Update(Domain.Internal.Tenants entity);
		
	}
}