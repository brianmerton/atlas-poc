using System;
namespace Infrastructure.Services.Internal
{
    public interface IMenuRoleMapService
    {
        Guid Add(Domain.Internal.MenuRoleMap entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Internal.MenuRoleMap> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.MenuRoleMap, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Internal.MenuRoleMap> GetAll();
		Domain.Internal.MenuRoleMap GetById(Guid id);
		void Update(Domain.Internal.MenuRoleMap entity);
		
        System.Collections.Generic.IEnumerable<Domain.Internal.MenuRoleMap> GetByMenuId(Guid MenuId);

        System.Collections.Generic.IEnumerable<Domain.Internal.MenuRoleMap> GetByRoleId(Guid RoleId);

	}
}