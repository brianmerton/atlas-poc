using System;
namespace Infrastructure.Services.Internal
{
    public interface IMigrationsService
    {
        Guid Add(Domain.Internal.Migrations entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Internal.Migrations> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.Migrations, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Internal.Migrations> GetAll();
		Domain.Internal.Migrations GetById(Guid id);
		void Update(Domain.Internal.Migrations entity);
		
	}
}