using System;
namespace Infrastructure.Services.Shared
{
    public interface ITrackerSettingsService
    {
        Guid Add(Domain.Shared.TrackerSettings entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.TrackerSettings> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.TrackerSettings, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.TrackerSettings> GetAll();
		Domain.Shared.TrackerSettings GetById(Guid id);
		void Update(Domain.Shared.TrackerSettings entity);
		
	}
}