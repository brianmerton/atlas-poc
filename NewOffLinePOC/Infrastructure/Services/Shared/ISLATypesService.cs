using System;
namespace Infrastructure.Services.Shared
{
    public interface ISLATypesService
    {
        Guid Add(Domain.Shared.SLATypes entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.SLATypes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.SLATypes, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.SLATypes> GetAll();
		Domain.Shared.SLATypes GetById(Guid id);
		void Update(Domain.Shared.SLATypes entity);
		
	}
}