using System;
namespace Infrastructure.Services.Shared
{
    public interface IMainFactorService
    {
        Guid Add(Domain.Shared.MainFactor entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.MainFactor> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.MainFactor, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.MainFactor> GetAll();
		Domain.Shared.MainFactor GetById(Guid id);
		void Update(Domain.Shared.MainFactor entity);
		
	}
}