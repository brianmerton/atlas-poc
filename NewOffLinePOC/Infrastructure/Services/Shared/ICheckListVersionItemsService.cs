using System;
namespace Infrastructure.Services.Shared
{
    public interface ICheckListVersionItemsService
    {
        Guid Add(Domain.Shared.CheckListVersionItems entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.CheckListVersionItems> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.CheckListVersionItems, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.CheckListVersionItems> GetAll();
		Domain.Shared.CheckListVersionItems GetById(Guid id);
		void Update(Domain.Shared.CheckListVersionItems entity);
		
        System.Collections.Generic.IEnumerable<Domain.Shared.CheckListVersionItems> GetByCheckItemId(Guid CheckItemId);

        System.Collections.Generic.IEnumerable<Domain.Shared.CheckListVersionItems> GetByCheckListVersionId(Guid CheckListVersionId);

	}
}