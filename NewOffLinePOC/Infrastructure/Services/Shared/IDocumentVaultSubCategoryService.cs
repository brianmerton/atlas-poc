using System;
namespace Infrastructure.Services.Shared
{
    public interface IDocumentVaultSubCategoryService
    {
        Guid Add(Domain.Shared.DocumentVaultSubCategory entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.DocumentVaultSubCategory> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.DocumentVaultSubCategory, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.DocumentVaultSubCategory> GetAll();
		Domain.Shared.DocumentVaultSubCategory GetById(Guid id);
		void Update(Domain.Shared.DocumentVaultSubCategory entity);
		
	}
}