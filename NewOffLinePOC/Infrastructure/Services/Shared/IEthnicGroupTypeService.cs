using System;
namespace Infrastructure.Services.Shared
{
    public interface IEthnicGroupTypeService
    {
        Guid Add(Domain.Shared.EthnicGroupType entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.EthnicGroupType> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.EthnicGroupType, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.EthnicGroupType> GetAll();
		Domain.Shared.EthnicGroupType GetById(Guid id);
		void Update(Domain.Shared.EthnicGroupType entity);
		
	}
}