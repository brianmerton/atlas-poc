using System;
namespace Infrastructure.Services.Shared
{
    public interface IWidgetsService
    {
        Guid Add(Domain.Shared.Widgets entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.Widgets> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Widgets, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.Widgets> GetAll();
		Domain.Shared.Widgets GetById(Guid id);
		void Update(Domain.Shared.Widgets entity);
		
	}
}