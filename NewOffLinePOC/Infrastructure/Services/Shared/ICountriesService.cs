using System;
namespace Infrastructure.Services.Shared
{
    public interface ICountriesService
    {
        Guid Add(Domain.Shared.Countries entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.Countries> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Countries, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.Countries> GetAll();
		Domain.Shared.Countries GetById(Guid id);
		void Update(Domain.Shared.Countries entity);
		
	}
}