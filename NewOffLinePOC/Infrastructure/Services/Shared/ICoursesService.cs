using System;
namespace Infrastructure.Services.Shared
{
    public interface ICoursesService
    {
        Guid Add(Domain.Shared.Courses entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.Courses> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Courses, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.Courses> GetAll();
		Domain.Shared.Courses GetById(Guid id);
		void Update(Domain.Shared.Courses entity);
		
	}
}