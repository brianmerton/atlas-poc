using System;
namespace Infrastructure.Services.Shared
{
    public interface IOperationsService
    {
        Guid Add(Domain.Shared.Operations entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.Operations> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Operations, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.Operations> GetAll();
		Domain.Shared.Operations GetById(Guid id);
		void Update(Domain.Shared.Operations entity);
		
	}
}