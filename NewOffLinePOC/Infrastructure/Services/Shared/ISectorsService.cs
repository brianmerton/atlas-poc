using System;
namespace Infrastructure.Services.Shared
{
    public interface ISectorsService
    {
        Guid Add(Domain.Shared.Sectors entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.Sectors> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Sectors, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.Sectors> GetAll();
		Domain.Shared.Sectors GetById(Guid id);
		void Update(Domain.Shared.Sectors entity);
		
	}
}