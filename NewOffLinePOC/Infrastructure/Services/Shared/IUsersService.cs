using System;
namespace Infrastructure.Services.Shared
{
    public interface IUsersService
    {
        Guid Add(Domain.Shared.Users entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.Users> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Users, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.Users> GetAll();
		Domain.Shared.Users GetById(Guid id);
		void Update(Domain.Shared.Users entity);
		
	}
}