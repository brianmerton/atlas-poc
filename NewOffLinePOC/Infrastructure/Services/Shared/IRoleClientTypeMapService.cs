using System;
namespace Infrastructure.Services.Shared
{
    public interface IRoleClientTypeMapService
    {
        Guid Add(Domain.Shared.RoleClientTypeMap entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.RoleClientTypeMap> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.RoleClientTypeMap, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.RoleClientTypeMap> GetAll();
		Domain.Shared.RoleClientTypeMap GetById(Guid id);
		void Update(Domain.Shared.RoleClientTypeMap entity);
		
        System.Collections.Generic.IEnumerable<Domain.Shared.RoleClientTypeMap> GetByClientTypeId(Guid ClientTypeId);

        System.Collections.Generic.IEnumerable<Domain.Shared.RoleClientTypeMap> GetByRoleId(Guid RoleId);

	}
}