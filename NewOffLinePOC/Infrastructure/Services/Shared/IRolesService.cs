using System;
namespace Infrastructure.Services.Shared
{
    public interface IRolesService
    {
        Guid Add(Domain.Shared.Roles entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.Roles> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Roles, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.Roles> GetAll();
		Domain.Shared.Roles GetById(Guid id);
		void Update(Domain.Shared.Roles entity);
		
	}
}