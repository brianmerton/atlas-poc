using System;
namespace Infrastructure.Services.Shared
{
    public interface IMainActivityService
    {
        Guid Add(Domain.Shared.MainActivity entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.MainActivity> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.MainActivity, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.MainActivity> GetAll();
		Domain.Shared.MainActivity GetById(Guid id);
		void Update(Domain.Shared.MainActivity entity);
		
	}
}