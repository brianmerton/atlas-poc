using System;
namespace Infrastructure.Services.Shared
{
    public interface ICheckListsService
    {
        Guid Add(Domain.Shared.CheckLists entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.CheckLists> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.CheckLists, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.CheckLists> GetAll();
		Domain.Shared.CheckLists GetById(Guid id);
		void Update(Domain.Shared.CheckLists entity);
		
	}
}