using System;
namespace Infrastructure.Services.Shared
{
    public interface IWorkProcessService
    {
        Guid Add(Domain.Shared.WorkProcess entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.WorkProcess> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.WorkProcess, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.WorkProcess> GetAll();
		Domain.Shared.WorkProcess GetById(Guid id);
		void Update(Domain.Shared.WorkProcess entity);
		
	}
}