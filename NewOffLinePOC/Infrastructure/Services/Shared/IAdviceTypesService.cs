using System;
namespace Infrastructure.Services.Shared
{
    public interface IAdviceTypesService
    {
        Guid Add(Domain.Shared.AdviceTypes entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.AdviceTypes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.AdviceTypes, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.AdviceTypes> GetAll();
		Domain.Shared.AdviceTypes GetById(Guid id);
		void Update(Domain.Shared.AdviceTypes entity);
		
	}
}