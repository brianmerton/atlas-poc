using System;
namespace Infrastructure.Services.Shared
{
    public interface ISiteAssignmentsService
    {
        Guid Add(Domain.Shared.SiteAssignments entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.SiteAssignments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.SiteAssignments, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.SiteAssignments> GetAll();
		Domain.Shared.SiteAssignments GetById(Guid id);
		void Update(Domain.Shared.SiteAssignments entity);
		
	}
}