using System;
namespace Infrastructure.Services.Shared
{
    public interface IEthnicGroupValueService
    {
        Guid Add(Domain.Shared.EthnicGroupValue entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.EthnicGroupValue> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.EthnicGroupValue, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.EthnicGroupValue> GetAll();
		Domain.Shared.EthnicGroupValue GetById(Guid id);
		void Update(Domain.Shared.EthnicGroupValue entity);
		
	}
}