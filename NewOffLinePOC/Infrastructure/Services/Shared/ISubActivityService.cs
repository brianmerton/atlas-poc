using System;
namespace Infrastructure.Services.Shared
{
    public interface ISubActivityService
    {
        Guid Add(Domain.Shared.SubActivity entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.SubActivity> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.SubActivity, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.SubActivity> GetAll();
		Domain.Shared.SubActivity GetById(Guid id);
		void Update(Domain.Shared.SubActivity entity);
		
	}
}