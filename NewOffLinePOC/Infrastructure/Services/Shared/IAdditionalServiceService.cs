using System;
namespace Infrastructure.Services.Shared
{
    public interface IAdditionalServiceService
    {
        Guid Add(Domain.Shared.AdditionalService entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.AdditionalService> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.AdditionalService, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.AdditionalService> GetAll();
		Domain.Shared.AdditionalService GetById(Guid id);
		void Update(Domain.Shared.AdditionalService entity);
		
	}
}