using System;
namespace Infrastructure.Services.Shared
{
    public interface IVisitTypesService
    {
        Guid Add(Domain.Shared.VisitTypes entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.VisitTypes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.VisitTypes, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.VisitTypes> GetAll();
		Domain.Shared.VisitTypes GetById(Guid id);
		void Update(Domain.Shared.VisitTypes entity);
		
        System.Collections.Generic.IEnumerable<Domain.Shared.VisitTypes> GetByClientTypeId(Guid ClientTypeId);

	}
}