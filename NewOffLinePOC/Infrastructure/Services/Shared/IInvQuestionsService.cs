using System;
namespace Infrastructure.Services.Shared
{
    public interface IInvQuestionsService
    {
        Guid Add(Domain.Shared.InvQuestions entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.InvQuestions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.InvQuestions, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.InvQuestions> GetAll();
		Domain.Shared.InvQuestions GetById(Guid id);
		void Update(Domain.Shared.InvQuestions entity);
		
        System.Collections.Generic.IEnumerable<Domain.Shared.InvQuestions> GetByInvSectionId(Guid InvSectionId);

	}
}