using System;
namespace Infrastructure.Services.Shared
{
    public interface IOptionsService
    {
        Guid Add(Domain.Shared.Options entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.Options> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Options, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.Options> GetAll();
		Domain.Shared.Options GetById(Guid id);
		void Update(Domain.Shared.Options entity);
		
	}
}