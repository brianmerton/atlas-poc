using System;
namespace Infrastructure.Services.Shared
{
    public interface IAccidentCategoryService
    {
        Guid Add(Domain.Shared.AccidentCategory entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.AccidentCategory> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.AccidentCategory, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.AccidentCategory> GetAll();
		Domain.Shared.AccidentCategory GetById(Guid id);
		void Update(Domain.Shared.AccidentCategory entity);
		
	}
}