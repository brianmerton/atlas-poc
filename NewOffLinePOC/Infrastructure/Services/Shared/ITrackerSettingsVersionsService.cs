using System;
namespace Infrastructure.Services.Shared
{
    public interface ITrackerSettingsVersionsService
    {
        Guid Add(Domain.Shared.TrackerSettingsVersions entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.TrackerSettingsVersions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.TrackerSettingsVersions, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.TrackerSettingsVersions> GetAll();
		Domain.Shared.TrackerSettingsVersions GetById(Guid id);
		void Update(Domain.Shared.TrackerSettingsVersions entity);
		
	}
}