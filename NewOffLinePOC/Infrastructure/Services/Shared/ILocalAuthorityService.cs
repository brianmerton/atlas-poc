using System;
namespace Infrastructure.Services.Shared
{
    public interface ILocalAuthorityService
    {
        Guid Add(Domain.Shared.LocalAuthority entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.LocalAuthority> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.LocalAuthority, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.LocalAuthority> GetAll();
		Domain.Shared.LocalAuthority GetById(Guid id);
		void Update(Domain.Shared.LocalAuthority entity);
		
	}
}