using System;
namespace Infrastructure.Services.Shared
{
    public interface IInjuredPartsService
    {
        Guid Add(Domain.Shared.InjuredParts entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.InjuredParts> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.InjuredParts, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.InjuredParts> GetAll();
		Domain.Shared.InjuredParts GetById(Guid id);
		void Update(Domain.Shared.InjuredParts entity);
		
	}
}