using System;
namespace Infrastructure.Services.Shared
{
    public interface ISectorWorkspaceMapService
    {
        Guid Add(Domain.Shared.SectorWorkspaceMap entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.SectorWorkspaceMap> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.SectorWorkspaceMap, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.SectorWorkspaceMap> GetAll();
		Domain.Shared.SectorWorkspaceMap GetById(Guid id);
		void Update(Domain.Shared.SectorWorkspaceMap entity);
		
        System.Collections.Generic.IEnumerable<Domain.Shared.SectorWorkspaceMap> GetBySectorId(Guid SectorId);

        System.Collections.Generic.IEnumerable<Domain.Shared.SectorWorkspaceMap> GetByWorkspaceTypeId(Guid WorkspaceTypeId);

	}
}