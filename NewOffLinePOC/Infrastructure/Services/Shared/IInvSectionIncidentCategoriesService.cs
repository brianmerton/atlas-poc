using System;
namespace Infrastructure.Services.Shared
{
    public interface IInvSectionIncidentCategoriesService
    {
        Guid Add(Domain.Shared.InvSectionIncidentCategories entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.InvSectionIncidentCategories> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.InvSectionIncidentCategories, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.InvSectionIncidentCategories> GetAll();
		Domain.Shared.InvSectionIncidentCategories GetById(Guid id);
		void Update(Domain.Shared.InvSectionIncidentCategories entity);
		
        System.Collections.Generic.IEnumerable<Domain.Shared.InvSectionIncidentCategories> GetByIncidentCategoryId(Guid IncidentCategoryId);

        System.Collections.Generic.IEnumerable<Domain.Shared.InvSectionIncidentCategories> GetByInvSectionId(Guid InvSectionId);

	}
}