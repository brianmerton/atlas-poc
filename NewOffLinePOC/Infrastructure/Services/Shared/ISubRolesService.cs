using System;
namespace Infrastructure.Services.Shared
{
    public interface ISubRolesService
    {
        Guid Add(Domain.Shared.SubRoles entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.SubRoles> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.SubRoles, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.SubRoles> GetAll();
		Domain.Shared.SubRoles GetById(Guid id);
		void Update(Domain.Shared.SubRoles entity);
		
        System.Collections.Generic.IEnumerable<Domain.Shared.SubRoles> GetByCreatedBy(Guid CreatedBy);

        System.Collections.Generic.IEnumerable<Domain.Shared.SubRoles> GetByModifiedBy(Guid ModifiedBy);

	}
}