using System;
namespace Infrastructure.Services.Shared
{
    public interface IAttributesService
    {
        Guid Add(Domain.Shared.Attributes entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.Attributes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Attributes, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.Attributes> GetAll();
		Domain.Shared.Attributes GetById(Guid id);
		void Update(Domain.Shared.Attributes entity);
		
	}
}