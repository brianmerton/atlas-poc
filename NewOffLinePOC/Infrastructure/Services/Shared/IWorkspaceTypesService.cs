using System;
namespace Infrastructure.Services.Shared
{
    public interface IWorkspaceTypesService
    {
        Guid Add(Domain.Shared.WorkspaceTypes entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Shared.WorkspaceTypes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.WorkspaceTypes, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Shared.WorkspaceTypes> GetAll();
		Domain.Shared.WorkspaceTypes GetById(Guid id);
		void Update(Domain.Shared.WorkspaceTypes entity);
		
	}
}