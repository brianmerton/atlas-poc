using System;
namespace Infrastructure.Services.Incidents
{
    public interface IAutomationRulesService
    {
        Guid Add(Domain.Incidents.AutomationRules entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.AutomationRules> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.AutomationRules, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.AutomationRules> GetAll();
		Domain.Incidents.AutomationRules GetById(Guid id);
		void Update(Domain.Incidents.AutomationRules entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.AutomationRules> GetByRuleID(Guid RuleID);

	}
}