using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdCategorySubmitPermissionsService
    {
        Guid Add(Domain.Incidents.hdCategorySubmitPermissions entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdCategorySubmitPermissions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdCategorySubmitPermissions, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdCategorySubmitPermissions> GetAll();
		Domain.Incidents.hdCategorySubmitPermissions GetById(Guid id);
		void Update(Domain.Incidents.hdCategorySubmitPermissions entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCategorySubmitPermissions> GetByCategoryID(Guid CategoryID);

        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCategorySubmitPermissions> GetByUserID(Guid UserID);

	}
}