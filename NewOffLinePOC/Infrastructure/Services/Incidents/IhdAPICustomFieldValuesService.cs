using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdAPICustomFieldValuesService
    {
        Guid Add(Domain.Incidents.hdAPICustomFieldValues entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAPICustomFieldValues> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAPICustomFieldValues, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAPICustomFieldValues> GetAll();
		Domain.Incidents.hdAPICustomFieldValues GetById(Guid id);
		void Update(Domain.Incidents.hdAPICustomFieldValues entity);
		
	}
}