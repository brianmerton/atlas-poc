using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdAssetSuppliersService
    {
        Guid Add(Domain.Incidents.hdAssetSuppliers entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetSuppliers> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAssetSuppliers, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetSuppliers> GetAll();
		Domain.Incidents.hdAssetSuppliers GetById(Guid id);
		void Update(Domain.Incidents.hdAssetSuppliers entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetSuppliers> GetBySupplierID(Guid SupplierID);

	}
}