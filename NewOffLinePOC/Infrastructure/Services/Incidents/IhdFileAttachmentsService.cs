using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdFileAttachmentsService
    {
        Guid Add(Domain.Incidents.hdFileAttachments entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdFileAttachments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdFileAttachments, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdFileAttachments> GetAll();
		Domain.Incidents.hdFileAttachments GetById(Guid id);
		void Update(Domain.Incidents.hdFileAttachments entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdFileAttachments> GetByFileID(Guid FileID);

	}
}