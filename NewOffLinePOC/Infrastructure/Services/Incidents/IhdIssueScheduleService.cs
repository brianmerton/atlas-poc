using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdIssueScheduleService
    {
        Guid Add(Domain.Incidents.hdIssueSchedule entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssueSchedule> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdIssueSchedule, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssueSchedule> GetAll();
		Domain.Incidents.hdIssueSchedule GetById(Guid id);
		void Update(Domain.Incidents.hdIssueSchedule entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssueSchedule> GetByIssueID(Guid IssueID);

	}
}