using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdMergedIssuesService
    {
        Guid Add(Domain.Incidents.hdMergedIssues entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdMergedIssues> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdMergedIssues, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdMergedIssues> GetAll();
		Domain.Incidents.hdMergedIssues GetById(Guid id);
		void Update(Domain.Incidents.hdMergedIssues entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdMergedIssues> GetByIssueID(Guid IssueID);

        System.Collections.Generic.IEnumerable<Domain.Incidents.hdMergedIssues> GetByIssueMergedID(Guid IssueMergedID);

	}
}