using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdIssueSubscribersService
    {
        Guid Add(Domain.Incidents.hdIssueSubscribers entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssueSubscribers> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdIssueSubscribers, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssueSubscribers> GetAll();
		Domain.Incidents.hdIssueSubscribers GetById(Guid id);
		void Update(Domain.Incidents.hdIssueSubscribers entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssueSubscribers> GetByIssueID(Guid IssueID);

        System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssueSubscribers> GetByUserID(Guid UserID);

	}
}