using System;
namespace Infrastructure.Services.Incidents
{
    public interface IOverdueAutomationLogService
    {
        Guid Add(Domain.Incidents.OverdueAutomationLog entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.OverdueAutomationLog> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.OverdueAutomationLog, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.OverdueAutomationLog> GetAll();
		Domain.Incidents.OverdueAutomationLog GetById(Guid id);
		void Update(Domain.Incidents.OverdueAutomationLog entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.OverdueAutomationLog> GetByIssueID(Guid IssueID);

        System.Collections.Generic.IEnumerable<Domain.Incidents.OverdueAutomationLog> GetByRuleID(Guid RuleID);

	}
}