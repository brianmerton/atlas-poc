using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdCommentTemplatesService
    {
        Guid Add(Domain.Incidents.hdCommentTemplates entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdCommentTemplates> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdCommentTemplates, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdCommentTemplates> GetAll();
		Domain.Incidents.hdCommentTemplates GetById(Guid id);
		void Update(Domain.Incidents.hdCommentTemplates entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCommentTemplates> GetByTemplateID(Guid TemplateID);

	}
}