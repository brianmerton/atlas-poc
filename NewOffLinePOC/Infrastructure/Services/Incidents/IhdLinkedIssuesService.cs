using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdLinkedIssuesService
    {
        Guid Add(Domain.Incidents.hdLinkedIssues entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdLinkedIssues> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdLinkedIssues, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdLinkedIssues> GetAll();
		Domain.Incidents.hdLinkedIssues GetById(Guid id);
		void Update(Domain.Incidents.hdLinkedIssues entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdLinkedIssues> GetByIssue1ID(Guid Issue1ID);

        System.Collections.Generic.IEnumerable<Domain.Incidents.hdLinkedIssues> GetByIssue2ID(Guid Issue2ID);

	}
}