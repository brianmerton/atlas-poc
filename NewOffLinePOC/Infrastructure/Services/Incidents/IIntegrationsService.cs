using System;
namespace Infrastructure.Services.Incidents
{
    public interface IIntegrationsService
    {
        Guid Add(Domain.Incidents.Integrations entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.Integrations> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.Integrations, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.Integrations> GetAll();
		Domain.Incidents.Integrations GetById(Guid id);
		void Update(Domain.Incidents.Integrations entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.Integrations> GetByInstanceID(Guid InstanceID);

	}
}