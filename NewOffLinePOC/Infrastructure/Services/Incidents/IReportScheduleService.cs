using System;
namespace Infrastructure.Services.Incidents
{
    public interface IReportScheduleService
    {
        Guid Add(Domain.Incidents.ReportSchedule entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.ReportSchedule> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.ReportSchedule, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.ReportSchedule> GetAll();
		Domain.Incidents.ReportSchedule GetById(Guid id);
		void Update(Domain.Incidents.ReportSchedule entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.ReportSchedule> GetByInstanceID(Guid InstanceID);

	}
}