using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdAssetTypesService
    {
        Guid Add(Domain.Incidents.hdAssetTypes entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetTypes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAssetTypes, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetTypes> GetAll();
		Domain.Incidents.hdAssetTypes GetById(Guid id);
		void Update(Domain.Incidents.hdAssetTypes entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetTypes> GetByTypeID(Guid TypeID);

	}
}