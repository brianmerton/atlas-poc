using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdCategoriesService
    {
        Guid Add(Domain.Incidents.hdCategories entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdCategories> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdCategories, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdCategories> GetAll();
		Domain.Incidents.hdCategories GetById(Guid id);
		void Update(Domain.Incidents.hdCategories entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCategories> GetByCategoryID(Guid CategoryID);

	}
}