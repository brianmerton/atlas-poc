using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdAssetManufacturersService
    {
        Guid Add(Domain.Incidents.hdAssetManufacturers entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetManufacturers> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAssetManufacturers, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetManufacturers> GetAll();
		Domain.Incidents.hdAssetManufacturers GetById(Guid id);
		void Update(Domain.Incidents.hdAssetManufacturers entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetManufacturers> GetByManufacturerID(Guid ManufacturerID);

	}
}