using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdTagsService
    {
        Guid Add(Domain.Incidents.hdTags entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdTags> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdTags, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdTags> GetAll();
		Domain.Incidents.hdTags GetById(Guid id);
		void Update(Domain.Incidents.hdTags entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdTags> GetByTagID(Guid TagID);

	}
}