using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdAPICustomFieldParamsService
    {
        Guid Add(Domain.Incidents.hdAPICustomFieldParams entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAPICustomFieldParams> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAPICustomFieldParams, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdAPICustomFieldParams> GetAll();
		Domain.Incidents.hdAPICustomFieldParams GetById(Guid id);
		void Update(Domain.Incidents.hdAPICustomFieldParams entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAPICustomFieldParams> GetByAPICustomFieldParamsID(Guid APICustomFieldParamsID);

        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAPICustomFieldParams> GetByAPICustomFieldSettingsID(Guid APICustomFieldSettingsID);

	}
}