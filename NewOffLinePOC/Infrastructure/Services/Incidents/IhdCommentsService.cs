using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdCommentsService
    {
        Guid Add(Domain.Incidents.hdComments entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdComments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdComments, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdComments> GetAll();
		Domain.Incidents.hdComments GetById(Guid id);
		void Update(Domain.Incidents.hdComments entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdComments> GetByCommentID(Guid CommentID);

        System.Collections.Generic.IEnumerable<Domain.Incidents.hdComments> GetByIssueID(Guid IssueID);

        System.Collections.Generic.IEnumerable<Domain.Incidents.hdComments> GetByUserID(Guid UserID);

	}
}