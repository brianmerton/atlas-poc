using System;
namespace Infrastructure.Services.Incidents
{
    public interface INonupdatedAutomationLogService
    {
        Guid Add(Domain.Incidents.NonupdatedAutomationLog entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.NonupdatedAutomationLog> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.NonupdatedAutomationLog, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.NonupdatedAutomationLog> GetAll();
		Domain.Incidents.NonupdatedAutomationLog GetById(Guid id);
		void Update(Domain.Incidents.NonupdatedAutomationLog entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.NonupdatedAutomationLog> GetByIssueID(Guid IssueID);

        System.Collections.Generic.IEnumerable<Domain.Incidents.NonupdatedAutomationLog> GetByRuleID(Guid RuleID);

	}
}