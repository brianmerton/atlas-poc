using System;
namespace Infrastructure.Services.Incidents
{
    public interface IhdPOPServersService
    {
        Guid Add(Domain.Incidents.hdPOPServers entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdPOPServers> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdPOPServers, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Incidents.hdPOPServers> GetAll();
		Domain.Incidents.hdPOPServers GetById(Guid id);
		void Update(Domain.Incidents.hdPOPServers entity);
		
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdPOPServers> GetByServerID(Guid ServerID);

	}
}