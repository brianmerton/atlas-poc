using System;
namespace Infrastructure.Services.Citation
{
    public interface IBlockInContainerService
    {
        Guid Add(Domain.Citation.BlockInContainer entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockInContainer> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.BlockInContainer, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockInContainer> GetAll();
		Domain.Citation.BlockInContainer GetById(Guid id);
		void Update(Domain.Citation.BlockInContainer entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.BlockInContainer> GetByBlockId(Guid BlockId);

        System.Collections.Generic.IEnumerable<Domain.Citation.BlockInContainer> GetByContainerId(Guid ContainerId);

	}
}