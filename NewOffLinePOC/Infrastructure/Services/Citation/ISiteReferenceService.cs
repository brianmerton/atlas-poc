using System;
namespace Infrastructure.Services.Citation
{
    public interface ISiteReferenceService
    {
        Guid Add(Domain.Citation.SiteReference entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.SiteReference> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.SiteReference, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.SiteReference> GetAll();
		Domain.Citation.SiteReference GetById(Guid id);
		void Update(Domain.Citation.SiteReference entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.SiteReference> GetByCompanyId(Guid CompanyId);

	}
}