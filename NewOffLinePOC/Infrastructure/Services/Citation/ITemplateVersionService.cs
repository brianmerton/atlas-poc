using System;
namespace Infrastructure.Services.Citation
{
    public interface ITemplateVersionService
    {
        Guid Add(Domain.Citation.TemplateVersion entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.TemplateVersion> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.TemplateVersion, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.TemplateVersion> GetAll();
		Domain.Citation.TemplateVersion GetById(Guid id);
		void Update(Domain.Citation.TemplateVersion entity);
		
	}
}