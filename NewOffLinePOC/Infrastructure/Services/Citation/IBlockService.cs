using System;
namespace Infrastructure.Services.Citation
{
    public interface IBlockService
    {
        Guid Add(Domain.Citation.Block entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.Block> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.Block, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.Block> GetAll();
		Domain.Citation.Block GetById(Guid id);
		void Update(Domain.Citation.Block entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.Block> GetByCountryId(Guid CountryId);

        System.Collections.Generic.IEnumerable<Domain.Citation.Block> GetBySectorId(Guid SectorId);

        System.Collections.Generic.IEnumerable<Domain.Citation.Block> GetBySubCategoryId(Guid SubCategoryId);

	}
}