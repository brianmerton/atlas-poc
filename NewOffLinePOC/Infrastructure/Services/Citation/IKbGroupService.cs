using System;
namespace Infrastructure.Services.Citation
{
    public interface IKbGroupService
    {
        Guid Add(Domain.Citation.KbGroup entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.KbGroup> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.KbGroup, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.KbGroup> GetAll();
		Domain.Citation.KbGroup GetById(Guid id);
		void Update(Domain.Citation.KbGroup entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.KbGroup> GetByCreatedBy(Guid CreatedBy);

        System.Collections.Generic.IEnumerable<Domain.Citation.KbGroup> GetByModifiedBy(Guid ModifiedBy);

	}
}