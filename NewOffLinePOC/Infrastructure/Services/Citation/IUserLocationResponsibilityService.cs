using System;
namespace Infrastructure.Services.Citation
{
    public interface IUserLocationResponsibilityService
    {
        Guid Add(Domain.Citation.UserLocationResponsibility entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.UserLocationResponsibility> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.UserLocationResponsibility, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.UserLocationResponsibility> GetAll();
		Domain.Citation.UserLocationResponsibility GetById(Guid id);
		void Update(Domain.Citation.UserLocationResponsibility entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.UserLocationResponsibility> GetByUserId(Guid UserId);

	}
}