using System;
namespace Infrastructure.Services.Citation
{
    public interface IBlockVersionService
    {
        Guid Add(Domain.Citation.BlockVersion entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersion> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.BlockVersion, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersion> GetAll();
		Domain.Citation.BlockVersion GetById(Guid id);
		void Update(Domain.Citation.BlockVersion entity);
		
	}
}