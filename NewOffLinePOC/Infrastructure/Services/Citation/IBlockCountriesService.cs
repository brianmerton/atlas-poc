using System;
namespace Infrastructure.Services.Citation
{
    public interface IBlockCountriesService
    {
        Guid Add(Domain.Citation.BlockCountries entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockCountries> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.BlockCountries, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockCountries> GetAll();
		Domain.Citation.BlockCountries GetById(Guid id);
		void Update(Domain.Citation.BlockCountries entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.BlockCountries> GetByBlockId(Guid BlockId);

        System.Collections.Generic.IEnumerable<Domain.Citation.BlockCountries> GetByCountryId(Guid CountryId);

	}
}