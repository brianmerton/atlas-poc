using System;
namespace Infrastructure.Services.Citation
{
    public interface IWorkItemsService
    {
        Guid Add(Domain.Citation.WorkItems entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.WorkItems> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.WorkItems, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.WorkItems> GetAll();
		Domain.Citation.WorkItems GetById(Guid id);
		void Update(Domain.Citation.WorkItems entity);
		
	}
}