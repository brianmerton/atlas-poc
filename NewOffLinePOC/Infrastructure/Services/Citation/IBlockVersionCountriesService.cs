using System;
namespace Infrastructure.Services.Citation
{
    public interface IBlockVersionCountriesService
    {
        Guid Add(Domain.Citation.BlockVersionCountries entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersionCountries> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.BlockVersionCountries, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersionCountries> GetAll();
		Domain.Citation.BlockVersionCountries GetById(Guid id);
		void Update(Domain.Citation.BlockVersionCountries entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersionCountries> GetByBlockVersionId(Guid BlockVersionId);

        System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersionCountries> GetByCountryId(Guid CountryId);

	}
}