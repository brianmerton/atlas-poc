using System;
namespace Infrastructure.Services.Citation
{
    public interface ICompaniesService
    {
        Guid Add(Domain.Citation.Companies entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.Companies> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.Companies, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.Companies> GetAll();
		Domain.Citation.Companies GetById(Guid id);
		void Update(Domain.Citation.Companies entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.Companies> GetBySectorId(Guid SectorId);

	}
}