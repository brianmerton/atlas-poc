using System;
namespace Infrastructure.Services.Citation
{
    public interface ILogsService
    {
        Guid Add(Domain.Citation.Logs entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.Logs> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.Logs, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.Logs> GetAll();
		Domain.Citation.Logs GetById(Guid id);
		void Update(Domain.Citation.Logs entity);
		
	}
}