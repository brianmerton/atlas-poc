using System;
namespace Infrastructure.Services.Citation
{
    public interface ICompanyClientTypesService
    {
        Guid Add(Domain.Citation.CompanyClientTypes entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.CompanyClientTypes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.CompanyClientTypes, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.CompanyClientTypes> GetAll();
		Domain.Citation.CompanyClientTypes GetById(Guid id);
		void Update(Domain.Citation.CompanyClientTypes entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.CompanyClientTypes> GetByClientTypeId(Guid ClientTypeId);

        System.Collections.Generic.IEnumerable<Domain.Citation.CompanyClientTypes> GetByCompanyId(Guid CompanyId);

	}
}