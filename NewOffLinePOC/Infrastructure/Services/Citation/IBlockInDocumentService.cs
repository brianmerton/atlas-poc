using System;
namespace Infrastructure.Services.Citation
{
    public interface IBlockInDocumentService
    {
        Guid Add(Domain.Citation.BlockInDocument entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockInDocument> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.BlockInDocument, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockInDocument> GetAll();
		Domain.Citation.BlockInDocument GetById(Guid id);
		void Update(Domain.Citation.BlockInDocument entity);
		
	}
}