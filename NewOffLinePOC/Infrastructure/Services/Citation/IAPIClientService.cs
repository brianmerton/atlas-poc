using System;
namespace Infrastructure.Services.Citation
{
    public interface IAPIClientService
    {
        Guid Add(Domain.Citation.APIClient entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.APIClient> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.APIClient, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.APIClient> GetAll();
		Domain.Citation.APIClient GetById(Guid id);
		void Update(Domain.Citation.APIClient entity);
		
	}
}