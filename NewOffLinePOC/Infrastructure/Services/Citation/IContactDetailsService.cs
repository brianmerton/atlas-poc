using System;
namespace Infrastructure.Services.Citation
{
    public interface IContactDetailsService
    {
        Guid Add(Domain.Citation.ContactDetails entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.ContactDetails> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.ContactDetails, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.ContactDetails> GetAll();
		Domain.Citation.ContactDetails GetById(Guid id);
		void Update(Domain.Citation.ContactDetails entity);
		
	}
}