using System;
namespace Infrastructure.Services.Citation
{
    public interface IEventReasonsService
    {
        Guid Add(Domain.Citation.EventReasons entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.EventReasons> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.EventReasons, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.EventReasons> GetAll();
		Domain.Citation.EventReasons GetById(Guid id);
		void Update(Domain.Citation.EventReasons entity);
		
	}
}