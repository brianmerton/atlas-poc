using System;
namespace Infrastructure.Services.Citation
{
    public interface IAssignmentsService
    {
        Guid Add(Domain.Citation.Assignments entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.Assignments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.Assignments, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.Assignments> GetAll();
		Domain.Citation.Assignments GetById(Guid id);
		void Update(Domain.Citation.Assignments entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.Assignments> GetByCompanyId(Guid CompanyId);

	}
}