using System;
namespace Infrastructure.Services.Citation
{
    public interface IBlockVersionSectorsService
    {
        Guid Add(Domain.Citation.BlockVersionSectors entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersionSectors> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.BlockVersionSectors, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersionSectors> GetAll();
		Domain.Citation.BlockVersionSectors GetById(Guid id);
		void Update(Domain.Citation.BlockVersionSectors entity);
		
        System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersionSectors> GetByBlockVersionId(Guid BlockVersionId);

        System.Collections.Generic.IEnumerable<Domain.Citation.BlockVersionSectors> GetBySectorId(Guid SectorId);

	}
}