using System;
namespace Infrastructure.Services.Customer
{
    public interface IEmployeesService
    {
        Guid Add(Domain.Customer.Employees entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.Employees> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Employees, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.Employees> GetAll();
		Domain.Customer.Employees GetById(Guid id);
		void Update(Domain.Customer.Employees entity);
		
	}
}