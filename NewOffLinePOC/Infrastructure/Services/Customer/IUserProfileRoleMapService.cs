using System;
namespace Infrastructure.Services.Customer
{
    public interface IUserProfileRoleMapService
    {
        Guid Add(Domain.Customer.UserProfileRoleMap entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserProfileRoleMap> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.UserProfileRoleMap, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserProfileRoleMap> GetAll();
		Domain.Customer.UserProfileRoleMap GetById(Guid id);
		void Update(Domain.Customer.UserProfileRoleMap entity);
		
	}
}