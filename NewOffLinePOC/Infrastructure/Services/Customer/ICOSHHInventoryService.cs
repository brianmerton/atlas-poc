using System;
namespace Infrastructure.Services.Customer
{
    public interface ICOSHHInventoryService
    {
        Guid Add(Domain.Customer.COSHHInventory entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.COSHHInventory> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.COSHHInventory, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.COSHHInventory> GetAll();
		Domain.Customer.COSHHInventory GetById(Guid id);
		void Update(Domain.Customer.COSHHInventory entity);
		
	}
}