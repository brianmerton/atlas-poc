using System;
namespace Infrastructure.Services.Customer
{
    public interface IEmployeeTrainingDetailsService
    {
        Guid Add(Domain.Customer.EmployeeTrainingDetails entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeTrainingDetails> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.EmployeeTrainingDetails, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeTrainingDetails> GetAll();
		Domain.Customer.EmployeeTrainingDetails GetById(Guid id);
		void Update(Domain.Customer.EmployeeTrainingDetails entity);
		
	}
}