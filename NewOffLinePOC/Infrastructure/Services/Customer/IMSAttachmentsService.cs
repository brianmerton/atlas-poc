using System;
namespace Infrastructure.Services.Customer
{
    public interface IMSAttachmentsService
    {
        Guid Add(Domain.Customer.MSAttachments entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.MSAttachments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.MSAttachments, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.MSAttachments> GetAll();
		Domain.Customer.MSAttachments GetById(Guid id);
		void Update(Domain.Customer.MSAttachments entity);
		
	}
}