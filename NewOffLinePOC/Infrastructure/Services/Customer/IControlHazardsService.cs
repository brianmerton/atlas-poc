using System;
namespace Infrastructure.Services.Customer
{
    public interface IControlHazardsService
    {
        Guid Add(Domain.Customer.ControlHazards entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.ControlHazards> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.ControlHazards, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.ControlHazards> GetAll();
		Domain.Customer.ControlHazards GetById(Guid id);
		void Update(Domain.Customer.ControlHazards entity);
		
	}
}