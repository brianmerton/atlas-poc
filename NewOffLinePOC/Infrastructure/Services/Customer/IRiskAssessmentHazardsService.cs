using System;
namespace Infrastructure.Services.Customer
{
    public interface IRiskAssessmentHazardsService
    {
        Guid Add(Domain.Customer.RiskAssessmentHazards entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessmentHazards> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.RiskAssessmentHazards, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessmentHazards> GetAll();
		Domain.Customer.RiskAssessmentHazards GetById(Guid id);
		void Update(Domain.Customer.RiskAssessmentHazards entity);
		
	}
}