using System;
namespace Infrastructure.Services.Customer
{
    public interface IRiskAssessmentsService
    {
        Guid Add(Domain.Customer.RiskAssessments entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.RiskAssessments, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessments> GetAll();
		Domain.Customer.RiskAssessments GetById(Guid id);
		void Update(Domain.Customer.RiskAssessments entity);
		
	}
}