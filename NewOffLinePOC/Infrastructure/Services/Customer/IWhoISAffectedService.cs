using System;
namespace Infrastructure.Services.Customer
{
    public interface IWhoISAffectedService
    {
        Guid Add(Domain.Customer.WhoISAffected entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.WhoISAffected> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.WhoISAffected, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.WhoISAffected> GetAll();
		Domain.Customer.WhoISAffected GetById(Guid id);
		void Update(Domain.Customer.WhoISAffected entity);
		
	}
}