using System;
namespace Infrastructure.Services.Customer
{
    public interface IMSSafetyPrecautionsService
    {
        Guid Add(Domain.Customer.MSSafetyPrecautions entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.MSSafetyPrecautions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.MSSafetyPrecautions, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.MSSafetyPrecautions> GetAll();
		Domain.Customer.MSSafetyPrecautions GetById(Guid id);
		void Update(Domain.Customer.MSSafetyPrecautions entity);
		
	}
}