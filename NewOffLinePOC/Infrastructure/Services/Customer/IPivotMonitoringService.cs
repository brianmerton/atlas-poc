using System;
namespace Infrastructure.Services.Customer
{
    public interface IPivotMonitoringService
    {
        Guid Add(Domain.Customer.PivotMonitoring entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.PivotMonitoring> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.PivotMonitoring, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.PivotMonitoring> GetAll();
		Domain.Customer.PivotMonitoring GetById(Guid id);
		void Update(Domain.Customer.PivotMonitoring entity);
		
	}
}