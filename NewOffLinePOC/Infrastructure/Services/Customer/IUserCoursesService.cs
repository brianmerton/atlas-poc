using System;
namespace Infrastructure.Services.Customer
{
    public interface IUserCoursesService
    {
        Guid Add(Domain.Customer.UserCourses entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserCourses> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.UserCourses, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserCourses> GetAll();
		Domain.Customer.UserCourses GetById(Guid id);
		void Update(Domain.Customer.UserCourses entity);
		
	}
}