using System;
namespace Infrastructure.Services.Customer
{
    public interface IIncidentInjuryTypesService
    {
        Guid Add(Domain.Customer.IncidentInjuryTypes entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.IncidentInjuryTypes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.IncidentInjuryTypes, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.IncidentInjuryTypes> GetAll();
		Domain.Customer.IncidentInjuryTypes GetById(Guid id);
		void Update(Domain.Customer.IncidentInjuryTypes entity);
		
	}
}