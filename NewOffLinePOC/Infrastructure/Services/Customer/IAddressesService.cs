using System;
namespace Infrastructure.Services.Customer
{
    public interface IAddressesService
    {
        Guid Add(Domain.Customer.Addresses entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.Addresses> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Addresses, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.Addresses> GetAll();
		Domain.Customer.Addresses GetById(Guid id);
		void Update(Domain.Customer.Addresses entity);
		
	}
}