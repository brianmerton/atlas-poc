using System;
namespace Infrastructure.Services.Customer
{
    public interface IEmployeeBankDetailsService
    {
        Guid Add(Domain.Customer.EmployeeBankDetails entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeBankDetails> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.EmployeeBankDetails, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeBankDetails> GetAll();
		Domain.Customer.EmployeeBankDetails GetById(Guid id);
		void Update(Domain.Customer.EmployeeBankDetails entity);
		
	}
}