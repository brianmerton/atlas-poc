using System;
namespace Infrastructure.Services.Customer
{
    public interface IEmployeeEducationDetailsService
    {
        Guid Add(Domain.Customer.EmployeeEducationDetails entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeEducationDetails> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.EmployeeEducationDetails, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeEducationDetails> GetAll();
		Domain.Customer.EmployeeEducationDetails GetById(Guid id);
		void Update(Domain.Customer.EmployeeEducationDetails entity);
		
	}
}