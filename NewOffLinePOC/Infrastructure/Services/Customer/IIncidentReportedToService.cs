using System;
namespace Infrastructure.Services.Customer
{
    public interface IIncidentReportedToService
    {
        Guid Add(Domain.Customer.IncidentReportedTo entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.IncidentReportedTo> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.IncidentReportedTo, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.IncidentReportedTo> GetAll();
		Domain.Customer.IncidentReportedTo GetById(Guid id);
		void Update(Domain.Customer.IncidentReportedTo entity);
		
	}
}