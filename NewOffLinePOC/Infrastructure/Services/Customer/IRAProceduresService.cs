using System;
namespace Infrastructure.Services.Customer
{
    public interface IRAProceduresService
    {
        Guid Add(Domain.Customer.RAProcedures entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.RAProcedures> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.RAProcedures, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.RAProcedures> GetAll();
		Domain.Customer.RAProcedures GetById(Guid id);
		void Update(Domain.Customer.RAProcedures entity);
		
	}
}