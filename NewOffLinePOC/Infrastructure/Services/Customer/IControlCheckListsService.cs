using System;
namespace Infrastructure.Services.Customer
{
    public interface IControlCheckListsService
    {
        Guid Add(Domain.Customer.ControlCheckLists entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.ControlCheckLists> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.ControlCheckLists, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.ControlCheckLists> GetAll();
		Domain.Customer.ControlCheckLists GetById(Guid id);
		void Update(Domain.Customer.ControlCheckLists entity);
		
	}
}