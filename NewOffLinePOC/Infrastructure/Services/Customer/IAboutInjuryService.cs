using System;
namespace Infrastructure.Services.Customer
{
    public interface IAboutInjuryService
    {
        Guid Add(Domain.Customer.AboutInjury entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.AboutInjury> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.AboutInjury, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.AboutInjury> GetAll();
		Domain.Customer.AboutInjury GetById(Guid id);
		void Update(Domain.Customer.AboutInjury entity);
		
	}
}