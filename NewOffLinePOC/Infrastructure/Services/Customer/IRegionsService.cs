using System;
namespace Infrastructure.Services.Customer
{
    public interface IRegionsService
    {
        Guid Add(Domain.Customer.Regions entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.Regions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Regions, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.Regions> GetAll();
		Domain.Customer.Regions GetById(Guid id);
		void Update(Domain.Customer.Regions entity);
		
	}
}