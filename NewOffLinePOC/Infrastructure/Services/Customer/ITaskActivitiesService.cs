using System;
namespace Infrastructure.Services.Customer
{
    public interface ITaskActivitiesService
    {
        Guid Add(Domain.Customer.TaskActivities entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.TaskActivities> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.TaskActivities, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.TaskActivities> GetAll();
		Domain.Customer.TaskActivities GetById(Guid id);
		void Update(Domain.Customer.TaskActivities entity);
		
	}
}