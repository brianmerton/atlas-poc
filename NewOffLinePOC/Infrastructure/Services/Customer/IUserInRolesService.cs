using System;
namespace Infrastructure.Services.Customer
{
    public interface IUserInRolesService
    {
        Guid Add(Domain.Customer.UserInRoles entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserInRoles> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.UserInRoles, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserInRoles> GetAll();
		Domain.Customer.UserInRoles GetById(Guid id);
		void Update(Domain.Customer.UserInRoles entity);
		
	}
}