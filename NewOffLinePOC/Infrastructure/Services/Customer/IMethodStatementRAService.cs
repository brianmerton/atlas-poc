using System;
namespace Infrastructure.Services.Customer
{
    public interface IMethodStatementRAService
    {
        Guid Add(Domain.Customer.MethodStatementRA entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.MethodStatementRA> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.MethodStatementRA, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.MethodStatementRA> GetAll();
		Domain.Customer.MethodStatementRA GetById(Guid id);
		void Update(Domain.Customer.MethodStatementRA entity);
		
	}
}