using System;
namespace Infrastructure.Services.Customer
{
    public interface IUserCheckListsService
    {
        Guid Add(Domain.Customer.UserCheckLists entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserCheckLists> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.UserCheckLists, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserCheckLists> GetAll();
		Domain.Customer.UserCheckLists GetById(Guid id);
		void Update(Domain.Customer.UserCheckLists entity);
		
	}
}