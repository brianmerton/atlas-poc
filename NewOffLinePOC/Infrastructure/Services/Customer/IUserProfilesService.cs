using System;
namespace Infrastructure.Services.Customer
{
    public interface IUserProfilesService
    {
        Guid Add(Domain.Customer.UserProfiles entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserProfiles> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.UserProfiles, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserProfiles> GetAll();
		Domain.Customer.UserProfiles GetById(Guid id);
		void Update(Domain.Customer.UserProfiles entity);
		
	}
}