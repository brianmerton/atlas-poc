using System;
namespace Infrastructure.Services.Customer
{
    public interface IMSAdditionalInfoService
    {
        Guid Add(Domain.Customer.MSAdditionalInfo entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.MSAdditionalInfo> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.MSAdditionalInfo, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.MSAdditionalInfo> GetAll();
		Domain.Customer.MSAdditionalInfo GetById(Guid id);
		void Update(Domain.Customer.MSAdditionalInfo entity);
		
	}
}