using System;
namespace Infrastructure.Services.Customer
{
    public interface IMethodStatementsService
    {
        Guid Add(Domain.Customer.MethodStatements entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.MethodStatements> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.MethodStatements, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.MethodStatements> GetAll();
		Domain.Customer.MethodStatements GetById(Guid id);
		void Update(Domain.Customer.MethodStatements entity);
		
	}
}