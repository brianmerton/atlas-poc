using System;
namespace Infrastructure.Services.Customer
{
    public interface IIncidentInvAnswersService
    {
        Guid Add(Domain.Customer.IncidentInvAnswers entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.IncidentInvAnswers> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.IncidentInvAnswers, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.IncidentInvAnswers> GetAll();
		Domain.Customer.IncidentInvAnswers GetById(Guid id);
		void Update(Domain.Customer.IncidentInvAnswers entity);
		
	}
}