using System;
namespace Infrastructure.Services.Customer
{
    public interface IControlsService
    {
        Guid Add(Domain.Customer.Controls entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.Controls> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Controls, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.Controls> GetAll();
		Domain.Customer.Controls GetById(Guid id);
		void Update(Domain.Customer.Controls entity);
		
	}
}