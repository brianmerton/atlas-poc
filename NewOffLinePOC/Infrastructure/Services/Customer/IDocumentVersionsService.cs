using System;
namespace Infrastructure.Services.Customer
{
    public interface IDocumentVersionsService
    {
        Guid Add(Domain.Customer.DocumentVersions entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.DocumentVersions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.DocumentVersions, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.DocumentVersions> GetAll();
		Domain.Customer.DocumentVersions GetById(Guid id);
		void Update(Domain.Customer.DocumentVersions entity);
		
	}
}