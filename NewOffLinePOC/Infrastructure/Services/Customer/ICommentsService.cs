using System;
namespace Infrastructure.Services.Customer
{
    public interface ICommentsService
    {
        Guid Add(Domain.Customer.Comments entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.Comments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Comments, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.Comments> GetAll();
		Domain.Customer.Comments GetById(Guid id);
		void Update(Domain.Customer.Comments entity);
		
	}
}