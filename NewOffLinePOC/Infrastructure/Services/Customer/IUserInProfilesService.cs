using System;
namespace Infrastructure.Services.Customer
{
    public interface IUserInProfilesService
    {
        Guid Add(Domain.Customer.UserInProfiles entity);
		void DeleteById(Guid id);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserInProfiles> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.UserInProfiles, bool>> predicate);
		System.Collections.Generic.IEnumerable<Domain.Customer.UserInProfiles> GetAll();
		Domain.Customer.UserInProfiles GetById(Guid id);
		void Update(Domain.Customer.UserInProfiles entity);
		
	}
}