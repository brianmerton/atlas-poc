using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IMSSitesRepository
	{
		Guid Add(Domain.Customer.MSSites entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.MSSites> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.MSSites, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.MSSites> GetAll();
        Domain.Customer.MSSites GetById(Guid id);
        void Update(Domain.Customer.MSSites entity);
	}
}