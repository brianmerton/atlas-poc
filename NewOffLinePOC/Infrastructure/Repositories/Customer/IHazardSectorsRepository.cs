using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IHazardSectorsRepository
	{
		Guid Add(Domain.Customer.HazardSectors entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.HazardSectors> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.HazardSectors, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.HazardSectors> GetAll();
        Domain.Customer.HazardSectors GetById(Guid id);
        void Update(Domain.Customer.HazardSectors entity);
	}
}