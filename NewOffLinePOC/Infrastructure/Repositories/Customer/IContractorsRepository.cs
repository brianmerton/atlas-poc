using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IContractorsRepository
	{
		Guid Add(Domain.Customer.Contractors entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Contractors> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Contractors, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Contractors> GetAll();
        Domain.Customer.Contractors GetById(Guid id);
        void Update(Domain.Customer.Contractors entity);
	}
}