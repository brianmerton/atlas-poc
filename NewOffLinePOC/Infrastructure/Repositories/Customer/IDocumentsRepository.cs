using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IDocumentsRepository
	{
		Guid Add(Domain.Customer.Documents entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Documents> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Documents, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Documents> GetAll();
        Domain.Customer.Documents GetById(Guid id);
        void Update(Domain.Customer.Documents entity);
	}
}