using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IInjuredPersonRepository
	{
		Guid Add(Domain.Customer.InjuredPerson entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.InjuredPerson> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.InjuredPerson, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.InjuredPerson> GetAll();
        Domain.Customer.InjuredPerson GetById(Guid id);
        void Update(Domain.Customer.InjuredPerson entity);
	}
}