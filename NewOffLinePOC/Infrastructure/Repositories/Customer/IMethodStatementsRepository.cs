using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IMethodStatementsRepository
	{
		Guid Add(Domain.Customer.MethodStatements entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.MethodStatements> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.MethodStatements, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.MethodStatements> GetAll();
        Domain.Customer.MethodStatements GetById(Guid id);
        void Update(Domain.Customer.MethodStatements entity);
	}
}