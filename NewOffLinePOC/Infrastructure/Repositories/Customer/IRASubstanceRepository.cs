using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IRASubstanceRepository
	{
		Guid Add(Domain.Customer.RASubstance entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.RASubstance> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.RASubstance, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.RASubstance> GetAll();
        Domain.Customer.RASubstance GetById(Guid id);
        void Update(Domain.Customer.RASubstance entity);
	}
}