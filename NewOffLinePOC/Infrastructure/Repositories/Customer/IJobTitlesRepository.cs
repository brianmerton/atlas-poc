using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IJobTitlesRepository
	{
		Guid Add(Domain.Customer.JobTitles entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.JobTitles> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.JobTitles, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.JobTitles> GetAll();
        Domain.Customer.JobTitles GetById(Guid id);
        void Update(Domain.Customer.JobTitles entity);
	}
}