using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IEmployeePayrollDetailsRepository
	{
		Guid Add(Domain.Customer.EmployeePayrollDetails entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeePayrollDetails> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.EmployeePayrollDetails, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeePayrollDetails> GetAll();
        Domain.Customer.EmployeePayrollDetails GetById(Guid id);
        void Update(Domain.Customer.EmployeePayrollDetails entity);
	}
}