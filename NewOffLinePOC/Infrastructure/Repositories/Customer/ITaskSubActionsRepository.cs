using System;
namespace Infrastructure.Repositories.Customer
{
	public interface ITaskSubActionsRepository
	{
		Guid Add(Domain.Customer.TaskSubActions entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.TaskSubActions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.TaskSubActions, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.TaskSubActions> GetAll();
        Domain.Customer.TaskSubActions GetById(Guid id);
        void Update(Domain.Customer.TaskSubActions entity);
	}
}