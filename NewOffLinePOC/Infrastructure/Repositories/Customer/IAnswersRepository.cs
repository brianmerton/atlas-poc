using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IAnswersRepository
	{
		Guid Add(Domain.Customer.Answers entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Answers> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Answers, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Answers> GetAll();
        Domain.Customer.Answers GetById(Guid id);
        void Update(Domain.Customer.Answers entity);
	}
}