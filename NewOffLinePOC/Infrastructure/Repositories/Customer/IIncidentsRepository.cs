using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IIncidentsRepository
	{
		Guid Add(Domain.Customer.Incidents entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Incidents> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Incidents, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Incidents> GetAll();
        Domain.Customer.Incidents GetById(Guid id);
        void Update(Domain.Customer.Incidents entity);
	}
}