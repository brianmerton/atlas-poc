using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IRiskAssessmentWorkspaceTypesRepository
	{
		Guid Add(Domain.Customer.RiskAssessmentWorkspaceTypes entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessmentWorkspaceTypes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.RiskAssessmentWorkspaceTypes, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessmentWorkspaceTypes> GetAll();
        Domain.Customer.RiskAssessmentWorkspaceTypes GetById(Guid id);
        void Update(Domain.Customer.RiskAssessmentWorkspaceTypes entity);
	}
}