using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IRiskAssessmentSectorsRepository
	{
		Guid Add(Domain.Customer.RiskAssessmentSectors entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessmentSectors> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.RiskAssessmentSectors, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessmentSectors> GetAll();
        Domain.Customer.RiskAssessmentSectors GetById(Guid id);
        void Update(Domain.Customer.RiskAssessmentSectors entity);
	}
}