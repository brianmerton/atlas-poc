using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IAttachmentsRepository
	{
		Guid Add(Domain.Customer.Attachments entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Attachments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Attachments, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Attachments> GetAll();
        Domain.Customer.Attachments GetById(Guid id);
        void Update(Domain.Customer.Attachments entity);
	}
}