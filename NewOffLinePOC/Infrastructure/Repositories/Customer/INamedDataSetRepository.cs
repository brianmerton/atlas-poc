using System;
namespace Infrastructure.Repositories.Customer
{
	public interface INamedDataSetRepository
	{
		Guid Add(Domain.Customer.NamedDataSet entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.NamedDataSet> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.NamedDataSet, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.NamedDataSet> GetAll();
        Domain.Customer.NamedDataSet GetById(Guid id);
        void Update(Domain.Customer.NamedDataSet entity);
	}
}