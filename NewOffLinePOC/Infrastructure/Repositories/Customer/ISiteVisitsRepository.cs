using System;
namespace Infrastructure.Repositories.Customer
{
	public interface ISiteVisitsRepository
	{
		Guid Add(Domain.Customer.SiteVisits entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.SiteVisits> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.SiteVisits, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.SiteVisits> GetAll();
        Domain.Customer.SiteVisits GetById(Guid id);
        void Update(Domain.Customer.SiteVisits entity);
	}
}