using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IEthnicGroupRepository
	{
		Guid Add(Domain.Customer.EthnicGroup entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.EthnicGroup> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.EthnicGroup, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.EthnicGroup> GetAll();
        Domain.Customer.EthnicGroup GetById(Guid id);
        void Update(Domain.Customer.EthnicGroup entity);
	}
}