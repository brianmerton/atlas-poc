using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IEmployeePreviousEmploymentDetailsRepository
	{
		Guid Add(Domain.Customer.EmployeePreviousEmploymentDetails entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeePreviousEmploymentDetails> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.EmployeePreviousEmploymentDetails, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeePreviousEmploymentDetails> GetAll();
        Domain.Customer.EmployeePreviousEmploymentDetails GetById(Guid id);
        void Update(Domain.Customer.EmployeePreviousEmploymentDetails entity);
	}
}