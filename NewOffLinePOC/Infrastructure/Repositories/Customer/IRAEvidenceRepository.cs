using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IRAEvidenceRepository
	{
		Guid Add(Domain.Customer.RAEvidence entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.RAEvidence> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.RAEvidence, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.RAEvidence> GetAll();
        Domain.Customer.RAEvidence GetById(Guid id);
        void Update(Domain.Customer.RAEvidence entity);
	}
}