using System;
namespace Infrastructure.Repositories.Customer
{
	public interface ICheckListAttachmentsRepository
	{
		Guid Add(Domain.Customer.CheckListAttachments entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.CheckListAttachments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.CheckListAttachments, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.CheckListAttachments> GetAll();
        Domain.Customer.CheckListAttachments GetById(Guid id);
        void Update(Domain.Customer.CheckListAttachments entity);
	}
}