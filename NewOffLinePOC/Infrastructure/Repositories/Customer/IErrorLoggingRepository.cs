using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IErrorLoggingRepository
	{
		Guid Add(Domain.Customer.ErrorLogging entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.ErrorLogging> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.ErrorLogging, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.ErrorLogging> GetAll();
        Domain.Customer.ErrorLogging GetById(Guid id);
        void Update(Domain.Customer.ErrorLogging entity);
	}
}