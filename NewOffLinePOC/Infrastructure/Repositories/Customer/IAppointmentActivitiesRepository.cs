using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IAppointmentActivitiesRepository
	{
		Guid Add(Domain.Customer.AppointmentActivities entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.AppointmentActivities> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.AppointmentActivities, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.AppointmentActivities> GetAll();
        Domain.Customer.AppointmentActivities GetById(Guid id);
        void Update(Domain.Customer.AppointmentActivities entity);
	}
}