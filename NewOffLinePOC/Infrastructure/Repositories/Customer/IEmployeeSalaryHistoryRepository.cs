using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IEmployeeSalaryHistoryRepository
	{
		Guid Add(Domain.Customer.EmployeeSalaryHistory entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeSalaryHistory> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.EmployeeSalaryHistory, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeSalaryHistory> GetAll();
        Domain.Customer.EmployeeSalaryHistory GetById(Guid id);
        void Update(Domain.Customer.EmployeeSalaryHistory entity);
	}
}