using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IRolesRepository
	{
		Guid Add(Domain.Customer.Roles entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Roles> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Roles, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Roles> GetAll();
        Domain.Customer.Roles GetById(Guid id);
        void Update(Domain.Customer.Roles entity);
	}
}