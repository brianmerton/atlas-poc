using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IEmployeeEmergencyContactsRepository
	{
		Guid Add(Domain.Customer.EmployeeEmergencyContacts entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeEmergencyContacts> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.EmployeeEmergencyContacts, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeEmergencyContacts> GetAll();
        Domain.Customer.EmployeeEmergencyContacts GetById(Guid id);
        void Update(Domain.Customer.EmployeeEmergencyContacts entity);
	}
}