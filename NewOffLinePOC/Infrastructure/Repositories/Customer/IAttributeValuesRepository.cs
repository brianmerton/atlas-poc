using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IAttributeValuesRepository
	{
		Guid Add(Domain.Customer.AttributeValues entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.AttributeValues> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.AttributeValues, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.AttributeValues> GetAll();
        Domain.Customer.AttributeValues GetById(Guid id);
        void Update(Domain.Customer.AttributeValues entity);
	}
}