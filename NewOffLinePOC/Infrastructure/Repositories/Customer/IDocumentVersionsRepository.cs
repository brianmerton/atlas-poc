using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IDocumentVersionsRepository
	{
		Guid Add(Domain.Customer.DocumentVersions entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.DocumentVersions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.DocumentVersions, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.DocumentVersions> GetAll();
        Domain.Customer.DocumentVersions GetById(Guid id);
        void Update(Domain.Customer.DocumentVersions entity);
	}
}