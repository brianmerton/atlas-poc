using System;
namespace Infrastructure.Repositories.Customer
{
	public interface INotesRepository
	{
		Guid Add(Domain.Customer.Notes entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Notes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Notes, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Notes> GetAll();
        Domain.Customer.Notes GetById(Guid id);
        void Update(Domain.Customer.Notes entity);
	}
}