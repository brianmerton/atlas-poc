using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IUserCheckListItemsRepository
	{
		Guid Add(Domain.Customer.UserCheckListItems entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.UserCheckListItems> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.UserCheckListItems, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.UserCheckListItems> GetAll();
        Domain.Customer.UserCheckListItems GetById(Guid id);
        void Update(Domain.Customer.UserCheckListItems entity);
	}
}