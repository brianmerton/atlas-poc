using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IMSEventsRepository
	{
		Guid Add(Domain.Customer.MSEvents entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.MSEvents> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.MSEvents, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.MSEvents> GetAll();
        Domain.Customer.MSEvents GetById(Guid id);
        void Update(Domain.Customer.MSEvents entity);
	}
}