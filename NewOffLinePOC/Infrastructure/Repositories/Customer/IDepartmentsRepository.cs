using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IDepartmentsRepository
	{
		Guid Add(Domain.Customer.Departments entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Departments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Departments, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Departments> GetAll();
        Domain.Customer.Departments GetById(Guid id);
        void Update(Domain.Customer.Departments entity);
	}
}