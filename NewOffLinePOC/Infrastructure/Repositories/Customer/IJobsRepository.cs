using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IJobsRepository
	{
		Guid Add(Domain.Customer.Jobs entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Jobs> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Jobs, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Jobs> GetAll();
        Domain.Customer.Jobs GetById(Guid id);
        void Update(Domain.Customer.Jobs entity);
	}
}