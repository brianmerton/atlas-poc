using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IRiskAssessmentControlsRepository
	{
		Guid Add(Domain.Customer.RiskAssessmentControls entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessmentControls> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.RiskAssessmentControls, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.RiskAssessmentControls> GetAll();
        Domain.Customer.RiskAssessmentControls GetById(Guid id);
        void Update(Domain.Customer.RiskAssessmentControls entity);
	}
}