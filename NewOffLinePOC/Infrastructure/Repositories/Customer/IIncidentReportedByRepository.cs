using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IIncidentReportedByRepository
	{
		Guid Add(Domain.Customer.IncidentReportedBy entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.IncidentReportedBy> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.IncidentReportedBy, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.IncidentReportedBy> GetAll();
        Domain.Customer.IncidentReportedBy GetById(Guid id);
        void Update(Domain.Customer.IncidentReportedBy entity);
	}
}