using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IInvAnswersRepository
	{
		Guid Add(Domain.Customer.InvAnswers entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.InvAnswers> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.InvAnswers, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.InvAnswers> GetAll();
        Domain.Customer.InvAnswers GetById(Guid id);
        void Update(Domain.Customer.InvAnswers entity);
	}
}