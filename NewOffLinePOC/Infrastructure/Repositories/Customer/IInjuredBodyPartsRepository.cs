using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IInjuredBodyPartsRepository
	{
		Guid Add(Domain.Customer.InjuredBodyParts entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.InjuredBodyParts> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.InjuredBodyParts, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.InjuredBodyParts> GetAll();
        Domain.Customer.InjuredBodyParts GetById(Guid id);
        void Update(Domain.Customer.InjuredBodyParts entity);
	}
}