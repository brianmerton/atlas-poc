using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IIncidentCommentsRepository
	{
		Guid Add(Domain.Customer.IncidentComments entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.IncidentComments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.IncidentComments, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.IncidentComments> GetAll();
        Domain.Customer.IncidentComments GetById(Guid id);
        void Update(Domain.Customer.IncidentComments entity);
	}
}