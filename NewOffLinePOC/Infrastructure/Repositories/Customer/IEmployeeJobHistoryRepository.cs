using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IEmployeeJobHistoryRepository
	{
		Guid Add(Domain.Customer.EmployeeJobHistory entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeJobHistory> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.EmployeeJobHistory, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.EmployeeJobHistory> GetAll();
        Domain.Customer.EmployeeJobHistory GetById(Guid id);
        void Update(Domain.Customer.EmployeeJobHistory entity);
	}
}