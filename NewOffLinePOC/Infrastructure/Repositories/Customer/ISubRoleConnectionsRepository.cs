using System;
namespace Infrastructure.Repositories.Customer
{
	public interface ISubRoleConnectionsRepository
	{
		Guid Add(Domain.Customer.SubRoleConnections entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.SubRoleConnections> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.SubRoleConnections, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.SubRoleConnections> GetAll();
        Domain.Customer.SubRoleConnections GetById(Guid id);
        void Update(Domain.Customer.SubRoleConnections entity);
	}
}