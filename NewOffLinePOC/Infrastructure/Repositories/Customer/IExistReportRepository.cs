using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IExistReportRepository
	{
		Guid Add(Domain.Customer.ExistReport entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.ExistReport> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.ExistReport, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.ExistReport> GetAll();
        Domain.Customer.ExistReport GetById(Guid id);
        void Update(Domain.Customer.ExistReport entity);
	}
}