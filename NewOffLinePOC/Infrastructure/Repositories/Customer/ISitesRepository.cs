using System;
namespace Infrastructure.Repositories.Customer
{
	public interface ISitesRepository
	{
		Guid Add(Domain.Customer.Sites entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Sites> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Sites, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Sites> GetAll();
        Domain.Customer.Sites GetById(Guid id);
        void Update(Domain.Customer.Sites entity);
	}
}