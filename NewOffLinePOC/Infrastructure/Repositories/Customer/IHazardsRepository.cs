using System;
namespace Infrastructure.Repositories.Customer
{
	public interface IHazardsRepository
	{
		Guid Add(Domain.Customer.Hazards entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Customer.Hazards> FindBy(System.Linq.Expressions.Expression<Func<Domain.Customer.Hazards, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Customer.Hazards> GetAll();
        Domain.Customer.Hazards GetById(Guid id);
        void Update(Domain.Customer.Hazards entity);
	}
}