using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IAppointmentsRepository
	{
		Guid Add(Domain.Citation.Appointments entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.Appointments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.Appointments, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.Appointments> GetAll();
        Domain.Citation.Appointments GetById(Guid id);
        void Update(Domain.Citation.Appointments entity);
	}
}