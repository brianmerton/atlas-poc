using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IAdviceCardTypeMapRepository
	{
		Guid Add(Domain.Citation.AdviceCardTypeMap entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.AdviceCardTypeMap> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.AdviceCardTypeMap, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.AdviceCardTypeMap> GetAll();
        Domain.Citation.AdviceCardTypeMap GetById(Guid id);
        void Update(Domain.Citation.AdviceCardTypeMap entity);
        System.Collections.Generic. IEnumerable<Domain.Citation.AdviceCardTypeMap> GetByAdviceCardId(Guid AdviceCardId);
        System.Collections.Generic. IEnumerable<Domain.Citation.AdviceCardTypeMap> GetByAdviceTypeId(Guid AdviceTypeId);
	}
}