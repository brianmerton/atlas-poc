using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IMonitoringRepository
	{
		Guid Add(Domain.Citation.Monitoring entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.Monitoring> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.Monitoring, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.Monitoring> GetAll();
        Domain.Citation.Monitoring GetById(Guid id);
        void Update(Domain.Citation.Monitoring entity);
        System.Collections.Generic. IEnumerable<Domain.Citation.Monitoring> GetByOperationId(Guid OperationId);
        System.Collections.Generic. IEnumerable<Domain.Citation.Monitoring> GetByUserId(Guid UserId);
	}
}