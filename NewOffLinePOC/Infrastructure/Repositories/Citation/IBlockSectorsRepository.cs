using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IBlockSectorsRepository
	{
		Guid Add(Domain.Citation.BlockSectors entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.BlockSectors> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.BlockSectors, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.BlockSectors> GetAll();
        Domain.Citation.BlockSectors GetById(Guid id);
        void Update(Domain.Citation.BlockSectors entity);
        System.Collections.Generic. IEnumerable<Domain.Citation.BlockSectors> GetByBlockId(Guid BlockId);
        System.Collections.Generic. IEnumerable<Domain.Citation.BlockSectors> GetBySectorId(Guid SectorId);
	}
}