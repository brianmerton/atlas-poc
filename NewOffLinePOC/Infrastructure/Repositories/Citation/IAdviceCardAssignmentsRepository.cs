using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IAdviceCardAssignmentsRepository
	{
		Guid Add(Domain.Citation.AdviceCardAssignments entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.AdviceCardAssignments> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.AdviceCardAssignments, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.AdviceCardAssignments> GetAll();
        Domain.Citation.AdviceCardAssignments GetById(Guid id);
        void Update(Domain.Citation.AdviceCardAssignments entity);
	}
}