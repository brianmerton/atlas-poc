using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IClientTypesRepository
	{
		Guid Add(Domain.Citation.ClientTypes entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.ClientTypes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.ClientTypes, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.ClientTypes> GetAll();
        Domain.Citation.ClientTypes GetById(Guid id);
        void Update(Domain.Citation.ClientTypes entity);
	}
}