using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IQuestionsRepository
	{
		Guid Add(Domain.Citation.Questions entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.Questions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.Questions, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.Questions> GetAll();
        Domain.Citation.Questions GetById(Guid id);
        void Update(Domain.Citation.Questions entity);
        System.Collections.Generic. IEnumerable<Domain.Citation.Questions> GetByAssociatedContentId(Guid AssociatedContentId);
	}
}