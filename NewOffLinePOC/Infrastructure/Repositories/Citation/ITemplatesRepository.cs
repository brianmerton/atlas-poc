using System;
namespace Infrastructure.Repositories.Citation
{
	public interface ITemplatesRepository
	{
		Guid Add(Domain.Citation.Templates entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.Templates> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.Templates, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.Templates> GetAll();
        Domain.Citation.Templates GetById(Guid id);
        void Update(Domain.Citation.Templates entity);
	}
}