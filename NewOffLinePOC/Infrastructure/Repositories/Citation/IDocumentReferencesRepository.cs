using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IDocumentReferencesRepository
	{
		Guid Add(Domain.Citation.DocumentReferences entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.DocumentReferences> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.DocumentReferences, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.DocumentReferences> GetAll();
        Domain.Citation.DocumentReferences GetById(Guid id);
        void Update(Domain.Citation.DocumentReferences entity);
        System.Collections.Generic. IEnumerable<Domain.Citation.DocumentReferences> GetByCreatedBy(Guid CreatedBy);
        System.Collections.Generic. IEnumerable<Domain.Citation.DocumentReferences> GetByModifiedBy(Guid ModifiedBy);
        System.Collections.Generic. IEnumerable<Domain.Citation.DocumentReferences> GetByTenantId(Guid TenantId);
	}
}