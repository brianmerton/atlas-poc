using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IAdviceCardsRepository
	{
		Guid Add(Domain.Citation.AdviceCards entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.AdviceCards> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.AdviceCards, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.AdviceCards> GetAll();
        Domain.Citation.AdviceCards GetById(Guid id);
        void Update(Domain.Citation.AdviceCards entity);
        System.Collections.Generic. IEnumerable<Domain.Citation.AdviceCards> GetByCompanyId(Guid CompanyId);
	}
}