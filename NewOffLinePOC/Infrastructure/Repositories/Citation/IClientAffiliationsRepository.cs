using System;
namespace Infrastructure.Repositories.Citation
{
	public interface IClientAffiliationsRepository
	{
		Guid Add(Domain.Citation.ClientAffiliations entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Citation.ClientAffiliations> FindBy(System.Linq.Expressions.Expression<Func<Domain.Citation.ClientAffiliations, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Citation.ClientAffiliations> GetAll();
        Domain.Citation.ClientAffiliations GetById(Guid id);
        void Update(Domain.Citation.ClientAffiliations entity);
	}
}