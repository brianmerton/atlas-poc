using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IRiskAssessmentTypeRepository
	{
		Guid Add(Domain.Shared.RiskAssessmentType entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.RiskAssessmentType> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.RiskAssessmentType, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.RiskAssessmentType> GetAll();
        Domain.Shared.RiskAssessmentType GetById(Guid id);
        void Update(Domain.Shared.RiskAssessmentType entity);
	}
}