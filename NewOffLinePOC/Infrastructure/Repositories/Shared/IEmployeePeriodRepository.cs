using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IEmployeePeriodRepository
	{
		Guid Add(Domain.Shared.EmployeePeriod entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmployeePeriod> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.EmployeePeriod, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmployeePeriod> GetAll();
        Domain.Shared.EmployeePeriod GetById(Guid id);
        void Update(Domain.Shared.EmployeePeriod entity);
	}
}