using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ICheckListVersionRepository
	{
		Guid Add(Domain.Shared.CheckListVersion entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.CheckListVersion> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.CheckListVersion, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.CheckListVersion> GetAll();
        Domain.Shared.CheckListVersion GetById(Guid id);
        void Update(Domain.Shared.CheckListVersion entity);
	}
}