using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IInjuryTypeRepository
	{
		Guid Add(Domain.Shared.InjuryType entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.InjuryType> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.InjuryType, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.InjuryType> GetAll();
        Domain.Shared.InjuryType GetById(Guid id);
        void Update(Domain.Shared.InjuryType entity);
        System.Collections.Generic. IEnumerable<Domain.Shared.InjuryType> GetByInjuredPartyId(Guid InjuredPartyId);
	}
}