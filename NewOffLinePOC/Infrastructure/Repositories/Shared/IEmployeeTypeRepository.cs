using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IEmployeeTypeRepository
	{
		Guid Add(Domain.Shared.EmployeeType entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmployeeType> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.EmployeeType, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmployeeType> GetAll();
        Domain.Shared.EmployeeType GetById(Guid id);
        void Update(Domain.Shared.EmployeeType entity);
	}
}