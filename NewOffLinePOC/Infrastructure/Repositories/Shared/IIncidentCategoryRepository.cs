using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IIncidentCategoryRepository
	{
		Guid Add(Domain.Shared.IncidentCategory entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.IncidentCategory> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.IncidentCategory, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.IncidentCategory> GetAll();
        Domain.Shared.IncidentCategory GetById(Guid id);
        void Update(Domain.Shared.IncidentCategory entity);
	}
}