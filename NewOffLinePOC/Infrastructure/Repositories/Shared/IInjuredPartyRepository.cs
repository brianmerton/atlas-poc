using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IInjuredPartyRepository
	{
		Guid Add(Domain.Shared.InjuredParty entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.InjuredParty> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.InjuredParty, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.InjuredParty> GetAll();
        Domain.Shared.InjuredParty GetById(Guid id);
        void Update(Domain.Shared.InjuredParty entity);
	}
}