using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IEthnicGroupValueRepository
	{
		Guid Add(Domain.Shared.EthnicGroupValue entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.EthnicGroupValue> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.EthnicGroupValue, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.EthnicGroupValue> GetAll();
        Domain.Shared.EthnicGroupValue GetById(Guid id);
        void Update(Domain.Shared.EthnicGroupValue entity);
	}
}