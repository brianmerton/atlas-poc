using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IAttributesRepository
	{
		Guid Add(Domain.Shared.Attributes entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.Attributes> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Attributes, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.Attributes> GetAll();
        Domain.Shared.Attributes GetById(Guid id);
        void Update(Domain.Shared.Attributes entity);
	}
}