using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IIncidentTypeRepository
	{
		Guid Add(Domain.Shared.IncidentType entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.IncidentType> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.IncidentType, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.IncidentType> GetAll();
        Domain.Shared.IncidentType GetById(Guid id);
        void Update(Domain.Shared.IncidentType entity);
        System.Collections.Generic. IEnumerable<Domain.Shared.IncidentType> GetByIncidentCategoryId(Guid IncidentCategoryId);
	}
}