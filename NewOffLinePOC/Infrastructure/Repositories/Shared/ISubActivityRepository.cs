using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ISubActivityRepository
	{
		Guid Add(Domain.Shared.SubActivity entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.SubActivity> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.SubActivity, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.SubActivity> GetAll();
        Domain.Shared.SubActivity GetById(Guid id);
        void Update(Domain.Shared.SubActivity entity);
	}
}