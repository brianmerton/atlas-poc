using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IMainIndustryRepository
	{
		Guid Add(Domain.Shared.MainIndustry entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.MainIndustry> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.MainIndustry, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.MainIndustry> GetAll();
        Domain.Shared.MainIndustry GetById(Guid id);
        void Update(Domain.Shared.MainIndustry entity);
	}
}