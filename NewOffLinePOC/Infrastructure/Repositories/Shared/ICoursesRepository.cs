using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ICoursesRepository
	{
		Guid Add(Domain.Shared.Courses entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.Courses> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Courses, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.Courses> GetAll();
        Domain.Shared.Courses GetById(Guid id);
        void Update(Domain.Shared.Courses entity);
	}
}