using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ICountiesRepository
	{
		Guid Add(Domain.Shared.Counties entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.Counties> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Counties, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.Counties> GetAll();
        Domain.Shared.Counties GetById(Guid id);
        void Update(Domain.Shared.Counties entity);
	}
}