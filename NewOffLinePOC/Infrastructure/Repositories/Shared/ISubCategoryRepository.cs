using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ISubCategoryRepository
	{
		Guid Add(Domain.Shared.SubCategory entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.SubCategory> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.SubCategory, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.SubCategory> GetAll();
        Domain.Shared.SubCategory GetById(Guid id);
        void Update(Domain.Shared.SubCategory entity);
        System.Collections.Generic. IEnumerable<Domain.Shared.SubCategory> GetByCreatedBy(Guid CreatedBy);
        System.Collections.Generic. IEnumerable<Domain.Shared.SubCategory> GetByModifiedBy(Guid ModifiedBy);
	}
}