using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IAdditionalServiceRepository
	{
		Guid Add(Domain.Shared.AdditionalService entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.AdditionalService> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.AdditionalService, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.AdditionalService> GetAll();
        Domain.Shared.AdditionalService GetById(Guid id);
        void Update(Domain.Shared.AdditionalService entity);
	}
}