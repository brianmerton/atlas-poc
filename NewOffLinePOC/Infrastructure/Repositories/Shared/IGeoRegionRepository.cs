using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IGeoRegionRepository
	{
		Guid Add(Domain.Shared.GeoRegion entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.GeoRegion> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.GeoRegion, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.GeoRegion> GetAll();
        Domain.Shared.GeoRegion GetById(Guid id);
        void Update(Domain.Shared.GeoRegion entity);
	}
}