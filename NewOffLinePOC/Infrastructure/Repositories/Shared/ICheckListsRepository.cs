using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ICheckListsRepository
	{
		Guid Add(Domain.Shared.CheckLists entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.CheckLists> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.CheckLists, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.CheckLists> GetAll();
        Domain.Shared.CheckLists GetById(Guid id);
        void Update(Domain.Shared.CheckLists entity);
	}
}