using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IEmployeeRelationRepository
	{
		Guid Add(Domain.Shared.EmployeeRelation entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmployeeRelation> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.EmployeeRelation, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmployeeRelation> GetAll();
        Domain.Shared.EmployeeRelation GetById(Guid id);
        void Update(Domain.Shared.EmployeeRelation entity);
	}
}