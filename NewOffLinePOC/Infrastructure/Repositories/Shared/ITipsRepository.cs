using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ITipsRepository
	{
		Guid Add(Domain.Shared.Tips entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.Tips> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Tips, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.Tips> GetAll();
        Domain.Shared.Tips GetById(Guid id);
        void Update(Domain.Shared.Tips entity);
	}
}