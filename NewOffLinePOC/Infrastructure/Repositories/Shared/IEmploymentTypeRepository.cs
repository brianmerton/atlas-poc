using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IEmploymentTypeRepository
	{
		Guid Add(Domain.Shared.EmploymentType entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmploymentType> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.EmploymentType, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmploymentType> GetAll();
        Domain.Shared.EmploymentType GetById(Guid id);
        void Update(Domain.Shared.EmploymentType entity);
	}
}