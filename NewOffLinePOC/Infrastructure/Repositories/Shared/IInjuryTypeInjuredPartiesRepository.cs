using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IInjuryTypeInjuredPartiesRepository
	{
		Guid Add(Domain.Shared.InjuryTypeInjuredParties entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.InjuryTypeInjuredParties> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.InjuryTypeInjuredParties, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.InjuryTypeInjuredParties> GetAll();
        Domain.Shared.InjuryTypeInjuredParties GetById(Guid id);
        void Update(Domain.Shared.InjuryTypeInjuredParties entity);
        System.Collections.Generic. IEnumerable<Domain.Shared.InjuryTypeInjuredParties> GetByInjuredPartyId(Guid InjuredPartyId);
        System.Collections.Generic. IEnumerable<Domain.Shared.InjuryTypeInjuredParties> GetByInjuryTypeId(Guid InjuryTypeId);
	}
}