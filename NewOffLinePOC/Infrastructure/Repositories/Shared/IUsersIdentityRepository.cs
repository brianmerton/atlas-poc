using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IUsersIdentityRepository
	{
		Guid Add(Domain.Shared.UsersIdentity entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.UsersIdentity> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.UsersIdentity, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.UsersIdentity> GetAll();
        Domain.Shared.UsersIdentity GetById(Guid id);
        void Update(Domain.Shared.UsersIdentity entity);
	}
}