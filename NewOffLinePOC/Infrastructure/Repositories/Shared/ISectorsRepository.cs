using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ISectorsRepository
	{
		Guid Add(Domain.Shared.Sectors entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.Sectors> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.Sectors, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.Sectors> GetAll();
        Domain.Shared.Sectors GetById(Guid id);
        void Update(Domain.Shared.Sectors entity);
	}
}