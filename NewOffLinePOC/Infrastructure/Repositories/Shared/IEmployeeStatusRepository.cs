using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IEmployeeStatusRepository
	{
		Guid Add(Domain.Shared.EmployeeStatus entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmployeeStatus> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.EmployeeStatus, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.EmployeeStatus> GetAll();
        Domain.Shared.EmployeeStatus GetById(Guid id);
        void Update(Domain.Shared.EmployeeStatus entity);
	}
}