using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ICheckListItemsRepository
	{
		Guid Add(Domain.Shared.CheckListItems entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.CheckListItems> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.CheckListItems, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.CheckListItems> GetAll();
        Domain.Shared.CheckListItems GetById(Guid id);
        void Update(Domain.Shared.CheckListItems entity);
        System.Collections.Generic. IEnumerable<Domain.Shared.CheckListItems> GetByCheckItemId(Guid CheckItemId);
        System.Collections.Generic. IEnumerable<Domain.Shared.CheckListItems> GetByCheckListId(Guid CheckListId);
	}
}