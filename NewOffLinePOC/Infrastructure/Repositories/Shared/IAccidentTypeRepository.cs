using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IAccidentTypeRepository
	{
		Guid Add(Domain.Shared.AccidentType entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.AccidentType> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.AccidentType, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.AccidentType> GetAll();
        Domain.Shared.AccidentType GetById(Guid id);
        void Update(Domain.Shared.AccidentType entity);
        System.Collections.Generic. IEnumerable<Domain.Shared.AccidentType> GetByAccidentCategoryId(Guid AccidentCategoryId);
	}
}