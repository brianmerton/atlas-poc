using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IEthnicGroupTypeRepository
	{
		Guid Add(Domain.Shared.EthnicGroupType entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.EthnicGroupType> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.EthnicGroupType, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.EthnicGroupType> GetAll();
        Domain.Shared.EthnicGroupType GetById(Guid id);
        void Update(Domain.Shared.EthnicGroupType entity);
	}
}