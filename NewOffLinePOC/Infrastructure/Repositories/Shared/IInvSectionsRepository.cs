using System;
namespace Infrastructure.Repositories.Shared
{
	public interface IInvSectionsRepository
	{
		Guid Add(Domain.Shared.InvSections entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.InvSections> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.InvSections, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.InvSections> GetAll();
        Domain.Shared.InvSections GetById(Guid id);
        void Update(Domain.Shared.InvSections entity);
        System.Collections.Generic. IEnumerable<Domain.Shared.InvSections> GetByParentSectionId(Guid ParentSectionId);
	}
}