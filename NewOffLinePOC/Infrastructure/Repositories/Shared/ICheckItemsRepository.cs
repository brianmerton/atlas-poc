using System;
namespace Infrastructure.Repositories.Shared
{
	public interface ICheckItemsRepository
	{
		Guid Add(Domain.Shared.CheckItems entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Shared.CheckItems> FindBy(System.Linq.Expressions.Expression<Func<Domain.Shared.CheckItems, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Shared.CheckItems> GetAll();
        Domain.Shared.CheckItems GetById(Guid id);
        void Update(Domain.Shared.CheckItems entity);
	}
}