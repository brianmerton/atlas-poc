using System;
namespace Infrastructure.Repositories.Internal
{
	public interface IErrorsRepository
	{
		Guid Add(Domain.Internal.Errors entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Internal.Errors> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.Errors, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Internal.Errors> GetAll();
        Domain.Internal.Errors GetById(Guid id);
        void Update(Domain.Internal.Errors entity);
	}
}