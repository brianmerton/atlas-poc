using System;
namespace Infrastructure.Repositories.Internal
{
	public interface IMapReferencesRepository
	{
		Guid Add(Domain.Internal.MapReferences entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Internal.MapReferences> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.MapReferences, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Internal.MapReferences> GetAll();
        Domain.Internal.MapReferences GetById(Guid id);
        void Update(Domain.Internal.MapReferences entity);
	}
}