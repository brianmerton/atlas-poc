using System;
namespace Infrastructure.Repositories.Internal
{
	public interface IHelpEntriesRepository
	{
		Guid Add(Domain.Internal.HelpEntries entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Internal.HelpEntries> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.HelpEntries, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Internal.HelpEntries> GetAll();
        Domain.Internal.HelpEntries GetById(Guid id);
        void Update(Domain.Internal.HelpEntries entity);
	}
}