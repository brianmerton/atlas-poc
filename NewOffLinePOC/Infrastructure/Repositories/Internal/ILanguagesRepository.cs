using System;
namespace Infrastructure.Repositories.Internal
{
	public interface ILanguagesRepository
	{
		Guid Add(Domain.Internal.Languages entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Internal.Languages> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.Languages, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Internal.Languages> GetAll();
        Domain.Internal.Languages GetById(Guid id);
        void Update(Domain.Internal.Languages entity);
	}
}