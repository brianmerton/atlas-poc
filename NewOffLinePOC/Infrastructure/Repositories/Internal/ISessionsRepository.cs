using System;
namespace Infrastructure.Repositories.Internal
{
	public interface ISessionsRepository
	{
		Guid Add(Domain.Internal.Sessions entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Internal.Sessions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.Sessions, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Internal.Sessions> GetAll();
        Domain.Internal.Sessions GetById(Guid id);
        void Update(Domain.Internal.Sessions entity);
	}
}