using System;
namespace Infrastructure.Repositories.Internal
{
	public interface IRoleOperationMapRepository
	{
		Guid Add(Domain.Internal.RoleOperationMap entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Internal.RoleOperationMap> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.RoleOperationMap, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Internal.RoleOperationMap> GetAll();
        Domain.Internal.RoleOperationMap GetById(Guid id);
        void Update(Domain.Internal.RoleOperationMap entity);
        System.Collections.Generic. IEnumerable<Domain.Internal.RoleOperationMap> GetByOperationId(Guid OperationId);
        System.Collections.Generic. IEnumerable<Domain.Internal.RoleOperationMap> GetByRoleId(Guid RoleId);
	}
}