using System;
namespace Infrastructure.Repositories.Internal
{
	public interface ITockensRepository
	{
		Guid Add(Domain.Internal.Tockens entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Internal.Tockens> FindBy(System.Linq.Expressions.Expression<Func<Domain.Internal.Tockens, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Internal.Tockens> GetAll();
        Domain.Internal.Tockens GetById(Guid id);
        void Update(Domain.Internal.Tockens entity);
	}
}