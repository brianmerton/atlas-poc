using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdScheduledIssuesRepository
	{
		Guid Add(Domain.Incidents.hdScheduledIssues entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdScheduledIssues> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdScheduledIssues, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdScheduledIssues> GetAll();
        Domain.Incidents.hdScheduledIssues GetById(Guid id);
        void Update(Domain.Incidents.hdScheduledIssues entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdScheduledIssues> GetByScheduledIssueID(Guid ScheduledIssueID);
	}
}