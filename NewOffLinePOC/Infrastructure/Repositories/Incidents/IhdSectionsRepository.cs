using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdSectionsRepository
	{
		Guid Add(Domain.Incidents.hdSections entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdSections> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdSections, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdSections> GetAll();
        Domain.Incidents.hdSections GetById(Guid id);
        void Update(Domain.Incidents.hdSections entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdSections> GetBySectionID(Guid SectionID);
	}
}