using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IEmailAuditLogRepository
	{
		Guid Add(Domain.Incidents.EmailAuditLog entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.EmailAuditLog> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.EmailAuditLog, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.EmailAuditLog> GetAll();
        Domain.Incidents.EmailAuditLog GetById(Guid id);
        void Update(Domain.Incidents.EmailAuditLog entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.EmailAuditLog> GetByEmailAuditLogId(Guid EmailAuditLogId);
	}
}