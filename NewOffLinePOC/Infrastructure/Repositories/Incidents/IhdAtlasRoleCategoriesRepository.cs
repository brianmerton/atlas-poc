using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdAtlasRoleCategoriesRepository
	{
		Guid Add(Domain.Incidents.hdAtlasRoleCategories entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAtlasRoleCategories> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAtlasRoleCategories, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAtlasRoleCategories> GetAll();
        Domain.Incidents.hdAtlasRoleCategories GetById(Guid id);
        void Update(Domain.Incidents.hdAtlasRoleCategories entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdAtlasRoleCategories> GetByAtlasRoleId(Guid AtlasRoleId);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdAtlasRoleCategories> GetByCategoryId(Guid CategoryId);
	}
}