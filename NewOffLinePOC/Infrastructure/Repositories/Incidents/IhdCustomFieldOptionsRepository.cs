using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdCustomFieldOptionsRepository
	{
		Guid Add(Domain.Incidents.hdCustomFieldOptions entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCustomFieldOptions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdCustomFieldOptions, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCustomFieldOptions> GetAll();
        Domain.Incidents.hdCustomFieldOptions GetById(Guid id);
        void Update(Domain.Incidents.hdCustomFieldOptions entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdCustomFieldOptions> GetByOptionID(Guid OptionID);
	}
}