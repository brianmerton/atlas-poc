using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IIntegrationTicketDataRepository
	{
		Guid Add(Domain.Incidents.IntegrationTicketData entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.IntegrationTicketData> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.IntegrationTicketData, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.IntegrationTicketData> GetAll();
        Domain.Incidents.IntegrationTicketData GetById(Guid id);
        void Update(Domain.Incidents.IntegrationTicketData entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.IntegrationTicketData> GetByIssueID(Guid IssueID);
	}
}