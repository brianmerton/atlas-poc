using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdAPICustomFieldSettingsRepository
	{
		Guid Add(Domain.Incidents.hdAPICustomFieldSettings entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAPICustomFieldSettings, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> GetAll();
        Domain.Incidents.hdAPICustomFieldSettings GetById(Guid id);
        void Update(Domain.Incidents.hdAPICustomFieldSettings entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> GetByAPICustomFieldSettingsID(Guid APICustomFieldSettingsID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> GetByFieldId(Guid FieldId);
	}
}