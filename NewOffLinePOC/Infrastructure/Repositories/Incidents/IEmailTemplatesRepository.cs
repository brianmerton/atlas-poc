using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IEmailTemplatesRepository
	{
		Guid Add(Domain.Incidents.EmailTemplates entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.EmailTemplates> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.EmailTemplates, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.EmailTemplates> GetAll();
        Domain.Incidents.EmailTemplates GetById(Guid id);
        void Update(Domain.Incidents.EmailTemplates entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.EmailTemplates> GetByInstanceID(Guid InstanceID);
	}
}