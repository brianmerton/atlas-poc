using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdOnBehalfIssuesRepository
	{
		Guid Add(Domain.Incidents.hdOnBehalfIssues entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdOnBehalfIssues> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdOnBehalfIssues, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdOnBehalfIssues> GetAll();
        Domain.Incidents.hdOnBehalfIssues GetById(Guid id);
        void Update(Domain.Incidents.hdOnBehalfIssues entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdOnBehalfIssues> GetByIssueID(Guid IssueID);
	}
}