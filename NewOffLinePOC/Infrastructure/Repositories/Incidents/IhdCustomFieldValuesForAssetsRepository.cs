using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdCustomFieldValuesForAssetsRepository
	{
		Guid Add(Domain.Incidents.hdCustomFieldValuesForAssets entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCustomFieldValuesForAssets> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdCustomFieldValuesForAssets, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCustomFieldValuesForAssets> GetAll();
        Domain.Incidents.hdCustomFieldValuesForAssets GetById(Guid id);
        void Update(Domain.Incidents.hdCustomFieldValuesForAssets entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdCustomFieldValuesForAssets> GetByFieldID(Guid FieldID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdCustomFieldValuesForAssets> GetByItemID(Guid ItemID);
	}
}