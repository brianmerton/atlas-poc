using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdAssetsRepository
	{
		Guid Add(Domain.Incidents.hdAssets entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssets> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAssets, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssets> GetAll();
        Domain.Incidents.hdAssets GetById(Guid id);
        void Update(Domain.Incidents.hdAssets entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdAssets> GetByItemID(Guid ItemID);
	}
}