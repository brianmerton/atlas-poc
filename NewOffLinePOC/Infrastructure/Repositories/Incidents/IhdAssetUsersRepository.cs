using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdAssetUsersRepository
	{
		Guid Add(Domain.Incidents.hdAssetUsers entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetUsers> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAssetUsers, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetUsers> GetAll();
        Domain.Incidents.hdAssetUsers GetById(Guid id);
        void Update(Domain.Incidents.hdAssetUsers entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdAssetUsers> GetByItemID(Guid ItemID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdAssetUsers> GetByUserID(Guid UserID);
	}
}