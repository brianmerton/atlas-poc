using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdIssueTagsRepository
	{
		Guid Add(Domain.Incidents.hdIssueTags entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssueTags> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdIssueTags, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssueTags> GetAll();
        Domain.Incidents.hdIssueTags GetById(Guid id);
        void Update(Domain.Incidents.hdIssueTags entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdIssueTags> GetByIssueID(Guid IssueID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdIssueTags> GetByTagID(Guid TagID);
	}
}