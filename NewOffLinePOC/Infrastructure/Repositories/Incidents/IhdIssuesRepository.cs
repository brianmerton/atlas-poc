using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdIssuesRepository
	{
		Guid Add(Domain.Incidents.hdIssues entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssues> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdIssues, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdIssues> GetAll();
        Domain.Incidents.hdIssues GetById(Guid id);
        void Update(Domain.Incidents.hdIssues entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdIssues> GetByAssignedToUserID(Guid AssignedToUserID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdIssues> GetByCategoryID(Guid CategoryID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdIssues> GetByIssueID(Guid IssueID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdIssues> GetByStatusID(Guid StatusID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdIssues> GetByUserID(Guid UserID);
	}
}