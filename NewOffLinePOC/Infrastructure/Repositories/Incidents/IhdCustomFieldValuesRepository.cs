using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdCustomFieldValuesRepository
	{
		Guid Add(Domain.Incidents.hdCustomFieldValues entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCustomFieldValues> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdCustomFieldValues, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCustomFieldValues> GetAll();
        Domain.Incidents.hdCustomFieldValues GetById(Guid id);
        void Update(Domain.Incidents.hdCustomFieldValues entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdCustomFieldValues> GetByFieldID(Guid FieldID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdCustomFieldValues> GetByIssueID(Guid IssueID);
	}
}