using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IUserAvatarsRepository
	{
		Guid Add(Domain.Incidents.UserAvatars entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.UserAvatars> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.UserAvatars, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.UserAvatars> GetAll();
        Domain.Incidents.UserAvatars GetById(Guid id);
        void Update(Domain.Incidents.UserAvatars entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.UserAvatars> GetByUserID(Guid UserID);
	}
}