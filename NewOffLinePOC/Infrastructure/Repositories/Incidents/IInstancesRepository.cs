using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IInstancesRepository
	{
		Guid Add(Domain.Incidents.Instances entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.Instances> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.Instances, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.Instances> GetAll();
        Domain.Incidents.Instances GetById(Guid id);
        void Update(Domain.Incidents.Instances entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.Instances> GetByInstanceID(Guid InstanceID);
	}
}