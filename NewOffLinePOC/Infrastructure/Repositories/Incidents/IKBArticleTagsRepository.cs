using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IKBArticleTagsRepository
	{
		Guid Add(Domain.Incidents.KBArticleTags entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.KBArticleTags> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.KBArticleTags, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.KBArticleTags> GetAll();
        Domain.Incidents.KBArticleTags GetById(Guid id);
        void Update(Domain.Incidents.KBArticleTags entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.KBArticleTags> GetByArticleID(Guid ArticleID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.KBArticleTags> GetByTagID(Guid TagID);
	}
}