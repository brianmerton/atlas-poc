using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdCustomFieldsRepository
	{
		Guid Add(Domain.Incidents.hdCustomFields entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCustomFields> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdCustomFields, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCustomFields> GetAll();
        Domain.Incidents.hdCustomFields GetById(Guid id);
        void Update(Domain.Incidents.hdCustomFields entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdCustomFields> GetByFieldID(Guid FieldID);
	}
}