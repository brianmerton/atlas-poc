using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface ICustomFieldCategoriesRepository
	{
		Guid Add(Domain.Incidents.CustomFieldCategories entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.CustomFieldCategories> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.CustomFieldCategories, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.CustomFieldCategories> GetAll();
        Domain.Incidents.CustomFieldCategories GetById(Guid id);
        void Update(Domain.Incidents.CustomFieldCategories entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.CustomFieldCategories> GetByCategoryID(Guid CategoryID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.CustomFieldCategories> GetByFieldID(Guid FieldID);
	}
}