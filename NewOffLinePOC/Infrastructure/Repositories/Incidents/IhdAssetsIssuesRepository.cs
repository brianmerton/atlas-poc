using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdAssetsIssuesRepository
	{
		Guid Add(Domain.Incidents.hdAssetsIssues entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetsIssues> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdAssetsIssues, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdAssetsIssues> GetAll();
        Domain.Incidents.hdAssetsIssues GetById(Guid id);
        void Update(Domain.Incidents.hdAssetsIssues entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdAssetsIssues> GetByAssetID(Guid AssetID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdAssetsIssues> GetByIssueID(Guid IssueID);
	}
}