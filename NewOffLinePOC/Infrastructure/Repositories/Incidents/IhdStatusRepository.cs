using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdStatusRepository
	{
		Guid Add(Domain.Incidents.hdStatus entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdStatus> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdStatus, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdStatus> GetAll();
        Domain.Incidents.hdStatus GetById(Guid id);
        void Update(Domain.Incidents.hdStatus entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdStatus> GetByStatusID(Guid StatusID);
	}
}