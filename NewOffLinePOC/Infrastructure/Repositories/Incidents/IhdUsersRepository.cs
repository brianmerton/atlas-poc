using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdUsersRepository
	{
		Guid Add(Domain.Incidents.hdUsers entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdUsers> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdUsers, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdUsers> GetAll();
        Domain.Incidents.hdUsers GetById(Guid id);
        void Update(Domain.Incidents.hdUsers entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdUsers> GetByInstanceID(Guid InstanceID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdUsers> GetByUserID(Guid UserID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdUsers> GetByUserName(Guid UserName);
	}
}