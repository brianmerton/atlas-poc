using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdCompaniesRepository
	{
		Guid Add(Domain.Incidents.hdCompanies entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCompanies> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdCompanies, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCompanies> GetAll();
        Domain.Incidents.hdCompanies GetById(Guid id);
        void Update(Domain.Incidents.hdCompanies entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdCompanies> GetByCompanyID(Guid CompanyID);
	}
}