using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IKBArticlesRepository
	{
		Guid Add(Domain.Incidents.KBArticles entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.KBArticles> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.KBArticles, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.KBArticles> GetAll();
        Domain.Incidents.KBArticles GetById(Guid id);
        void Update(Domain.Incidents.KBArticles entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.KBArticles> GetByArticleID(Guid ArticleID);
	}
}