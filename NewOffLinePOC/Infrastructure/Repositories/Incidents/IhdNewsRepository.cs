using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdNewsRepository
	{
		Guid Add(Domain.Incidents.hdNews entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdNews> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdNews, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdNews> GetAll();
        Domain.Incidents.hdNews GetById(Guid id);
        void Update(Domain.Incidents.hdNews entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdNews> GetByNewsID(Guid NewsID);
	}
}