using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdCategoryPermissionsRepository
	{
		Guid Add(Domain.Incidents.hdCategoryPermissions entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCategoryPermissions> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdCategoryPermissions, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdCategoryPermissions> GetAll();
        Domain.Incidents.hdCategoryPermissions GetById(Guid id);
        void Update(Domain.Incidents.hdCategoryPermissions entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdCategoryPermissions> GetByCategoryID(Guid CategoryID);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdCategoryPermissions> GetByUserID(Guid UserID);
	}
}