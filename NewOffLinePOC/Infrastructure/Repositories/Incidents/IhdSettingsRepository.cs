using System;
namespace Infrastructure.Repositories.Incidents
{
	public interface IhdSettingsRepository
	{
		Guid Add(Domain.Incidents.hdSettings entity);
        void DeleteById(Guid id);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdSettings> FindBy(System.Linq.Expressions.Expression<Func<Domain.Incidents.hdSettings, bool>> predicate);
        System.Collections.Generic.IEnumerable<Domain.Incidents.hdSettings> GetAll();
        Domain.Incidents.hdSettings GetById(Guid id);
        void Update(Domain.Incidents.hdSettings entity);
        System.Collections.Generic. IEnumerable<Domain.Incidents.hdSettings> GetByInstanceID(Guid InstanceID);
	}
}