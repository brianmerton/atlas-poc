using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Internal
{
    public class Tockens
    {
		public byte[] Data { get; set; }
		public string Hash { get; set; }
		public Guid Id { get; set; }
	}
}
