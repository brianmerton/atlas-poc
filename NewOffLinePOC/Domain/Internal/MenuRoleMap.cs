using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Internal
{
    public class MenuRoleMap
    {
		public Guid MenuId { get; set; }
		public Guid RoleId { get; set; }
	}
}
