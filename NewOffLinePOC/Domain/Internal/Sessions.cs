using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Internal
{
    public class Sessions
    {
		public DateTime EndDate { get; set; }
		public Guid Id { get; set; }
		public string SessionId { get; set; }
		public DateTime StartDate { get; set; }
		public Guid UserId { get; set; }
		public string UserName { get; set; }
	}
}
