using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Internal
{
    public class MapReferences
    {
		public string ExternalReferenceKey { get; set; }
		public Guid Id { get; set; }
		public Guid ObjectId { get; set; }
		public int Type { get; set; }
	}
}
