using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Internal
{
    public class Migrations
    {
		public Guid Id { get; set; }
		public string TypeName { get; set; }
	}
}
