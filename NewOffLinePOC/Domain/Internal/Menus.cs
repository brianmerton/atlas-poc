using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Internal
{
    public class Menus
    {
		public string Action { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string CssClass { get; set; }
		public string Description { get; set; }
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public string Key { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public string NavigateTo { get; set; }
		public int OrderNumber { get; set; }
		public Guid ParentId { get; set; }
		public bool PassParams { get; set; }
		public bool RequireCid { get; set; }
		public string Sector { get; set; }
		public string Title { get; set; }
		public string Version { get; set; }
	}
}
