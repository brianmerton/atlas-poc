using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Internal
{
    public class RoleOperationMap
    {
		public Guid OperationId { get; set; }
		public Guid RoleId { get; set; }
	}
}
