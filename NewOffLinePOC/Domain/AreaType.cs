﻿namespace Domain
{
    public enum AreaType : int
    {
        General = 0,
        HealthAndSafety = 1,
        EmploymentLaw = 2
    }
    public enum AreaWithAll
    {
        All = -1,
        General = 0,
        HealthAndSafety = 1,
        EmploymentLaw = 2
    }
}
