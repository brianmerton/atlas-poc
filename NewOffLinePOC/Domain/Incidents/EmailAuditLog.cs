using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class EmailAuditLog
    {
		public string Attachment { get; set; }
		public string Body { get; set; }
		public string CCEmailAddress { get; set; }
		public DateTime CreatedDateTime { get; set; }
		public int EmailAuditLogId { get; set; }
		public string FromEmailAddress { get; set; }
		public bool IsInBound { get; set; }
		public int IssueId { get; set; }
		public DateTime ReceivedDateTime { get; set; }
		public DateTime SentDateTime { get; set; }
		public int ServerID { get; set; }
		public string Subject { get; set; }
		public string ToEmailAddress { get; set; }
	}
}
