using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class AutomationRules
    {
		public int InstanceID { get; set; }
		public int OrderByNumber { get; set; }
		public int RuleID { get; set; }
		public string RuleXml { get; set; }
	}
}
