using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdAssets
    {
		public string Comments { get; set; }
		public int InstanceID { get; set; }
		public int ItemID { get; set; }
		public string Location { get; set; }
		public int ManufacturerID { get; set; }
		public string ModelName { get; set; }
		public int Quantity { get; set; }
		public string SerialNumber { get; set; }
		public int SupplierID { get; set; }
		public int TypeID { get; set; }
	}
}
