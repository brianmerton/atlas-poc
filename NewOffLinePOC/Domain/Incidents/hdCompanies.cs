using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdCompanies
    {
		public Guid ATLASCOMPANYID { get; set; }
		public int CompanyID { get; set; }
		public int CRMID { get; set; }
		public bool Disabled { get; set; }
		public string EmailDomain { get; set; }
		public int InstanceID { get; set; }
		public string Name { get; set; }
		public string Notes { get; set; }
	}
}
