using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class CustomFieldCategories
    {
		public int CategoryID { get; set; }
		public int FieldID { get; set; }
	}
}
