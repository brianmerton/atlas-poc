using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdAPICustomFieldValues
    {
		public int Fieldid { get; set; }
		public int IssueId { get; set; }
		public Guid Value { get; set; }
	}
}
