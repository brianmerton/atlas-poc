using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdScheduledIssues
    {
		public int OriginalIssueID { get; set; }
		public int ScheduledIssueID { get; set; }
	}
}
