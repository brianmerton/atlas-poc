using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdFileAttachments
    {
		public Guid AtlasDocumentId { get; set; }
		public int CommentID { get; set; }
		public string DropboxUrl { get; set; }
		public byte[] FileData { get; set; }
		public byte[] FileHash { get; set; }
		public int FileID { get; set; }
		public string FileName { get; set; }
		public int FileSize { get; set; }
		public string GoogleDriveUrl { get; set; }
		public int IssueID { get; set; }
		public int KBArticleID { get; set; }
		public int UserID { get; set; }
	}
}
