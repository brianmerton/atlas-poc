using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdNews
    {
		public string Body { get; set; }
		public int InstanceID { get; set; }
		public DateTime NewsDate { get; set; }
		public int NewsID { get; set; }
		public string Subject { get; set; }
		public int UserID { get; set; }
	}
}
