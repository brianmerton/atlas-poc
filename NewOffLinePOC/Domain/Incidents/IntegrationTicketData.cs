using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class IntegrationTicketData
    {
		public string BitbucketIssueUrl { get; set; }
		public string GitHubIssueUrl { get; set; }
		public int IssueID { get; set; }
		public string JiraIssueUrl { get; set; }
	}
}
