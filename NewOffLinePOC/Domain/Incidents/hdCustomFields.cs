using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdCustomFields
    {
		public int FieldID { get; set; }
		public string FieldName { get; set; }
		public bool ForTechsOnly { get; set; }
		public int InstanceID { get; set; }
		public bool Mandatory { get; set; }
		public int OrderByNumber { get; set; }
		public int ParentCustomFieldId { get; set; }
		public bool ShowInGrid { get; set; }
		public int Type { get; set; }
		public int UsageType { get; set; }
	}
}
