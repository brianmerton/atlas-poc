using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class UserAvatars
    {
		public byte[] ImageData { get; set; }
		public int UserID { get; set; }
	}
}
