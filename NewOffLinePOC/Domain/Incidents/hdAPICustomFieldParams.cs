using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdAPICustomFieldParams
    {
		public int APICustomFieldParamsID { get; set; }
		public int APICustomFieldSettingsID { get; set; }
		public string JitbitReference { get; set; }
		public string ParamName { get; set; }
	}
}
