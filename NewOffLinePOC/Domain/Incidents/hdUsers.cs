using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdUsers
    {
		public bool AdminTutorialShown { get; set; }
		public Guid ATLASUSERID { get; set; }
		public int CompanyID { get; set; }
		public string Department { get; set; }
		public bool Disabled { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string Greeting { get; set; }
		public string HostName { get; set; }
		public int InstanceID { get; set; }
		public string IPAddress { get; set; }
		public bool IsAdministrator { get; set; }
		public bool IsManager { get; set; }
		public string Lang { get; set; }
		public string LastName { get; set; }
		public DateTime LastSeen { get; set; }
		public string Location { get; set; }
		public string Notes { get; set; }
		public string Password { get; set; }
		public string Phone { get; set; }
		public string PushToken { get; set; }
		public bool SendEmail { get; set; }
		public string Signature { get; set; }
		public bool TechTutorialShown { get; set; }
		public string UserAgent { get; set; }
		public int UserID { get; set; }
		public string UserName { get; set; }
	}
}
