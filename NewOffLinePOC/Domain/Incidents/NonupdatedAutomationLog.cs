using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class NonupdatedAutomationLog
    {
		public int IssueID { get; set; }
		public int RuleID { get; set; }
	}
}
