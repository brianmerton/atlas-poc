using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class KBArticleTags
    {
		public int ArticleID { get; set; }
		public int TagID { get; set; }
	}
}
