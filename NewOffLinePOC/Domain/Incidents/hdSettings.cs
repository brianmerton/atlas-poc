using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdSettings
    {
		public string ActiveTabTextColor { get; set; }
		public bool AllowAnonymousKBAccess { get; set; }
		public bool AllowGuestSubmission { get; set; }
		public bool AllowUserRegistration { get; set; }
		public bool AutoAssignFirstReplier { get; set; }
		public bool AutoCloseImportedCases { get; set; }
		public string AutoLoginSharedSecret { get; set; }
		public string CRMRootUrl { get; set; }
		public string CRMSharedKey { get; set; }
		public string CustomCSS { get; set; }
		public int DaysToCloseAfter { get; set; }
		public bool DisableAssets { get; set; }
		public bool DisableAutoTimer { get; set; }
		public bool DisableAvatars { get; set; }
		public bool DisableCloseNotification { get; set; }
		public bool DisableKB { get; set; }
		public bool DisableTicketConfirmationNotification { get; set; }
		public bool DontAddCCToSubscribers { get; set; }
		public bool DontReopenOnReply { get; set; }
		public bool EmailNotificationsEnabled { get; set; }
		public bool EmailOtherTechsOnTakeover { get; set; }
		public bool EnableSaml { get; set; }
		public string FromAddress { get; set; }
		public string FromName { get; set; }
		public char HeaderBGColor { get; set; }
		public char HeaderTextColor { get; set; }
		public bool HidePoweredBy { get; set; }
		public int InstanceID { get; set; }
		public string Lang { get; set; }
		public byte[] LogoImage { get; set; }
		public bool MailCheckerAllowUnregisteredUsers { get; set; }
		public int MailCheckerNewIssuesCategoryID { get; set; }
		public int MaxAttachSize { get; set; }
		public char MenuBarBGColor { get; set; }
		public char MenuBarTextColor { get; set; }
		public int NewIssuesDefaultCategoryID { get; set; }
		public string NewTicketNote { get; set; }
		public bool NotifyAllTechsOnReply { get; set; }
		public string RemoteLoginURL { get; set; }
		public string ReplyToAddress { get; set; }
		public bool RestrictIssueClosing { get; set; }
		public bool RestrictIssueDeletion { get; set; }
		public bool SameCompanyUserPermission { get; set; }
		public string SamlCertificate { get; set; }
		public string SamlEndpoint { get; set; }
		public bool SeeEveryonesIssues { get; set; }
		public bool SendAdminEmailOnNewIssue { get; set; }
		public bool SendTechniciansEmailOnNewIssue { get; set; }
		public bool SendTechSMSOnNewIssue { get; set; }
		public int ServerTimeOffset { get; set; }
		public bool SMTPAuthRequired { get; set; }
		public string SMTPPassword { get; set; }
		public int SMTPPort { get; set; }
		public string SMTPServer { get; set; }
		public string SMTPUserName { get; set; }
		public bool SMTPUseSSL { get; set; }
		public string TimezoneID { get; set; }
		public string Title { get; set; }
		public bool UseDefaultSmtp { get; set; }
		public bool UseFromNameForUserOriginatedMsgs { get; set; }
		public int WorkHourFrom { get; set; }
		public int WorkHourTo { get; set; }
	}
}
