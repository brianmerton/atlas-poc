using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdIssueSchedule
    {
		public DateTime FirstRecurrenceDate { get; set; }
		public int FreqType { get; set; }
		public int IssueID { get; set; }
		public int PrevRepeatsCount { get; set; }
		public bool ReopenOriginal { get; set; }
	}
}
