using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdComments
    {
		public string Body { get; set; }
		public DateTime CommentDate { get; set; }
		public int CommentID { get; set; }
		public string EmailHeaders { get; set; }
		public bool ForTechsOnly { get; set; }
		public bool HiddenFromKB { get; set; }
		public int IssueID { get; set; }
		public bool IsSystem { get; set; }
		public string Recipients { get; set; }
		public int UserID { get; set; }
	}
}
