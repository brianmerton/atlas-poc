using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdStatus
    {
		public string ButtonCaption { get; set; }
		public bool ForTechsOnly { get; set; }
		public int InstanceID { get; set; }
		public string Name { get; set; }
		public int StatusID { get; set; }
	}
}
