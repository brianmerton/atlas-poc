using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class Integrations
    {
		public bool BitbucketEnabled { get; set; }
		public string BitbucketPassword { get; set; }
		public string BitbucketUsername { get; set; }
		public string DropboxAppID { get; set; }
		public bool DropboxEnabled { get; set; }
		public bool GitHubEnabled { get; set; }
		public string GitHubPassword { get; set; }
		public string GitHubUsername { get; set; }
		public string HipChatAuthToken { get; set; }
		public bool HipChatEnabled { get; set; }
		public int HipChatRoomId { get; set; }
		public int InstanceID { get; set; }
		public bool JiraEnabled { get; set; }
		public string JiraPassword { get; set; }
		public string JiraUrl { get; set; }
		public string JiraUsername { get; set; }
		public bool SlackEnabled { get; set; }
		public string SlackWebhookURL { get; set; }
	}
}
