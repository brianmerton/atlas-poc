using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class EmailTemplates
    {
		public string ClosedTicketEmailSubject { get; set; }
		public string ClosedTicketEmailTemplate { get; set; }
		public string EmailSubjectTemplate { get; set; }
		public string EmailTemplate { get; set; }
		public int InstanceID { get; set; }
		public string NewIssueConfirmationBody { get; set; }
		public string NewIssueConfirmationSubj { get; set; }
		public string NewIssueEmailSubject { get; set; }
		public string NewIssueEmailTemplate { get; set; }
		public string WelcomeEmailBody { get; set; }
		public string WelcomeEmailSubj { get; set; }
	}
}
