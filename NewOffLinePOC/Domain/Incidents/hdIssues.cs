using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdIssues
    {
		public int AssignedToUserID { get; set; }
		public string Body { get; set; }
		public int CategoryID { get; set; }
		public int ChannelId { get; set; }
		public DateTime DueDate { get; set; }
		public int InstanceID { get; set; }
		public bool IsDeleted { get; set; }
		public DateTime IssueDate { get; set; }
		public int IssueID { get; set; }
		public bool KBForTechsOnly { get; set; }
		public DateTime LastUpdated { get; set; }
		public int Priority { get; set; }
		public bool PublishToKB { get; set; }
		public DateTime ResolvedDate { get; set; }
		public DateTime StartDate { get; set; }
		public int StatusID { get; set; }
		public string Subject { get; set; }
		public int TimeSpentInSeconds { get; set; }
		public bool UpdatedByPerformer { get; set; }
		public bool UpdatedByUser { get; set; }
		public bool UpdatedForTechView { get; set; }
		public int UserID { get; set; }
	}
}
