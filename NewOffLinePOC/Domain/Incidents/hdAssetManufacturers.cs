using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdAssetManufacturers
    {
		public int InstanceID { get; set; }
		public int ManufacturerID { get; set; }
		public string Name { get; set; }
	}
}
