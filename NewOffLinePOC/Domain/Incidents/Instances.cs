using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class Instances
    {
		public string AvangateSubscriptionID { get; set; }
		public string CustomDomain { get; set; }
		public int CustomerID { get; set; }
		public int InstanceID { get; set; }
		public string Name { get; set; }
		public DateTime ValidTill { get; set; }
		public bool WidgetInstalled { get; set; }
	}
}
