using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdCategories
    {
		public int CategoryID { get; set; }
		public bool ForSpecificUsers { get; set; }
		public bool ForTechsOnly { get; set; }
		public string FromAddress { get; set; }
		public int InstanceID { get; set; }
		public bool IsDeleted { get; set; }
		public string Name { get; set; }
		public int OrderByNumber { get; set; }
		public int SectionID { get; set; }
	}
}
