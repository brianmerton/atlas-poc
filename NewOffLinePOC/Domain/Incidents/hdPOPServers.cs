using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdPOPServers
    {
		public string HostName { get; set; }
		public int InstanceID { get; set; }
		public bool IsImap { get; set; }
		public int NewTicketsCategoryID { get; set; }
		public string POPLogin { get; set; }
		public string POPPassword { get; set; }
		public int Port { get; set; }
		public int ServerID { get; set; }
		public string SharedMailBoxAlias { get; set; }
		public bool UseSSL { get; set; }
	}
}
