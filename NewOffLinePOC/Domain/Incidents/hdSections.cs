using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdSections
    {
		public int InstanceID { get; set; }
		public string Name { get; set; }
		public int OrderByNumber { get; set; }
		public int SectionID { get; set; }
	}
}
