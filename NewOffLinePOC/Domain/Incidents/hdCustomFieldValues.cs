using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdCustomFieldValues
    {
		public Guid AtlasRefId { get; set; }
		public int FieldID { get; set; }
		public int IssueID { get; set; }
		public string Value { get; set; }
	}
}
