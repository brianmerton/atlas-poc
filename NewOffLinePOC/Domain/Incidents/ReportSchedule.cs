using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class ReportSchedule
    {
		public int InstanceID { get; set; }
		public DateTime LastSent { get; set; }
		public string ReportModelXml { get; set; }
	}
}
