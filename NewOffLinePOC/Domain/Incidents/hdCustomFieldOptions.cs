using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdCustomFieldOptions
    {
		public int FieldID { get; set; }
		public bool IsDefault { get; set; }
		public int OptionID { get; set; }
		public string OptionValue { get; set; }
		public int ParentCustomFieldOptionId { get; set; }
	}
}
