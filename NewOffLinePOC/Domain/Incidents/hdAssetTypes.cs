using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdAssetTypes
    {
		public int InstanceID { get; set; }
		public string Name { get; set; }
		public int TypeID { get; set; }
	}
}
