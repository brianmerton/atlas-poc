using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdCustomFieldValuesForAssets
    {
		public int FieldID { get; set; }
		public int ItemID { get; set; }
		public string Value { get; set; }
	}
}
