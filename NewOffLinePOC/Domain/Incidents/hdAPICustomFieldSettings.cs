using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdAPICustomFieldSettings
    {
		public int APICustomFieldSettingsID { get; set; }
		public string APILink { get; set; }
		public string ClientId { get; set; }
		public int FieldId { get; set; }
		public string SecurityKey { get; set; }
		public string TextField { get; set; }
		public string UrlLink { get; set; }
		public string ValueField { get; set; }
	}
}
