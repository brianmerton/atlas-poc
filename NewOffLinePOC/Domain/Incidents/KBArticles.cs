using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class KBArticles
    {
		public int ArticleID { get; set; }
		public string Body { get; set; }
		public int CategoryID { get; set; }
		public bool ForTechsOnly { get; set; }
		public int InstanceID { get; set; }
		public int OrderByNumber { get; set; }
		public string Subject { get; set; }
	}
}
