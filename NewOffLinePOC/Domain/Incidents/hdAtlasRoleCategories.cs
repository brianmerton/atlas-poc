using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Incidents
{
    public class hdAtlasRoleCategories
    {
		public int AreaType { get; set; }
		public Guid AtlasRoleId { get; set; }
		public int CategoryId { get; set; }
	}
}
