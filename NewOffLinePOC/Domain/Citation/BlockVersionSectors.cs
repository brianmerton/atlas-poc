using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class BlockVersionSectors
    {
		public Guid BlockVersionId { get; set; }
		public Guid SectorId { get; set; }
	}
}
