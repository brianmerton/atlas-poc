using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class ContactDetails
    {
		public Guid ClientTypeId { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public string LastName { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public string Notes { get; set; }
		public string Phone { get; set; }
		public Guid UserId { get; set; }
		public string Version { get; set; }
	}
}
