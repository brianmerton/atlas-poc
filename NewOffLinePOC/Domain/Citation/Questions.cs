using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class Questions
    {
		public int ActionType { get; set; }
		public Guid AssociatedContentId { get; set; }
		public Guid AttributeId { get; set; }
		public Guid BlockId { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string DefaultObservation { get; set; }
		public string Description { get; set; }
		public int ExpectedAnswer { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public int OrderIndex { get; set; }
		public int Priority { get; set; }
		public string Target { get; set; }
		public string Title { get; set; }
		public int Type { get; set; }
		public string Version { get; set; }
	}
}
