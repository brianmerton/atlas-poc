using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class WorkItems
    {
		public int AttemptsCount { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public byte[] Data { get; set; }
		public string ErrorMessage { get; set; }
		public string ErrorStack { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public Guid ProcessJobId { get; set; }
		public DateTime ScheduledStart { get; set; }
		public int State { get; set; }
		public Guid TenantId { get; set; }
		public string Version { get; set; }
	}
}
