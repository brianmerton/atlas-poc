using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class DocumentReferences
    {
		public int Category { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public Guid RegardingObjectId { get; set; }
		public int RegardingObjectTypeCode { get; set; }
		public Guid TemplateId { get; set; }
		public Guid TenantId { get; set; }
		public string Title { get; set; }
		public int Usage { get; set; }
		public string Version { get; set; }
	}
}
