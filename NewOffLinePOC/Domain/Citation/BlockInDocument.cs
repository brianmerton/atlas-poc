using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class BlockInDocument
    {
		public Guid BlockId { get; set; }
		public Guid DocumentId { get; set; }
		public Guid Id { get; set; }
	}
}
