using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class BlockSectors
    {
		public Guid BlockId { get; set; }
		public Guid SectorId { get; set; }
	}
}
