using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class Logs
    {
		public int Category { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public string Message { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public int Type { get; set; }
		public string Version { get; set; }
	}
}
