using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class CompanyClientTypes
    {
		public Guid ClientTypeId { get; set; }
		public Guid CompanyId { get; set; }
	}
}
