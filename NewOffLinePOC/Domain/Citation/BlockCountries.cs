using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class BlockCountries
    {
		public Guid BlockId { get; set; }
		public Guid CountryId { get; set; }
	}
}
