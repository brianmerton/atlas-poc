using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class Companies
    {
		public Guid ClientAffiliationId { get; set; }
		public int ClientArea { get; set; }
		public DateTime ContractEndDate { get; set; }
		public DateTime ContractStartDate { get; set; }
		public Guid CountryId { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string Description { get; set; }
		public bool ExtraEvents { get; set; }
		public string FullName { get; set; }
		public Guid Id { get; set; }
		public bool IsActive { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public int jitbitCompanyId { get; set; }
		public int LCid { get; set; }
		public bool Migrate { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public Guid ParentCompanyId { get; set; }
		public string PostCode { get; set; }
		public string Prefix { get; set; }
		public string SalesforceAccountID { get; set; }
		public Guid SectorId { get; set; }
		public string ShortName { get; set; }
		public string SiteUrl { get; set; }
		public bool SystemEvents { get; set; }
		public string TenantName { get; set; }
		public string Version { get; set; }
	}
}
