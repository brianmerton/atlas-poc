using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class Block
    {
		public int Area { get; set; }
		public int Category { get; set; }
		public bool Checked { get; set; }
		public string Comment { get; set; }
		public string Content { get; set; }
		public Guid CountryId { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public int DeletionState { get; set; }
		public string Description { get; set; }
		public string Discriminator { get; set; }
		public int FixedPositionIndex { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public bool IsOriginal { get; set; }
		public bool IsSystem { get; set; }
		public Guid KbGroupId { get; set; }
		public int LastChange { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public int Multiplicity { get; set; }
		public string Observation { get; set; }
		public bool Options_ActAsGroup { get; set; }
		public bool Options_IncludeIndexNumbers { get; set; }
		public bool Options_IsReadonly { get; set; }
		public bool Options_LandscapeLayout { get; set; }
		public bool Options_PageBreakAfter { get; set; }
		public bool Options_PageBreakBefore { get; set; }
		public int OrderIndex { get; set; }
		public Guid OriginalBlockId { get; set; }
		public Guid ParentBlockId { get; set; }
		public int Priority { get; set; }
		public string Recommendation { get; set; }
		public Guid RootId { get; set; }
		public Guid SectorId { get; set; }
		public Guid SourceBlockId { get; set; }
		public string SourceBlockVersion { get; set; }
		public Guid SubCategoryId { get; set; }
		public string Title { get; set; }
		public int Type { get; set; }
		public string Version { get; set; }
	}
}
