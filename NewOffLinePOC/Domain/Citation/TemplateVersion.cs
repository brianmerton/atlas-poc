using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class TemplateVersion
    {
		public int Area { get; set; }
		public int Category { get; set; }
		public string Comment { get; set; }
		public Guid CountryId { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public int DeletionState { get; set; }
		public string Description { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public int LastChange { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public Guid OriginalTemplateId { get; set; }
		public Guid PrototypeId { get; set; }
		public Guid SectorId { get; set; }
		public string TargetEntityOTC { get; set; }
		public string Title { get; set; }
		public string Version { get; set; }
	}
}
