using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class AdviceCardTypeMap
    {
		public Guid AdviceCardId { get; set; }
		public Guid AdviceTypeId { get; set; }
	}
}
