using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class BlockVersionCountries
    {
		public Guid BlockVersionId { get; set; }
		public Guid CountryId { get; set; }
	}
}
