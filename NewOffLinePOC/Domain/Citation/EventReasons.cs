using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class EventReasons
    {
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public string Reason { get; set; }
		public string ShortCode { get; set; }
		public string Version { get; set; }
	}
}
