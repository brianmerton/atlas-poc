using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class BlockInContainer
    {
		public Guid BlockId { get; set; }
		public Guid ContainerId { get; set; }
		public int OrderIndex { get; set; }
	}
}
