using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class Appointments
    {
		public Guid CompanyId { get; set; }
		public Guid ConsultantId { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime EndDateTime { get; set; }
		public string exchCalItemID { get; set; }
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public string Notes { get; set; }
		public Guid PrototypeId { get; set; }
		public string Reason { get; set; }
		public Guid SiteId { get; set; }
		public Guid SLATypeId { get; set; }
		public DateTime StartDateTime { get; set; }
		public int Status { get; set; }
		public Guid TaskId { get; set; }
		public decimal TimeSpent { get; set; }
		public string Version { get; set; }
	}
}
