using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Citation
{
    public class Monitoring
    {
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public byte[] Data { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public bool IsFailed { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public DateTime OperationDate { get; set; }
		public Guid OperationId { get; set; }
		public Guid UserId { get; set; }
		public string Version { get; set; }
	}
}
