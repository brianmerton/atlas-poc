﻿using System;

namespace Domain
{
    public class SecurityCore
    {
        public const string SYSTEM_TENANT_NAME = "Citation";
        public const string INTERNAL_SCHEMA_NAME = "Internal";
        public const string SHARED_SCHEMA_NAME = "Shared";
        public const string CASEMANAGEMNT_SCHEMA_NAME = "Incidents";
        public const string DBO_SCHEMA_NAME = "dbo";

        public static readonly Guid AdminUserId = new Guid("{89504E36-557B-4691-8F1B-7E86F9CF95EA}");
        public static readonly Guid CitationTenantId = AdminUserId;
        public static readonly Guid SystemUserId = new Guid("{EC69E0DB-078A-4D86-A44D-8DC4AB876026}");
        public const string CITATION_ADMIN_NAME = "Citation Administrator";
        public const string CITATION_CONSULTANT_NAME = "Citation Consultant";
        public const string CASE_MANAGEMENT_NAME = "Case Management";
        public static readonly Guid CitationAdminId = new Guid("{9D3DCA6A-7B60-4900-9444-E19A8834D161}");
        public static readonly Guid CitationConsultantId = new Guid("{C479389E-5EE5-42D5-A5C6-6E9E4023C494}");
        public static readonly Guid CitationAffliationId = new Guid("{0D7A4CEA-09C6-4001-B2F1-27DAC20BCE5A}");
        public static readonly Guid ClientRelation = new Guid("{428B542A-CA7C-4CD4-8B84-98FD785352A8}");
        public const string CitationAffiliateName = "Citation";
        //TODO:To be replaced when role based model is implemented.
        public const string SERVICE_OWNER = "Service Owner";


        //Below are the User roles/claims that needs to be used in the system.
        public const string AdminCitationEmail = "admin@citation.co.uk";
    }
}
