using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class UsersIdentity
    {
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string Email { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public string Password { get; set; }
		public string PasswordAnswer { get; set; }
		public string PasswordQuestion { get; set; }
		public Guid ResetToken { get; set; }
		public bool TcAccepted { get; set; }
		public string TcBrowserMetaData { get; set; }
		public string TcIpAddress { get; set; }
		public DateTime TcShownOn { get; set; }
		public string TcVersion { get; set; }
		public string TenantName { get; set; }
		public DateTime TokenExpiryDate { get; set; }
		public string Version { get; set; }
	}
}
