using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class CheckListVersionItems
    {
		public Guid CheckItemId { get; set; }
		public Guid CheckListVersionId { get; set; }
	}
}
