using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class RiskAssessmentType
    {
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public string Name { get; set; }
		public Guid PictureId { get; set; }
		public int Sequence { get; set; }
		public string Version { get; set; }
	}
}
