using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class SectorWorkspaceMap
    {
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid Icon { get; set; }
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public string Label { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public Guid SectorId { get; set; }
		public string Version { get; set; }
		public Guid WorkspaceTypeId { get; set; }
	}
}
