using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class RoleClientTypeMap
    {
		public Guid ClientTypeId { get; set; }
		public Guid RoleId { get; set; }
	}
}
