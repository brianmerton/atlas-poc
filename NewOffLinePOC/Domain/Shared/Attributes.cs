using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class Attributes
    {
		public int Area { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string DisplayName { get; set; }
		public string Group { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public bool IsMandatory { get; set; }
		public int LCid { get; set; }
		public string LogicalName { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public string ObjectTypeCode { get; set; }
		public Guid ParentAttributeId { get; set; }
		public int Type { get; set; }
		public string Version { get; set; }
	}
}
