using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class Users
    {
		public int Area { get; set; }
		public Guid CompanyId { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public bool HasEmployee { get; set; }
		public Guid Id { get; set; }
		public bool IsArchived { get; set; }
		public bool IsDeleted { get; set; }
		public int jitbitUserId { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public Guid PictureId { get; set; }
		public string Qualifications { get; set; }
		public string SalesforceUserID { get; set; }
		public string SecondName { get; set; }
		public Guid SignatureFileId { get; set; }
		public string Telephone { get; set; }
		public string Version { get; set; }
	}
}
