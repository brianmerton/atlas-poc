using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class InvQuestions
    {
		public int AnswerType { get; set; }
		public int AttachObjectType { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid Id { get; set; }
		public Guid InvSectionId { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public string Question { get; set; }
		public string RelatedAttribute { get; set; }
		public int RelatedObject { get; set; }
		public int Sequence { get; set; }
		public string Validations { get; set; }
		public string Version { get; set; }
	}
}
