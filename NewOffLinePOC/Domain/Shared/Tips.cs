using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class Tips
    {
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public int Service { get; set; }
		public string Tip { get; set; }
		public string Url { get; set; }
		public string Version { get; set; }
	}
}
