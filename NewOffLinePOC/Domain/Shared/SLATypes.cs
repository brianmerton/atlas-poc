using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class SLATypes
    {
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public int CreateReminderBefore { get; set; }
		public int CreateTaskBefore { get; set; }
		public int DaysAfter { get; set; }
		public Guid EmailTemplateId { get; set; }
		public Guid IconId { get; set; }
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public bool IsRecursive { get; set; }
		public bool IsSystemTriggered { get; set; }
		public int LCid { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public bool OnlyforHeadOffice { get; set; }
		public int SLADateType { get; set; }
		public string Version { get; set; }
		public Guid VisitTypeId { get; set; }
	}
}
