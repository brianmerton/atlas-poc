using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class InvSectionIncidentCategories
    {
		public Guid IncidentCategoryId { get; set; }
		public Guid InvSectionId { get; set; }
	}
}
