using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class InjuryTypeInjuredParties
    {
		public Guid InjuredPartyId { get; set; }
		public Guid InjuryTypeId { get; set; }
	}
}
