using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared
{
    public class TrackerSettingsVersions
    {
		public string AppointmentBookedColor { get; set; }
		public string AppointmentCanceleldColor { get; set; }
		public string AppointmentCompletedColor { get; set; }
		public string AppointmentOverdueColor { get; set; }
		public string AppointmentPlannedColor { get; set; }
		public string AppointmentRescheduledColor { get; set; }
		public string Comment { get; set; }
		public decimal ConsultantMinimalTimeSlot { get; set; }
		public decimal ConsultantWorkStartAt { get; set; }
		public Guid CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public int LCid { get; set; }
		public int MatchMax { get; set; }
		public int MatchStep { get; set; }
		public Guid ModifiedBy { get; set; }
		public DateTime ModifiedOn { get; set; }
		public Guid PrototypeId { get; set; }
		public int SLAOverdueDays { get; set; }
		public int TempAssignmentDaysAfter { get; set; }
		public int TempAssignmentDaysBefore { get; set; }
		public string Version { get; set; }
	}
}
