using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Customer
{
	public class NotesRepository : Infrastructure.Repositories.Shared.INotesRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Customer.Notes> mapper { get; set; }

        public NotesRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Customer.Notes> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Customer.Notes> FindBy(Expression<Func<Domain.Customer.Notes, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Customer.Notes> GetAll()
        {
            var items = new List<Domain.Customer.Notes>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[NotesGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Customer.Notes GetById(Guid id)
        {
            var ItemToReturn = new Domain.Customer.Notes();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[NotesGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Customer.Notes entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[NotesInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Customer.Notes entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[NotesUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[NotesDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}