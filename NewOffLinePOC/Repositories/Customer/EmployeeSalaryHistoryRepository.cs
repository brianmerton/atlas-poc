using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Customer
{
	public class EmployeeSalaryHistoryRepository : Infrastructure.Repositories.Shared.IEmployeeSalaryHistoryRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Customer.EmployeeSalaryHistory> mapper { get; set; }

        public EmployeeSalaryHistoryRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Customer.EmployeeSalaryHistory> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Customer.EmployeeSalaryHistory> FindBy(Expression<Func<Domain.Customer.EmployeeSalaryHistory, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Customer.EmployeeSalaryHistory> GetAll()
        {
            var items = new List<Domain.Customer.EmployeeSalaryHistory>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[EmployeeSalaryHistoryGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Customer.EmployeeSalaryHistory GetById(Guid id)
        {
            var ItemToReturn = new Domain.Customer.EmployeeSalaryHistory();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[EmployeeSalaryHistoryGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Customer.EmployeeSalaryHistory entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[EmployeeSalaryHistoryInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Customer.EmployeeSalaryHistory entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[EmployeeSalaryHistoryUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[EmployeeSalaryHistoryDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}