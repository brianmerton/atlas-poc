using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Customer
{
	public class DocumentsRepository : Infrastructure.Repositories.Shared.IDocumentsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Customer.Documents> mapper { get; set; }

        public DocumentsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Customer.Documents> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Customer.Documents> FindBy(Expression<Func<Domain.Customer.Documents, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Customer.Documents> GetAll()
        {
            var items = new List<Domain.Customer.Documents>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[DocumentsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Customer.Documents GetById(Guid id)
        {
            var ItemToReturn = new Domain.Customer.Documents();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[DocumentsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Customer.Documents entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[DocumentsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Customer.Documents entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[DocumentsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Customer].[DocumentsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}