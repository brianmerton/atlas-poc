using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Shared
{
	public class TrackerSettingsRepository : Infrastructure.Repositories.Shared.ITrackerSettingsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Shared.TrackerSettings> mapper { get; set; }

        public TrackerSettingsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Shared.TrackerSettings> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Shared.TrackerSettings> FindBy(Expression<Func<Domain.Shared.TrackerSettings, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Shared.TrackerSettings> GetAll()
        {
            var items = new List<Domain.Shared.TrackerSettings>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Shared.TrackerSettings GetById(Guid id)
        {
            var ItemToReturn = new Domain.Shared.TrackerSettings();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Shared.TrackerSettings entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AppointmentBookedColor", entity.AppointmentBookedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentCanceleldColor", entity.AppointmentCanceleldColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentCompletedColor", entity.AppointmentCompletedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentOverdueColor", entity.AppointmentOverdueColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentPlannedColor", entity.AppointmentPlannedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentRescheduledColor", entity.AppointmentRescheduledColor));
						cmd.Parameters.Add(new SqlParameter("@Comment", entity.Comment));
						cmd.Parameters.Add(new SqlParameter("@ConsultantMinimalTimeSlot", entity.ConsultantMinimalTimeSlot));
						cmd.Parameters.Add(new SqlParameter("@ConsultantWorkStartAt", entity.ConsultantWorkStartAt));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@MatchMax", entity.MatchMax));
						cmd.Parameters.Add(new SqlParameter("@MatchStep", entity.MatchStep));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@SLAOverdueDays", entity.SLAOverdueDays));
						cmd.Parameters.Add(new SqlParameter("@TempAssignmentDaysAfter", entity.TempAssignmentDaysAfter));
						cmd.Parameters.Add(new SqlParameter("@TempAssignmentDaysBefore", entity.TempAssignmentDaysBefore));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Shared.TrackerSettings entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AppointmentBookedColor", entity.AppointmentBookedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentCanceleldColor", entity.AppointmentCanceleldColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentCompletedColor", entity.AppointmentCompletedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentOverdueColor", entity.AppointmentOverdueColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentPlannedColor", entity.AppointmentPlannedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentRescheduledColor", entity.AppointmentRescheduledColor));
						cmd.Parameters.Add(new SqlParameter("@Comment", entity.Comment));
						cmd.Parameters.Add(new SqlParameter("@ConsultantMinimalTimeSlot", entity.ConsultantMinimalTimeSlot));
						cmd.Parameters.Add(new SqlParameter("@ConsultantWorkStartAt", entity.ConsultantWorkStartAt));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@MatchMax", entity.MatchMax));
						cmd.Parameters.Add(new SqlParameter("@MatchStep", entity.MatchStep));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@SLAOverdueDays", entity.SLAOverdueDays));
						cmd.Parameters.Add(new SqlParameter("@TempAssignmentDaysAfter", entity.TempAssignmentDaysAfter));
						cmd.Parameters.Add(new SqlParameter("@TempAssignmentDaysBefore", entity.TempAssignmentDaysBefore));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}