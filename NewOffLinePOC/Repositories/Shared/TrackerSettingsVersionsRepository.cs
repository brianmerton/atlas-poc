using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Shared
{
	public class TrackerSettingsVersionsRepository : Infrastructure.Repositories.Shared.ITrackerSettingsVersionsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Shared.TrackerSettingsVersions> mapper { get; set; }

        public TrackerSettingsVersionsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Shared.TrackerSettingsVersions> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Shared.TrackerSettingsVersions> FindBy(Expression<Func<Domain.Shared.TrackerSettingsVersions, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Shared.TrackerSettingsVersions> GetAll()
        {
            var items = new List<Domain.Shared.TrackerSettingsVersions>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsVersionsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Shared.TrackerSettingsVersions GetById(Guid id)
        {
            var ItemToReturn = new Domain.Shared.TrackerSettingsVersions();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsVersionsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Shared.TrackerSettingsVersions entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsVersionsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AppointmentBookedColor", entity.AppointmentBookedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentCanceleldColor", entity.AppointmentCanceleldColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentCompletedColor", entity.AppointmentCompletedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentOverdueColor", entity.AppointmentOverdueColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentPlannedColor", entity.AppointmentPlannedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentRescheduledColor", entity.AppointmentRescheduledColor));
						cmd.Parameters.Add(new SqlParameter("@Comment", entity.Comment));
						cmd.Parameters.Add(new SqlParameter("@ConsultantMinimalTimeSlot", entity.ConsultantMinimalTimeSlot));
						cmd.Parameters.Add(new SqlParameter("@ConsultantWorkStartAt", entity.ConsultantWorkStartAt));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@MatchMax", entity.MatchMax));
						cmd.Parameters.Add(new SqlParameter("@MatchStep", entity.MatchStep));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@PrototypeId", entity.PrototypeId));
						cmd.Parameters.Add(new SqlParameter("@SLAOverdueDays", entity.SLAOverdueDays));
						cmd.Parameters.Add(new SqlParameter("@TempAssignmentDaysAfter", entity.TempAssignmentDaysAfter));
						cmd.Parameters.Add(new SqlParameter("@TempAssignmentDaysBefore", entity.TempAssignmentDaysBefore));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Shared.TrackerSettingsVersions entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsVersionsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AppointmentBookedColor", entity.AppointmentBookedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentCanceleldColor", entity.AppointmentCanceleldColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentCompletedColor", entity.AppointmentCompletedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentOverdueColor", entity.AppointmentOverdueColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentPlannedColor", entity.AppointmentPlannedColor));
						cmd.Parameters.Add(new SqlParameter("@AppointmentRescheduledColor", entity.AppointmentRescheduledColor));
						cmd.Parameters.Add(new SqlParameter("@Comment", entity.Comment));
						cmd.Parameters.Add(new SqlParameter("@ConsultantMinimalTimeSlot", entity.ConsultantMinimalTimeSlot));
						cmd.Parameters.Add(new SqlParameter("@ConsultantWorkStartAt", entity.ConsultantWorkStartAt));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@MatchMax", entity.MatchMax));
						cmd.Parameters.Add(new SqlParameter("@MatchStep", entity.MatchStep));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@PrototypeId", entity.PrototypeId));
						cmd.Parameters.Add(new SqlParameter("@SLAOverdueDays", entity.SLAOverdueDays));
						cmd.Parameters.Add(new SqlParameter("@TempAssignmentDaysAfter", entity.TempAssignmentDaysAfter));
						cmd.Parameters.Add(new SqlParameter("@TempAssignmentDaysBefore", entity.TempAssignmentDaysBefore));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TrackerSettingsVersionsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}