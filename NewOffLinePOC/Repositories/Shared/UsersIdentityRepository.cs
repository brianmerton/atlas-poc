using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Shared
{
	public class UsersIdentityRepository : Infrastructure.Repositories.Shared.IUsersIdentityRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Shared.UsersIdentity> mapper { get; set; }

        public UsersIdentityRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Shared.UsersIdentity> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Shared.UsersIdentity> FindBy(Expression<Func<Domain.Shared.UsersIdentity, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Shared.UsersIdentity> GetAll()
        {
            var items = new List<Domain.Shared.UsersIdentity>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[UsersIdentityGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Shared.UsersIdentity GetById(Guid id)
        {
            var ItemToReturn = new Domain.Shared.UsersIdentity();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[UsersIdentityGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Shared.UsersIdentity entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[UsersIdentityInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Email", entity.Email));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Password", entity.Password));
						cmd.Parameters.Add(new SqlParameter("@PasswordAnswer", entity.PasswordAnswer));
						cmd.Parameters.Add(new SqlParameter("@PasswordQuestion", entity.PasswordQuestion));
						cmd.Parameters.Add(new SqlParameter("@ResetToken", entity.ResetToken));
						cmd.Parameters.Add(new SqlParameter("@TcAccepted", entity.TcAccepted));
						cmd.Parameters.Add(new SqlParameter("@TcBrowserMetaData", entity.TcBrowserMetaData));
						cmd.Parameters.Add(new SqlParameter("@TcIpAddress", entity.TcIpAddress));
						cmd.Parameters.Add(new SqlParameter("@TcShownOn", entity.TcShownOn));
						cmd.Parameters.Add(new SqlParameter("@TcVersion", entity.TcVersion));
						cmd.Parameters.Add(new SqlParameter("@TenantName", entity.TenantName));
						cmd.Parameters.Add(new SqlParameter("@TokenExpiryDate", entity.TokenExpiryDate));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Shared.UsersIdentity entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[UsersIdentityUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Email", entity.Email));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Password", entity.Password));
						cmd.Parameters.Add(new SqlParameter("@PasswordAnswer", entity.PasswordAnswer));
						cmd.Parameters.Add(new SqlParameter("@PasswordQuestion", entity.PasswordQuestion));
						cmd.Parameters.Add(new SqlParameter("@ResetToken", entity.ResetToken));
						cmd.Parameters.Add(new SqlParameter("@TcAccepted", entity.TcAccepted));
						cmd.Parameters.Add(new SqlParameter("@TcBrowserMetaData", entity.TcBrowserMetaData));
						cmd.Parameters.Add(new SqlParameter("@TcIpAddress", entity.TcIpAddress));
						cmd.Parameters.Add(new SqlParameter("@TcShownOn", entity.TcShownOn));
						cmd.Parameters.Add(new SqlParameter("@TcVersion", entity.TcVersion));
						cmd.Parameters.Add(new SqlParameter("@TenantName", entity.TenantName));
						cmd.Parameters.Add(new SqlParameter("@TokenExpiryDate", entity.TokenExpiryDate));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[UsersIdentityDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}