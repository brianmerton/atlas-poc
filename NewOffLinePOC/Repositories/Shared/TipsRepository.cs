using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Shared
{
	public class TipsRepository : Infrastructure.Repositories.Shared.ITipsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Shared.Tips> mapper { get; set; }

        public TipsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Shared.Tips> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Shared.Tips> FindBy(Expression<Func<Domain.Shared.Tips, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Shared.Tips> GetAll()
        {
            var items = new List<Domain.Shared.Tips>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TipsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Shared.Tips GetById(Guid id)
        {
            var ItemToReturn = new Domain.Shared.Tips();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TipsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Shared.Tips entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TipsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Service", entity.Service));
						cmd.Parameters.Add(new SqlParameter("@Tip", entity.Tip));
						cmd.Parameters.Add(new SqlParameter("@Url", entity.Url));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Shared.Tips entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TipsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Service", entity.Service));
						cmd.Parameters.Add(new SqlParameter("@Tip", entity.Tip));
						cmd.Parameters.Add(new SqlParameter("@Url", entity.Url));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[TipsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}