using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Shared
{
	public class SiteAssignmentsRepository : Infrastructure.Repositories.Shared.ISiteAssignmentsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Shared.SiteAssignments> mapper { get; set; }

        public SiteAssignmentsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Shared.SiteAssignments> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Shared.SiteAssignments> FindBy(Expression<Func<Domain.Shared.SiteAssignments, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Shared.SiteAssignments> GetAll()
        {
            var items = new List<Domain.Shared.SiteAssignments>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SiteAssignmentsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Shared.SiteAssignments GetById(Guid id)
        {
            var ItemToReturn = new Domain.Shared.SiteAssignments();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SiteAssignmentsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Shared.SiteAssignments entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SiteAssignmentsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CompanyId", entity.CompanyId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@SiteId", entity.SiteId));
						cmd.Parameters.Add(new SqlParameter("@UserId", entity.UserId));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Shared.SiteAssignments entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SiteAssignmentsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CompanyId", entity.CompanyId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@SiteId", entity.SiteId));
						cmd.Parameters.Add(new SqlParameter("@UserId", entity.UserId));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SiteAssignmentsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}