using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Shared
{
	public class MainIndustryRepository : Infrastructure.Repositories.Shared.IMainIndustryRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Shared.MainIndustry> mapper { get; set; }

        public MainIndustryRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Shared.MainIndustry> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Shared.MainIndustry> FindBy(Expression<Func<Domain.Shared.MainIndustry, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Shared.MainIndustry> GetAll()
        {
            var items = new List<Domain.Shared.MainIndustry>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[MainIndustryGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Shared.MainIndustry GetById(Guid id)
        {
            var ItemToReturn = new Domain.Shared.MainIndustry();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[MainIndustryGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Shared.MainIndustry entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[MainIndustryInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Name", entity.Name));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Shared.MainIndustry entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[MainIndustryUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Name", entity.Name));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[MainIndustryDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}