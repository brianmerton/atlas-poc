using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Shared
{
	public class InvSectionsRepository : Infrastructure.Repositories.Shared.IInvSectionsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Shared.InvSections> mapper { get; set; }

        public InvSectionsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Shared.InvSections> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Shared.InvSections> FindBy(Expression<Func<Domain.Shared.InvSections, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Shared.InvSections> GetAll()
        {
            var items = new List<Domain.Shared.InvSections>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InvSectionsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Shared.InvSections GetById(Guid id)
        {
            var ItemToReturn = new Domain.Shared.InvSections();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InvSectionsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Shared.InvSections entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InvSectionsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@ParentSectionId", entity.ParentSectionId));
						cmd.Parameters.Add(new SqlParameter("@SectionName", entity.SectionName));
						cmd.Parameters.Add(new SqlParameter("@SectorId", entity.SectorId));
						cmd.Parameters.Add(new SqlParameter("@Sequence", entity.Sequence));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Shared.InvSections entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InvSectionsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@ParentSectionId", entity.ParentSectionId));
						cmd.Parameters.Add(new SqlParameter("@SectionName", entity.SectionName));
						cmd.Parameters.Add(new SqlParameter("@SectorId", entity.SectorId));
						cmd.Parameters.Add(new SqlParameter("@Sequence", entity.Sequence));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InvSectionsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Shared.InvSections> GetByParentSectionId(Guid ParentSectionId)
        {
            var items = new List<Domain.Shared.InvSections>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InvSectionsGetByParentSectionId]";
                    cmd.Parameters.Add(new SqlParameter("@ParentSectionId", ParentSectionId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}