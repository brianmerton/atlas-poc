using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Shared
{
	public class InjuryTypeInjuredPartiesRepository : Infrastructure.Repositories.Shared.IInjuryTypeInjuredPartiesRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Shared.InjuryTypeInjuredParties> mapper { get; set; }

        public InjuryTypeInjuredPartiesRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Shared.InjuryTypeInjuredParties> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Shared.InjuryTypeInjuredParties> FindBy(Expression<Func<Domain.Shared.InjuryTypeInjuredParties, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Shared.InjuryTypeInjuredParties> GetAll()
        {
            var items = new List<Domain.Shared.InjuryTypeInjuredParties>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InjuryTypeInjuredPartiesGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Shared.InjuryTypeInjuredParties GetById(Guid id)
        {
            var ItemToReturn = new Domain.Shared.InjuryTypeInjuredParties();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InjuryTypeInjuredPartiesGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Shared.InjuryTypeInjuredParties entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InjuryTypeInjuredPartiesInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@InjuredPartyId", entity.InjuredPartyId));
						cmd.Parameters.Add(new SqlParameter("@InjuryTypeId", entity.InjuryTypeId));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Shared.InjuryTypeInjuredParties entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InjuryTypeInjuredPartiesUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@InjuredPartyId", entity.InjuredPartyId));
						cmd.Parameters.Add(new SqlParameter("@InjuryTypeId", entity.InjuryTypeId));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InjuryTypeInjuredPartiesDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Shared.InjuryTypeInjuredParties> GetByInjuredPartyId(Guid InjuredPartyId)
        {
            var items = new List<Domain.Shared.InjuryTypeInjuredParties>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InjuryTypeInjuredPartiesGetByInjuredPartyId]";
                    cmd.Parameters.Add(new SqlParameter("@InjuredPartyId", InjuredPartyId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Shared.InjuryTypeInjuredParties> GetByInjuryTypeId(Guid InjuryTypeId)
        {
            var items = new List<Domain.Shared.InjuryTypeInjuredParties>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[InjuryTypeInjuredPartiesGetByInjuryTypeId]";
                    cmd.Parameters.Add(new SqlParameter("@InjuryTypeId", InjuryTypeId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}