using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Shared
{
	public class SLATypesRepository : Infrastructure.Repositories.Shared.ISLATypesRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Shared.SLATypes> mapper { get; set; }

        public SLATypesRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Shared.SLATypes> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Shared.SLATypes> FindBy(Expression<Func<Domain.Shared.SLATypes, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Shared.SLATypes> GetAll()
        {
            var items = new List<Domain.Shared.SLATypes>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SLATypesGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Shared.SLATypes GetById(Guid id)
        {
            var ItemToReturn = new Domain.Shared.SLATypes();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SLATypesGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Shared.SLATypes entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SLATypesInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@CreateReminderBefore", entity.CreateReminderBefore));
						cmd.Parameters.Add(new SqlParameter("@CreateTaskBefore", entity.CreateTaskBefore));
						cmd.Parameters.Add(new SqlParameter("@DaysAfter", entity.DaysAfter));
						cmd.Parameters.Add(new SqlParameter("@EmailTemplateId", entity.EmailTemplateId));
						cmd.Parameters.Add(new SqlParameter("@IconId", entity.IconId));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@IsRecursive", entity.IsRecursive));
						cmd.Parameters.Add(new SqlParameter("@IsSystemTriggered", entity.IsSystemTriggered));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@OnlyforHeadOffice", entity.OnlyforHeadOffice));
						cmd.Parameters.Add(new SqlParameter("@SLADateType", entity.SLADateType));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
						cmd.Parameters.Add(new SqlParameter("@VisitTypeId", entity.VisitTypeId));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Shared.SLATypes entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SLATypesUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@CreateReminderBefore", entity.CreateReminderBefore));
						cmd.Parameters.Add(new SqlParameter("@CreateTaskBefore", entity.CreateTaskBefore));
						cmd.Parameters.Add(new SqlParameter("@DaysAfter", entity.DaysAfter));
						cmd.Parameters.Add(new SqlParameter("@EmailTemplateId", entity.EmailTemplateId));
						cmd.Parameters.Add(new SqlParameter("@IconId", entity.IconId));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@IsRecursive", entity.IsRecursive));
						cmd.Parameters.Add(new SqlParameter("@IsSystemTriggered", entity.IsSystemTriggered));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@OnlyforHeadOffice", entity.OnlyforHeadOffice));
						cmd.Parameters.Add(new SqlParameter("@SLADateType", entity.SLADateType));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
						cmd.Parameters.Add(new SqlParameter("@VisitTypeId", entity.VisitTypeId));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Shared].[SLATypesDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}