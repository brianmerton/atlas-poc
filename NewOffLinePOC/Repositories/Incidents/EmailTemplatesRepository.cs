using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class EmailTemplatesRepository : Infrastructure.Repositories.Shared.IEmailTemplatesRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.EmailTemplates> mapper { get; set; }

        public EmailTemplatesRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.EmailTemplates> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.EmailTemplates> FindBy(Expression<Func<Domain.Incidents.EmailTemplates, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.EmailTemplates> GetAll()
        {
            var items = new List<Domain.Incidents.EmailTemplates>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailTemplatesGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.EmailTemplates GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.EmailTemplates();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailTemplatesGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.EmailTemplates entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailTemplatesInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ClosedTicketEmailSubject", entity.ClosedTicketEmailSubject));
						cmd.Parameters.Add(new SqlParameter("@ClosedTicketEmailTemplate", entity.ClosedTicketEmailTemplate));
						cmd.Parameters.Add(new SqlParameter("@EmailSubjectTemplate", entity.EmailSubjectTemplate));
						cmd.Parameters.Add(new SqlParameter("@EmailTemplate", entity.EmailTemplate));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@NewIssueConfirmationBody", entity.NewIssueConfirmationBody));
						cmd.Parameters.Add(new SqlParameter("@NewIssueConfirmationSubj", entity.NewIssueConfirmationSubj));
						cmd.Parameters.Add(new SqlParameter("@NewIssueEmailSubject", entity.NewIssueEmailSubject));
						cmd.Parameters.Add(new SqlParameter("@NewIssueEmailTemplate", entity.NewIssueEmailTemplate));
						cmd.Parameters.Add(new SqlParameter("@WelcomeEmailBody", entity.WelcomeEmailBody));
						cmd.Parameters.Add(new SqlParameter("@WelcomeEmailSubj", entity.WelcomeEmailSubj));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.EmailTemplates entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailTemplatesUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ClosedTicketEmailSubject", entity.ClosedTicketEmailSubject));
						cmd.Parameters.Add(new SqlParameter("@ClosedTicketEmailTemplate", entity.ClosedTicketEmailTemplate));
						cmd.Parameters.Add(new SqlParameter("@EmailSubjectTemplate", entity.EmailSubjectTemplate));
						cmd.Parameters.Add(new SqlParameter("@EmailTemplate", entity.EmailTemplate));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@NewIssueConfirmationBody", entity.NewIssueConfirmationBody));
						cmd.Parameters.Add(new SqlParameter("@NewIssueConfirmationSubj", entity.NewIssueConfirmationSubj));
						cmd.Parameters.Add(new SqlParameter("@NewIssueEmailSubject", entity.NewIssueEmailSubject));
						cmd.Parameters.Add(new SqlParameter("@NewIssueEmailTemplate", entity.NewIssueEmailTemplate));
						cmd.Parameters.Add(new SqlParameter("@WelcomeEmailBody", entity.WelcomeEmailBody));
						cmd.Parameters.Add(new SqlParameter("@WelcomeEmailSubj", entity.WelcomeEmailSubj));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailTemplatesDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.EmailTemplates> GetByInstanceID(Guid InstanceID)
        {
            var items = new List<Domain.Incidents.EmailTemplates>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailTemplatesGetByInstanceID]";
                    cmd.Parameters.Add(new SqlParameter("@InstanceID", InstanceID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}