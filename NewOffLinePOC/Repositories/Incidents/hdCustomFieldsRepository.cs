using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdCustomFieldsRepository : Infrastructure.Repositories.Shared.IhdCustomFieldsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdCustomFields> mapper { get; set; }

        public hdCustomFieldsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdCustomFields> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdCustomFields> FindBy(Expression<Func<Domain.Incidents.hdCustomFields, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdCustomFields> GetAll()
        {
            var items = new List<Domain.Incidents.hdCustomFields>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdCustomFields GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdCustomFields();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdCustomFields entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@FieldID", entity.FieldID));
						cmd.Parameters.Add(new SqlParameter("@FieldName", entity.FieldName));
						cmd.Parameters.Add(new SqlParameter("@ForTechsOnly", entity.ForTechsOnly));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@Mandatory", entity.Mandatory));
						cmd.Parameters.Add(new SqlParameter("@OrderByNumber", entity.OrderByNumber));
						cmd.Parameters.Add(new SqlParameter("@ParentCustomFieldId", entity.ParentCustomFieldId));
						cmd.Parameters.Add(new SqlParameter("@ShowInGrid", entity.ShowInGrid));
						cmd.Parameters.Add(new SqlParameter("@Type", entity.Type));
						cmd.Parameters.Add(new SqlParameter("@UsageType", entity.UsageType));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdCustomFields entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@FieldID", entity.FieldID));
						cmd.Parameters.Add(new SqlParameter("@FieldName", entity.FieldName));
						cmd.Parameters.Add(new SqlParameter("@ForTechsOnly", entity.ForTechsOnly));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@Mandatory", entity.Mandatory));
						cmd.Parameters.Add(new SqlParameter("@OrderByNumber", entity.OrderByNumber));
						cmd.Parameters.Add(new SqlParameter("@ParentCustomFieldId", entity.ParentCustomFieldId));
						cmd.Parameters.Add(new SqlParameter("@ShowInGrid", entity.ShowInGrid));
						cmd.Parameters.Add(new SqlParameter("@Type", entity.Type));
						cmd.Parameters.Add(new SqlParameter("@UsageType", entity.UsageType));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdCustomFields> GetByFieldID(Guid FieldID)
        {
            var items = new List<Domain.Incidents.hdCustomFields>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldsGetByFieldID]";
                    cmd.Parameters.Add(new SqlParameter("@FieldID", FieldID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}