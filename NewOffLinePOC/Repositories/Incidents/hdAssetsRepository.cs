using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdAssetsRepository : Infrastructure.Repositories.Shared.IhdAssetsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdAssets> mapper { get; set; }

        public hdAssetsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdAssets> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdAssets> FindBy(Expression<Func<Domain.Incidents.hdAssets, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdAssets> GetAll()
        {
            var items = new List<Domain.Incidents.hdAssets>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAssetsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdAssets GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdAssets();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAssetsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdAssets entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAssetsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Comments", entity.Comments));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@ItemID", entity.ItemID));
						cmd.Parameters.Add(new SqlParameter("@Location", entity.Location));
						cmd.Parameters.Add(new SqlParameter("@ManufacturerID", entity.ManufacturerID));
						cmd.Parameters.Add(new SqlParameter("@ModelName", entity.ModelName));
						cmd.Parameters.Add(new SqlParameter("@Quantity", entity.Quantity));
						cmd.Parameters.Add(new SqlParameter("@SerialNumber", entity.SerialNumber));
						cmd.Parameters.Add(new SqlParameter("@SupplierID", entity.SupplierID));
						cmd.Parameters.Add(new SqlParameter("@TypeID", entity.TypeID));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdAssets entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAssetsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Comments", entity.Comments));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@ItemID", entity.ItemID));
						cmd.Parameters.Add(new SqlParameter("@Location", entity.Location));
						cmd.Parameters.Add(new SqlParameter("@ManufacturerID", entity.ManufacturerID));
						cmd.Parameters.Add(new SqlParameter("@ModelName", entity.ModelName));
						cmd.Parameters.Add(new SqlParameter("@Quantity", entity.Quantity));
						cmd.Parameters.Add(new SqlParameter("@SerialNumber", entity.SerialNumber));
						cmd.Parameters.Add(new SqlParameter("@SupplierID", entity.SupplierID));
						cmd.Parameters.Add(new SqlParameter("@TypeID", entity.TypeID));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAssetsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdAssets> GetByItemID(Guid ItemID)
        {
            var items = new List<Domain.Incidents.hdAssets>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAssetsGetByItemID]";
                    cmd.Parameters.Add(new SqlParameter("@ItemID", ItemID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}