using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdFileAttachmentsRepository : Infrastructure.Repositories.Shared.IhdFileAttachmentsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdFileAttachments> mapper { get; set; }

        public hdFileAttachmentsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdFileAttachments> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdFileAttachments> FindBy(Expression<Func<Domain.Incidents.hdFileAttachments, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdFileAttachments> GetAll()
        {
            var items = new List<Domain.Incidents.hdFileAttachments>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdFileAttachmentsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdFileAttachments GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdFileAttachments();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdFileAttachmentsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdFileAttachments entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdFileAttachmentsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AtlasDocumentId", entity.AtlasDocumentId));
						cmd.Parameters.Add(new SqlParameter("@CommentID", entity.CommentID));
						cmd.Parameters.Add(new SqlParameter("@DropboxUrl", entity.DropboxUrl));
						cmd.Parameters.Add(new SqlParameter("@FileData", entity.FileData));
						cmd.Parameters.Add(new SqlParameter("@FileHash", entity.FileHash));
						cmd.Parameters.Add(new SqlParameter("@FileID", entity.FileID));
						cmd.Parameters.Add(new SqlParameter("@FileName", entity.FileName));
						cmd.Parameters.Add(new SqlParameter("@FileSize", entity.FileSize));
						cmd.Parameters.Add(new SqlParameter("@GoogleDriveUrl", entity.GoogleDriveUrl));
						cmd.Parameters.Add(new SqlParameter("@IssueID", entity.IssueID));
						cmd.Parameters.Add(new SqlParameter("@KBArticleID", entity.KBArticleID));
						cmd.Parameters.Add(new SqlParameter("@UserID", entity.UserID));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdFileAttachments entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdFileAttachmentsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AtlasDocumentId", entity.AtlasDocumentId));
						cmd.Parameters.Add(new SqlParameter("@CommentID", entity.CommentID));
						cmd.Parameters.Add(new SqlParameter("@DropboxUrl", entity.DropboxUrl));
						cmd.Parameters.Add(new SqlParameter("@FileData", entity.FileData));
						cmd.Parameters.Add(new SqlParameter("@FileHash", entity.FileHash));
						cmd.Parameters.Add(new SqlParameter("@FileID", entity.FileID));
						cmd.Parameters.Add(new SqlParameter("@FileName", entity.FileName));
						cmd.Parameters.Add(new SqlParameter("@FileSize", entity.FileSize));
						cmd.Parameters.Add(new SqlParameter("@GoogleDriveUrl", entity.GoogleDriveUrl));
						cmd.Parameters.Add(new SqlParameter("@IssueID", entity.IssueID));
						cmd.Parameters.Add(new SqlParameter("@KBArticleID", entity.KBArticleID));
						cmd.Parameters.Add(new SqlParameter("@UserID", entity.UserID));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdFileAttachmentsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdFileAttachments> GetByFileID(Guid FileID)
        {
            var items = new List<Domain.Incidents.hdFileAttachments>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdFileAttachmentsGetByFileID]";
                    cmd.Parameters.Add(new SqlParameter("@FileID", FileID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}