using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdUsersRepository : Infrastructure.Repositories.Shared.IhdUsersRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdUsers> mapper { get; set; }

        public hdUsersRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdUsers> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdUsers> FindBy(Expression<Func<Domain.Incidents.hdUsers, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdUsers> GetAll()
        {
            var items = new List<Domain.Incidents.hdUsers>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdUsersGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdUsers GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdUsers();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdUsersGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdUsers entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdUsersInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AdminTutorialShown", entity.AdminTutorialShown));
						cmd.Parameters.Add(new SqlParameter("@ATLASUSERID", entity.ATLASUSERID));
						cmd.Parameters.Add(new SqlParameter("@CompanyID", entity.CompanyID));
						cmd.Parameters.Add(new SqlParameter("@Department", entity.Department));
						cmd.Parameters.Add(new SqlParameter("@Disabled", entity.Disabled));
						cmd.Parameters.Add(new SqlParameter("@Email", entity.Email));
						cmd.Parameters.Add(new SqlParameter("@FirstName", entity.FirstName));
						cmd.Parameters.Add(new SqlParameter("@Greeting", entity.Greeting));
						cmd.Parameters.Add(new SqlParameter("@HostName", entity.HostName));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@IPAddress", entity.IPAddress));
						cmd.Parameters.Add(new SqlParameter("@IsAdministrator", entity.IsAdministrator));
						cmd.Parameters.Add(new SqlParameter("@IsManager", entity.IsManager));
						cmd.Parameters.Add(new SqlParameter("@Lang", entity.Lang));
						cmd.Parameters.Add(new SqlParameter("@LastName", entity.LastName));
						cmd.Parameters.Add(new SqlParameter("@LastSeen", entity.LastSeen));
						cmd.Parameters.Add(new SqlParameter("@Location", entity.Location));
						cmd.Parameters.Add(new SqlParameter("@Notes", entity.Notes));
						cmd.Parameters.Add(new SqlParameter("@Password", entity.Password));
						cmd.Parameters.Add(new SqlParameter("@Phone", entity.Phone));
						cmd.Parameters.Add(new SqlParameter("@PushToken", entity.PushToken));
						cmd.Parameters.Add(new SqlParameter("@SendEmail", entity.SendEmail));
						cmd.Parameters.Add(new SqlParameter("@Signature", entity.Signature));
						cmd.Parameters.Add(new SqlParameter("@TechTutorialShown", entity.TechTutorialShown));
						cmd.Parameters.Add(new SqlParameter("@UserAgent", entity.UserAgent));
						cmd.Parameters.Add(new SqlParameter("@UserID", entity.UserID));
						cmd.Parameters.Add(new SqlParameter("@UserName", entity.UserName));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdUsers entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdUsersUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AdminTutorialShown", entity.AdminTutorialShown));
						cmd.Parameters.Add(new SqlParameter("@ATLASUSERID", entity.ATLASUSERID));
						cmd.Parameters.Add(new SqlParameter("@CompanyID", entity.CompanyID));
						cmd.Parameters.Add(new SqlParameter("@Department", entity.Department));
						cmd.Parameters.Add(new SqlParameter("@Disabled", entity.Disabled));
						cmd.Parameters.Add(new SqlParameter("@Email", entity.Email));
						cmd.Parameters.Add(new SqlParameter("@FirstName", entity.FirstName));
						cmd.Parameters.Add(new SqlParameter("@Greeting", entity.Greeting));
						cmd.Parameters.Add(new SqlParameter("@HostName", entity.HostName));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@IPAddress", entity.IPAddress));
						cmd.Parameters.Add(new SqlParameter("@IsAdministrator", entity.IsAdministrator));
						cmd.Parameters.Add(new SqlParameter("@IsManager", entity.IsManager));
						cmd.Parameters.Add(new SqlParameter("@Lang", entity.Lang));
						cmd.Parameters.Add(new SqlParameter("@LastName", entity.LastName));
						cmd.Parameters.Add(new SqlParameter("@LastSeen", entity.LastSeen));
						cmd.Parameters.Add(new SqlParameter("@Location", entity.Location));
						cmd.Parameters.Add(new SqlParameter("@Notes", entity.Notes));
						cmd.Parameters.Add(new SqlParameter("@Password", entity.Password));
						cmd.Parameters.Add(new SqlParameter("@Phone", entity.Phone));
						cmd.Parameters.Add(new SqlParameter("@PushToken", entity.PushToken));
						cmd.Parameters.Add(new SqlParameter("@SendEmail", entity.SendEmail));
						cmd.Parameters.Add(new SqlParameter("@Signature", entity.Signature));
						cmd.Parameters.Add(new SqlParameter("@TechTutorialShown", entity.TechTutorialShown));
						cmd.Parameters.Add(new SqlParameter("@UserAgent", entity.UserAgent));
						cmd.Parameters.Add(new SqlParameter("@UserID", entity.UserID));
						cmd.Parameters.Add(new SqlParameter("@UserName", entity.UserName));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdUsersDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdUsers> GetByInstanceID(Guid InstanceID)
        {
            var items = new List<Domain.Incidents.hdUsers>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdUsersGetByInstanceID]";
                    cmd.Parameters.Add(new SqlParameter("@InstanceID", InstanceID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdUsers> GetByUserID(Guid UserID)
        {
            var items = new List<Domain.Incidents.hdUsers>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdUsersGetByUserID]";
                    cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdUsers> GetByUserName(Guid UserName)
        {
            var items = new List<Domain.Incidents.hdUsers>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdUsersGetByUserName]";
                    cmd.Parameters.Add(new SqlParameter("@UserName", UserName));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}