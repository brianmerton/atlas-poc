using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdSettingsRepository : Infrastructure.Repositories.Shared.IhdSettingsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdSettings> mapper { get; set; }

        public hdSettingsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdSettings> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdSettings> FindBy(Expression<Func<Domain.Incidents.hdSettings, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdSettings> GetAll()
        {
            var items = new List<Domain.Incidents.hdSettings>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdSettingsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdSettings GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdSettings();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdSettingsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdSettings entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdSettingsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ActiveTabTextColor", entity.ActiveTabTextColor));
						cmd.Parameters.Add(new SqlParameter("@AllowAnonymousKBAccess", entity.AllowAnonymousKBAccess));
						cmd.Parameters.Add(new SqlParameter("@AllowGuestSubmission", entity.AllowGuestSubmission));
						cmd.Parameters.Add(new SqlParameter("@AllowUserRegistration", entity.AllowUserRegistration));
						cmd.Parameters.Add(new SqlParameter("@AutoAssignFirstReplier", entity.AutoAssignFirstReplier));
						cmd.Parameters.Add(new SqlParameter("@AutoCloseImportedCases", entity.AutoCloseImportedCases));
						cmd.Parameters.Add(new SqlParameter("@AutoLoginSharedSecret", entity.AutoLoginSharedSecret));
						cmd.Parameters.Add(new SqlParameter("@CRMRootUrl", entity.CRMRootUrl));
						cmd.Parameters.Add(new SqlParameter("@CRMSharedKey", entity.CRMSharedKey));
						cmd.Parameters.Add(new SqlParameter("@CustomCSS", entity.CustomCSS));
						cmd.Parameters.Add(new SqlParameter("@DaysToCloseAfter", entity.DaysToCloseAfter));
						cmd.Parameters.Add(new SqlParameter("@DisableAssets", entity.DisableAssets));
						cmd.Parameters.Add(new SqlParameter("@DisableAutoTimer", entity.DisableAutoTimer));
						cmd.Parameters.Add(new SqlParameter("@DisableAvatars", entity.DisableAvatars));
						cmd.Parameters.Add(new SqlParameter("@DisableCloseNotification", entity.DisableCloseNotification));
						cmd.Parameters.Add(new SqlParameter("@DisableKB", entity.DisableKB));
						cmd.Parameters.Add(new SqlParameter("@DisableTicketConfirmationNotification", entity.DisableTicketConfirmationNotification));
						cmd.Parameters.Add(new SqlParameter("@DontAddCCToSubscribers", entity.DontAddCCToSubscribers));
						cmd.Parameters.Add(new SqlParameter("@DontReopenOnReply", entity.DontReopenOnReply));
						cmd.Parameters.Add(new SqlParameter("@EmailNotificationsEnabled", entity.EmailNotificationsEnabled));
						cmd.Parameters.Add(new SqlParameter("@EmailOtherTechsOnTakeover", entity.EmailOtherTechsOnTakeover));
						cmd.Parameters.Add(new SqlParameter("@EnableSaml", entity.EnableSaml));
						cmd.Parameters.Add(new SqlParameter("@FromAddress", entity.FromAddress));
						cmd.Parameters.Add(new SqlParameter("@FromName", entity.FromName));
						cmd.Parameters.Add(new SqlParameter("@HeaderBGColor", entity.HeaderBGColor));
						cmd.Parameters.Add(new SqlParameter("@HeaderTextColor", entity.HeaderTextColor));
						cmd.Parameters.Add(new SqlParameter("@HidePoweredBy", entity.HidePoweredBy));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@Lang", entity.Lang));
						cmd.Parameters.Add(new SqlParameter("@LogoImage", entity.LogoImage));
						cmd.Parameters.Add(new SqlParameter("@MailCheckerAllowUnregisteredUsers", entity.MailCheckerAllowUnregisteredUsers));
						cmd.Parameters.Add(new SqlParameter("@MailCheckerNewIssuesCategoryID", entity.MailCheckerNewIssuesCategoryID));
						cmd.Parameters.Add(new SqlParameter("@MaxAttachSize", entity.MaxAttachSize));
						cmd.Parameters.Add(new SqlParameter("@MenuBarBGColor", entity.MenuBarBGColor));
						cmd.Parameters.Add(new SqlParameter("@MenuBarTextColor", entity.MenuBarTextColor));
						cmd.Parameters.Add(new SqlParameter("@NewIssuesDefaultCategoryID", entity.NewIssuesDefaultCategoryID));
						cmd.Parameters.Add(new SqlParameter("@NewTicketNote", entity.NewTicketNote));
						cmd.Parameters.Add(new SqlParameter("@NotifyAllTechsOnReply", entity.NotifyAllTechsOnReply));
						cmd.Parameters.Add(new SqlParameter("@RemoteLoginURL", entity.RemoteLoginURL));
						cmd.Parameters.Add(new SqlParameter("@ReplyToAddress", entity.ReplyToAddress));
						cmd.Parameters.Add(new SqlParameter("@RestrictIssueClosing", entity.RestrictIssueClosing));
						cmd.Parameters.Add(new SqlParameter("@RestrictIssueDeletion", entity.RestrictIssueDeletion));
						cmd.Parameters.Add(new SqlParameter("@SameCompanyUserPermission", entity.SameCompanyUserPermission));
						cmd.Parameters.Add(new SqlParameter("@SamlCertificate", entity.SamlCertificate));
						cmd.Parameters.Add(new SqlParameter("@SamlEndpoint", entity.SamlEndpoint));
						cmd.Parameters.Add(new SqlParameter("@SeeEveryonesIssues", entity.SeeEveryonesIssues));
						cmd.Parameters.Add(new SqlParameter("@SendAdminEmailOnNewIssue", entity.SendAdminEmailOnNewIssue));
						cmd.Parameters.Add(new SqlParameter("@SendTechniciansEmailOnNewIssue", entity.SendTechniciansEmailOnNewIssue));
						cmd.Parameters.Add(new SqlParameter("@SendTechSMSOnNewIssue", entity.SendTechSMSOnNewIssue));
						cmd.Parameters.Add(new SqlParameter("@ServerTimeOffset", entity.ServerTimeOffset));
						cmd.Parameters.Add(new SqlParameter("@SMTPAuthRequired", entity.SMTPAuthRequired));
						cmd.Parameters.Add(new SqlParameter("@SMTPPassword", entity.SMTPPassword));
						cmd.Parameters.Add(new SqlParameter("@SMTPPort", entity.SMTPPort));
						cmd.Parameters.Add(new SqlParameter("@SMTPServer", entity.SMTPServer));
						cmd.Parameters.Add(new SqlParameter("@SMTPUserName", entity.SMTPUserName));
						cmd.Parameters.Add(new SqlParameter("@SMTPUseSSL", entity.SMTPUseSSL));
						cmd.Parameters.Add(new SqlParameter("@TimezoneID", entity.TimezoneID));
						cmd.Parameters.Add(new SqlParameter("@Title", entity.Title));
						cmd.Parameters.Add(new SqlParameter("@UseDefaultSmtp", entity.UseDefaultSmtp));
						cmd.Parameters.Add(new SqlParameter("@UseFromNameForUserOriginatedMsgs", entity.UseFromNameForUserOriginatedMsgs));
						cmd.Parameters.Add(new SqlParameter("@WorkHourFrom", entity.WorkHourFrom));
						cmd.Parameters.Add(new SqlParameter("@WorkHourTo", entity.WorkHourTo));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdSettings entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdSettingsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ActiveTabTextColor", entity.ActiveTabTextColor));
						cmd.Parameters.Add(new SqlParameter("@AllowAnonymousKBAccess", entity.AllowAnonymousKBAccess));
						cmd.Parameters.Add(new SqlParameter("@AllowGuestSubmission", entity.AllowGuestSubmission));
						cmd.Parameters.Add(new SqlParameter("@AllowUserRegistration", entity.AllowUserRegistration));
						cmd.Parameters.Add(new SqlParameter("@AutoAssignFirstReplier", entity.AutoAssignFirstReplier));
						cmd.Parameters.Add(new SqlParameter("@AutoCloseImportedCases", entity.AutoCloseImportedCases));
						cmd.Parameters.Add(new SqlParameter("@AutoLoginSharedSecret", entity.AutoLoginSharedSecret));
						cmd.Parameters.Add(new SqlParameter("@CRMRootUrl", entity.CRMRootUrl));
						cmd.Parameters.Add(new SqlParameter("@CRMSharedKey", entity.CRMSharedKey));
						cmd.Parameters.Add(new SqlParameter("@CustomCSS", entity.CustomCSS));
						cmd.Parameters.Add(new SqlParameter("@DaysToCloseAfter", entity.DaysToCloseAfter));
						cmd.Parameters.Add(new SqlParameter("@DisableAssets", entity.DisableAssets));
						cmd.Parameters.Add(new SqlParameter("@DisableAutoTimer", entity.DisableAutoTimer));
						cmd.Parameters.Add(new SqlParameter("@DisableAvatars", entity.DisableAvatars));
						cmd.Parameters.Add(new SqlParameter("@DisableCloseNotification", entity.DisableCloseNotification));
						cmd.Parameters.Add(new SqlParameter("@DisableKB", entity.DisableKB));
						cmd.Parameters.Add(new SqlParameter("@DisableTicketConfirmationNotification", entity.DisableTicketConfirmationNotification));
						cmd.Parameters.Add(new SqlParameter("@DontAddCCToSubscribers", entity.DontAddCCToSubscribers));
						cmd.Parameters.Add(new SqlParameter("@DontReopenOnReply", entity.DontReopenOnReply));
						cmd.Parameters.Add(new SqlParameter("@EmailNotificationsEnabled", entity.EmailNotificationsEnabled));
						cmd.Parameters.Add(new SqlParameter("@EmailOtherTechsOnTakeover", entity.EmailOtherTechsOnTakeover));
						cmd.Parameters.Add(new SqlParameter("@EnableSaml", entity.EnableSaml));
						cmd.Parameters.Add(new SqlParameter("@FromAddress", entity.FromAddress));
						cmd.Parameters.Add(new SqlParameter("@FromName", entity.FromName));
						cmd.Parameters.Add(new SqlParameter("@HeaderBGColor", entity.HeaderBGColor));
						cmd.Parameters.Add(new SqlParameter("@HeaderTextColor", entity.HeaderTextColor));
						cmd.Parameters.Add(new SqlParameter("@HidePoweredBy", entity.HidePoweredBy));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@Lang", entity.Lang));
						cmd.Parameters.Add(new SqlParameter("@LogoImage", entity.LogoImage));
						cmd.Parameters.Add(new SqlParameter("@MailCheckerAllowUnregisteredUsers", entity.MailCheckerAllowUnregisteredUsers));
						cmd.Parameters.Add(new SqlParameter("@MailCheckerNewIssuesCategoryID", entity.MailCheckerNewIssuesCategoryID));
						cmd.Parameters.Add(new SqlParameter("@MaxAttachSize", entity.MaxAttachSize));
						cmd.Parameters.Add(new SqlParameter("@MenuBarBGColor", entity.MenuBarBGColor));
						cmd.Parameters.Add(new SqlParameter("@MenuBarTextColor", entity.MenuBarTextColor));
						cmd.Parameters.Add(new SqlParameter("@NewIssuesDefaultCategoryID", entity.NewIssuesDefaultCategoryID));
						cmd.Parameters.Add(new SqlParameter("@NewTicketNote", entity.NewTicketNote));
						cmd.Parameters.Add(new SqlParameter("@NotifyAllTechsOnReply", entity.NotifyAllTechsOnReply));
						cmd.Parameters.Add(new SqlParameter("@RemoteLoginURL", entity.RemoteLoginURL));
						cmd.Parameters.Add(new SqlParameter("@ReplyToAddress", entity.ReplyToAddress));
						cmd.Parameters.Add(new SqlParameter("@RestrictIssueClosing", entity.RestrictIssueClosing));
						cmd.Parameters.Add(new SqlParameter("@RestrictIssueDeletion", entity.RestrictIssueDeletion));
						cmd.Parameters.Add(new SqlParameter("@SameCompanyUserPermission", entity.SameCompanyUserPermission));
						cmd.Parameters.Add(new SqlParameter("@SamlCertificate", entity.SamlCertificate));
						cmd.Parameters.Add(new SqlParameter("@SamlEndpoint", entity.SamlEndpoint));
						cmd.Parameters.Add(new SqlParameter("@SeeEveryonesIssues", entity.SeeEveryonesIssues));
						cmd.Parameters.Add(new SqlParameter("@SendAdminEmailOnNewIssue", entity.SendAdminEmailOnNewIssue));
						cmd.Parameters.Add(new SqlParameter("@SendTechniciansEmailOnNewIssue", entity.SendTechniciansEmailOnNewIssue));
						cmd.Parameters.Add(new SqlParameter("@SendTechSMSOnNewIssue", entity.SendTechSMSOnNewIssue));
						cmd.Parameters.Add(new SqlParameter("@ServerTimeOffset", entity.ServerTimeOffset));
						cmd.Parameters.Add(new SqlParameter("@SMTPAuthRequired", entity.SMTPAuthRequired));
						cmd.Parameters.Add(new SqlParameter("@SMTPPassword", entity.SMTPPassword));
						cmd.Parameters.Add(new SqlParameter("@SMTPPort", entity.SMTPPort));
						cmd.Parameters.Add(new SqlParameter("@SMTPServer", entity.SMTPServer));
						cmd.Parameters.Add(new SqlParameter("@SMTPUserName", entity.SMTPUserName));
						cmd.Parameters.Add(new SqlParameter("@SMTPUseSSL", entity.SMTPUseSSL));
						cmd.Parameters.Add(new SqlParameter("@TimezoneID", entity.TimezoneID));
						cmd.Parameters.Add(new SqlParameter("@Title", entity.Title));
						cmd.Parameters.Add(new SqlParameter("@UseDefaultSmtp", entity.UseDefaultSmtp));
						cmd.Parameters.Add(new SqlParameter("@UseFromNameForUserOriginatedMsgs", entity.UseFromNameForUserOriginatedMsgs));
						cmd.Parameters.Add(new SqlParameter("@WorkHourFrom", entity.WorkHourFrom));
						cmd.Parameters.Add(new SqlParameter("@WorkHourTo", entity.WorkHourTo));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdSettingsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdSettings> GetByInstanceID(Guid InstanceID)
        {
            var items = new List<Domain.Incidents.hdSettings>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdSettingsGetByInstanceID]";
                    cmd.Parameters.Add(new SqlParameter("@InstanceID", InstanceID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}