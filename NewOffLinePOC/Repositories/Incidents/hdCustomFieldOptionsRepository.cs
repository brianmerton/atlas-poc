using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdCustomFieldOptionsRepository : Infrastructure.Repositories.Shared.IhdCustomFieldOptionsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdCustomFieldOptions> mapper { get; set; }

        public hdCustomFieldOptionsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdCustomFieldOptions> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdCustomFieldOptions> FindBy(Expression<Func<Domain.Incidents.hdCustomFieldOptions, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdCustomFieldOptions> GetAll()
        {
            var items = new List<Domain.Incidents.hdCustomFieldOptions>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldOptionsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdCustomFieldOptions GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdCustomFieldOptions();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldOptionsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdCustomFieldOptions entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldOptionsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@FieldID", entity.FieldID));
						cmd.Parameters.Add(new SqlParameter("@IsDefault", entity.IsDefault));
						cmd.Parameters.Add(new SqlParameter("@OptionID", entity.OptionID));
						cmd.Parameters.Add(new SqlParameter("@OptionValue", entity.OptionValue));
						cmd.Parameters.Add(new SqlParameter("@ParentCustomFieldOptionId", entity.ParentCustomFieldOptionId));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdCustomFieldOptions entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldOptionsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@FieldID", entity.FieldID));
						cmd.Parameters.Add(new SqlParameter("@IsDefault", entity.IsDefault));
						cmd.Parameters.Add(new SqlParameter("@OptionID", entity.OptionID));
						cmd.Parameters.Add(new SqlParameter("@OptionValue", entity.OptionValue));
						cmd.Parameters.Add(new SqlParameter("@ParentCustomFieldOptionId", entity.ParentCustomFieldOptionId));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldOptionsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdCustomFieldOptions> GetByOptionID(Guid OptionID)
        {
            var items = new List<Domain.Incidents.hdCustomFieldOptions>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCustomFieldOptionsGetByOptionID]";
                    cmd.Parameters.Add(new SqlParameter("@OptionID", OptionID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}