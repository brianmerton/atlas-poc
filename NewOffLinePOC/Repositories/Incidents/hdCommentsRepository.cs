using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdCommentsRepository : Infrastructure.Repositories.Shared.IhdCommentsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdComments> mapper { get; set; }

        public hdCommentsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdComments> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdComments> FindBy(Expression<Func<Domain.Incidents.hdComments, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdComments> GetAll()
        {
            var items = new List<Domain.Incidents.hdComments>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCommentsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdComments GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdComments();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCommentsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdComments entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCommentsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Body", entity.Body));
						cmd.Parameters.Add(new SqlParameter("@CommentDate", entity.CommentDate));
						cmd.Parameters.Add(new SqlParameter("@CommentID", entity.CommentID));
						cmd.Parameters.Add(new SqlParameter("@EmailHeaders", entity.EmailHeaders));
						cmd.Parameters.Add(new SqlParameter("@ForTechsOnly", entity.ForTechsOnly));
						cmd.Parameters.Add(new SqlParameter("@HiddenFromKB", entity.HiddenFromKB));
						cmd.Parameters.Add(new SqlParameter("@IssueID", entity.IssueID));
						cmd.Parameters.Add(new SqlParameter("@IsSystem", entity.IsSystem));
						cmd.Parameters.Add(new SqlParameter("@Recipients", entity.Recipients));
						cmd.Parameters.Add(new SqlParameter("@UserID", entity.UserID));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdComments entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCommentsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Body", entity.Body));
						cmd.Parameters.Add(new SqlParameter("@CommentDate", entity.CommentDate));
						cmd.Parameters.Add(new SqlParameter("@CommentID", entity.CommentID));
						cmd.Parameters.Add(new SqlParameter("@EmailHeaders", entity.EmailHeaders));
						cmd.Parameters.Add(new SqlParameter("@ForTechsOnly", entity.ForTechsOnly));
						cmd.Parameters.Add(new SqlParameter("@HiddenFromKB", entity.HiddenFromKB));
						cmd.Parameters.Add(new SqlParameter("@IssueID", entity.IssueID));
						cmd.Parameters.Add(new SqlParameter("@IsSystem", entity.IsSystem));
						cmd.Parameters.Add(new SqlParameter("@Recipients", entity.Recipients));
						cmd.Parameters.Add(new SqlParameter("@UserID", entity.UserID));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCommentsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdComments> GetByCommentID(Guid CommentID)
        {
            var items = new List<Domain.Incidents.hdComments>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCommentsGetByCommentID]";
                    cmd.Parameters.Add(new SqlParameter("@CommentID", CommentID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdComments> GetByIssueID(Guid IssueID)
        {
            var items = new List<Domain.Incidents.hdComments>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCommentsGetByIssueID]";
                    cmd.Parameters.Add(new SqlParameter("@IssueID", IssueID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdComments> GetByUserID(Guid UserID)
        {
            var items = new List<Domain.Incidents.hdComments>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCommentsGetByUserID]";
                    cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}