using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdPOPServersRepository : Infrastructure.Repositories.Shared.IhdPOPServersRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdPOPServers> mapper { get; set; }

        public hdPOPServersRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdPOPServers> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdPOPServers> FindBy(Expression<Func<Domain.Incidents.hdPOPServers, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdPOPServers> GetAll()
        {
            var items = new List<Domain.Incidents.hdPOPServers>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdPOPServersGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdPOPServers GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdPOPServers();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdPOPServersGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdPOPServers entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdPOPServersInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@HostName", entity.HostName));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@IsImap", entity.IsImap));
						cmd.Parameters.Add(new SqlParameter("@NewTicketsCategoryID", entity.NewTicketsCategoryID));
						cmd.Parameters.Add(new SqlParameter("@POPLogin", entity.POPLogin));
						cmd.Parameters.Add(new SqlParameter("@POPPassword", entity.POPPassword));
						cmd.Parameters.Add(new SqlParameter("@Port", entity.Port));
						cmd.Parameters.Add(new SqlParameter("@ServerID", entity.ServerID));
						cmd.Parameters.Add(new SqlParameter("@SharedMailBoxAlias", entity.SharedMailBoxAlias));
						cmd.Parameters.Add(new SqlParameter("@UseSSL", entity.UseSSL));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdPOPServers entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdPOPServersUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@HostName", entity.HostName));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@IsImap", entity.IsImap));
						cmd.Parameters.Add(new SqlParameter("@NewTicketsCategoryID", entity.NewTicketsCategoryID));
						cmd.Parameters.Add(new SqlParameter("@POPLogin", entity.POPLogin));
						cmd.Parameters.Add(new SqlParameter("@POPPassword", entity.POPPassword));
						cmd.Parameters.Add(new SqlParameter("@Port", entity.Port));
						cmd.Parameters.Add(new SqlParameter("@ServerID", entity.ServerID));
						cmd.Parameters.Add(new SqlParameter("@SharedMailBoxAlias", entity.SharedMailBoxAlias));
						cmd.Parameters.Add(new SqlParameter("@UseSSL", entity.UseSSL));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdPOPServersDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdPOPServers> GetByServerID(Guid ServerID)
        {
            var items = new List<Domain.Incidents.hdPOPServers>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdPOPServersGetByServerID]";
                    cmd.Parameters.Add(new SqlParameter("@ServerID", ServerID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}