using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdCompaniesRepository : Infrastructure.Repositories.Shared.IhdCompaniesRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdCompanies> mapper { get; set; }

        public hdCompaniesRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdCompanies> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdCompanies> FindBy(Expression<Func<Domain.Incidents.hdCompanies, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdCompanies> GetAll()
        {
            var items = new List<Domain.Incidents.hdCompanies>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCompaniesGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdCompanies GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdCompanies();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCompaniesGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdCompanies entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCompaniesInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ATLASCOMPANYID", entity.ATLASCOMPANYID));
						cmd.Parameters.Add(new SqlParameter("@CompanyID", entity.CompanyID));
						cmd.Parameters.Add(new SqlParameter("@CRMID", entity.CRMID));
						cmd.Parameters.Add(new SqlParameter("@Disabled", entity.Disabled));
						cmd.Parameters.Add(new SqlParameter("@EmailDomain", entity.EmailDomain));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@Name", entity.Name));
						cmd.Parameters.Add(new SqlParameter("@Notes", entity.Notes));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdCompanies entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCompaniesUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ATLASCOMPANYID", entity.ATLASCOMPANYID));
						cmd.Parameters.Add(new SqlParameter("@CompanyID", entity.CompanyID));
						cmd.Parameters.Add(new SqlParameter("@CRMID", entity.CRMID));
						cmd.Parameters.Add(new SqlParameter("@Disabled", entity.Disabled));
						cmd.Parameters.Add(new SqlParameter("@EmailDomain", entity.EmailDomain));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@Name", entity.Name));
						cmd.Parameters.Add(new SqlParameter("@Notes", entity.Notes));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCompaniesDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdCompanies> GetByCompanyID(Guid CompanyID)
        {
            var items = new List<Domain.Incidents.hdCompanies>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdCompaniesGetByCompanyID]";
                    cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}