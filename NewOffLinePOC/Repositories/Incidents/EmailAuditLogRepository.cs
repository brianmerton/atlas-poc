using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class EmailAuditLogRepository : Infrastructure.Repositories.Shared.IEmailAuditLogRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.EmailAuditLog> mapper { get; set; }

        public EmailAuditLogRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.EmailAuditLog> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.EmailAuditLog> FindBy(Expression<Func<Domain.Incidents.EmailAuditLog, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.EmailAuditLog> GetAll()
        {
            var items = new List<Domain.Incidents.EmailAuditLog>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailAuditLogGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.EmailAuditLog GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.EmailAuditLog();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailAuditLogGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.EmailAuditLog entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailAuditLogInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Attachment", entity.Attachment));
						cmd.Parameters.Add(new SqlParameter("@Body", entity.Body));
						cmd.Parameters.Add(new SqlParameter("@CCEmailAddress", entity.CCEmailAddress));
						cmd.Parameters.Add(new SqlParameter("@CreatedDateTime", entity.CreatedDateTime));
						cmd.Parameters.Add(new SqlParameter("@EmailAuditLogId", entity.EmailAuditLogId));
						cmd.Parameters.Add(new SqlParameter("@FromEmailAddress", entity.FromEmailAddress));
						cmd.Parameters.Add(new SqlParameter("@IsInBound", entity.IsInBound));
						cmd.Parameters.Add(new SqlParameter("@IssueId", entity.IssueId));
						cmd.Parameters.Add(new SqlParameter("@ReceivedDateTime", entity.ReceivedDateTime));
						cmd.Parameters.Add(new SqlParameter("@SentDateTime", entity.SentDateTime));
						cmd.Parameters.Add(new SqlParameter("@ServerID", entity.ServerID));
						cmd.Parameters.Add(new SqlParameter("@Subject", entity.Subject));
						cmd.Parameters.Add(new SqlParameter("@ToEmailAddress", entity.ToEmailAddress));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.EmailAuditLog entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailAuditLogUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Attachment", entity.Attachment));
						cmd.Parameters.Add(new SqlParameter("@Body", entity.Body));
						cmd.Parameters.Add(new SqlParameter("@CCEmailAddress", entity.CCEmailAddress));
						cmd.Parameters.Add(new SqlParameter("@CreatedDateTime", entity.CreatedDateTime));
						cmd.Parameters.Add(new SqlParameter("@EmailAuditLogId", entity.EmailAuditLogId));
						cmd.Parameters.Add(new SqlParameter("@FromEmailAddress", entity.FromEmailAddress));
						cmd.Parameters.Add(new SqlParameter("@IsInBound", entity.IsInBound));
						cmd.Parameters.Add(new SqlParameter("@IssueId", entity.IssueId));
						cmd.Parameters.Add(new SqlParameter("@ReceivedDateTime", entity.ReceivedDateTime));
						cmd.Parameters.Add(new SqlParameter("@SentDateTime", entity.SentDateTime));
						cmd.Parameters.Add(new SqlParameter("@ServerID", entity.ServerID));
						cmd.Parameters.Add(new SqlParameter("@Subject", entity.Subject));
						cmd.Parameters.Add(new SqlParameter("@ToEmailAddress", entity.ToEmailAddress));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailAuditLogDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.EmailAuditLog> GetByEmailAuditLogId(Guid EmailAuditLogId)
        {
            var items = new List<Domain.Incidents.EmailAuditLog>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[EmailAuditLogGetByEmailAuditLogId]";
                    cmd.Parameters.Add(new SqlParameter("@EmailAuditLogId", EmailAuditLogId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}