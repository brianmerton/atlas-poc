using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdIssuesRepository : Infrastructure.Repositories.Shared.IhdIssuesRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdIssues> mapper { get; set; }

        public hdIssuesRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdIssues> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdIssues> FindBy(Expression<Func<Domain.Incidents.hdIssues, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetAll()
        {
            var items = new List<Domain.Incidents.hdIssues>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdIssues GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdIssues();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdIssues entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AssignedToUserID", entity.AssignedToUserID));
						cmd.Parameters.Add(new SqlParameter("@Body", entity.Body));
						cmd.Parameters.Add(new SqlParameter("@CategoryID", entity.CategoryID));
						cmd.Parameters.Add(new SqlParameter("@ChannelId", entity.ChannelId));
						cmd.Parameters.Add(new SqlParameter("@DueDate", entity.DueDate));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@IssueDate", entity.IssueDate));
						cmd.Parameters.Add(new SqlParameter("@IssueID", entity.IssueID));
						cmd.Parameters.Add(new SqlParameter("@KBForTechsOnly", entity.KBForTechsOnly));
						cmd.Parameters.Add(new SqlParameter("@LastUpdated", entity.LastUpdated));
						cmd.Parameters.Add(new SqlParameter("@Priority", entity.Priority));
						cmd.Parameters.Add(new SqlParameter("@PublishToKB", entity.PublishToKB));
						cmd.Parameters.Add(new SqlParameter("@ResolvedDate", entity.ResolvedDate));
						cmd.Parameters.Add(new SqlParameter("@StartDate", entity.StartDate));
						cmd.Parameters.Add(new SqlParameter("@StatusID", entity.StatusID));
						cmd.Parameters.Add(new SqlParameter("@Subject", entity.Subject));
						cmd.Parameters.Add(new SqlParameter("@TimeSpentInSeconds", entity.TimeSpentInSeconds));
						cmd.Parameters.Add(new SqlParameter("@UpdatedByPerformer", entity.UpdatedByPerformer));
						cmd.Parameters.Add(new SqlParameter("@UpdatedByUser", entity.UpdatedByUser));
						cmd.Parameters.Add(new SqlParameter("@UpdatedForTechView", entity.UpdatedForTechView));
						cmd.Parameters.Add(new SqlParameter("@UserID", entity.UserID));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdIssues entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@AssignedToUserID", entity.AssignedToUserID));
						cmd.Parameters.Add(new SqlParameter("@Body", entity.Body));
						cmd.Parameters.Add(new SqlParameter("@CategoryID", entity.CategoryID));
						cmd.Parameters.Add(new SqlParameter("@ChannelId", entity.ChannelId));
						cmd.Parameters.Add(new SqlParameter("@DueDate", entity.DueDate));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@IssueDate", entity.IssueDate));
						cmd.Parameters.Add(new SqlParameter("@IssueID", entity.IssueID));
						cmd.Parameters.Add(new SqlParameter("@KBForTechsOnly", entity.KBForTechsOnly));
						cmd.Parameters.Add(new SqlParameter("@LastUpdated", entity.LastUpdated));
						cmd.Parameters.Add(new SqlParameter("@Priority", entity.Priority));
						cmd.Parameters.Add(new SqlParameter("@PublishToKB", entity.PublishToKB));
						cmd.Parameters.Add(new SqlParameter("@ResolvedDate", entity.ResolvedDate));
						cmd.Parameters.Add(new SqlParameter("@StartDate", entity.StartDate));
						cmd.Parameters.Add(new SqlParameter("@StatusID", entity.StatusID));
						cmd.Parameters.Add(new SqlParameter("@Subject", entity.Subject));
						cmd.Parameters.Add(new SqlParameter("@TimeSpentInSeconds", entity.TimeSpentInSeconds));
						cmd.Parameters.Add(new SqlParameter("@UpdatedByPerformer", entity.UpdatedByPerformer));
						cmd.Parameters.Add(new SqlParameter("@UpdatedByUser", entity.UpdatedByUser));
						cmd.Parameters.Add(new SqlParameter("@UpdatedForTechView", entity.UpdatedForTechView));
						cmd.Parameters.Add(new SqlParameter("@UserID", entity.UserID));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdIssues> GetByAssignedToUserID(Guid AssignedToUserID)
        {
            var items = new List<Domain.Incidents.hdIssues>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesGetByAssignedToUserID]";
                    cmd.Parameters.Add(new SqlParameter("@AssignedToUserID", AssignedToUserID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetByCategoryID(Guid CategoryID)
        {
            var items = new List<Domain.Incidents.hdIssues>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesGetByCategoryID]";
                    cmd.Parameters.Add(new SqlParameter("@CategoryID", CategoryID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetByIssueID(Guid IssueID)
        {
            var items = new List<Domain.Incidents.hdIssues>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesGetByIssueID]";
                    cmd.Parameters.Add(new SqlParameter("@IssueID", IssueID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetByStatusID(Guid StatusID)
        {
            var items = new List<Domain.Incidents.hdIssues>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesGetByStatusID]";
                    cmd.Parameters.Add(new SqlParameter("@StatusID", StatusID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdIssues> GetByUserID(Guid UserID)
        {
            var items = new List<Domain.Incidents.hdIssues>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssuesGetByUserID]";
                    cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}