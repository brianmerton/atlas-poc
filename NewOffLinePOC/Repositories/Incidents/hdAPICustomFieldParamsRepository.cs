using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdAPICustomFieldParamsRepository : Infrastructure.Repositories.Shared.IhdAPICustomFieldParamsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdAPICustomFieldParams> mapper { get; set; }

        public hdAPICustomFieldParamsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdAPICustomFieldParams> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdAPICustomFieldParams> FindBy(Expression<Func<Domain.Incidents.hdAPICustomFieldParams, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldParams> GetAll()
        {
            var items = new List<Domain.Incidents.hdAPICustomFieldParams>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldParamsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdAPICustomFieldParams GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdAPICustomFieldParams();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldParamsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdAPICustomFieldParams entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldParamsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@APICustomFieldParamsID", entity.APICustomFieldParamsID));
						cmd.Parameters.Add(new SqlParameter("@APICustomFieldSettingsID", entity.APICustomFieldSettingsID));
						cmd.Parameters.Add(new SqlParameter("@JitbitReference", entity.JitbitReference));
						cmd.Parameters.Add(new SqlParameter("@ParamName", entity.ParamName));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdAPICustomFieldParams entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldParamsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@APICustomFieldParamsID", entity.APICustomFieldParamsID));
						cmd.Parameters.Add(new SqlParameter("@APICustomFieldSettingsID", entity.APICustomFieldSettingsID));
						cmd.Parameters.Add(new SqlParameter("@JitbitReference", entity.JitbitReference));
						cmd.Parameters.Add(new SqlParameter("@ParamName", entity.ParamName));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldParamsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdAPICustomFieldParams> GetByAPICustomFieldParamsID(Guid APICustomFieldParamsID)
        {
            var items = new List<Domain.Incidents.hdAPICustomFieldParams>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldParamsGetByAPICustomFieldParamsID]";
                    cmd.Parameters.Add(new SqlParameter("@APICustomFieldParamsID", APICustomFieldParamsID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldParams> GetByAPICustomFieldSettingsID(Guid APICustomFieldSettingsID)
        {
            var items = new List<Domain.Incidents.hdAPICustomFieldParams>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldParamsGetByAPICustomFieldSettingsID]";
                    cmd.Parameters.Add(new SqlParameter("@APICustomFieldSettingsID", APICustomFieldSettingsID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}