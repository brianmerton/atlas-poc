using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdAPICustomFieldSettingsRepository : Infrastructure.Repositories.Shared.IhdAPICustomFieldSettingsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdAPICustomFieldSettings> mapper { get; set; }

        public hdAPICustomFieldSettingsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdAPICustomFieldSettings> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> FindBy(Expression<Func<Domain.Incidents.hdAPICustomFieldSettings, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> GetAll()
        {
            var items = new List<Domain.Incidents.hdAPICustomFieldSettings>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldSettingsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdAPICustomFieldSettings GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdAPICustomFieldSettings();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldSettingsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdAPICustomFieldSettings entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldSettingsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@APICustomFieldSettingsID", entity.APICustomFieldSettingsID));
						cmd.Parameters.Add(new SqlParameter("@APILink", entity.APILink));
						cmd.Parameters.Add(new SqlParameter("@ClientId", entity.ClientId));
						cmd.Parameters.Add(new SqlParameter("@FieldId", entity.FieldId));
						cmd.Parameters.Add(new SqlParameter("@SecurityKey", entity.SecurityKey));
						cmd.Parameters.Add(new SqlParameter("@TextField", entity.TextField));
						cmd.Parameters.Add(new SqlParameter("@UrlLink", entity.UrlLink));
						cmd.Parameters.Add(new SqlParameter("@ValueField", entity.ValueField));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdAPICustomFieldSettings entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldSettingsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@APICustomFieldSettingsID", entity.APICustomFieldSettingsID));
						cmd.Parameters.Add(new SqlParameter("@APILink", entity.APILink));
						cmd.Parameters.Add(new SqlParameter("@ClientId", entity.ClientId));
						cmd.Parameters.Add(new SqlParameter("@FieldId", entity.FieldId));
						cmd.Parameters.Add(new SqlParameter("@SecurityKey", entity.SecurityKey));
						cmd.Parameters.Add(new SqlParameter("@TextField", entity.TextField));
						cmd.Parameters.Add(new SqlParameter("@UrlLink", entity.UrlLink));
						cmd.Parameters.Add(new SqlParameter("@ValueField", entity.ValueField));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldSettingsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> GetByAPICustomFieldSettingsID(Guid APICustomFieldSettingsID)
        {
            var items = new List<Domain.Incidents.hdAPICustomFieldSettings>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldSettingsGetByAPICustomFieldSettingsID]";
                    cmd.Parameters.Add(new SqlParameter("@APICustomFieldSettingsID", APICustomFieldSettingsID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Incidents.hdAPICustomFieldSettings> GetByFieldId(Guid FieldId)
        {
            var items = new List<Domain.Incidents.hdAPICustomFieldSettings>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdAPICustomFieldSettingsGetByFieldId]";
                    cmd.Parameters.Add(new SqlParameter("@FieldId", FieldId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}