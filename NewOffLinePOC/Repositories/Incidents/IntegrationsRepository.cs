using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class IntegrationsRepository : Infrastructure.Repositories.Shared.IIntegrationsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.Integrations> mapper { get; set; }

        public IntegrationsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.Integrations> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.Integrations> FindBy(Expression<Func<Domain.Incidents.Integrations, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.Integrations> GetAll()
        {
            var items = new List<Domain.Incidents.Integrations>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[IntegrationsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.Integrations GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.Integrations();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[IntegrationsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.Integrations entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[IntegrationsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@BitbucketEnabled", entity.BitbucketEnabled));
						cmd.Parameters.Add(new SqlParameter("@BitbucketPassword", entity.BitbucketPassword));
						cmd.Parameters.Add(new SqlParameter("@BitbucketUsername", entity.BitbucketUsername));
						cmd.Parameters.Add(new SqlParameter("@DropboxAppID", entity.DropboxAppID));
						cmd.Parameters.Add(new SqlParameter("@DropboxEnabled", entity.DropboxEnabled));
						cmd.Parameters.Add(new SqlParameter("@GitHubEnabled", entity.GitHubEnabled));
						cmd.Parameters.Add(new SqlParameter("@GitHubPassword", entity.GitHubPassword));
						cmd.Parameters.Add(new SqlParameter("@GitHubUsername", entity.GitHubUsername));
						cmd.Parameters.Add(new SqlParameter("@HipChatAuthToken", entity.HipChatAuthToken));
						cmd.Parameters.Add(new SqlParameter("@HipChatEnabled", entity.HipChatEnabled));
						cmd.Parameters.Add(new SqlParameter("@HipChatRoomId", entity.HipChatRoomId));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@JiraEnabled", entity.JiraEnabled));
						cmd.Parameters.Add(new SqlParameter("@JiraPassword", entity.JiraPassword));
						cmd.Parameters.Add(new SqlParameter("@JiraUrl", entity.JiraUrl));
						cmd.Parameters.Add(new SqlParameter("@JiraUsername", entity.JiraUsername));
						cmd.Parameters.Add(new SqlParameter("@SlackEnabled", entity.SlackEnabled));
						cmd.Parameters.Add(new SqlParameter("@SlackWebhookURL", entity.SlackWebhookURL));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.Integrations entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[IntegrationsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@BitbucketEnabled", entity.BitbucketEnabled));
						cmd.Parameters.Add(new SqlParameter("@BitbucketPassword", entity.BitbucketPassword));
						cmd.Parameters.Add(new SqlParameter("@BitbucketUsername", entity.BitbucketUsername));
						cmd.Parameters.Add(new SqlParameter("@DropboxAppID", entity.DropboxAppID));
						cmd.Parameters.Add(new SqlParameter("@DropboxEnabled", entity.DropboxEnabled));
						cmd.Parameters.Add(new SqlParameter("@GitHubEnabled", entity.GitHubEnabled));
						cmd.Parameters.Add(new SqlParameter("@GitHubPassword", entity.GitHubPassword));
						cmd.Parameters.Add(new SqlParameter("@GitHubUsername", entity.GitHubUsername));
						cmd.Parameters.Add(new SqlParameter("@HipChatAuthToken", entity.HipChatAuthToken));
						cmd.Parameters.Add(new SqlParameter("@HipChatEnabled", entity.HipChatEnabled));
						cmd.Parameters.Add(new SqlParameter("@HipChatRoomId", entity.HipChatRoomId));
						cmd.Parameters.Add(new SqlParameter("@InstanceID", entity.InstanceID));
						cmd.Parameters.Add(new SqlParameter("@JiraEnabled", entity.JiraEnabled));
						cmd.Parameters.Add(new SqlParameter("@JiraPassword", entity.JiraPassword));
						cmd.Parameters.Add(new SqlParameter("@JiraUrl", entity.JiraUrl));
						cmd.Parameters.Add(new SqlParameter("@JiraUsername", entity.JiraUsername));
						cmd.Parameters.Add(new SqlParameter("@SlackEnabled", entity.SlackEnabled));
						cmd.Parameters.Add(new SqlParameter("@SlackWebhookURL", entity.SlackWebhookURL));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[IntegrationsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.Integrations> GetByInstanceID(Guid InstanceID)
        {
            var items = new List<Domain.Incidents.Integrations>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[IntegrationsGetByInstanceID]";
                    cmd.Parameters.Add(new SqlParameter("@InstanceID", InstanceID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}