using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Incidents
{
	public class hdIssueScheduleRepository : Infrastructure.Repositories.Shared.IhdIssueScheduleRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Incidents.hdIssueSchedule> mapper { get; set; }

        public hdIssueScheduleRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Incidents.hdIssueSchedule> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Incidents.hdIssueSchedule> FindBy(Expression<Func<Domain.Incidents.hdIssueSchedule, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Incidents.hdIssueSchedule> GetAll()
        {
            var items = new List<Domain.Incidents.hdIssueSchedule>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssueScheduleGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Incidents.hdIssueSchedule GetById(Guid id)
        {
            var ItemToReturn = new Domain.Incidents.hdIssueSchedule();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssueScheduleGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Incidents.hdIssueSchedule entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssueScheduleInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@FirstRecurrenceDate", entity.FirstRecurrenceDate));
						cmd.Parameters.Add(new SqlParameter("@FreqType", entity.FreqType));
						cmd.Parameters.Add(new SqlParameter("@IssueID", entity.IssueID));
						cmd.Parameters.Add(new SqlParameter("@PrevRepeatsCount", entity.PrevRepeatsCount));
						cmd.Parameters.Add(new SqlParameter("@ReopenOriginal", entity.ReopenOriginal));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Incidents.hdIssueSchedule entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssueScheduleUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@FirstRecurrenceDate", entity.FirstRecurrenceDate));
						cmd.Parameters.Add(new SqlParameter("@FreqType", entity.FreqType));
						cmd.Parameters.Add(new SqlParameter("@IssueID", entity.IssueID));
						cmd.Parameters.Add(new SqlParameter("@PrevRepeatsCount", entity.PrevRepeatsCount));
						cmd.Parameters.Add(new SqlParameter("@ReopenOriginal", entity.ReopenOriginal));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssueScheduleDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Incidents.hdIssueSchedule> GetByIssueID(Guid IssueID)
        {
            var items = new List<Domain.Incidents.hdIssueSchedule>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Incidents].[hdIssueScheduleGetByIssueID]";
                    cmd.Parameters.Add(new SqlParameter("@IssueID", IssueID));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}