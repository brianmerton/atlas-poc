using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Citation
{
	public class BlockRepository : Infrastructure.Repositories.Shared.IBlockRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Citation.Block> mapper { get; set; }

        public BlockRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Citation.Block> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Citation.Block> FindBy(Expression<Func<Domain.Citation.Block, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Citation.Block> GetAll()
        {
            var items = new List<Domain.Citation.Block>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[BlockGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Citation.Block GetById(Guid id)
        {
            var ItemToReturn = new Domain.Citation.Block();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[BlockGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Citation.Block entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[BlockInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Area", entity.Area));
						cmd.Parameters.Add(new SqlParameter("@Category", entity.Category));
						cmd.Parameters.Add(new SqlParameter("@Checked", entity.Checked));
						cmd.Parameters.Add(new SqlParameter("@Comment", entity.Comment));
						cmd.Parameters.Add(new SqlParameter("@Content", entity.Content));
						cmd.Parameters.Add(new SqlParameter("@CountryId", entity.CountryId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@DeletionState", entity.DeletionState));
						cmd.Parameters.Add(new SqlParameter("@Description", entity.Description));
						cmd.Parameters.Add(new SqlParameter("@Discriminator", entity.Discriminator));
						cmd.Parameters.Add(new SqlParameter("@FixedPositionIndex", entity.FixedPositionIndex));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@IsOriginal", entity.IsOriginal));
						cmd.Parameters.Add(new SqlParameter("@IsSystem", entity.IsSystem));
						cmd.Parameters.Add(new SqlParameter("@KbGroupId", entity.KbGroupId));
						cmd.Parameters.Add(new SqlParameter("@LastChange", entity.LastChange));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Multiplicity", entity.Multiplicity));
						cmd.Parameters.Add(new SqlParameter("@Observation", entity.Observation));
						cmd.Parameters.Add(new SqlParameter("@Options_ActAsGroup", entity.Options_ActAsGroup));
						cmd.Parameters.Add(new SqlParameter("@Options_IncludeIndexNumbers", entity.Options_IncludeIndexNumbers));
						cmd.Parameters.Add(new SqlParameter("@Options_IsReadonly", entity.Options_IsReadonly));
						cmd.Parameters.Add(new SqlParameter("@Options_LandscapeLayout", entity.Options_LandscapeLayout));
						cmd.Parameters.Add(new SqlParameter("@Options_PageBreakAfter", entity.Options_PageBreakAfter));
						cmd.Parameters.Add(new SqlParameter("@Options_PageBreakBefore", entity.Options_PageBreakBefore));
						cmd.Parameters.Add(new SqlParameter("@OrderIndex", entity.OrderIndex));
						cmd.Parameters.Add(new SqlParameter("@OriginalBlockId", entity.OriginalBlockId));
						cmd.Parameters.Add(new SqlParameter("@ParentBlockId", entity.ParentBlockId));
						cmd.Parameters.Add(new SqlParameter("@Priority", entity.Priority));
						cmd.Parameters.Add(new SqlParameter("@Recommendation", entity.Recommendation));
						cmd.Parameters.Add(new SqlParameter("@RootId", entity.RootId));
						cmd.Parameters.Add(new SqlParameter("@SectorId", entity.SectorId));
						cmd.Parameters.Add(new SqlParameter("@SourceBlockId", entity.SourceBlockId));
						cmd.Parameters.Add(new SqlParameter("@SourceBlockVersion", entity.SourceBlockVersion));
						cmd.Parameters.Add(new SqlParameter("@SubCategoryId", entity.SubCategoryId));
						cmd.Parameters.Add(new SqlParameter("@Title", entity.Title));
						cmd.Parameters.Add(new SqlParameter("@Type", entity.Type));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Citation.Block entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[BlockUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Area", entity.Area));
						cmd.Parameters.Add(new SqlParameter("@Category", entity.Category));
						cmd.Parameters.Add(new SqlParameter("@Checked", entity.Checked));
						cmd.Parameters.Add(new SqlParameter("@Comment", entity.Comment));
						cmd.Parameters.Add(new SqlParameter("@Content", entity.Content));
						cmd.Parameters.Add(new SqlParameter("@CountryId", entity.CountryId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@DeletionState", entity.DeletionState));
						cmd.Parameters.Add(new SqlParameter("@Description", entity.Description));
						cmd.Parameters.Add(new SqlParameter("@Discriminator", entity.Discriminator));
						cmd.Parameters.Add(new SqlParameter("@FixedPositionIndex", entity.FixedPositionIndex));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@IsOriginal", entity.IsOriginal));
						cmd.Parameters.Add(new SqlParameter("@IsSystem", entity.IsSystem));
						cmd.Parameters.Add(new SqlParameter("@KbGroupId", entity.KbGroupId));
						cmd.Parameters.Add(new SqlParameter("@LastChange", entity.LastChange));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Multiplicity", entity.Multiplicity));
						cmd.Parameters.Add(new SqlParameter("@Observation", entity.Observation));
						cmd.Parameters.Add(new SqlParameter("@Options_ActAsGroup", entity.Options_ActAsGroup));
						cmd.Parameters.Add(new SqlParameter("@Options_IncludeIndexNumbers", entity.Options_IncludeIndexNumbers));
						cmd.Parameters.Add(new SqlParameter("@Options_IsReadonly", entity.Options_IsReadonly));
						cmd.Parameters.Add(new SqlParameter("@Options_LandscapeLayout", entity.Options_LandscapeLayout));
						cmd.Parameters.Add(new SqlParameter("@Options_PageBreakAfter", entity.Options_PageBreakAfter));
						cmd.Parameters.Add(new SqlParameter("@Options_PageBreakBefore", entity.Options_PageBreakBefore));
						cmd.Parameters.Add(new SqlParameter("@OrderIndex", entity.OrderIndex));
						cmd.Parameters.Add(new SqlParameter("@OriginalBlockId", entity.OriginalBlockId));
						cmd.Parameters.Add(new SqlParameter("@ParentBlockId", entity.ParentBlockId));
						cmd.Parameters.Add(new SqlParameter("@Priority", entity.Priority));
						cmd.Parameters.Add(new SqlParameter("@Recommendation", entity.Recommendation));
						cmd.Parameters.Add(new SqlParameter("@RootId", entity.RootId));
						cmd.Parameters.Add(new SqlParameter("@SectorId", entity.SectorId));
						cmd.Parameters.Add(new SqlParameter("@SourceBlockId", entity.SourceBlockId));
						cmd.Parameters.Add(new SqlParameter("@SourceBlockVersion", entity.SourceBlockVersion));
						cmd.Parameters.Add(new SqlParameter("@SubCategoryId", entity.SubCategoryId));
						cmd.Parameters.Add(new SqlParameter("@Title", entity.Title));
						cmd.Parameters.Add(new SqlParameter("@Type", entity.Type));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[BlockDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Citation.Block> GetByCountryId(Guid CountryId)
        {
            var items = new List<Domain.Citation.Block>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[BlockGetByCountryId]";
                    cmd.Parameters.Add(new SqlParameter("@CountryId", CountryId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Citation.Block> GetBySectorId(Guid SectorId)
        {
            var items = new List<Domain.Citation.Block>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[BlockGetBySectorId]";
                    cmd.Parameters.Add(new SqlParameter("@SectorId", SectorId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

        public IEnumerable<Domain.Citation.Block> GetBySubCategoryId(Guid SubCategoryId)
        {
            var items = new List<Domain.Citation.Block>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[BlockGetBySubCategoryId]";
                    cmd.Parameters.Add(new SqlParameter("@SubCategoryId", SubCategoryId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}