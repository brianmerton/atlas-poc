using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Citation
{
	public class AppointmentsRepository : Infrastructure.Repositories.Shared.IAppointmentsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Citation.Appointments> mapper { get; set; }

        public AppointmentsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Citation.Appointments> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Citation.Appointments> FindBy(Expression<Func<Domain.Citation.Appointments, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Citation.Appointments> GetAll()
        {
            var items = new List<Domain.Citation.Appointments>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[AppointmentsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Citation.Appointments GetById(Guid id)
        {
            var ItemToReturn = new Domain.Citation.Appointments();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[AppointmentsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Citation.Appointments entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[AppointmentsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CompanyId", entity.CompanyId));
						cmd.Parameters.Add(new SqlParameter("@ConsultantId", entity.ConsultantId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@EndDateTime", entity.EndDateTime));
						cmd.Parameters.Add(new SqlParameter("@exchCalItemID", entity.exchCalItemID));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Notes", entity.Notes));
						cmd.Parameters.Add(new SqlParameter("@PrototypeId", entity.PrototypeId));
						cmd.Parameters.Add(new SqlParameter("@Reason", entity.Reason));
						cmd.Parameters.Add(new SqlParameter("@SiteId", entity.SiteId));
						cmd.Parameters.Add(new SqlParameter("@SLATypeId", entity.SLATypeId));
						cmd.Parameters.Add(new SqlParameter("@StartDateTime", entity.StartDateTime));
						cmd.Parameters.Add(new SqlParameter("@Status", entity.Status));
						cmd.Parameters.Add(new SqlParameter("@TaskId", entity.TaskId));
						cmd.Parameters.Add(new SqlParameter("@TimeSpent", entity.TimeSpent));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Citation.Appointments entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[AppointmentsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CompanyId", entity.CompanyId));
						cmd.Parameters.Add(new SqlParameter("@ConsultantId", entity.ConsultantId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@EndDateTime", entity.EndDateTime));
						cmd.Parameters.Add(new SqlParameter("@exchCalItemID", entity.exchCalItemID));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Notes", entity.Notes));
						cmd.Parameters.Add(new SqlParameter("@PrototypeId", entity.PrototypeId));
						cmd.Parameters.Add(new SqlParameter("@Reason", entity.Reason));
						cmd.Parameters.Add(new SqlParameter("@SiteId", entity.SiteId));
						cmd.Parameters.Add(new SqlParameter("@SLATypeId", entity.SLATypeId));
						cmd.Parameters.Add(new SqlParameter("@StartDateTime", entity.StartDateTime));
						cmd.Parameters.Add(new SqlParameter("@Status", entity.Status));
						cmd.Parameters.Add(new SqlParameter("@TaskId", entity.TaskId));
						cmd.Parameters.Add(new SqlParameter("@TimeSpent", entity.TimeSpent));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[AppointmentsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}