using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Citation
{
	public class SiteReferenceRepository : Infrastructure.Repositories.Shared.ISiteReferenceRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Citation.SiteReference> mapper { get; set; }

        public SiteReferenceRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Citation.SiteReference> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Citation.SiteReference> FindBy(Expression<Func<Domain.Citation.SiteReference, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Citation.SiteReference> GetAll()
        {
            var items = new List<Domain.Citation.SiteReference>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[SiteReferenceGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Citation.SiteReference GetById(Guid id)
        {
            var ItemToReturn = new Domain.Citation.SiteReference();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[SiteReferenceGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Citation.SiteReference entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[SiteReferenceInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CompanyId", entity.CompanyId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@IsHeadOffice", entity.IsHeadOffice));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Name", entity.Name));
						cmd.Parameters.Add(new SqlParameter("@PostCode", entity.PostCode));
						cmd.Parameters.Add(new SqlParameter("@SystemEvents", entity.SystemEvents));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Citation.SiteReference entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[SiteReferenceUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@CompanyId", entity.CompanyId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@IsHeadOffice", entity.IsHeadOffice));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@Name", entity.Name));
						cmd.Parameters.Add(new SqlParameter("@PostCode", entity.PostCode));
						cmd.Parameters.Add(new SqlParameter("@SystemEvents", entity.SystemEvents));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[SiteReferenceDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Citation.SiteReference> GetByCompanyId(Guid CompanyId)
        {
            var items = new List<Domain.Citation.SiteReference>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[SiteReferenceGetByCompanyId]";
                    cmd.Parameters.Add(new SqlParameter("@CompanyId", CompanyId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}