using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Citation
{
	public class CompaniesRepository : Infrastructure.Repositories.Shared.ICompaniesRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Citation.Companies> mapper { get; set; }

        public CompaniesRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Citation.Companies> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Citation.Companies> FindBy(Expression<Func<Domain.Citation.Companies, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Citation.Companies> GetAll()
        {
            var items = new List<Domain.Citation.Companies>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[CompaniesGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Citation.Companies GetById(Guid id)
        {
            var ItemToReturn = new Domain.Citation.Companies();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[CompaniesGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Citation.Companies entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[CompaniesInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ClientAffiliationId", entity.ClientAffiliationId));
						cmd.Parameters.Add(new SqlParameter("@ClientArea", entity.ClientArea));
						cmd.Parameters.Add(new SqlParameter("@ContractEndDate", entity.ContractEndDate));
						cmd.Parameters.Add(new SqlParameter("@ContractStartDate", entity.ContractStartDate));
						cmd.Parameters.Add(new SqlParameter("@CountryId", entity.CountryId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Description", entity.Description));
						cmd.Parameters.Add(new SqlParameter("@ExtraEvents", entity.ExtraEvents));
						cmd.Parameters.Add(new SqlParameter("@FullName", entity.FullName));
						cmd.Parameters.Add(new SqlParameter("@IsActive", entity.IsActive));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@jitbitCompanyId", entity.jitbitCompanyId));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@Migrate", entity.Migrate));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@ParentCompanyId", entity.ParentCompanyId));
						cmd.Parameters.Add(new SqlParameter("@PostCode", entity.PostCode));
						cmd.Parameters.Add(new SqlParameter("@Prefix", entity.Prefix));
						cmd.Parameters.Add(new SqlParameter("@SalesforceAccountID", entity.SalesforceAccountID));
						cmd.Parameters.Add(new SqlParameter("@SectorId", entity.SectorId));
						cmd.Parameters.Add(new SqlParameter("@ShortName", entity.ShortName));
						cmd.Parameters.Add(new SqlParameter("@SiteUrl", entity.SiteUrl));
						cmd.Parameters.Add(new SqlParameter("@SystemEvents", entity.SystemEvents));
						cmd.Parameters.Add(new SqlParameter("@TenantName", entity.TenantName));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Citation.Companies entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[CompaniesUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ClientAffiliationId", entity.ClientAffiliationId));
						cmd.Parameters.Add(new SqlParameter("@ClientArea", entity.ClientArea));
						cmd.Parameters.Add(new SqlParameter("@ContractEndDate", entity.ContractEndDate));
						cmd.Parameters.Add(new SqlParameter("@ContractStartDate", entity.ContractStartDate));
						cmd.Parameters.Add(new SqlParameter("@CountryId", entity.CountryId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@Description", entity.Description));
						cmd.Parameters.Add(new SqlParameter("@ExtraEvents", entity.ExtraEvents));
						cmd.Parameters.Add(new SqlParameter("@FullName", entity.FullName));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsActive", entity.IsActive));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@jitbitCompanyId", entity.jitbitCompanyId));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@Migrate", entity.Migrate));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@ParentCompanyId", entity.ParentCompanyId));
						cmd.Parameters.Add(new SqlParameter("@PostCode", entity.PostCode));
						cmd.Parameters.Add(new SqlParameter("@Prefix", entity.Prefix));
						cmd.Parameters.Add(new SqlParameter("@SalesforceAccountID", entity.SalesforceAccountID));
						cmd.Parameters.Add(new SqlParameter("@SectorId", entity.SectorId));
						cmd.Parameters.Add(new SqlParameter("@ShortName", entity.ShortName));
						cmd.Parameters.Add(new SqlParameter("@SiteUrl", entity.SiteUrl));
						cmd.Parameters.Add(new SqlParameter("@SystemEvents", entity.SystemEvents));
						cmd.Parameters.Add(new SqlParameter("@TenantName", entity.TenantName));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[CompaniesDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Citation.Companies> GetBySectorId(Guid SectorId)
        {
            var items = new List<Domain.Citation.Companies>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[CompaniesGetBySectorId]";
                    cmd.Parameters.Add(new SqlParameter("@SectorId", SectorId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}