using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Citation
{
	public class TemplateVersionRepository : Infrastructure.Repositories.Shared.ITemplateVersionRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Citation.TemplateVersion> mapper { get; set; }

        public TemplateVersionRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Citation.TemplateVersion> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Citation.TemplateVersion> FindBy(Expression<Func<Domain.Citation.TemplateVersion, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Citation.TemplateVersion> GetAll()
        {
            var items = new List<Domain.Citation.TemplateVersion>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[TemplateVersionGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Citation.TemplateVersion GetById(Guid id)
        {
            var ItemToReturn = new Domain.Citation.TemplateVersion();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[TemplateVersionGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Citation.TemplateVersion entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[TemplateVersionInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Area", entity.Area));
						cmd.Parameters.Add(new SqlParameter("@Category", entity.Category));
						cmd.Parameters.Add(new SqlParameter("@Comment", entity.Comment));
						cmd.Parameters.Add(new SqlParameter("@CountryId", entity.CountryId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@DeletionState", entity.DeletionState));
						cmd.Parameters.Add(new SqlParameter("@Description", entity.Description));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LastChange", entity.LastChange));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@OriginalTemplateId", entity.OriginalTemplateId));
						cmd.Parameters.Add(new SqlParameter("@PrototypeId", entity.PrototypeId));
						cmd.Parameters.Add(new SqlParameter("@SectorId", entity.SectorId));
						cmd.Parameters.Add(new SqlParameter("@TargetEntityOTC", entity.TargetEntityOTC));
						cmd.Parameters.Add(new SqlParameter("@Title", entity.Title));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Citation.TemplateVersion entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[TemplateVersionUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@Area", entity.Area));
						cmd.Parameters.Add(new SqlParameter("@Category", entity.Category));
						cmd.Parameters.Add(new SqlParameter("@Comment", entity.Comment));
						cmd.Parameters.Add(new SqlParameter("@CountryId", entity.CountryId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@DeletionState", entity.DeletionState));
						cmd.Parameters.Add(new SqlParameter("@Description", entity.Description));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LastChange", entity.LastChange));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@OriginalTemplateId", entity.OriginalTemplateId));
						cmd.Parameters.Add(new SqlParameter("@PrototypeId", entity.PrototypeId));
						cmd.Parameters.Add(new SqlParameter("@SectorId", entity.SectorId));
						cmd.Parameters.Add(new SqlParameter("@TargetEntityOTC", entity.TargetEntityOTC));
						cmd.Parameters.Add(new SqlParameter("@Title", entity.Title));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[TemplateVersionDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

	}
}