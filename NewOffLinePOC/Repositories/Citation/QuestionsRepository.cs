using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Citation
{
	public class QuestionsRepository : Infrastructure.Repositories.Shared.IQuestionsRepository
	{
		private string ConnectionString { get; set; }
        private Mappers.IDataMapper<IDataReader, Domain.Citation.Questions> mapper { get; set; }

        public QuestionsRepository(string connectionString, Mappers.IDataMapper<IDataReader, Domain.Citation.Questions> mapper)
        {
            this.ConnectionString = connectionString;
            this.mapper = mapper;
        }

		public IEnumerable<Domain.Citation.Questions> FindBy(Expression<Func<Domain.Citation.Questions, bool>> predicate)
        {
            return GetAll().AsQueryable().Where(predicate).ToList();
        }

        public IEnumerable<Domain.Citation.Questions> GetAll()
        {
            var items = new List<Domain.Citation.Questions>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[QuestionsGetAll]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }
            return items;
        }

        public Domain.Citation.Questions GetById(Guid id)
        {
            var ItemToReturn = new Domain.Citation.Questions();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[QuestionsGetById]";
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemToReturn = mapper.Map(reader);
                        }
                    }
                }
            }

            return ItemToReturn;
        }
		public Guid Add(Domain.Citation.Questions entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[QuestionsInsert]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ActionType", entity.ActionType));
						cmd.Parameters.Add(new SqlParameter("@AssociatedContentId", entity.AssociatedContentId));
						cmd.Parameters.Add(new SqlParameter("@AttributeId", entity.AttributeId));
						cmd.Parameters.Add(new SqlParameter("@BlockId", entity.BlockId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@DefaultObservation", entity.DefaultObservation));
						cmd.Parameters.Add(new SqlParameter("@Description", entity.Description));
						cmd.Parameters.Add(new SqlParameter("@ExpectedAnswer", entity.ExpectedAnswer));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@OrderIndex", entity.OrderIndex));
						cmd.Parameters.Add(new SqlParameter("@Priority", entity.Priority));
						cmd.Parameters.Add(new SqlParameter("@Target", entity.Target));
						cmd.Parameters.Add(new SqlParameter("@Title", entity.Title));
						cmd.Parameters.Add(new SqlParameter("@Type", entity.Type));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    var obj = cmd.ExecuteScalar();
                    Guid returnId = (Guid)obj;
                    entity.Id = returnId;
                    return returnId;
                }
            }
        }

        public void Update(Domain.Citation.Questions entity)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[QuestionsUpdate]";
                    cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add(new SqlParameter("@ActionType", entity.ActionType));
						cmd.Parameters.Add(new SqlParameter("@AssociatedContentId", entity.AssociatedContentId));
						cmd.Parameters.Add(new SqlParameter("@AttributeId", entity.AttributeId));
						cmd.Parameters.Add(new SqlParameter("@BlockId", entity.BlockId));
						cmd.Parameters.Add(new SqlParameter("@CreatedBy", entity.CreatedBy));
						cmd.Parameters.Add(new SqlParameter("@CreatedOn", entity.CreatedOn));
						cmd.Parameters.Add(new SqlParameter("@DefaultObservation", entity.DefaultObservation));
						cmd.Parameters.Add(new SqlParameter("@Description", entity.Description));
						cmd.Parameters.Add(new SqlParameter("@ExpectedAnswer", entity.ExpectedAnswer));
						cmd.Parameters.Add(new SqlParameter("@Id", entity.Id));
						cmd.Parameters.Add(new SqlParameter("@IsArchived", entity.IsArchived));
						cmd.Parameters.Add(new SqlParameter("@IsDeleted", entity.IsDeleted));
						cmd.Parameters.Add(new SqlParameter("@LCid", entity.LCid));
						cmd.Parameters.Add(new SqlParameter("@ModifiedBy", entity.ModifiedBy));
						cmd.Parameters.Add(new SqlParameter("@ModifiedOn", entity.ModifiedOn));
						cmd.Parameters.Add(new SqlParameter("@OrderIndex", entity.OrderIndex));
						cmd.Parameters.Add(new SqlParameter("@Priority", entity.Priority));
						cmd.Parameters.Add(new SqlParameter("@Target", entity.Target));
						cmd.Parameters.Add(new SqlParameter("@Title", entity.Title));
						cmd.Parameters.Add(new SqlParameter("@Type", entity.Type));
						cmd.Parameters.Add(new SqlParameter("@Version", entity.Version));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteById(Guid id)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[QuestionsDeleteById]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }    

        public IEnumerable<Domain.Citation.Questions> GetByAssociatedContentId(Guid AssociatedContentId)
        {
            var items = new List<Domain.Citation.Questions>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (IDbCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "[Citation].[QuestionsGetByAssociatedContentId]";
                    cmd.Parameters.Add(new SqlParameter("@AssociatedContentId", AssociatedContentId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(mapper.Map(reader));
                        }
                    }
                }
            }

            return items;
        }

	}
}