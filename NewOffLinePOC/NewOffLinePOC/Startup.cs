﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewOffLinePOC.Startup))]
namespace NewOffLinePOC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
